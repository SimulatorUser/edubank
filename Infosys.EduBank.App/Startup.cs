﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Infosys.EduBank.Common;
using System.Text;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using Infosys.EduBank.App.Helper;
using System.Net.Http;
using System.Security.Claims;

namespace Infosys.EduBank.App
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.Configure<CookiePolicyOptions>(options =>
            //{
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
            //    options.CheckConsentNeeded = context => true;
            //    options.MinimumSameSitePolicy = SameSiteMode.None;
            //});
            services.AddCors();
             services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options => {
                options.LoginPath = "/Customer/EduBankSession/";
                options.AccessDeniedPath = "/Customer/EduBankSession/";
            });

            var p = new AuthorizationPolicyBuilder()
              .RequireClaim(ClaimTypes.Role, Constants.UserRoleKey)
              .AddAuthenticationSchemes(CookieAuthenticationDefaults.AuthenticationScheme)
              .RequireAuthenticatedUser().RequireRole(Constants.UserRoleKey)
              .Build();
              var p1 = new AuthorizationPolicyBuilder()
              .RequireClaim(ClaimTypes.Role, Constants.TellerRoleKey)
              .AddAuthenticationSchemes(CookieAuthenticationDefaults.AuthenticationScheme)
              .RequireAuthenticatedUser().RequireRole(Constants.TellerRoleKey)
              .Build();

            services.AddAuthorization(options => {
                options.AddPolicy("RequireUserRole", p);
                options.AddPolicy("RequireTellerRole",p1);
                options.DefaultPolicy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();

            });
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<Models.AppSettings>(appSettingsSection);
            var appSettings = appSettingsSection.Get<Models.AppSettings>();
            Common.Models.AppSettings settings=new Common.Models.AppSettings(){
                PublicKey=appSettings.PublicKey,
                SecurityKey1=appSettings.SecurityKey1,
                SecurityKey2=appSettings.SecurityKey2,
                SecurityKey3=appSettings.SecurityKey3,
                SecurityKey4=appSettings.SecurityKey4,
                SecurityKey5=appSettings.SecurityKey5,
                SecurityKey6=appSettings.SecurityKey6,
                SecurityKey7=appSettings.SecurityKey7,
                SecurityKey10=appSettings.SecurityKey10,
                AdoNetAppenderInfoBufferSize=appSettings.AdoNetAppenderInfoBufferSize,
                InfoLoggerName=appSettings.InfoLoggerName,
                AdoNetAppenderInfoConnectionType =appSettings.AdoNetAppenderInfoConnectionType,
                AdoNetAppenderErrorBufferSize =appSettings.AdoNetAppenderErrorBufferSize ,
                ErrorLoggerName =appSettings.ErrorLoggerName,
                AdoNetAppenderErrorConnectionType =appSettings.AdoNetAppenderErrorConnectionType,
                WarningLogPath =appSettings.WarningLogPath,
                InfoLogStatus =appSettings.InfoLogStatus,
                ErrorLogStatus =appSettings.ErrorLogStatus,
                WarningLogStatus =appSettings.WarningLogStatus
            };
            services.AddSingleton<AuthenticationHelper>(new AuthenticationHelper(new HttpClient(),settings));
            services.AddSingleton<TellerHelper>(new TellerHelper(new HttpClient(),settings));
            services.AddSingleton<CustomerHelper>(new CustomerHelper(new HttpClient(),settings));
            services.AddSingleton(settings);
            services.AddHttpContextAccessor();
            services.AddSession();
            services.AddAuthorization();
            services.AddMvc();//.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            app.UseAuthentication();
            var cookiePolicyOptions = new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.Strict,
            };
            app.UseStaticFiles();
            app.UseCookiePolicy(cookiePolicyOptions);
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Teller}/{action=TellerLogin}/{id?}");
            });
        }
    }
}

using System;
using System.Runtime.Serialization;

namespace Infosys.EduBank.App.Models
{
    public class Expenditures
    {
        public string AnalysisType{get; set;}
        public string SelectedPeriod { get; set; }
        public string XAxis { get; set; }
        public string YAxis { get; set; }
        public string Values { get; set; }
        public string Label { get; set; }
        public string Colors { get; set; }
    }
}
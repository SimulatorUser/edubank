using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Infosys.EduBank.App.Models
{
    public class Transaction
    {
        [DisplayName("Serial Number")]
        public string Id { get; set; }

        public string TransactionId { get; set; }

        public string TransactionDateTime { get; set; }

        public string Remarks { get; set; }

        [DisplayName("Withdrawal")]
        public string Withdrawl { get; set; }

        [Required (ErrorMessage ="From Date is required")]
        public string Deposit { get; set; }

        public string Balance { get; set; }
    }
}
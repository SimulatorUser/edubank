namespace Infosys.EduBank.App.Models
{
    public class DebitCardRequest
    {
        public int DebitCardRequestId{get; set;}
        public int AccountCustomerMappingId{get; set;}
        public int DebitCardId { get; set; }
        public string DebitCardNumber { get; set; }
        public string ValidThru { get; set; }
        public Reason Reason { get; set; }
        public string Status{get; set;}
    }
}
public enum Reason
{
        CardIsLost,
        CardIsDamaged,
        CardIsExpired 
}
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
namespace Infosys.EduBank.App.Models
{
    public class CustomerCurrentCredentials : CustomerCredentials
    {
        [Required(ErrorMessage = "Please enter the old Password")]
        [DisplayName("Old Password")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }
    }
}
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Infosys.EduBank.App.Models
{
    
    public class LoginCredentials
    {
        [DisplayName("Login Name:")]
        [Required(ErrorMessage = "Please enter the Login Name")]
        [RegularExpression(@"^[a-zA-Z][a-zA-Z0-9]*$", ErrorMessage = "Login name not in required format")]
        public string UserName { get; set; }

        [DisplayName("Password:")]
        [Required(ErrorMessage = "Please enter the password")]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 8, ErrorMessage = "Minimum 8 and maximum 20 characters are required")]
        [RegularExpression(@"(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*\(\)_\-+=])[a-zA-Z0-9!@#$%^&*\(\)_\-+=]{8,20}", ErrorMessage = "Password not in required format")]
        public string Password { get; set; }
    }
}
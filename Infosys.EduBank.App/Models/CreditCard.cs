﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.App.Models
{
    public partial class CreditCard
    {
        public long CreditCardId { get; set; }
        public string CardNo { get; set; }
        public byte? CardTypeId { get; set; }
        public long? CustomerId { get; set; }
        public string Cvv { get; set; }
        public string Pin { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidThru { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedTimeStamp { get; set; }
        public bool IsActive { get; set; }
        public bool IsLocked { get; set; }
    }
}

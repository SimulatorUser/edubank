using System;
using System.ComponentModel.DataAnnotations;
namespace Infosys.EduBank.App.Models
{
    public class Customer
    {
        [Required(ErrorMessage = "Please select the branch")]
        public string Branch { get; set; }

        [Required(ErrorMessage ="Email Id is mandatory")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid emailId, example: Sam@edu.com")]
        public string EmailId { get; set; }

        [RegularExpression("^[a-zA-Z ]*", ErrorMessage = "User Name should contain only alphabets and spaces")]
        [Required(ErrorMessage = "Name is mandatory")]
        public string Name { get; set; }

        [Required (ErrorMessage ="Please enter the date of birth")]
        //[App.Models.CurrentDateAttribute(ErrorMessage = "Date must be lesser than the current date")]
        public DateTime DateOfBirth { get; set; }

        [Required (ErrorMessage = "Please enter the UUID")]
        [RegularExpression(@"^[0-9]{12}$", ErrorMessage = "Please enter a 12 digit number")]
        public string UUID { get; set; }

        [Display(Name = "Address is verified")]
        public bool IsAddrVerified { get; set; }
    }
}
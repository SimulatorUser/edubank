using System.ComponentModel.DataAnnotations;

namespace Infosys.EduBank.App.Models
{
    public class CreditDetails
    {
        [Required(ErrorMessage = "Please enter the Account Number")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Account Number should contain only numbers")]
        [StringLength(15, MinimumLength = 15,  ErrorMessage = "Length should be 15")]
        public string AccountNumber { get; set; }
        public decimal Amount { get; set; }
        public string Name { get; set; }
    }
}
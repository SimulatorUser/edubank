using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
namespace Infosys.EduBank.App.Models
{
    public class Payee
    {
        
        public long PayeeId { get; set; }
        [Required]
        public string NickName { get; set; }
        [Required]
        [MinLength(15)]
        [MaxLength(15)]
        [RegularExpression("^[0-9]*")]
        public string AccountNumber { get; set; }
        [Required]
        public string IFSC { get; set; }
        public string UserName { get; set; }
        public decimal? PreferredAmount { get; set; }
        public Customer Customer { get; set; }
    }
}
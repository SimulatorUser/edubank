namespace Infosys.EduBank.App.Models
{
    public class AppSettings
    {
        public string PublicKey{get; set;}
        public string SecurityKey1 { get; set; }
        public string SecurityKey2 { get; set; }
        public string SecurityKey3 { get; set; }
        public string SecurityKey4 { get; set; }
        public string SecurityKey5 { get; set; }
        public string SecurityKey6 { get; set; }
        public string SecurityKey7 { get; set; }
        public string SecurityKey10 { get; set; }
        public string AdoNetAppenderInfoBufferSize {get;set;}
        public string InfoLoggerName{get;set;}
        public string AdoNetAppenderInfoConnectionType {get;set;}
        public string AdoNetAppenderErrorBufferSize {get;set;}
        public string ErrorLoggerName {get;set;}
        public string AdoNetAppenderErrorConnectionType {get;set;}
        public string WarningLogPath {get;set;}
        public bool InfoLogStatus{get; set;}
        public bool ErrorLogStatus{get; set;}
        public bool WarningLogStatus{get; set;}

    }
}
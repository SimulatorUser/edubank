using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Infosys.EduBank.App.Models
{
    public class CustomerCredentials
    {
        [Display(Name = "Login Name")]
        public string LoginName { get; set; }

        [Display(Name = "New Password")]
        [Required(ErrorMessage = "Please enter the password")]
        [StringLength(20, MinimumLength = 8, ErrorMessage = "Minimum 8 and maximum 20 characters are required")]
        [RegularExpression(@"(?=.*[A-Z])(?=.*[0-9])(?=.*[!#$%^&*\(\)_\-+=])[a-zA-Z0-9!#$%^&*\(\)_\-+=]{8,20}", ErrorMessage = "Password not in required format")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required (ErrorMessage ="Please confirm the password")]
        [Display(Name = "Re-type Password")]
        [CompareAttribute("NewPassword",  ErrorMessage = "Passwords do not match")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
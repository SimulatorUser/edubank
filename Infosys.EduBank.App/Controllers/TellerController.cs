﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Infosys.EduBank.App.Models;
using System.Net;
using System.Net.Http;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.App.Helper;
using Microsoft.AspNetCore.Mvc.Rendering;


namespace Infosys.EduBank.App.Controllers
{
    public class TellerController : Controller
    {
        Common.Models.AppSettings AppSettings{get; set;}
        public AuthenticationHelper AuthHelper { get; set; }
        public TellerHelper HelperObj{get; set;}
        public TellerController(AuthenticationHelper authHelper,TellerHelper tellerHelper,Common.Models.AppSettings settings)
        {
            AuthHelper=authHelper;
            HelperObj=tellerHelper;
            AppSettings=settings;
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult TellerLogin()
        {
            // Sets the UserName value to empty string using the Session
                HttpContext.Session.SetString(Constants.SessionUserNameKey,string.Empty);

                //Checks whether the Value of LoginStatus in TempData is null
                if (TempData[Constants.TempDataLoginStatusKey] == null)
                {
                    // Sets the LoginStatus value to empty string using TempData
                    TempData[Constants.TempDataLoginStatusKey] = string.Empty;
                }

                return View("TellerLogin");
        }


        [AllowAnonymous]
        [HttpPost]  
        public IActionResult TellerLogin(LoginCredentials objLogin)
        {
            bool status = false;
            try
            {
                if (ModelState.IsValid)
                {
                    
                    // Sets up the SessionState under the 
                    // name "UserName" as the parameter's UserName Attribute
                    HttpContext.Session.SetString(Constants.SessionUserNameKey,objLogin.UserName);
                    status=AuthHelper.Login(objLogin.UserName,objLogin.Password,HttpContext);
                    
                    if (status )
                    {
                        return RedirectToAction("TellerHomePage");
                    }
                    else
                    {
                        // Sets the HttpContext.Current to null
                        HttpContext.Session.Clear();

                        // Sets the LoginStatus value to Teller does not exist using TempData
                        TempData[Constants.TempDataLoginStatusKey] = "Teller does not exist";

                        return RedirectToAction("TellerLogin", objLogin);
                    }
                }


                return RedirectToAction("Failure", new { msg = "TellerLogin Model is Invalid"});
            }

            catch (Exception ex)
            {

                // Sets the HttpContext.Current to null
                HttpContext.Session.Clear();

                return RedirectToAction("Failure", new { msg = ex.Message });
            }
        }

        public IActionResult Failure(string msg)
        {
            // Sets the value of msg in the ViewBag to the value in the variable msg
            ViewBag.Msg = msg;

            // Sets the value of Controller in the ViewBag to Teller
            ViewBag.Controller = "Teller";
            return View("Failure");
        }
        [HttpGet]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public IActionResult CalculateInterest()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public IActionResult CalculateInterestPost()
        {
            TempData["InterestStatus"]=HelperObj.AddInterest(HttpContext.Session.GetString(Constants.UserIdKey)
            ,HttpContext.Session.GetString(Constants.SessionServiceTokenKey));
            return View("CalculateInterest");
        }

        [HttpGet]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public IActionResult TellerHomePage()
        {            
            string tellerName = string.Empty;
            try
            {
                // Checks whether the Name of the Current user is null
                if (!string.IsNullOrEmpty(HttpContext.Session.GetString(Constants.SessionUserNameKey)))
                {
                    tellerName = HttpContext.Session.GetString(Constants.NameKey);


                    // Calls the TellerBL to get all the branches available in the bank
                    var branchLst = HelperObj.GetAllBranches(HttpContext.Session.GetString(Constants.SessionServiceTokenKey));

                    List<SelectListItem> branches = new List<SelectListItem>();

                    //Checks whether there is any data in branchLst
                    if (branchLst.Any())
                    {
                        foreach (var item in branchLst)
                        {
                            if (item.BranchId != 0)
                            {
                                //Create the new SelectListItem object with BranchId to the value field and 
                                //BranchName in the Text field and add that to the branches (List of SelectListItem)  
                                branches.Add(new SelectListItem { Selected = false, Value = Convert.ToString(item.BranchId), Text = item.BranchName });
                            }
                            else
                            {
                                return RedirectToAction("Failure", new { msg = "No branches available for you!! Kindly contact the bank" });
                            }
                        }
                        //Sets the Branch in the ViewBag to the new SelectList with the branches
                        ViewBag.Branch = new SelectList(branches, "Value", "Text");

                        // Checks whether the value in CustomerAdd TempData is null 
                        if (TempData["CustomerAdd"] != null)
                        {
                            //Sets the value of Customer in ViewBag to the value in CustomerAdd TempData
                            ViewBag.Customer = "Customer Added Successfully";
                            ViewBag.CustomerDetail=JsonConvert.DeserializeObject<Common.Models.Customer>( TempData["CustomerAdd"].ToString());

                        }

                        // Checks whether the value in AddMoneyStatus TempData is null 
                        else if (TempData["AddMoneyStatus"] != null)
                        {
                            //Sets the value of Money in ViewBag to the value in AddMoneyStatus TempData
                            ViewBag.Money = TempData["AddMoneyStatus"];

                        }

                        // Checks whether the value in ErrorMoney TempData is null 
                        else if (TempData["ErrorMoney"] != null)
                        {
                            //Sets the value of ErrorMoney in ViewBag to the value in ErrorMoney TempData
                            ViewBag.ErrorMoney = TempData["ErrorMoney"];

                        }

                        // Checks whether the value in ErrorCustomer TempData is null 
                        else if (TempData["ErrorCustomer"] != null)
                        {
                            //Sets the value of ErrorCustomer in ViewBag to the value in ErrorCustomer TempData
                            ViewBag.ErrorCustomer = TempData["ErrorCustomer"];

                        }
                    }
                    else
                    {
                       
                        return RedirectToAction("Failure", new { msg = "No Branches are fetched" });
                    }
 
                    return View("TellerHomePage");
                }

                return RedirectToAction("Failure", new { msg = "Session Expired" });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Failure", new { msg = ex.Message });
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public JsonResult FetchAccountDetails(string AccountNumber)
        {
            string name = string.Empty;
            string tellerName = string.Empty;
            string message = string.Empty;
            try
            {

                //objTeller = new TellerBL();

                // Calls the function FetchAccountHolderName in the TellerBL to get the account holder name if exists
                name = HelperObj.FetchAccountHolderName(AccountNumber);

               
                // Checks whether the name is empty string or not
                if (name == string.Empty || name==null)
                {
                    // Sets the value of name in TempData to No data present
                    TempData["name"] = "No data present";
                    name = string.Empty;

                    message = "This account number is inactive or incorrect";
                }
                else
                {
                    // Sets the value of name in TempData to the value of local variable name
                    TempData["name"] = name;

                    // Sets the value of accountNumber in TempData to the value of local variable accountNumber
                    TempData["accountNumber"] = AccountNumber; 
                }
            }
            catch (Exception ex)
            {

                // Sets the value of ErrorMoney in TempData to "Some Error Occurred"
                TempData["name"] = "Some error occurred";

                name = string.Empty;
                message = string.Empty;
            }

            return Json(new object[] { name, message });
        }



        [HttpGet]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public IActionResult AddCustomer()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult TellerLogOut()
        {
            try
            {
                
                var status=AuthHelper.LogOut(HttpContext);
                if(status)
                {
                    return RedirectToAction("TellerLogin");
                }else
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Failure", new { msg = "Some error in logout method" });
                }
                
            }
            catch(Exception ex)
            {
                return RedirectToAction("Failure", new { msg = ex.Message });
            }
            
        }

        public ActionResult EduBankSession()
        {
            try
            {
                // Sets the value of UserName to empty string in the Session
                HttpContext.Session.Clear();
                AuthHelper.LogOut(HttpContext);
                return RedirectToAction("Failure", new { msg = "Session Expired" });
            }
            catch (Exception ex)
            {

                return RedirectToAction("Failure", new { msg = ex.Message });
            }
        }

        [HttpPost]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public async Task<ActionResult> AddCustomer(Customer registerCustomer)
        {
            string tellerName = string.Empty;
            Customer registerCommonObj =new  Customer();
            try
            {
                
                    // Checks whether the Branch in the input object is not equal to null
                    if (registerCustomer.Branch != null)
                    {
                        Common.Models.Customer customer= new Common.Models.Customer(){
                            EmailId= registerCustomer.EmailId,
                            Branch=registerCustomer.Branch,
                            DateOfBirth= registerCustomer.DateOfBirth,
                            IsAddrVerified= registerCustomer.IsAddrVerified,
                            Name= registerCustomer.Name,
                            UUID= registerCustomer.UUID,
                            TellerId=HttpContext.Session.GetString(Constants.UserIdKey)
                        };
                       var retVal= HelperObj.AddCustomer(customer,HttpContext.Session.GetString(Constants.SessionServiceTokenKey));

                        // Checks whether the return value is 1, which is for successful addition of user 
                        if (retVal!=null)
                        {

                            // Sets the value for CustomerAdd in TempData 

                            TempData["CustomerAdd"] =JsonConvert.SerializeObject(retVal);

                            // Logging the info with the Type of Controller as TellerController and specifying the message
                            // Info Logging the AddCustomer execution with the message
                           // EduBankLogger.Log(typeof(TellerController), tellerName, "AddCustomer[Post] has been executed successfully and the customer is added", 1);

                        }
                        else
                        {
                            // Sets the value for ErrorCustomer in TempData 
                            TempData["ErrorCustomer"] = "Some Error Occurred";


                            // Logging the info with the Type of Controller as TellerController and specifying the message
                            // Info Logging the AddCustomer execution with the message
                           // EduBankLogger.Log(typeof(TellerController), tellerName, "In AddCustomer[Post], customer is not added as the BL returned failure", 1);

                        }
                    }
                    else
                    {
                        // Sets the value for ErrorCustomer in TempData 
                        TempData["ErrorCustomer"] = "No Branch was selected please select branch";


                        // Logging the info with the Type of Controller as TellerController and specifying the message
                        // Info Logging the AddCustomer execution with the message
                       // EduBankLogger.Log(typeof(TellerController), tellerName, "In AddCustomer[Post], customer is not added as the branch is not selected or invalid branch", 1);

                    }
                
            }
            catch (Exception ex)
            {
                // Logging in case of any errors with the Type of Controller, TellerController.
                // Specifying the error message in the error logger
               // EduBankLogger.Log(typeof(TellerController), "TellerController", "Error occurred during adding customer to the bank", 2, ex);

                // Sets the value for ErrorCustomer in TempData 
                TempData["ErrorCustomer"] = "Some Error Occurred";
            }

            return RedirectToAction("TellerHomePage");

        }

        [HttpPost]
        [Authorize(Roles="Teller")]
        public ActionResult AddMoney(CreditDetails addMoneyObj)
        {
            string tellerName = string.Empty;
            try
            {

                // Calls the function AddMoneyToAccount in the TellerBL and gets the return value
                Common.Models.InsertTransaction transaction =new Common.Models.InsertTransaction();
                transaction.AccountNumber=addMoneyObj.AccountNumber;
                transaction.AccountName=addMoneyObj.Name;
                transaction.Amount=addMoneyObj.Amount;
                transaction.Mode=Common.Constants.TellerTransactionMode;
                transaction.TellerId=HttpContext.Session.GetString(Constants.UserIdKey);
                long retVal = HelperObj.AddMoneyToAccount(transaction,HttpContext.Session.GetString(Constants.SessionServiceTokenKey));

                // Checks if the retVal is 1 which is a value for successful transaction
                if (retVal!=0 && retVal!=-99)
                {
                    // Sets the value of AddMoneyStatus in TempData 
                    TempData["AddMoneyStatus"] = "Money Added Successfully TransactionId :"+retVal;

                }
                else
                {
                    // Sets the value of ErrorMoney in TempData 
                    TempData["ErrorMoney"] = "Some Error Occurred";

                }
            }
            catch (Exception ex)
            {

                // Sets the value of ErrorMoney in TempData 
                TempData["ErrorMoney"] = "Some Error Occurred";

            }

            return RedirectToAction("TellerHomePage");

        }
        
        [HttpGet]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public ActionResult ViewAllAccounts()
        {
            List<Account> accounts = new List<Account>();
            List<Common.Models.Account> accountList = new List<Common.Models.Account>();
            Account account;
            try
            {
                accountList = HelperObj.GetAllAccounts(HttpContext.Session.GetString(Constants.SessionServiceTokenKey));
                if(accountList.Any())
                {
                    foreach (var accountObj in accountList)
                    {
                        account = new Account();
                        account.AccountId = accountObj.AccountId;
                        account.AccountNumber = accountObj.AccountNumber;
                        account.BranchId= accountObj.BranchId;
                        account.CreatedBy = accountObj.CreatedBy;
                        account.CreatedTimestamp = accountObj.CreatedTimestamp;
                        account.IsActive = accountObj.IsActive;
                        account.ModifiedBy = accountObj.ModifiedBy;
                        account.ModifiedTimestamp = accountObj.ModifiedTimestamp;
                        account.Status = accountObj.Status;
                        accounts.Add(account);
                    }
                }
                else
                {
                    return View("Error");
                }
                return View(accounts);                
            }
            catch (System.Exception ex)
            {
                return View("Error");
            }
        }
    
        [HttpPost]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public ActionResult UpdateAccountStatus(Account account)
        {
            bool result = false;
            Common.Models.Account accountObj = new Common.Models.Account();

            try
            {
                accountObj.AccountId = account.AccountId;
                accountObj.AccountNumber = account.AccountNumber;
                accountObj.BranchId = account.BranchId;
                accountObj.CreatedBy = account.CreatedBy;
                accountObj.CreatedTimestamp = account.CreatedTimestamp;
                accountObj.IsActive = account.IsActive;
                accountObj.ModifiedBy = account.ModifiedBy;
                accountObj.ModifiedTimestamp = account.ModifiedTimestamp;
                accountObj.Status = account.Status;
                result = HelperObj.UpdateAccountStatus(accountObj,HttpContext.Session.GetString(Constants.SessionServiceTokenKey));
                if(result)
                {
                    return RedirectToAction("ViewAllAccounts");
                }
            }
            catch (System.Exception ex)
            {
                return View("Error");
            }
            return View("Error");
        }

        [HttpGet]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public ActionResult CreditCardRequest()
        {
            List<App.Models.CardRequest> cardRequests;
            string userName=HttpContext.Session.GetString(Constants.NameKey);
            string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
            try
            {
            cardRequests=HelperObj.GetAllRequests(token);
            
            return View("CreditCardRequest",cardRequests);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }

        [HttpPost]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public ActionResult GenerateCreditCard(long requestId){
            Common.Models.CreditCard creditCard=new Common.Models.CreditCard();
            string tellerId=HttpContext.Session.GetString(Constants.UserIdKey);
            string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
            bool status=false;
            try{
                // creditCard.CustomerId=customerId;
                // creditCard.CardTypeId=cardTypeId;
                //creditCard.CreatedBy=tellerId;
                creditCard= HelperObj.GenerateCreditCard(requestId,tellerId,token);
                if(creditCard.CreditCardId!=0){
                    status=HelperObj.UpdateRequestStatus(requestId,"Success",token);
                }
                //string data=string.Format("Card Number :{0}, Cvv: {1}, Valid Thru: {2} ,PIN: {3} ",creditCard.CardNo,creditCard.Cvv,creditCard.ValidThru,creditCard.Pin);
                if(status){
                    TempData["CustomerDetails"]="Success";
                    TempData["CreditCardNumber"]=creditCard.CardNo;
                    TempData["CVV"]=creditCard.Cvv;
                    TempData["Pin"]=creditCard.Pin;
                    
                }
                return RedirectToAction("CreditCardRequest");
            }
            catch(Exception ex){
                return null;
            }

        }
        [HttpPost]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public ActionResult RejectRequestStatus(long requestId){
           string status="Rejected";
             string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
             TempData["RejectStatus"]="The request not processed";
            try
            {
                 if(HelperObj.UpdateRequestStatus(requestId,status,token)){
                     TempData["RejectStatus"] ="The request has been successfully rejected";
                     
                 }
                 return  RedirectToAction("CreditCardRequest");
            }
            catch (Exception ex)
            {
                
                return  RedirectToAction("CreditCardRequest");
            }
        }

        //View all available Debit Cards requests minor enhancement
        [HttpGet]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public ActionResult DebitCardRequests()
        {
            List<App.Models.DebitCardRequest> cardRequests = new List<App.Models.DebitCardRequest>();
            string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
            try
            {
                cardRequests=HelperObj.GetDebitCardRequests(token);
                if(cardRequests.Count == 0 || cardRequests == null || cardRequests[0].DebitCardRequestId == 0)
                {
                    ViewBag.ErrorMessage ="No pending debit card requests";
                }
                return View("DebitCardRequests",cardRequests);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }
        //Approve Debit card request Minor Enhacement
        [HttpPost]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public ActionResult ApproveDebitCard(DebitCardRequest reqObj)
        {
            reqObj.Status="A";
            Common.Models.Customer custObj;
            string tellerID = HttpContext.Session.GetString(Constants.UserIdKey);
            string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
            try
            {
                custObj=HelperObj.UpdateDebitCardRequest(reqObj,tellerID,token);
                if(custObj == null)
                {
                    ViewBag.ErrorMessage ="Your request couldn't be processed";
                    custObj = null;
                }
                else if(custObj.DebitCardNumber != "" )
                {
                    TempData["CustomerDetails"]="Success";
                    TempData["DebitCardNumber"]=custObj.DebitCardNumber;
                    TempData["CVV"]=custObj.CVV;
                    TempData["Pin"]=custObj.Pin;
                }
                return RedirectToAction("DebitCardRequests");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }
        //Reject debit card request Minor enhacment
        [HttpPost]
        [Authorize(Roles=Constants.TellerRoleKey)]
        public ActionResult RejectDebitCard(DebitCardRequest reqObj)
        {
            reqObj.Status="R";
            Common.Models.Customer custObj;
            string tellerID = HttpContext.Session.GetString(Constants.UserIdKey);
            string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
            try
            {
                custObj=HelperObj.UpdateDebitCardRequest(reqObj,tellerID,token);
                if(custObj == null)
                {
                    ViewBag.ErrorMessage ="Your request couldn't be processed";
                }
                else if(custObj.DebitCardNumber == null )
                {
                    TempData["RejectStatus"] ="The request has been successfully rejected";
                    custObj = null;
                }
                return RedirectToAction("DebitCardRequests");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }
    
    }
}

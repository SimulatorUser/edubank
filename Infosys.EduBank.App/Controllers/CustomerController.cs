using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Infosys.EduBank.App.Models;
using System.Net;
using System.Net.Http;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.App.Helper;
using System.Globalization;
using Microsoft.AspNetCore.Mvc.Rendering;


namespace Infosys.EduBank.App.Controllers
{
    public class CustomerController: Controller
    {
        Common.Models.AppSettings AppSettings{get; set;}
        public AuthenticationHelper AuthHelper { get; set; }
        public CustomerHelper CustomerHelper { get; set; }
        public CustomerController(AuthenticationHelper helper, CustomerHelper customerHelper,Common.Models.AppSettings settings)
        {
            AuthHelper=helper;
            CustomerHelper = customerHelper;
            AppSettings=settings;
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult CustomerLogin()
        {
            // Sets the UserName value to empty string using the Session
                HttpContext.Session.SetString(Constants.SessionUserNameKey,string.Empty);
                // Sets the Balance to Empty string using Session
                HttpContext.Session.SetString("Balance",string.Empty);

                // Sets the UserName to Empty String using Session
                HttpContext.Session.SetString("UserName",string.Empty);

                // Sets the Attempt Count to 3 using Session
                HttpContext.Session.SetInt32("AttemptsCount",3);

                // Sets the Last User to Empty String Value
                HttpContext.Session.SetString("LastUser", String.Empty);

                if (TempData["Warning"] != null)
                {
                    //Sets the Warning message that needs to be displayed in CustomerLogin page
                    ViewBag.Warning = TempData["Warning"].ToString();
                }
                if (TempData["PassChangeStatus"] == null)
                {
                    //Sets the PassChangeStatus to empty string using TempData
                    TempData["PassChangeStatus"] = string.Empty;
                }
                if (TempData["PassChangeStatusFail"] == null)
                {
                    //Sets the PassChangeStatusFail to empty string using TempData
                    TempData["PassChangeStatusFail"] = string.Empty;
                }


                //Checks whether the Value of LoginStatus in TempData is null
                if (TempData[Constants.TempDataLoginStatusKey] == null)
                {
                    // Sets the LoginStatus value to empty string using TempData
                    TempData[Constants.TempDataLoginStatusKey] = string.Empty;
                }

                return View("CustomerLogin");
        }
        
        public ActionResult EduBankSession()
        {
            try
            {
                // Sets the value of UserName to empty string in the Session
                HttpContext.Session.Clear();
                AuthHelper.LogOut(HttpContext);
                return RedirectToAction("Failure", new { msg = "Session Expired" });
            }
            catch (Exception ex)
            {

                return RedirectToAction("Failure", new { msg = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult CustomerLogin(LoginCredentials objLogin)
        {
            bool status = false;
            try
            {
                if (ModelState.IsValid)
                {
                    
                    // Sets up the SessionState under the 
                    // name "UserName" as the parameter's UserName Attribute
                    HttpContext.Session.SetString(Constants.SessionUserNameKey,objLogin.UserName);
                    status=AuthHelper.Login(objLogin.UserName,objLogin.Password,HttpContext);
                    if (TempData["PassChangeStatus"] == null)
                    {
                        //Sets the PassChangeStatus to empty string using TempData
                        TempData["PassChangeStatus"] = string.Empty;
                    }
                    if (TempData["PassChangeStatusFail"] == null)
                    {
                        //Sets the PassChangeStatusFail to empty string using TempData
                        TempData["PassChangeStatusFail"] = string.Empty;
                    }

                    if (status )
                    {
                        
                        //HttpContext.Session.SetString("Balance",balance.ToString());
                        //TempData["Balance"]=balance.ToString();
                        return RedirectToAction("CustomerHomePage");
                    }
                    else
                    {
                        // Sets the HttpContext.Current to null
                        HttpContext.Session.Clear();

                        // Sets the LoginStatus value to Teller does not exist using TempData
                        TempData[Constants.TempDataLoginStatusKey] = "Teller does not exist";

                        return RedirectToAction("TellerLogin", objLogin);
                    }
                }


                return RedirectToAction("Failure", new { msg = "TellerLogin Model is Invalid"});
            }

            catch (Exception ex)
            {

                // Sets the HttpContext.Current to null
                HttpContext.Session.Clear();

                return RedirectToAction("Failure", new { msg = ex.Message });
            }
        }

        public ActionResult Failure(string msg)
        {
            // Sets the value of msg in the ViewBag to the value in the variable msg
            ViewBag.Msg = msg;

            // Sets the value of Controller in the ViewBag to Teller
            ViewBag.Controller = "Customer";
            return View("Failure");
        }

        [HttpGet]
        [Authorize(Roles=Constants.UserRoleKey)]
        public ActionResult CustomerHomePage()
        {
            try
            {
                var accountNumbers=CustomerHelper.GetAccountNumbers(
                            HttpContext.Session.GetString(Constants.SessionUserNameKey),
                            HttpContext.Session.GetString(Constants.SessionServiceTokenKey));
                string selectedAccountNumber=string.Empty;
                if(TempData["Balance"]!=null && TempData["Balance"].ToString()!=string.Empty && TempData["SelectedAccountNumber"]!=null && TempData["SelectedAccountNumber"].ToString()!=string.Empty )
                {
                    
                    ViewBag.Balance = TempData["Balance"].ToString();
                    selectedAccountNumber=TempData["SelectedAccountNumber"].ToString();

                }else
                {
                    selectedAccountNumber=accountNumbers.FirstOrDefault();
                    var balance=CustomerHelper.GetBalance(accountNumbers.FirstOrDefault(),
                        HttpContext.Session.GetString(Constants.SessionServiceTokenKey)).ToString();
                    
                    ViewBag.Balance = balance.ToString();
                    
                }
                var selectListArray=new SelectListItem[accountNumbers.Count];
                for(int i=0;i<accountNumbers.Count;i++)
                {
                    SelectListItem newSelectList= new SelectListItem()
                        {Text= accountNumbers[i]
                        ,Value=accountNumbers[i]
                        ,Selected=accountNumbers[i]==selectedAccountNumber
                        };
                    selectListArray[i]=newSelectList;
                    
                }
                
                ViewBag.AllAccounts=selectListArray;

                return View("CustomerHomePage");
            }
            catch (Exception ex)
            {
                // Redirects to the Failure action with "Some Error occurred !!!" as msg value.
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult CustomerLogOut()
        {
            try
            {
                var status=AuthHelper.LogOut(HttpContext);
                if(status)
                {
                    return RedirectToAction("CustomerLogin");
                }else
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Failure", new { msg = "Some error in logout method" });
                }
                
            }
            catch(Exception ex)
            {
                return RedirectToAction("Failure", new { msg = ex.Message });
            }
            
        }




        [HttpPost]
        [Authorize(Roles = Constants.UserRoleKey)]
        public IActionResult GetBalance(IFormCollection formCollection)
        {
            string userName = string.Empty;
            decimal? balance = 0;
            string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
            try
            {
                userName = HttpContext.Session.GetString(Constants.SessionUserNameKey);
                string accountNumber = formCollection["AccountNumbers"];
                TempData["SelectedAccountNumber"]=accountNumber;
                balance = CustomerHelper.GetBalance(accountNumber,token);
                if(balance > 0)
                {
                    TempData["Balance"] = String.Format(new CultureInfo("en-IN", false), "{0:n}", balance);
                }
                else
                {
                    //Save balance of passed AccountNumber using TempData
                    TempData["Balance"] = string.Format("{0:n2}", 0.00);
                }
                return RedirectToAction("CustomerHomePage");
            }
            catch(Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });                
            }
        }

        [HttpGet]
        [Authorize(Roles = Constants.UserRoleKey)]
        public IActionResult ViewTransactions(string accountNumber, string fromDate, string toDate)
        {
            string loginName = string.Empty;
            string token = string.Empty;
            List<string> accountNumbers = new List<string>();
            string selectedAccountNumber=accountNumber;
            DateTime dDate;
            try
            {
                
                loginName = HttpContext.Session.GetString(Constants.SessionUserNameKey);
                if(fromDate==null)
                {
                    fromDate = string.Empty;
                }
                if(toDate==null)
                {
                    toDate = string.Empty;
                }
                if ((!DateTime.TryParse(fromDate,out dDate)) && (fromDate != "" ||toDate != "") )
                {
                    
                    ViewBag.ErrorMessage = "Please enter the dates in 'DD/MM/YYYY' format";
                }
                else
                {
                    if (fromDate != "" && toDate != "")
                    {
                        DateTime start = DateTime.ParseExact(fromDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime end = DateTime.ParseExact(toDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        if (start > end)
                        {
                            ViewBag.ErrorMessage = "To Date should be greater than the From Date";
                        }
                    }
                }
                token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
                accountNumbers = CustomerHelper.GetAccountNumbers(loginName,token);
                if (accountNumbers == null || accountNumbers.Count == 0 || accountNumbers[0] == "000000000000000")
                {
                    // Sets the AccountNumbers in the ViewBag to null
                    ViewBag.AccountNumbers = null;

                    // Sets the Message in the ViewBag
                    ViewBag.Message = "Sorry, you currently don't have any active accounts.";
                }
                else
                {
                    // Sets the AccountNumbers in the ViewBag to the list of accountNumbers
                    ViewBag.AccountNumbers = accountNumbers.Distinct().ToList();
                }
                if(toDate==null ||toDate==string.Empty)
                {
                    toDate=DateTime.Now.ToString("MM/dd/yyyy"); 
                }
                if(fromDate==null||fromDate==string.Empty)
                {
                     DateTime end = DateTime.ParseExact(toDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    fromDate=end.AddDays(-30).ToString("MM/dd/yyyy");
                }
                // Sets the FromDate in the ViewBag to the FromDate
                ViewBag.FromDate = fromDate;

                // Sets the ToDate in the ViewBag to the ToDate
                ViewBag.ToDate = toDate;
                List<Common.Models.Transaction> transactions = new List<Common.Models.Transaction>();
                if(accountNumber == null)
                {
                    selectedAccountNumber=accountNumbers[0].ToString();
                    transactions = CustomerHelper.GetTransactions(fromDate,toDate,accountNumbers[0],token);
                }
                else
                {
                    transactions = CustomerHelper.GetTransactions(fromDate,toDate,accountNumber,token);
                }
                List<Transaction> transaction = new List<Transaction>();
                bool flag = false;
                if (transactions==null || transactions.Count == 0 || (transactions[0].Id == "0" && transactions[0].TransactionId == "0"))
                {
                    flag = false;
                }
                else
                {
                    flag = true;

                }
                if (flag == true)
                {
                    foreach (var row in transactions)
                    {
                        //Format the TransactionDateTime to MM/dd/yyyy format
                        string transactionDate = String.Format("{0:MM/dd/yyyy}", row.TransactionDateTime);

                        //Add the data in the transaction object(Common.Models) to the list of App.Models by 
                        //mapping a Transaction object of Common.Models to App.Models 
                        transaction.Add(new Transaction { Id = row.Id, TransactionId = row.TransactionId, TransactionDateTime = transactionDate, Remarks = row.Remarks, Withdrawl = String.Format(new CultureInfo("en-IN", false), "{0:n}", row.Withdrawal), Deposit = String.Format(new CultureInfo("en-IN", false), "{0:n}", row.Deposit), Balance = String.Format(new CultureInfo("en-IN", false), "{0:n}", row.Balance) });
                    }
                }
                var selectListArray=new SelectListItem[accountNumbers.Count];
                for(int i=0;i<accountNumbers.Count;i++)
                {
                    SelectListItem newSelectList= new SelectListItem()
                        {Text= accountNumbers[i]
                        ,Value=accountNumbers[i]
                        ,Selected=accountNumbers[i]==selectedAccountNumber
                        };
                    selectListArray[i]=newSelectList;
                    
                }
                ViewBag.AllAccounts=selectListArray;
                return View("ViewTransactions", transaction);

            }
            catch(Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }

        [HttpGet]
        [Authorize(Roles=Constants.UserRoleKey)]
        public ActionResult ChangePassword()
        {
 
            try
            {

                // Returns the Current View that is "ChangePassword".
                return View("ChangePassword");

            }
            catch (Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }

        [Authorize(Roles=Constants.UserRoleKey)]
        [HttpPost]
        public ActionResult ChangePassword(CustomerCurrentCredentials resetPasswordObj)
        {
            string userName = string.Empty;
            try
            {
                // Check if the Model State of the Passed model is Valid
                resetPasswordObj.LoginName=HttpContext.Session.GetString(Constants.SessionUserNameKey);
                if (ModelState.IsValid)
                {
                    userName = HttpContext.Session.GetString("UserName");

                    // Variable accountNumber is used to retrieve the formCollection value from Key=AccountNumbers.
                    if (resetPasswordObj.OldPassword == resetPasswordObj.NewPassword)
                    {
                        // Setting the TempData "SamePassword" Value to "Your Password has been Changed Successfully. Please Login Again."
                        // message and Redirect to the Login Page.
                        TempData["SamePassword"] = "New Password cannot be same as the previous password. Please Enter Again.";
                        
                        // Redirecting to the same View.
                        return View("ChangePassword");
                    }
                    else
                    {

                        // Variable validateStatus is used to store the result for the old password validation.
                        string validateStatus = AuthHelper.ValidateUser(userName, resetPasswordObj.OldPassword);
                        
                        // Check for the status is 1 that is for the correct values for current userName and password
                        if (validateStatus!=null && validateStatus!=string.Empty)
                        {
                            bool retVal = CustomerHelper.ChangePassword(resetPasswordObj,HttpContext.Session.GetString(Constants.SessionServiceTokenKey));

                            if (retVal)
                            {

                                // Setting the PassChangeStatus Value to "Your Password has been Changed Successfully. Please Login Again." using TempData
                                TempData["PassChangeStatus"] = "Your Password has been changed successfully. Please Login Again.";
                            }
                            else
                            {
                                // Setting the PassChangeStatusFail Value to "Your Password change failed" using TempData
                                TempData["PassChangeStatusFail"] = "Your Password change failed";
                            }
                        }
                        else
                        {
                            // Setting the TempData Value to "Entered Profile password is Wrong, Please Log in again."
                            // message and Redirect to the Login Page.
                            TempData["PassChangeStatusFail"] = "Entered Profile password is wrong, Please Log in again.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                // Redirects to the Failure action with "Some Error occurred !!!" as msg value.
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }

            // Redirects to the action Customer Login with the update status of the password.
            return RedirectToAction("CustomerLogin");

        }


        [Authorize(Roles=Constants.UserRoleKey)]
        [HttpGet]
        public ActionResult ChangeDebitCardPin()
        {
            string userName = string.Empty;
            try
            {
                userName = HttpContext.Session.GetString(Constants.SessionUserNameKey);


                List<string> debitCardsList;

                //Calls the GetDebitCards in CustomerBL and Sets the debitCardsList 
                //to the list of cards available for the user
                bool status = CustomerHelper.GetDebitCards(HttpContext.Session.GetString(Constants.UserIdKey), out debitCardsList,HttpContext.Session.GetString(Constants.SessionServiceTokenKey));

                List<SelectListItem> Cardslst = new List<SelectListItem>();

                //Checks whether there is any active card available for the logged in user
                if (debitCardsList[0] != "0000000000000000" && status)
                {
                    foreach (var item in debitCardsList)
                    {
                        //Create the new SelectListItem object with debit card numbers to the value field and 
                        //mask the CardNumber in the Text field and add that to the CardsLst                        
                        Cardslst.Add(new SelectListItem() { Value = item.ToString(), Text = "XXXX-XXXX-XXXX-" + item.Substring(12, 4) });

                    }
                    //Sets the DebitCardNumber to the CardsLst using ViewBag
                    ViewBag.DebitCardNumber = Cardslst;

                }
                else
                {
                    //Sets the DebitCardNumber to the null using ViewBag
                    ViewBag.DebitCardNumber = null;

                    //Sets the Message using ViewBag
                    ViewBag.Message = "Sorry, you currently don't have any active debit cards.";
                    
                }

                return View("ChangeDebitCardPin");

            }
            catch (Exception ex)
            {

                // Redirects to the Failure action with "Some Error occurred !!!" as msg value.
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }
        [Authorize(Roles=Constants.UserRoleKey)]
        [HttpPost]
        public ActionResult ChangeDebitCardPin(DebitCardPinDetails changePinObj)
        {
            string userName = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    userName =HttpContext.Session.GetString(Constants.SessionUserNameKey);

                    //objCustomer = new CustomerBL();
                    List<string> debitCardsList;

                    //Calls the GetDebitCards in CustomerBL and Sets the debitCardsList 
                    //to the list of cards available for the user
                    bool status = CustomerHelper.GetDebitCards(HttpContext.Session.GetString(Constants.UserIdKey), out debitCardsList,HttpContext.Session.GetString(Constants.SessionServiceTokenKey));

                    List<SelectListItem> Cardslst = new List<SelectListItem>();
                    if (debitCardsList[0] != "0000000000000000" && (status))
                    {
                        foreach (var item in debitCardsList)
                        {
                            //Create the new SelectListItem object with card numbers to the value field and 
                            //mask the CardNumber in the Text field and add that to the CardsLst          
                            Cardslst.Add(new SelectListItem() { Value = item.ToString(), Text = "XXXX-XXXX-XXXX-" + item.Substring(12, 4) });
                        }

                        //Sets the DebitCardNumber to the CardsLst using ViewBag
                        ViewBag.DebitCardNumber = Cardslst;
                        
                        int count = 0;
                        int difference;
                        
                        for (int i = 0; i < changePinObj.NewPin.Length - 1; i++)
                        {
                            difference = Convert.ToInt32(changePinObj.NewPin[i + 1].ToString()) - Convert.ToInt32(changePinObj.NewPin[i].ToString());

                            //Check whether the pin is in series
                            if (difference == 1 || difference == -1)
                            {
                                count++;
                            }
                        }

                        //Checks whether the old pin and new pin are same 
                        if (changePinObj.OldPin == changePinObj.NewPin)
                        {

                            //Sets the DebitChangeStatusFail to the Message using ViewBag
                            ViewBag.DebitChangeStatusFail = "Old and new pins are same";
                        }

                        else if (changePinObj.NewPin != changePinObj.ConfirmedPin)
                        {
                            //Sets the DebitChangeStatusFail to the Message using ViewBag
                            ViewBag.DebitChangeStatusFail = "New and confirm pins are not same";
                        }

                        else if (count == 3)
                        {
                            //Sets the DebitChangeStatusFail to the Message using ViewBag
                            ViewBag.DebitChangeStatusFail = "New pin is in series";
                            
                        }

                        else
                        {
                            //Calls the ChangeDebitCardPin in CustomerBL to change the debit card pin and gets the return status
                            bool retVal = CustomerHelper.ChangeDebitCardPin(changePinObj,HttpContext.Session.GetString(Constants.SessionServiceTokenKey));

                            //checks if the retVal is 1 which means the pin have been updated successfully
                            if (retVal)
                            {

                                //Sets the DebitChangeStatus to the Message using ViewBag
                                ViewBag.DebitChangeStatus = "Pin Changed successfully";
                            }
                            else
                            {

                                //Sets the DebitChangeStatusFail to the Message using ViewBag
                                ViewBag.DebitChangeStatusFail = "Some Error occurred !!!";
                            }

                        }
                    } 

                    else
                    {
                        //Sets the DebitCardNumber to the null using ViewBag
                        ViewBag.DebitCardNumber = null;

                        //Sets the value to Message in the ViewBag
                        ViewBag.Message = "Sorry, you currently don't have any active debit cards.";
                    }


                    return View("ChangeDebitCardPin");
                }

 
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
            catch (Exception ex)
            {

                // Redirects to the Failure action with "Some Error occurred !!!" as msg value.
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }
        [HttpGet]
        [Authorize(Roles=Constants.UserRoleKey)]
        public ActionResult AddPayee()
        {
            try
            {
              // Returns the Current View that is "AddPayee".
                return View("AddPayee");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }
        [HttpPost]
        [Authorize(Roles=Constants.UserRoleKey)]
        public ActionResult AddPayee(Payee payee){
            try{
                var token=HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
                if(ModelState.IsValid){
                    if(CustomerHelper.ValidateAccount(payee.AccountNumber,token)){
                        payee.UserName=HttpContext.Session.GetString(Constants.NameKey);
                        bool status=CustomerHelper.AddPayee(payee,token);
                        if(status){
                            ViewBag.Successfull="Payee Added Successfully !";
                        }
                        else{
                            ViewBag.UnSuccessfull="Payee Couldn't be Added !";
                        }
                    }
                    else{
                        ViewBag.ValidAccount="Enter Valid Account Number";
                    }
                    return View("AddPayee");
                }
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
            catch(Exception ex){
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });    
            }
        }
        #region Minor Enhncement Expenditure Analysis
        [HttpGet]
        [Authorize(Roles = Constants.UserRoleKey)]
        public IActionResult ExpenditureAnalysis(string accountNumber, string analysisType, string year, string month)
        {
            string loginName = string.Empty;
            string token = string.Empty;
            List<string> accountNumbers = new List<string>();
            try
            {
                //Get the login name of logged in user
                loginName = HttpContext.Session.GetString(Constants.SessionUserNameKey);
                if (analysisType == null)
                {
                    analysisType = string.Empty;
                }
                if (year == null)
                {
                    year = string.Empty;
                }
                if (month == null)
                {
                    month = string.Empty;
                }
                token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
                accountNumbers = CustomerHelper.GetAccountNumbers(loginName, token);
                Expenditures chartObj = new Expenditures();
                if (accountNumbers == null || accountNumbers.Count == 0 || accountNumbers[0] == "000000000000000")
                {
                    // Sets the AccountNumbers in the ViewBag to null
                    ViewBag.AccountNumbers = null;
                    // Sets the Message in the ViewBag
                    ViewBag.Message = "Sorry, you currently don't have any active accounts.";
                }
                else
                {
                    // Sets the AccountNumbers in the ViewBag to the list of accountNumbers
                    ViewBag.AccountNumbers = accountNumbers.Distinct().ToList();
                    if (year == null || year == string.Empty)
                    {
                        //Sets current year for first click
                        year = DateTime.Now.Year.ToString();
                    }
                    if (month == null || month == string.Empty)
                    {
                        //Sets current month for first click
                        month = DateTime.Today.Month.ToString();
                    }
                    else
                    {
                        //Converts month value if form submitted
                        month = DateTime.ParseExact(month, "MMMM", CultureInfo.CurrentCulture).Month.ToString();
                    }
                    if (analysisType == null || analysisType == string.Empty)
                    {
                        //Sets default month for first click
                        analysisType = "Daily";
                    }
                    if (accountNumber == null)
                    {
                        //Sets default account for first click
                        accountNumber = accountNumbers[0].ToString();
                    }
                    string currentDate = DateTime.Today.ToString("MM/dd/yyyy");
                    List<Common.Models.Transaction> transactions = new List<Common.Models.Transaction>();

                    #region Fetching transactions based upon analysis type
                    if (analysisType == "Daily")
                    {
                        var totalDays = DateTime.DaysInMonth(Convert.ToInt32(year), Convert.ToInt32(month));
                        transactions = CustomerHelper.GetTransactions(month + "/01/" + year, month + "/" + totalDays + "/" + year, accountNumber, token);
                    }
                    else if (analysisType == "Monthly")
                    {
                        transactions = CustomerHelper.GetTransactions("01/01/" + year, "12/31/" + year, accountNumber, token);
                    }
                    else if (analysisType == "Yearly")
                    {
                        string oldestDate = DateTime.Now.AddYears(-10).ToString("MM/dd/yyyy");
                        transactions = CustomerHelper.GetTransactions(oldestDate, currentDate, accountNumber, token);
                    }
                    #endregion
                    if (transactions == null || transactions.Count == 0 || (transactions[0].Id == "0" && transactions[0].TransactionId == "0"))
                    {
                        ViewBag.ErrorMessage = "No transactions exists for the selected period.";
                        chartObj = null;
                    }
                    else
                    {
                        //Filetrs only the withdrawl type transactions
                        var filteredtransactions = transactions.Where(x => x.Withdrawal != null).Select(x => x).OrderBy(y=>y.TransactionDateTime).ToList();
                        if (filteredtransactions == null || filteredtransactions.Count == 0 || (filteredtransactions[0].Id == "0" && filteredtransactions[0].TransactionId == "0"))
                        {
                            ViewBag.ErrorMessage = "No expenditures exists for the selected period.";
                            chartObj = null;
                        }
                        else
                        {
                            List<string> label = new List<string>();
                            List<double> values = new List<double>();
                            List<string> availableColors = new List<string>()
                            {
                                "rgba(51,102,204,0.7)",
                                "rgba(220,57,18,0.7)",
                                "rgba(255,153,0,0.7)",
                                "rgba(16,150,24,0.7)",
                                "rgba(153,0,153,0.7)",
                                "rgba(59,62,172,0.7)",
                                "rgba(0,153,198,0.7)",
                                "rgba(221,68,119,0.7)",
                                "rgba(102,170,0,0.7)",
                                "rgba(184,46,46,0.7)",
                                "rgba(49,99,149,0.7)",
                                "rgba(153,68,153,0.7)",
                                "rgba(34,170,153,0.7)",
                                "rgba(170,170,17,0.7)",
                                "rgba(102,51,204,0.7)",
                                "rgba(230,115,0,0.7)",
                                "rgba(139,7,7,0.7)",
                                "rgba(50,146,98,0.7)",
                                "rgba(85,116,166,0.7)",
                                "rgba(59,62,172,0.7)"
                            };
                            List<string> colors = new List<string>();
                            string xAxis = string.Empty;
                            string yAxis = string.Empty;
                            string analysisBy = string.Empty;
                            string selectedPeriod = string.Empty;
                            
                            #region Aggreagates expenditure based upon analysis type
                            if (analysisType == "Daily" && filteredtransactions!=null)
                            {
                                var distinctDates = filteredtransactions.Select(x => x.TransactionDateTime.ToString("dd/MM/yyyy")).Distinct().ToList();
                                foreach (var uniqueDate in distinctDates)
                                {
                                    double totalAmount = filteredtransactions.Where(x => x.TransactionDateTime.Date.ToString("dd/MM/yyyy") == uniqueDate).Select(xa => Convert.ToDouble(xa.Withdrawal)).Sum();
                                    var expenditure = new Expenditures();
                                    label.Add(uniqueDate.Substring(0,5));
                                    values.Add(totalAmount);
                                    colors.Add(availableColors[distinctDates.IndexOf(uniqueDate)]);
                                }
                                analysisBy = "Daily Analysis";
                                selectedPeriod = DateTimeFormatInfo.CurrentInfo.GetMonthName(Convert.ToInt32(month)) + " - " + year;
                                xAxis = "Date";
                            }
                            else if (analysisType == "Monthly")
                            {
                                var distinctMonths = filteredtransactions.Select(x => x.TransactionDateTime.ToString("MMMM")).Distinct().ToList();
                                foreach (var uniqueMonth in distinctMonths)
                                {
                                    double totalAmount = filteredtransactions.Where(x => x.TransactionDateTime.ToString("MMMM") == uniqueMonth).Select(xa => Convert.ToDouble(xa.Withdrawal)).Sum();
                                    var expenditure = new Expenditures();
                                    label.Add(uniqueMonth);
                                    values.Add(totalAmount);
                                    colors.Add(availableColors[distinctMonths.IndexOf(uniqueMonth)]);
                                }
                                analysisBy = "Monthly Analysis";
                                selectedPeriod = year;
                                xAxis = "Month";
                            }
                            else if (analysisType == "Yearly")
                            {
                                var distinctYears = filteredtransactions.Select(x => x.TransactionDateTime.Year.ToString()).Distinct().ToList();
                                foreach (var uniqueYear in distinctYears)
                                {
                                    double totalAmount = filteredtransactions.Where(x => x.TransactionDateTime.Year.ToString() == uniqueYear).Select(xa => Convert.ToDouble(xa.Withdrawal)).Sum();
                                    var expenditure = new Expenditures();
                                    label.Add(uniqueYear);
                                    values.Add(totalAmount);
                                    colors.Add(availableColors[distinctYears.IndexOf(uniqueYear)]);
                                }
                                analysisBy = "Yearly Analysis";
                                selectedPeriod = "Last 10 Years";
                                xAxis = "Years";
                            }
                            #endregion
                            //Sets data related to graph
                            chartObj.AnalysisType = JsonConvert.SerializeObject(analysisBy);
                            chartObj.SelectedPeriod = JsonConvert.SerializeObject(selectedPeriod);
                            chartObj.Label = JsonConvert.SerializeObject(label);
                            chartObj.Values = JsonConvert.SerializeObject(values);
                            chartObj.XAxis= JsonConvert.SerializeObject(xAxis);
                            chartObj.YAxis=JsonConvert.SerializeObject("Amount");
                            chartObj.Colors=JsonConvert.SerializeObject(colors);
                        }
                    }

                    #region Generates Drop down for All Accounts
                    var accountListArray = new SelectListItem[accountNumbers.Count];
                    for (int i = 0; i < accountNumbers.Count; i++)
                    {
                        SelectListItem newSelectList = new SelectListItem()
                        {
                            Text = accountNumbers[i]
                            ,
                            Value = accountNumbers[i]
                            ,
                            Selected = accountNumbers[i] == accountNumber
                        };
                        accountListArray[i] = newSelectList;
                    }
                    ViewBag.AllAccounts = accountListArray;
                    #endregion

                    #region Generates dropdown for Analysis Option
                    var analysisOption = new List<string>(){
                    "Yearly","Monthly","Daily" };
                    var analysisListArray = new SelectListItem[analysisOption.Count];
                    for (int i = 0; i < analysisOption.Count; i++)
                    {
                        SelectListItem newSelectList = new SelectListItem()
                        {
                            Text = analysisOption[i]
                            ,
                            Value = analysisOption[i]
                            ,
                            Selected = analysisOption[i] == "Daily"
                        };
                        analysisListArray[i] = newSelectList;
                    }
                    ViewBag.AnalysisOptions = analysisListArray;
                    #endregion

                    #region Generates dropdown for last ten years
                    var yearOption = Enumerable.Range(DateTime.Now.Year - 9, 10).Select(x => x.ToString()).OrderByDescending(x => x).ToList();
                    var yearListArray = new SelectListItem[yearOption.Count];
                    for (int i = 0; i < yearOption.Count; i++)
                    {
                        SelectListItem newSelectList = new SelectListItem()
                        {
                            Text = yearOption[i]
                            ,
                            Value = yearOption[i]
                            ,
                            Selected = yearOption[i] == yearOption.FirstOrDefault(),
                        };
                        yearListArray[i] = newSelectList;
                    }
                    ViewBag.Years = yearListArray;
                    #endregion

                    #region Generates drop down for month names
                    var monthOption = DateTimeFormatInfo.CurrentInfo.MonthNames.ToList();
                    var monthListArray = new SelectListItem[monthOption.Count - 1];
                    for (int i = 0; i < monthOption.Count - 1; i++)
                    {
                        SelectListItem newSelectList = new SelectListItem()
                        {
                            Text = monthOption[i]
                            ,
                            Value = monthOption[i]
                            ,
                            Selected = monthOption[i] == DateTimeFormatInfo.CurrentInfo.GetMonthName(DateTime.Now.Month)
                        };
                        monthListArray[i] = newSelectList;
                    }
                    ViewBag.Months = monthListArray;
                    #endregion
                }
                return View("ExpenditureAnalysis", chartObj);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }
        #endregion
        [HttpGet]
        [Authorize(Roles=Constants.UserRoleKey)]
        public ActionResult FundsTransfer(string accountNumber,string payeeAccount,string amount,string remarks)
        {
            List<string> accountNumbers;
            List<Models.Payee> payeeList;
            bool state=false;
            string userName=HttpContext.Session.GetString(Constants.NameKey);
            try
            {
                string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
                accountNumbers = CustomerHelper.GetAccountNumbers(userName,token);
                if (accountNumbers == null || accountNumbers.Count == 0 || accountNumbers[0] == "000000000000000")
                {
                    // Sets the AccountNumbers in the ViewBag to null
                    ViewBag.AccountNumbers = null;

                    // Sets the Message in the ViewBag
                    ViewBag.Message = "Sorry, you currently don't have any active accounts.";
                }
                else
                {
                    // Sets the AccountNumbers in the ViewBag to the list of accountNumbers
                    ViewBag.AccountNumbers = accountNumbers.Distinct().ToList();
                }
                var selectListArray=new SelectListItem[accountNumbers.Count];
                for(int i=0;i<accountNumbers.Count;i++)
                {
                    SelectListItem newSelectList= new SelectListItem()
                        {Text= accountNumbers[i]
                        ,Value=accountNumbers[i]
                        ,Selected=accountNumbers[i]==accountNumber
                        };
                    selectListArray[i]=newSelectList;
                    
                }
                ViewBag.AllAccounts=selectListArray;
                bool status=CustomerHelper.GetPayee(userName,token,out payeeList);
                if(!status){
                    ViewBag.Message="Couldn't fetch payees!";
                    ViewBag.AllPayee=null;
                }
                else{
                    var selectPayeeArray=new SelectListItem[payeeList.Count];
                for(int i=0;i<payeeList.Count;i++)
                {
                    SelectListItem newSelectList= new SelectListItem()
                        {Text= payeeList[i].NickName+" "+ payeeList[i].AccountNumber
                        ,Value=payeeList[i].AccountNumber
                        ,Selected=payeeList[i].AccountNumber==payeeAccount
                        };
                    selectPayeeArray[i]=newSelectList;
                    
                }
                ViewBag.AllPayee=selectPayeeArray;
                }
                //ViewBag.FundsTransfer=null;
               if(accountNumber!=null && remarks!=null && payeeAccount!=null && amount!=null){
                   FundsTransferRequest fundsTransfer=new FundsTransferRequest(){
                       UserAccountNumber=accountNumber,
                       PayeeAccountNumber=payeeAccount,
                       Amount=Convert.ToDecimal(amount),
                       Remarks=remarks,
                       UserName=userName
                   };
                  state= CustomerHelper.TransferFunds(token,fundsTransfer);
                  if(state){
                      ViewBag.Successfull="Funds Transfered Succesfully";
                  }
                  else{
                      ViewBag.UnSuccessfull="Funds Transfered Unsuccesfully";
                  }
                
               }

                // Returns the Current View that is "FundsTransfer".
                return View("FundsTransfer");

            }
            catch (Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }

        [HttpGet]
        [Authorize(Roles=Constants.UserRoleKey)]
        public ActionResult ApplyCreditCard(byte categoryId)
        {
            List<Models.CardType> cardTypes;
            string userName=HttpContext.Session.GetString(Constants.NameKey);
            string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
            try
            {
            bool status=CustomerHelper.GetCardTypes(userName,token,out cardTypes);
            if(status){
                var selectCardTypeArray=new SelectListItem[cardTypes.Count];
                for(int i=0;i<cardTypes.Count;i++)
                {
                    SelectListItem newSelectList= new SelectListItem()
                        {Text= cardTypes[i].CardCategory
                        ,Value=cardTypes[i].CardTypeId.ToString()
                        ,Selected=cardTypes[i].CardTypeId==categoryId
                        };
                    selectCardTypeArray[i]=newSelectList;
                    
                }
                ViewBag.CardType=selectCardTypeArray;
            }
            if(categoryId!=0){
                bool state=CustomerHelper.ApplyForCreditCard(userName,categoryId,token);
                if(state){
                    ViewBag.Successfull="Application For Credit card is succesfull";
                }
                else{
                    ViewBag.UnSuccessfull="Application For Credit card is succesfull";
                }
            }
              // Returns the Current View that is "AddPayee".
                return View("ApplyCreditCard");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }

        [HttpGet]
        [Authorize(Roles=Constants.UserRoleKey)]
        public decimal GetCreditLimit(byte categoryId)
        {
            decimal limit=0;
            string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
            try
            {
              // Returns the Current View that is "AddPayee".
                limit=CustomerHelper.GetCreditLimit(categoryId,token);
                
            }
            catch (Exception ex)
            {
                limit= 0;
            }
            return limit;
        }
        #region Block/Replace Debit Card Minor Enhancement
        //Gets the debit card for the select account number
        [HttpGet]
        [Authorize(Roles = Constants.UserRoleKey)]
        public IActionResult BlockReplaceDebitCard(string accountNumber)
        {
            string loginName = string.Empty;
            string token = string.Empty;
            List<string> accountNumbers = new List<string>();
            string selectedAccountNumber=accountNumber;
            try
            {
                
                loginName = HttpContext.Session.GetString(Constants.SessionUserNameKey);
                token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
                accountNumbers = CustomerHelper.GetAccountNumbers(loginName,token);
                Common.Models.DebitCardDetails debitCard = new Common.Models.DebitCardDetails();
                Models.DebitCardRequest availableDebitCard = new DebitCardRequest();
                if (accountNumbers == null || accountNumbers.Count == 0 || accountNumbers[0] == "000000000000000")
                {
                    // Sets the AccountNumbers in the ViewBag to null
                    ViewBag.AccountNumbers = null;

                    // Sets the Message in the ViewBag
                    ViewBag.Message = "Sorry, you currently don't have any active accounts.";
                }
                else
                {
                    // Sets the AccountNumbers in the ViewBag to the list of accountNumbers
                    ViewBag.AccountNumbers = accountNumbers.Distinct().ToList();
                    if(accountNumber == null)
                    {
                        selectedAccountNumber=accountNumbers[0].ToString();
                        debitCard = CustomerHelper.GetDebitCardDetails(loginName,selectedAccountNumber,token);
                    }
                    else
                    {
                        debitCard = CustomerHelper.GetDebitCardDetails(loginName,selectedAccountNumber,token);
                    }
                    if (debitCard==null || debitCard.DebitCardId == 0 || debitCard.DebitCardNumber == null)
                    {
                        ViewBag.ErrorMessage = "No debit cards available";
                        availableDebitCard = null;
                    }
                    else
                    {
                        availableDebitCard.DebitCardId=debitCard.DebitCardId;
                        availableDebitCard.DebitCardNumber = String.Format("{0}-XXXX-XXXX-{1}",debitCard.DebitCardNumber.Substring(0,4),debitCard.DebitCardNumber.Substring(12,4));
                        availableDebitCard.ValidThru = Convert.ToDateTime(debitCard.ValidThru).ToString("MM/dd/yyyy");
                    }
                    var accountListArray=new SelectListItem[accountNumbers.Count];
                    for(int i=0;i<accountNumbers.Count;i++)
                    {
                        SelectListItem newSelectList= new SelectListItem()
                            {Text= accountNumbers[i]
                            ,Value=accountNumbers[i]
                            ,Selected=accountNumbers[i]==selectedAccountNumber
                            };
                        accountListArray[i]=newSelectList;
                    }
                    ViewBag.AllAccounts=accountListArray;
                }
                return View("BlockReplaceDebitCard", availableDebitCard);

            }
            catch(Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });
            }
        }
        //Submits the replacement request for the debit card
        [Authorize(Roles=Constants.UserRoleKey)]
        [HttpPost]
        public IActionResult ReplaceDebitCard(int debitCardId, Reason reason)
        {
            bool status;
            string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
            try
            {
                status = CustomerHelper.BlockReplaceDebitCard(debitCardId, reason, token);
                if(status)
                {
                    TempData["BlockReplaceStatus"]="Replacement request has been sent to bank for approval";
                }
                else
                {
                    TempData["ErrorMessage"] = "Your request couldn't be processed. Try again later.";
                }
            }
            catch(Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });                
            }
            return RedirectToAction("BlockReplaceDebitCard");
        }
        //Submits the blocking request for the debit card
        [Authorize(Roles=Constants.UserRoleKey)]
        [HttpPost]
        public IActionResult BlockDebitCard(int debitCardId)
        {
            bool status;
            string token = HttpContext.Session.GetString(Constants.SessionServiceTokenKey);
            try
            {
                status = CustomerHelper.BlockDebitCard(debitCardId, token);
                if(status)
                {
                    TempData["BlockReplaceStatus"]="Your debit has been blocked";
                }
                else
                {
                    TempData["ErrorMessage"] = "Unable to process your request.";
                }
            }
            catch(Exception ex)
            {
                return RedirectToAction("Failure", new { msg = "Some Error occurred !!!" });                
            }
            return RedirectToAction("BlockReplaceDebitCard");
        }
        #endregion
    }
}
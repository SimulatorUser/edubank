using System.Collections.Generic;
using System.Net.Http;
using System;
using Newtonsoft.Json;
using System.Linq;

namespace Infosys.EduBank.App.Helper
{
    public class TellerHelper
    {
        Common.Models.AppSettings AppSettings{get; set;}
        HttpServiceHelper Helper{get; set;}
        public TellerHelper(HttpClient client,Common.Models.AppSettings settings)
        {
            Helper= new HttpServiceHelper(client,settings);
            AppSettings=settings;
        }
        public string FetchAccountHolderName(string accountNumber)
        {
            string output=string.Empty;
            try
            {
                var responseMessage= Helper.GetResponse("/BankAuthority/Teller?accountNumber="+accountNumber,string.Empty);
                if(responseMessage.IsSuccessStatusCode)
                {
                    var result=responseMessage.Content.ReadAsStringAsync().Result;
                    var completeData=JsonConvert.DeserializeObject<List<string>>(result);
                    if(completeData.Any(x=>x.ToUpper().Contains("IS ACTIVE")))
                    {
                        output=completeData[0];
                    }
                    else
                    {
                        output=completeData[1];
                    }
  
                }
            }
            catch(Exception e)
            {
                output=string.Empty;
            }
            return output;
        }
        public List<Common.Models.Branch> GetAllBranches(string token)
        {
            var output=new List<Common.Models.Branch>();
            try
            {
                var responseMessage= Helper.GetResponse("/BankAuthority/Teller/Branches",token);
                if(responseMessage.IsSuccessStatusCode)
                {
                    var result=responseMessage.Content.ReadAsStringAsync().Result;
                    output=JsonConvert.DeserializeObject<List<Common.Models.Branch>>(result);
                }
            }
            catch(Exception e)
            {
                output=new List<Common.Models.Branch>();
            }
            return output;
        }
        public Common.Models.Customer AddCustomer(Common.Models.Customer customerObj,string token)
        {
            var output= new Common.Models.Customer();
           var responseMsg=Helper.PostResponse("/BankAuthority/Teller",token,customerObj);
           if(responseMsg.IsSuccessStatusCode)
           {
               var registeredCustStr=responseMsg.Content.ReadAsStringAsync().Result;
               if(!string.IsNullOrEmpty(registeredCustStr))
               {
                   output=JsonConvert.DeserializeObject<Common.Models.Customer>(registeredCustStr);

               }
           }
           return output;
        }
        public long AddMoneyToAccount(Common.Models.InsertTransaction transactObj,string token)
        {
            long status=0;
            try
            {

                transactObj.Info="TellerCredit/"+transactObj.TellerId;
                transactObj.Remarks="Monney Added By Teller";
                transactObj.Mode=Common.Constants.TellerTransactionMode;
                var responseMsg=Helper.PostResponse("/creditMoney/Credit",token,transactObj);
                if(responseMsg.IsSuccessStatusCode)
                {
                    var registeredCustStr=responseMsg.Content.ReadAsStringAsync().Result;
                    if(!string.IsNullOrEmpty(registeredCustStr))
                    {
                        var st=JsonConvert.DeserializeObject<string>(registeredCustStr);
                        status= Convert.ToInt64(st);
                    }
                }
            }
            catch(Exception)
            {
                status=-99;
            }
            return status;

        }

        public string AddInterest(string tellerId,string token)
        {
            string result="Interest for all account is added successfully.";
            try
            {
                var responseMsg=Helper.PostResponse(string.Format("/BankAuthority/Teller/Interest?tellerId={0}",tellerId),token,null);
                if(responseMsg.IsSuccessStatusCode)
                {
                    var accountInterestStatusStr=responseMsg.Content.ReadAsStringAsync().Result;
                    if(!string.IsNullOrEmpty(accountInterestStatusStr))
                    {
                        var allAccountResult=JsonConvert.DeserializeObject<Dictionary<string, bool>>(accountInterestStatusStr);
                        if(allAccountResult!=null && allAccountResult.Count>0)
                        {
                            if(allAccountResult.ContainsValue(false))
                            {
                                result="Unable to add interest for following account-";
                            }
                            foreach(var accStatus in allAccountResult)
                            {
                                if(!accStatus.Value)
                                {
                                    result+=" "+accStatus.Key;
                                }
                            }
                        }
                    }else
                    {
                        result="Not able to fetch details of account for calculating interest";
                    }
                }
            }
            catch(Exception)
            {
                result="Some error occured";
            }
            return result;

        }

        //Gets the list of all accounts
        public List<Common.Models.Account> GetAllAccounts(string token)
        {
            List<Common.Models.Account> accounts = new List<Common.Models.Account>();
            try
            {
                var responseMessage= Helper.GetResponse("/BankAuthority/AccountActive/AccountDetails",token);
                if(responseMessage.IsSuccessStatusCode)
                {
                    var result=responseMessage.Content.ReadAsStringAsync().Result;
                    accounts=JsonConvert.DeserializeObject<List<Common.Models.Account>>(result);
                }
            }
            catch (System.Exception ex)
            {
                accounts = null;
            }
            return accounts;
        }
    
        //Updates the property IsActive of a selected account
        public bool UpdateAccountStatus(Common.Models.Account account,string token)
        {
            bool result = false;
            try
            {
                var responseMessage = Helper.PostResponse("/BankAuthority/AccountActive/UpdateAccountStatus",token,account);
                if(responseMessage.IsSuccessStatusCode)
                {
                    result = responseMessage.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch (System.Exception ex)
            {
                result = false;
            }
            return result;
        }

        public List<App.Models.CardRequest> GetAllRequests(string token){
           List<App.Models.CardRequest> requests = new List<App.Models.CardRequest>();
            try
            {
                var responseMessage= Helper.GetResponse("/CreditCard/CreditCardRequests/",token);
                if(responseMessage.IsSuccessStatusCode)
                {
                    var result=responseMessage.Content.ReadAsStringAsync().Result;
                    requests=JsonConvert.DeserializeObject<List<App.Models.CardRequest>>(result);
                }
            }
            catch (System.Exception ex)
            {
                requests = null;
            }
            return requests;
        }

        //Updates the property IsActive of a selected account
        public bool UpdateRequestStatus(long requestId,string status,string token)
        {
            bool result = false;
            try
            {
                var responseMessage = Helper.GetResponse("/CreditCard/CreditCardRequests/"+requestId+"/"+status,token);
                if(responseMessage.IsSuccessStatusCode)
                {
                    result = responseMessage.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch (System.Exception ex)
            {
                result = false;
            }
            return result;
        }
        //Updates the property IsActive of a selected account
        public Common.Models.CreditCard  GenerateCreditCard(long requestId,string tellerId,string token)
        {
            Common.Models.CreditCard creditCardInfo= new Common.Models.CreditCard();
           // bool result = false;
            try
            {
                var responseMessage = Helper.PostResponse("/CreditCard/CreditCardRequests/?requestId="+requestId+"&tellerId="+tellerId,token,creditCardInfo);
                if(responseMessage.IsSuccessStatusCode)
                {
                    creditCardInfo = responseMessage.Content.ReadAsAsync<Common.Models.CreditCard>().Result;
                }
            }
            catch (System.Exception ex)
            {
                creditCardInfo = null;
            }
            return creditCardInfo;
        }
        //Get all pending debit card requests for approval/rejection Minor Enhancement
        public List<Models.DebitCardRequest> GetDebitCardRequests(string token)
        {
            List<Models.DebitCardRequest> reqObj = null;
            try
            {
                var responseMessage= Helper.GetResponse("/BankAuthority/DebitCardRequests/GetPendingDebitCardRequests",token);
                if(responseMessage.IsSuccessStatusCode)
                {
                    var result=responseMessage.Content.ReadAsStringAsync().Result;
                    reqObj=JsonConvert.DeserializeObject<List<Models.DebitCardRequest>>(result);
                }
            }
            catch (System.Exception ex)
            {
                reqObj = null;
            }
            return reqObj;
        }
        //Update debit card request to approve/reject
        public Common.Models.Customer UpdateDebitCardRequest(App.Models.DebitCardRequest reqObj,string tellerID, string token)
        {
            Common.Models.Customer custObj = new Common.Models.Customer();
            try
            {
                var responseMessage = Helper.PostResponse(string.Format("/BankAuthority/DebitCardRequests?tellerId={0}",tellerID),token,reqObj);
                if(responseMessage.IsSuccessStatusCode)
                {
                    custObj = responseMessage.Content.ReadAsAsync<Common.Models.Customer>().Result;
                }
            }
            catch (System.Exception ex)
            {
                custObj = null;
            }
            return custObj;

        }

    }
}

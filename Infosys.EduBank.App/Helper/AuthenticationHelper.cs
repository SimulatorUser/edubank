using Microsoft.AspNetCore.Http;
using System.Net;
using System;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;


namespace Infosys.EduBank.App.Helper
{

    public class AuthenticationHelper
    {
        Common.Models.AppSettings AppSettings{get; set;}
        HttpServiceHelper ServiceHelper{get;set;}
        public AuthenticationHelper(HttpClient client,Common.Models.AppSettings settings)
        {
            ServiceHelper=new HttpServiceHelper(client,settings);
            AppSettings=settings;
            //Client=client;
        }

        public string ValidateUser(string userName,string password)
        {
            var response=ServiceHelper.PostResponse("/Identity/Authentication",string.Empty,new {UserName=userName,Password=password});
            if(response.IsSuccessStatusCode)
            {
                var  result=response.Content.ReadAsStringAsync().Result;
                return result;
            }
            return null;
        }


        public bool Login(string userName,string password,HttpContext context)
        {
            var result=ValidateUser(userName,password);
                if(!string.IsNullOrEmpty(result))
                {
                    var serviceUser= JsonConvert.DeserializeObject<Common.Models.UserAuth>(result);
                   context.Session.SetString(Constants.SessionServiceTokenKey,serviceUser.Token);
                   context.Session.SetString(Constants.NameKey,serviceUser.Name);
                   context.Session.SetString(Constants.UserIdKey,serviceUser.Id.ToString());
                    if(serviceUser.Role=="Teller")
                    {
                        CreateClaims(context,serviceUser.Name,false);
                    }else
                    {
                        CreateClaims(context,serviceUser.Name,true);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            
        }

        public bool LogOut(HttpContext context)
        {
            try
            {
                context.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme).Wait();
                // Sets the Last User to Empty String Value
                context.Session.Clear();
                return true;
            }
            catch (Exception ex)
            {
                
                return false;
            }
        }
        public void CreateClaims(HttpContext context,string EmailId,bool isUser)
        {
            var claims = new List<Claim>();
            if(isUser)
            {
                claims.Add(new Claim(ClaimTypes.Name, EmailId));
                claims.Add(new Claim(ClaimTypes.Role, Constants.UserRoleKey));
            }else
            {
                claims.Add(new Claim(ClaimTypes.Name, EmailId));
                claims.Add(new Claim(ClaimTypes.Role, Constants.TellerRoleKey));
            }
            

            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = false,
                // Refreshing the authentication session should be allowed.

                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(5),
                // The time at which the authentication ticket expires. A 
                // value set here overrides the ExpireTimeSpan option of 
                // CookieAuthenticationOptions set with AddCookie.

                IsPersistent = true,
                // Whether the authentication session is persisted across 
                // multiple requests. Required when setting the 
                // ExpireTimeSpan option of CookieAuthenticationOptions 
                // set with AddCookie. Also required when setting 
                // ExpiresUtc.

                //IssuedUtc = <DateTimeOffset>,
                // The time at which the authentication ticket was issued.

                RedirectUri = "Customer/EduBankSession",
                // The full path or absolute URI to be used as an http 
                // redirect response value.
            };
            context.SignInAsync(
             CookieAuthenticationDefaults.AuthenticationScheme,
             new ClaimsPrincipal(claimsIdentity),
             authProperties);
        }
    }
}
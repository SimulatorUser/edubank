using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Authentication.JwtBearer;


namespace Infosys.EduBank.App.Helper
{
    public class HttpServiceHelper
    {
        Common.Models.AppSettings AppSettings{get; set;}
        private string ServiceUrl="http://localhost:5101/";
        private HttpClient Client{get;set;}

        public HttpServiceHelper(HttpClient client,Common.Models.AppSettings settings)
        {
            Client=client;
            AppSettings=settings;
        }
        public HttpResponseMessage GetResponse(string url,string token)
        {
            Client= new HttpClient();
            Client.BaseAddress=new Uri(ServiceUrl);
            if(!token.Equals(string.Empty))
            {
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            return Client.GetAsync(url).Result; 
        }
        public HttpResponseMessage PostResponse(string url,string token,object body)
        {
              Client= new HttpClient();
            Client.BaseAddress=new Uri(ServiceUrl);
            if(!token.Equals(string.Empty))
            {
                Client.DefaultRequestHeaders.Add("Accept", "application/json");
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            return Client.PostAsJsonAsync(url,body).Result;
        }
    }
}
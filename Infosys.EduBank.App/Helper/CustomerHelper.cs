using System.Net.Http;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System;
using Newtonsoft.Json;

namespace Infosys.EduBank.App.Helper
{
    public class CustomerHelper
    {
        Common.Models.AppSettings AppSettings{get; set;}
        public HttpServiceHelper Helper { get; set; }
        public CustomerHelper(HttpClient client,Common.Models.AppSettings settings)
        {
            Helper = new HttpServiceHelper(client,settings);
            AppSettings=settings;
        }

        public List<string> GetAccountNumbers(string userName,string token)
        {
            List<string> accountNumbers = new List<string>();
            try
            {
                var responseMessage = Helper.GetResponse("/User/GetAccountNumbers?userName="+userName,token);
                if(responseMessage.IsSuccessStatusCode)
                {
                    accountNumbers = responseMessage.Content.ReadAsAsync<List<string>>().Result;
                }
            }
            catch(Exception ex)
            {
                accountNumbers = new List<string>();
            }
            return accountNumbers;
        }
            
        public decimal GetBalance(string accountNumber,string token)
        {
            decimal balance = 0;
            try
            {
                var responseMessage = Helper.GetResponse("/Transaction/ViewBalance?accountNumber="+accountNumber,token);
                if(responseMessage.IsSuccessStatusCode)
                {
                    balance = responseMessage.Content.ReadAsAsync<decimal>().Result;                    
                }
            }
            catch(Exception ex)
            {
                balance = 0;
            }
            return balance;
        }

        public List<Common.Models.Transaction> GetTransactions(string startDate, string endDate, string accountNumber,string token)
        {
            List<Common.Models.Transaction> transactions = new List<Common.Models.Transaction>();
            try
            {
                string body = JsonConvert.SerializeObject(new {startDate=startDate,endDate=endDate,accountNumber=accountNumber});
                var responseMessage = Helper.PostResponse(string.Format("/Transaction/ViewTransaction?startDate={0}&endDate={1}&accountNumber={2}",startDate,endDate,accountNumber),token,null);
                if(responseMessage.IsSuccessStatusCode)
                {
                    transactions = responseMessage.Content.ReadAsAsync<List<Common.Models.Transaction>>().Result;
                }
            }
            catch(Exception ex)
            {
                transactions = new List<Common.Models.Transaction>();
            }
            return transactions;
        }

        public bool ChangePassword(Models.CustomerCurrentCredentials credentialObj,string token)
        {
             var responseMessage = Helper.PostResponse("/User/ChangePassword",token,credentialObj);
             if(responseMessage.IsSuccessStatusCode)
             {
                 var result= responseMessage.Content.ReadAsAsync<bool>().Result;
                 return result;
             }
             return false;

        }
        public bool GetDebitCards(string userId, out List<string> debitCardList,string token)
        {
            var responseMessage = Helper.GetResponse(string.Format("/User/GetDebitCards?customerId={0}",userId),token);
             if(responseMessage.IsSuccessStatusCode)
             {
                 var result= responseMessage.Content.ReadAsAsync<List<string>>().Result;
                 debitCardList=result;
                 return true;
             }
             else
             {
                 debitCardList=new List<string>();
                 return false;
             }
        }
        public bool ChangeDebitCardPin(Models.DebitCardPinDetails debitPinChangeObj,string token)
        {
            var responseMessage = Helper.PostResponse("/User/ChangePin",token,debitPinChangeObj);
             if(responseMessage.IsSuccessStatusCode)
             {
                 var result= responseMessage.Content.ReadAsAsync<string>().Result;
                 return result.ToUpper()=="SUCCESS";
             }
             return false;
        }
        public bool AddPayee(Models.Payee payee,string token){
            var responseMessage = Helper.PostResponse("/User/AddPayee",token,payee);
             if(responseMessage.IsSuccessStatusCode)
             {
                 var result= responseMessage.Content.ReadAsAsync<string>().Result;
                 return Convert.ToBoolean(result);
             }
             return false;
        }

        public bool ValidateAccount(string accountNumber,string token){
            var responseMessage = Helper.GetResponse("/Banking/ValidateAccount/"+accountNumber,token);
             if(responseMessage.IsSuccessStatusCode)
             {
                 var result= responseMessage.Content.ReadAsAsync<string>().Result;
                 return Convert.ToBoolean(result);
             }
             return false;
        }

        public bool GetPayee(string userName,string token,out List<Models.Payee> payeeList){
            var responseMessage = Helper.GetResponse("/User/GetPayees/?userName="+userName,token);
             if(responseMessage.IsSuccessStatusCode)
             {
                 payeeList= responseMessage.Content.ReadAsAsync<List<Models.Payee>>().Result;
                 return true;
             }
             else{
                payeeList=new List<Models.Payee>();
                 return false;
             }
            
        }
        public bool TransferFunds(string token,Models.FundsTransferRequest fundsTransfer){
            var responseMessage = Helper.PostResponse("/User/FundsTransfer",token,fundsTransfer);
             if(responseMessage.IsSuccessStatusCode)
             {
                 var result= responseMessage.Content.ReadAsAsync<string>().Result;
                 return result.ToUpper()=="SUCCESS";
             }
             return false;
        }

        public bool GetCardTypes(string userName,string token,out List<Models.CardType> cardTypes){
            var responseMessage = Helper.GetResponse("/CreditCard/ApplyForCreditCard/",token);
             if(responseMessage.IsSuccessStatusCode)
             {
                cardTypes= responseMessage.Content.ReadAsAsync<List<Models.CardType>>().Result;;
                 return true;
             }
             cardTypes=new List<Models.CardType>();
             return false;
        }
        public bool ApplyForCreditCard(string userName,byte categoryId,string token){
            var responseMessage=Helper.PostResponse("/CreditCard/ApplyForCreditCard/?name="+userName+"&categoryId="+categoryId,token,null);
            if(responseMessage.IsSuccessStatusCode)
             {
            var result= responseMessage.Content.ReadAsAsync<string>().Result;
                 return Convert.ToBoolean(result);
             }
             return false;
        }

        public decimal GetCreditLimit(byte categoryId,string token){
            var responseMessage=Helper.GetResponse("/CreditCard/CreditLimit/?id="+categoryId,token);
            if(responseMessage.IsSuccessStatusCode)
             {
            var result= responseMessage.Content.ReadAsAsync<string>().Result;
                 return Convert.ToDecimal(result);
             }
             return 0;
        }
        public Common.Models.DebitCardDetails GetDebitCardDetails(string loginName, string accountNumber, string token)
        {
            Common.Models.DebitCardDetails debitCard = new Common.Models.DebitCardDetails();
            try
            {
                string body = JsonConvert.SerializeObject(new {loginName=loginName,accountNumber=accountNumber});
                var responseMessage = Helper.GetResponse(string.Format("/User/BlockReplaceDebitCard/{0}/{1}",loginName,accountNumber),token);
                if(responseMessage.IsSuccessStatusCode)
                {
                    debitCard = responseMessage.Content.ReadAsAsync<Common.Models.DebitCardDetails>().Result;
                }
            }
            catch(Exception ex)
            {
                debitCard = new Common.Models.DebitCardDetails();
            }
            return debitCard;
        }
        public bool BlockReplaceDebitCard(int debitCardId, Reason reason, string token)
        {
            bool status = false;
            try
            {
                var responseMessage = Helper.PostResponse(string.Format("/User/BlockReplaceDebitCard?debitCardId={0}&reason={1}",debitCardId,reason),token,null);
                if(responseMessage.IsSuccessStatusCode)
                {
                    status= responseMessage.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch(Exception ex)
            {
                status = false;
            }
            return status;
        }
        public bool BlockDebitCard(int debitCardId, string token)
        {
            bool status = false;
            try
            {
                var responseMessage = Helper.PostResponse(string.Format("/User/BlockReplaceDebitCard/block/{0}",debitCardId),token,null);
                if(responseMessage.IsSuccessStatusCode)
                {
                    status= responseMessage.Content.ReadAsAsync<bool>().Result;
                }
            }
            catch(Exception ex)
            {
                status = false;
            }
            return status;
        }
    }
}
namespace Infosys.EduBank.App
{
    public class Constants
    {
        public const string SessionServiceTokenKey="SessionServiceToken";
        public const string SessionUserNameKey="UserName";
        public const string SessionRoleKey="Role";
        public const string TempDataLoginStatusKey="LoginStatus";
        public const string UISecretKey="UISecret";
        public const string ServiceTokenKey="ServiceToken";
        public const string UiTokenKey="UiAuthToken";
        public const string NameKey="UserName";
        public const string UserRoleKey="User";
        public const string TellerRoleKey="Teller";
        public const string UserIdKey="UserId";

    }
}
﻿using Infosys.EduBank.AccountDAL.Repository;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TransactionDAL.Interface;
using Infosys.EduBank.AccountDAL.Interface;

namespace Infosys.EduBank.CreditDebitBL.Interface
{
    public interface ICreditDebitBLRepository
    {
        AppSettings AppSettings{set;get;}
        ITransactionRepository TransactionRepositoryObj { get; set; }

        bool UpdateTransactionStatus(long transactionId,char state);
        long CreditMoney(InsertTransaction transaction);
        long DebitMoney(InsertTransaction transaction);
    }
}
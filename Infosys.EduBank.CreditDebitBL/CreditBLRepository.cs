﻿using System;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.AccountDAL.Repository;
using Infosys.EduBank.TransactionDAL.Repository;
using Infosys.EduBank.CreditDebitBL.Interface;
using Infosys.EduBank.TransactionDAL.Interface;
using Infosys.EduBank.AccountDAL.Interface;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.Common;

namespace Infosys.EduBank.CreditDebitBL
{
    public class CreditDebitBLRepository : ICreditDebitBLRepository
    {
        public ITransactionRepository TransactionRepositoryObj {get; set;}
        public Common.Models.AppSettings AppSettings{get; set;}
        public CreditDebitBLRepository(Common.Models.AppSettings appSettings)
        {
            TransactionRepositoryObj=new TransactionRepository(appSettings);
            AppSettings=appSettings;
        }
        public long CreditMoney(Common.Models.InsertTransaction transaction){
            long transactionId = 0;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                transactionId = TransactionRepositoryObj.InsertTransaction(transaction,1);
                if(transaction.Mode==Common.Constants.TellerTransactionMode)
                {
                    TransactionRepositoryObj.UpdateStatus(transactionId,'S');
                }
                else if(transaction.Mode==Common.Constants.FundTransferTransactionMode)
                {
                    TransactionRepositoryObj.UpdateStatus(transactionId,'S');
                }else if(transaction.Mode==Common.Constants.InterestTransactionMode)
                {
                    TransactionRepositoryObj.UpdateStatus(transactionId,'S');
                }
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                transactionId = 0;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return transactionId;
        }
        
        public long DebitMoney(Common.Models.InsertTransaction transaction){
            long transactionId = 0;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                transactionId = TransactionRepositoryObj.InsertTransaction(transaction,0);
                if(transaction.Mode==Common.Constants.TellerTransactionMode)
                {
                    TransactionRepositoryObj.UpdateStatus(transactionId,'S');
                }
                else if(transaction.Mode==Common.Constants.FundTransferTransactionMode)
                {
                    TransactionRepositoryObj.UpdateStatus(transactionId,'S');
                }else if(transaction.Mode==Common.Constants.InterestTransactionMode)
                {
                    TransactionRepositoryObj.UpdateStatus(transactionId,'S');
                }
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                transactionId = 0;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return transactionId;
        }
        public bool UpdateTransactionStatus(long transactionId,char state){
            bool status=false;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
               status= TransactionRepositoryObj.UpdateStatus(transactionId,state);
            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
    }
}

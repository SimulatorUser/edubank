using System;
using System.Configuration;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.Common.Models;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics;

namespace Infosys.EduBank.Common
{
    public class Utilities : IUtilities
    {   
        Common.Models.AppSettings AppSettings{get; set;}
        public string Configuration { get; set; }
        /// <summary>
        /// Receives the value to be encrypted and the security key 
        /// </summary>
        /// <param name="value">value of the input value to be encrypted</param>
        /// <param name="securityKey">security key of the value of the key used for encryption</param>
        /// <returns>Hashed value of the encrypted password</returns>
        public string LoginEncryptDecrypt(string value, string securityKey)
        {
            // Variable hash to store the encrypted hash of the parameter "value" 
            // which will be obtained after Decrypting and encrypting.
            string hash = string.Empty;

            try
            {
                // Variable passwordKeyDecryted to store the decrypted security key with respect to WebConfig's key "PublicKey".
                var x= GetValueFromAppSettings("PublicKey");
                string passwordKeyDecrypted = EncryptDecrypt.Repository.Decrypt(securityKey,x);
                
                // Variable passwordEncrypt to encrypt the value with respect to the obtained decrypted password in the previous step.
                string passwordEncrypt = EncryptDecrypt.Repository.Encrypt(value, passwordKeyDecrypted);
               
                // hash value is set according to the CalucalatedMD5Hash
                hash = EncryptDecrypt.Repository.CalculateMD5Hash(passwordEncrypt);
            }
            catch (Exception ex)
            {
                // Logging in case of any errors with the Type of Repository, PaymentServicesController.
                // Specifying the error message with the error message "Error occurred while generating a hash code for a string".
                //EduBankLogger.Log(typeof(Utilities), "Utilities", "Error occurred while generating a hash code for a string", 2,ex);
                
                // Setting the hash value to Empty string in case of any Exception
                hash = string.Empty;
            }

            // Returns the hash value as the last statement of the method.
            return hash;
        }

        /// <summary>
        /// This function is to encrypt the password cvv pin
        /// </summary>
        /// <param name="value"></param>
        /// <param name="securityKey"></param>
        /// <returns>returns the encrypted string</returns>
        public string Encrypt(string value, string securityKey)
        {
            // Variable passwordEncrypt to store the encrypted password from the passed value and security key.
            string passwordEncrypt = string.Empty;

            try
            {
                // Variable passwordKeyDecryted to store the decrypted security key with respect to WebConfig's key "PublicKey". 
                string passwordKeyDecrypted = EncryptDecrypt.Repository.Decrypt(securityKey, GetValueFromAppSettings("PublicKey"));
                
                // Setting passwordEncrypt to the encrypted value with respect to the passwordkeyDecrypted obtained.
                passwordEncrypt = EncryptDecrypt.Repository.Encrypt(value, passwordKeyDecrypted);                
            }
            catch (Exception ex)
            {
                // Logging in case of any errors with the Type of Repository, PaymentServicesController.
                // Specifying the error message with the error message "Error occurred while doing encryption".
                //EduBankLogger.Log(typeof(Utilities), "Utilities", "Error occurred while doing encryption", 2, ex);

                // Setting the passwordEncrypt to Empty string in case of any Exception
                passwordEncrypt = string.Empty;
            }

            // returns passwordEncrypt at the end of the method.
            return passwordEncrypt;
        }

        /// <summary>
        /// This function is to get the value from the config file
        /// </summary>
        /// <param name="key"></param>
        /// <returns>returns the value corresponding to the key from web config</returns>
        public string GetValueFromAppSettings(string key)
        {
            // variable value to store the AppSettings section's return value.
            string value = string.Empty;

            try
            {
                // This stores the value returned from the ConfigurationManager's AppSettings 
                // taking key as the passed parameter and converting it to string.

                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");
                var setting = builder.Build();
                return setting["AppSettings:"+key];
                }
            catch (Exception ex)
            {
                // Logging in case of any errors with the Type of Repository, PaymentServicesController.
                // Specifying the error message with the error message "Error occurred while generating a value from web config for a key".
                //EduBankLogger.Log(typeof(Utilities), "Utilities", "Error occurred while generating a value from web config for a key", 2, ex);
                
                // Setting value to empty string is case of any Exception
                value = string.Empty;
            }

            // Returns the Successfully obtained value from the AppSettings at the end of the method.
            return value;
        }

        /// <summary>
        /// This function is to get a unique number for transactionId
        /// </summary>
        /// <returns>returns a unique reference number </returns>
        public string GetUniqueKey()
        {
            // Variable uniqueKey to set the return value for the method.
            string uniqueKey = string.Empty;
            try
            {
                // Object random created of class Random.
                Random random = new Random();
                
                // uniqueKey is set to the current date time in the mentioned format.
                uniqueKey = DateTime.Now.ToString("yyMMddHHmmss");
                
                // uniqueKey is appended with a 3 digit random number which does not contain any "0" 
                uniqueKey += Convert.ToString(random.Next(1, 10));
                uniqueKey += Convert.ToString(random.Next(1, 10));
                uniqueKey += Convert.ToString(random.Next(1, 10));                
            }
            catch (Exception ex)
            {
                // Logging in case of any errors with the Type of Repository, PaymentServicesController.
                // Specifying the error message with the error message "Error occurred while generating a unique key".
                //EduBankLogger.Log(typeof(Utilities), "Utilities", "Error occurred while generating a unique key", 2, ex);
                
                // Setting the unique key to empty string in case of any Exception
                uniqueKey = string.Empty;
            }

            // Returns the uniqueKey at the end of the method.
            return uniqueKey;
        }

        public string Decrypt(string value, string securityKey)
        {
            // Variable passwordDecrypt to store the decrypted value from the passed value and security key.
            string passwordDecrypt = string.Empty;
            try
            {
                // Variable passwordKeyDecryted to store the decrypted security key with respect to WebConfig's key "PublicKey". 
                string passwordKeyDecrypted = EncryptDecrypt.Repository.Decrypt(securityKey,GetValueFromAppSettings("PublicKey"));

                // Setting passwordDecrypt to the decrypted value with respect to the passwordkeyDecrypted obtained.
                passwordDecrypt = EncryptDecrypt.Repository.Decrypt(value, passwordKeyDecrypted);
            }
            catch (Exception ex)
            {
                // Logging in case of any errors with the Type of Repository, PaymentServicesController.
                // Specifying the error message with the error message "Error occurred while doing encryption".
                //EduBankLogger.Log(typeof(Utilities), "Utilities", "Error occurred while doing encryption", 2, ex);

                // Setting the passwordDecrypt to Empty string in case of any Exception
                passwordDecrypt = string.Empty;
            }

            // returns passwordDecrypt , i.e the decrypted Password at the end of the method.
            return passwordDecrypt;
        }

        [ExcludeFromCodeCoverage]
        public string GetConnectionString(string connection,string server,string db, string user,string password,string key)
        {
            string result=string.Empty;
            try
            {
                result=Decrypt(connection,key);
                result=String.Format(result,Decrypt(server,key),Decrypt(db,key),Decrypt(user,key),Decrypt(password,key));
            }
            catch(Exception e)
            {
                result=string.Empty;
            }
            return result;
        }

        
        public static string GetMethod()
        {
            return new StackTrace(1).GetFrame(0).GetMethod().Name;
        }
        
    }
}

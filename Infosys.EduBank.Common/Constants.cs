using System.Diagnostics.CodeAnalysis;
namespace Infosys.EduBank.Common
{
    [ExcludeFromCodeCoverage]
    public class Constants
    {
        public const string ConnectionKey="Key";
        public const string AuthSecret="Secret";
        public const string UIAuthSecret="UISecret";
        public const string ConfigUrl="ConfigurationPath";
        public const string ConnectionTemplate="Connection";
        public const string TellerServer= "TellerServer";
        public const string UserServer="UserServer";
        public const string AccountServer= "AccountServer";
        public const string TransactionServer="TransactionServer";
        public const string DebitCardServer="DebitCardServer";
        public const string TellerDB="TellerDB";
        public const string UserDB="UserDB";
        public const string AccountDB= "AccountDB";
        public const string TransactionDB= "TransactionDB";
        public const string DebitCardDB= "DebitCardDB";
        public const string TellerUser= "TellerUser";
        public const string UserUser= "UserUser";
        public const string AccountUser= "AccountUser";
        public const string TransactionUser= "TransactionUser";
        public const string DebitCardUser= "DebitCardUser";
        public const string TellerPassword= "TellerPassword";
        public const string UserPassword= "UserPassword";
        public const string AccountPassword= "AccountPassword";
        public const string TransactionPassword= "TransactionPassword";
        public const string DebitCardPassword= "DebitCardPassword";
        public const string ServiceDataType="";
        public const string TellerTransactionMode="Teller";
        public const string FundTransferTransactionMode="FundTransfer";
        public const string NetBankingTransactionMode="NetBanking";
        public const string DebitCardTransactionMode="DebitCard";
        public const string InterestTransactionMode="InterestAdded";
        public const string EduBankInterestConstant="InterestCalculated";
        public const string EntryLogMessage="Entry";
        public const string ExitLogMessage="Exit";
    }
}
namespace Infosys.EduBank.Common.Models
{
    public class Payee
    {
        public long PayeeId { get; set; }
        public string NickName { get; set; }
        public string AccountNumber { get; set; }
        public string UserName { get; set; }
        public decimal? PreferredAmount { get; set; }
        public Customer Customer { get; set; }
    }
}
namespace Infosys.EduBank.Common.Models
{
    public class UserAuth
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Token {get;set; }
        public string Role {set; get;}
    }
}
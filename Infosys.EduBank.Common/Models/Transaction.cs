using System;

namespace Infosys.EduBank.Common.Models
{
    public class Transaction
    {
        public string Id { get; set; }

        public string TransactionId { get; set; }

        public DateTime TransactionDateTime { get; set; }

        public string Remarks { get; set; }

        public string Withdrawal { get; set; }

        public string Deposit { get; set; }

        public string Balance { get; set; }
    }
}

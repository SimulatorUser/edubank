namespace Infosys.EduBank.Common.Models
{
    public class DebitCardRequest
    {
        public byte DebitCardRequestId { get; set; }
        public int AccountCustomerMappingId { get; set; }
        public int DebitCardId { get; set; }
        public string DebitCardNumber{get; set;}
        public string ValidThru { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
    }
}
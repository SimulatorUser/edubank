namespace Infosys.EduBank.Common.Models
{
    public enum Reason
    {
        CardIsLost,
        CardIsDamaged,
        CardIsExpired
    }
}
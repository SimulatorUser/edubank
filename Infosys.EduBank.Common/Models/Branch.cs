

namespace Infosys.EduBank.Common.Models
{
    public class Branch
    {
        public byte BranchId { get; set; }

        public string IFSC { get; set; }

        public string BranchName { get; set; }

        public string BranchCode { get; set; }
    }
}

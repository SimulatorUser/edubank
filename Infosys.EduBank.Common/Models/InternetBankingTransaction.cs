namespace Infosys.EduBank.Common.Models
{
    public class InternetBankingTransaction
    {
        public string LoginName { get; set; }

        public string Password { get; set; }

        public decimal? Amount { get; set; }

        public string TransactionInfo { get; set; }

        public string Remarks { get; set; }
    }
}

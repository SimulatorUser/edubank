namespace Infosys.EduBank.Common.Models
{
    public class NetBankingPayment
    {
        public string UserId { get; set; }

        public string ReturnURL { get; set; }

        public decimal Amount { get; set; }
    }
}

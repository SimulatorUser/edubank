using System;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Repository.Hierarchy;
using MicroKnights.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting.Internal;
using System.Diagnostics.CodeAnalysis;
namespace Infosys.EduBank.Common.Logger
{
    [ExcludeFromCodeCoverage]
    public class EduBankLogger
    {
        private static IHostingEnvironment hostingEnvironment;
        private static IHttpContextAccessor httpContextAccessor; 
        private static ILog Logger { get; set; }

        /// <summary>
        /// when exception happened in logging we call this method 
        /// and it log the exception in a text file named Error.txt inside LoggingErrors folder
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="t"></param>
        private static void LoggingExceptionHandling(Exception ex, Type t)
        {
            //if exception occurred then we are saving the error in Error.txt file present in our path
            try
            {
                //to get the root location
                 var path = hostingEnvironment.WebRootFileProvider.GetFileInfo(@"~\LoggingErrors").PhysicalPath; 
                //check if the path exist or not if not exist create the folder
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                //Create the message that we want to append in the error.txt file
                string message = Environment.NewLine;
                message += string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                message += string.Format("Message: {0}", ex.Message);
                message += Environment.NewLine;
                message += string.Format("StackTrace: {0}", ex.StackTrace);
                message += Environment.NewLine;
                message += string.Format("Source: {0}", ex.Source);
                message += Environment.NewLine;
                message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                //Finally appending the message generated above in the error.txt file
                System.IO.File.AppendAllText(path + @"\Error.txt", message);
            }
            catch (Exception)
            {
                Logger = LogManager.GetLogger(t);
                //not handling this deep error
            }
        }
        /// <summary>
        /// depending on the type parameter value it returns connection string
        /// and insert command
        /// </summary>
        /// <param name="t"></param>
        /// <param name="type"></param>
        /// <param name="insertCmd"></param>
        /// <returns></returns>
        private static string GetConnection(Type t,Models.AppSettings appSettings, byte type, out string insertCmd)
        {
            string retVal;
            try
            {
                //We don't want to expose the connection string in the web.config file so we encrypt 
                //the connection string and command using our encryption methods in Infosys.EduBank.Common.
                //To access those encrypted connection string and command we need to decrypt.

                //var common = new Common.Utilities();
                var keyOfKeys = Infosys.EncryptDecrypt.Repository.Decrypt(appSettings.SecurityKey1, appSettings.PublicKey);
                var connectionKey = Infosys.EncryptDecrypt.Repository.Decrypt(appSettings.SecurityKey2, keyOfKeys);
                retVal=@"Server=tcp:simulatorserver.database.windows.net,1433;Initial Catalog=ErrorInfoLog;Persist Security Info=False;User ID=EduBankUser;Password=EduUser@123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                //retVal = Infosys.EncryptDecrypt.Repository.Decrypt(appSettings.SecurityKey5, connectionKey);
                if (type != 2)
                {
                    //Now we to set the command to perform insertion of the info log.For that we decrypt the SecurityKey 4 and 7
                    //then assign the insert command and command type then commandType as text
                    var insertCommandKey = Infosys.EncryptDecrypt.Repository.Decrypt(appSettings.SecurityKey4, keyOfKeys);
                    //insertCmd="INSERT INTO InfoLog ([Date],[Message],[UserName]) VALUES (@logDate,@message,@logger)";
                    insertCmd = Infosys.EncryptDecrypt.Repository.Decrypt(appSettings.SecurityKey7, insertCommandKey);
                }
                else
                {
                    //Now we to set the command to perform insertion of the error log.For that we decrypt the SecurityKey 3 and 6
                    //then assign the insert command and command type then commandType as text
                    var insertCommandKey = Infosys.EncryptDecrypt.Repository.Decrypt(appSettings.SecurityKey3, keyOfKeys);
                    insertCmd="INSERT INTO ErrorLog ([Date],[Message],[Username],[StackOverFlow],[Exception]) VALUES (@logDate,@message,@logger,@stackTrace,@exception)";
                    //insertCmd = Infosys.EncryptDecrypt.Repository.Decrypt(appSettings.SecurityKey6, insertCommandKey);
                }
            }
            catch (Exception ex)
            {
                LoggingExceptionHandling(ex, t);
                retVal = string.Empty;
                insertCmd = string.Empty;
            }
            return retVal;
        }

        /// <summary>
        /// It returns a AdoNetAppender and configure the appender for error logging
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private static AdoNetAppender ConfigureErrorAppender(Type t,Models.AppSettings appSettings)
        {
            AdoNetAppender retVal;
            try
            {
                //Now that we have removed all the appenders and we need only ADO.NET Appender we define AdoNetAppedner to do the logging.
                //This will ensure that the logger will perform logging to SQL Server
                retVal = new AdoNetAppender();
                //All the appender should be set with buffer size and a name
                retVal.BufferSize = Convert.ToInt32(appSettings.AdoNetAppenderErrorBufferSize); //1;
                retVal.Name = appSettings.ErrorLoggerName;
                retVal.ConnectionType = appSettings.AdoNetAppenderErrorConnectionType;
                string insertCommand;
                //To log the errors to the DB we need to set the connection string, we achieve that using the following code
                retVal.ConnectionString = GetConnection(t,appSettings, 2, out insertCommand);

                //adoNetAppender.CommandText = "INSERT INTO ErrorLog ([Date],[Message],[Username],[Exception]) VALUES (@logDate,@message,@logger,@exception)";
                retVal.CommandText = insertCommand;
                retVal.CommandType = System.Data.CommandType.Text;

                //The RawLayoutConverter helps in converting the error messages to logger objects that is expected by log4Net
                var rawLayoutConverter = new log4net.Layout.RawLayoutConverter();

                //Add the necessary parameters thats needed to be logged
                //Each parameter should have a name, DBType and layout.The below parameter add the logging date and time when the information is logged
                retVal.AddParameter(new AdoNetAppenderParameter() { ParameterName = "@logDate", DbType = System.Data.DbType.DateTime, Layout = new log4net.Layout.RawTimeStampLayout() });

                //Creates the pattern of the log how it should be logged.
                var patternLayout = new log4net.Layout.PatternLayout("%property{UserName}");
                patternLayout.ActivateOptions();
                retVal.AddParameter(new AdoNetAppenderParameter() { ParameterName = "@logger", DbType = System.Data.DbType.String, Layout = (log4net.Layout.IRawLayout)rawLayoutConverter.ConvertFrom(patternLayout) });
                
                patternLayout = new log4net.Layout.PatternLayout("%property{StackTrace}");
                patternLayout.ActivateOptions();
                //This adds the parameter logger which holds the message/information that we want to error in the DB
                retVal.AddParameter(new AdoNetAppenderParameter() { ParameterName = "@stackTrace", DbType = System.Data.DbType.String, Layout = (log4net.Layout.IRawLayout)rawLayoutConverter.ConvertFrom(patternLayout) });

                //Message is set to the patternLayout same and this pattern layout will be used to get the message then activate the patternLayout
                patternLayout = new log4net.Layout.PatternLayout("%message");
                patternLayout.ActivateOptions();

                //The root level of the hierarchy as level info set configured property as true as we are logging only the information
                retVal.AddParameter(new AdoNetAppenderParameter() { ParameterName = "@message", DbType = System.Data.DbType.String, Layout = (log4net.Layout.IRawLayout)rawLayoutConverter.ConvertFrom(patternLayout) });

                //Message is set to the patternLayout same and this pattern layout will be used to get the message then activate the patternLayout
                var exceptionLayout = new log4net.Layout.ExceptionLayout();
                exceptionLayout.ActivateOptions();
                retVal.AddParameter(new AdoNetAppenderParameter()
                {
                    ParameterName = "@exception",
                    DbType = System.Data.DbType.String,
                    Layout = (log4net.Layout.IRawLayout)rawLayoutConverter.ConvertFrom(exceptionLayout)
                });
                retVal.ActivateOptions();

            }
            catch (Exception ex)
            {
                retVal = new AdoNetAppender();
                LoggingExceptionHandling(ex, t);
            }
            return retVal;

        }
        /// <summary>
        /// It return the AdoNetAppender object and configure it for info logging
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private static AdoNetAppender ConfigureInfoAppender(Type t,Models.AppSettings appSettings)
        {

            AdoNetAppender retVal;
            try
            {
                //Now that we have removed all the appenders and we need only ADO.NET Appender we define AdoNetAppedner to do the logging.
                //This will ensure that the logger will perform logging to SQL Server
                retVal = new AdoNetAppender();
                //var common = new Common.Utilities();
                //All the appender should be set with buffer size and a name
                retVal.BufferSize = Convert.ToInt32(appSettings.AdoNetAppenderInfoBufferSize); //1;
                retVal.Name = appSettings.InfoLoggerName; 
                retVal.ConnectionType = appSettings.AdoNetAppenderInfoConnectionType;
                string insertCommand;
                //To log the errors to the DB we need to set the connection string, we achieve that using the following code
                retVal.ConnectionString = GetConnection(t,appSettings, 1, out insertCommand);

                //adoNetAppender.CommandText = "INSERT INTO InfoLog ([Date],[Message],[UserName]) VALUES (@logDate,@message,@logger)";
                retVal.CommandText = insertCommand;
                retVal.CommandType = System.Data.CommandType.Text;

                //The RawLayoutConverter helps in converting the error messages to logger objects that is expected by log4Net
                var converter = new log4net.Layout.RawLayoutConverter();

                //Add the necessary parameters thats needed to be logged
                //Each parameter should have a name, DBType and layout.The below parameter add the logging date and time when the information is logged
                retVal.AddParameter(new AdoNetAppenderParameter() { ParameterName = "@logDate", DbType = System.Data.DbType.DateTime, Layout = new log4net.Layout.RawTimeStampLayout() });

                //Creates the pattern of the log how it should be logged.
                var patternLayout = new log4net.Layout.PatternLayout("%property{UserName}");
                patternLayout.ActivateOptions();

                //This adds the parameter logger which holds the message/information that we want to log in the DB
                retVal.AddParameter(new AdoNetAppenderParameter() { ParameterName = "@logger", DbType = System.Data.DbType.String, Layout = (log4net.Layout.IRawLayout)converter.ConvertFrom(patternLayout) });

                //Message is set to the patternLayout same and this pattern layout will be used to get the message then activate the patternLayout
                patternLayout = new log4net.Layout.PatternLayout("%message");
                patternLayout.ActivateOptions();
                retVal.AddParameter(new AdoNetAppenderParameter() { ParameterName = "@message", DbType = System.Data.DbType.String, Layout = (log4net.Layout.IRawLayout)converter.ConvertFrom(patternLayout) });
                retVal.ActivateOptions();
            }
            catch (Exception ex)
            {
                retVal = new AdoNetAppender();
                LoggingExceptionHandling(ex, t);
            }
            return retVal;
        }
        /// <summary>
        /// It returns fileAppender object and configure it for warning log
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private static FileAppender ConfigureWarningAppender(Type t,Models.AppSettings appSettings)
        {
            FileAppender retVal;
            try
            {
                retVal = new log4net.Appender.FileAppender();

                //All the appender should be set a name
                retVal.Name = "MyWarningAppender";

                //Getting the file path from the server where the application is hosted
                var path = appSettings.WarningLogPath; 

                //if the path does not exist , the we create the path
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                //Every day a new file should be created with current date. Since date contains / and it is also used in the file path.This will lead to a problem.
                //So we replace the / in the date with - to create a file with date
                var date = DateTime.Now.ToShortDateString().Replace(@"/", "-");
                var filePath = @"\Warnings" + date + ".txt";

                //If the file does exists in the path with the same file name we create it
                if (!System.IO.File.Exists(path + filePath))
                {
                    System.IO.File.AppendAllText(path + filePath, "========Date " + date + " ========\n");
                }
                //Else we go ahead with appending the log information to the already existing file.
                //Add the file path to the file appender
                retVal.File = path + filePath;
                retVal.AppendToFile = true;

                //We are now defining what should be the pattern of the log should be when its saved to a file
                var patternLayout = new log4net.Layout.PatternLayout("%date [%thread] %level %logger %property{UserName} - %message%newline");
                patternLayout.ActivateOptions();

                //Now we define the FileAppender with the layout we have created above to be used for logging
                retVal.Layout = patternLayout;

                //LockingModel will ensure that the file is locked properly while concurrent users are using the application.
                retVal.LockingModel = new FileAppender.MinimalLock();
                retVal.ActivateOptions();
            }
            catch (Exception ex)
            {
                retVal = new FileAppender();
                LoggingExceptionHandling(ex, t);
            }
            return retVal;
        }

        /// <summary>
        /// use ConfigureErrorAppender ,ConfigureInfoAppender and ConfigureWarningAppender
        /// and configure the logger 
        /// </summary>
        /// <param name="t"></param>
        /// <param name="name"></param>
        /// <param name="type"></param>
        private static void ConfigureLogger(Type t,Models.AppSettings appSettings, string name, byte type)
        {
            try
            {
                //Setting the UserName to the logged in user of e-wallet
                log4net.GlobalContext.Properties["UserName"] = name;

                //The following code helps you to get all the possible logging functionality by log4Net
                Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository(t.Assembly);

                //RemoveAllAppenders method cleans up the logger if there were any previously initialized appenders
                hierarchy.Root.RemoveAllAppenders();

                //The root level of the hierarchy as level info set configured property as true as we are logging only the information
                if (type == 1)
                {
                    hierarchy.Root.Level = Level.Info;
                    AdoNetAppender adoNetAppender = ConfigureInfoAppender(t,appSettings);
                    hierarchy.Root.AddAppender(adoNetAppender);
                }
                else if (type == 2)
                {
                    hierarchy.Root.Level = Level.Error;
                    AdoNetAppender adoNetAppender = ConfigureErrorAppender(t,appSettings);
                    hierarchy.Root.AddAppender(adoNetAppender);
                }
                else if (type == 3)
                {
                    hierarchy.Root.Level = Level.Warn;
                    FileAppender fileAppender = ConfigureWarningAppender(t,appSettings);
                    hierarchy.Root.AddAppender(fileAppender);

                }
                else
                {
                    hierarchy.Root.Level = Level.Info;
                    AdoNetAppender adoNetAppender = ConfigureInfoAppender(t,appSettings);
                    hierarchy.Root.AddAppender(adoNetAppender);
                }

                hierarchy.Configured = true;


                //Adding the above appender with all the parameter we wanted laid out in the layout to the root of hierarchy object that we created in the start of this application


                //The following code will activate the adoNetappender so that it can start performing the logging the information details
                log4net.Config.BasicConfigurator.Configure(hierarchy);


                //Getting the type of logger and assigning it to logger property present in the class
                Logger = LogManager.GetLogger(t);
                log4net.Util.LogLog.InternalDebugging=true;
                //Setting the logger hierarchy property to the hierarchy object created above
                ((log4net.Repository.Hierarchy.Logger)(Logger.Logger)).Hierarchy = hierarchy;
            }
            catch (Exception ex)
            {
                LoggingExceptionHandling(ex, t);
            }
        }

        /// <summary>
        /// call the CoufigureLogger method which will configure the logger 
        /// and the depending on the type value it is logging info, error and warning 
        /// </summary>
        /// <param name="t"></param>
        /// <param name="name"></param>
        /// <param name="message"></param>
        /// <param name="type"></param>
        /// <param name="e"></param>
        public static void Log(Type t, Models.AppSettings appSettings, string message, byte type, Exception e = null)
        {
            try
            {
                hostingEnvironment = new HostingEnvironment();
                httpContextAccessor = new HttpContextAccessor(); 
                //configure the logger based on the type value
                if(appSettings.InfoLogStatus||appSettings.WarningLogStatus||appSettings.ErrorLogStatus)
                {
                    ConfigureLogger(t,appSettings, appSettings.UserName, type);
                }
                if (type == 1 && appSettings.InfoLogStatus)//1 for info log
                {
                    Logger.Info(message);
                }
                else if (type == 2 && appSettings.ErrorLogStatus)//2 for error log
                {
                    log4net.GlobalContext.Properties["StackTrace"]=e.StackTrace;
                    Logger.Error(message, e);
                }
                else if (type == 3 && appSettings.WarningLogStatus)//3 for warning log
                {
                    Logger.Warn(message);
                }
                else// by default it will be info logging
                {
                    if(appSettings.InfoLogStatus)
                        Logger.Info(message);
                }
            }
            catch (Exception ex)
            {
                LoggingExceptionHandling(ex, t);
            }

        }
    }
}

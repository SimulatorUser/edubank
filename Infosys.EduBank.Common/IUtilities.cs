namespace Infosys.EduBank.Common
{
    public interface IUtilities
    {
        string Configuration { get; set; }
        string Decrypt(string value, string securityKey);
        string Encrypt(string value, string securityKey);
        string GetUniqueKey();
        string GetValueFromAppSettings(string key);
        string LoginEncryptDecrypt(string value, string securityKey);
        string GetConnectionString(string connection,string server,string db, string user,string password,string key);
    }
}
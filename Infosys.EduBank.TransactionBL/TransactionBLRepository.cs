﻿using System;
using System.Linq;
using System.Collections.Generic;
using Infosys.EduBank.TransactionDAL.Repository;
using Infosys.EduBank.TransactionDAL.Models;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TransactionBL.Interface;
using Infosys.EduBank.TransactionDAL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.TransactionBL
{
    public class TransactionBLRepository : ITransactionBLRepository
    {
        public ITransactionRepository TransactionRepositoryObj { get; set; }
        public AppSettings AppSettings { get; set; }

        public TransactionBLRepository(AppSettings appSettings)
        {
            AppSettings = appSettings;
            TransactionRepositoryObj = new TransactionRepository(appSettings);
        }
        

        public decimal GetBalance(string accountNumber){
            decimal balance = 0;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                balance = TransactionRepositoryObj.GetBalance(accountNumber);
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                balance=-99;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return balance;
        } 


        public List<Common.Models.Transaction> ViewTransactions(DateTime startDate,DateTime endDate,string accountNumber){
            List<Common.Models.Transaction> transactionsToReturn = new List<Common.Models.Transaction>();
            List<TransactionDAL.Models.Transaction> transactionList = new List<TransactionDAL.Models.Transaction>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                transactionList = TransactionRepositoryObj.ViewTransactions(startDate,endDate,accountNumber);
                foreach(var transaction in transactionList){
                    Common.Models.Transaction transObj=new Common.Models.Transaction();
                    transObj.TransactionId=transaction.TransactionId.ToString();
                    transObj.TransactionDateTime=transaction.TransactionDateTime;
                    if(transaction.Type){
                        transObj.Deposit=transaction.Amount.ToString();
                    }
                    else{
                        transObj.Withdrawal=transaction.Amount.ToString();
                    }
                    transObj.Remarks=transaction.Remarks;
                    transactionsToReturn.Add(transObj);
                }
            }
            catch(Exception ex){
                transactionsToReturn=null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return transactionsToReturn;
        }

        public Dictionary<DateTime,decimal> GetBalancePerDay(string accountNumber){
            Dictionary<DateTime,decimal> result = new Dictionary<DateTime, decimal>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                var allTransactions=TransactionRepositoryObj.ViewTransactions(DateTime.Now.AddYears(-200),DateTime.Now.AddDays(1),accountNumber);
                List<TransactionDAL.Models.Transaction> interestTransaction=allTransactions.Where(x=>x.Info.ToUpper().Contains(Common.Constants.EduBankInterestConstant.ToUpper())).ToList();
                DateTime tempDate= allTransactions.Min(x=>x.TransactionDateTime);
                if(interestTransaction!=null && interestTransaction.Count>0)
                {
                    tempDate=interestTransaction.Max(x=>x.TransactionDateTime).AddMinutes(1);
                }
                
                while(tempDate<=DateTime.Now)
                {
                    var dayBalance=TransactionRepositoryObj.GetBalance(accountNumber,tempDate);
                    result.Add(tempDate,dayBalance);
                    tempDate=tempDate.AddDays(1);
                }
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                result=new Dictionary<DateTime, decimal>();
            }
            finally{
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return result;
        }

    }
}

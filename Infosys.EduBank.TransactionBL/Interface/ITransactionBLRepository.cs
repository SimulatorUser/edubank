﻿using System;
using System.Collections.Generic;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TransactionDAL.Repository;
using Infosys.EduBank.TransactionDAL.Interface;

namespace Infosys.EduBank.TransactionBL.Interface
{
    public interface ITransactionBLRepository
    {
        AppSettings AppSettings { get; set; }
        ITransactionRepository TransactionRepositoryObj { get; set; }
        decimal GetBalance(string accountNumber);
        List<Transaction> ViewTransactions(DateTime startDate, DateTime endDate, string accountNumber);
        Dictionary<DateTime,decimal> GetBalancePerDay(string accountNumber);
    }
}
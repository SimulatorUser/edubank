USE [CreditCard]
GO
/****** Object:  StoredProcedure [dbo].[usp_IssueCreditCard]    Script Date: 1/2/2019 12:02:45 PM ******/
DROP PROCEDURE [dbo].[usp_IssueCreditCard]
GO
ALTER TABLE [dbo].[UnbilledTransaction] DROP CONSTRAINT [chk_unbilledtxn_amount]
GO
ALTER TABLE [dbo].[EMIPlan] DROP CONSTRAINT [chk_emiplans_planinterest]
GO
ALTER TABLE [dbo].[CustomerBill] DROP CONSTRAINT [chk_customerbill_minamountdue]
GO
ALTER TABLE [dbo].[CustomerBill] DROP CONSTRAINT [chk_customerbill_interest]
GO
ALTER TABLE [dbo].[CustomerBill] DROP CONSTRAINT [chk_customerbill_amountpaid]
GO
ALTER TABLE [dbo].[CardType] DROP CONSTRAINT [chk_cardtype_creditlimit]
GO
ALTER TABLE [dbo].[CardRequest] DROP CONSTRAINT [chk_cardrequest_Status]
GO
ALTER TABLE [dbo].[BilledTransaction] DROP CONSTRAINT [chk_billedtxn_interest]
GO
ALTER TABLE [dbo].[BilledTransaction] DROP CONSTRAINT [chk_billedtxn_amount]
GO
ALTER TABLE [dbo].[UnbilledTransaction] DROP CONSTRAINT [fk_unbilledtxn_planid]
GO
ALTER TABLE [dbo].[UnbilledTransaction] DROP CONSTRAINT [fk_unbilledtxn_creditcardid]
GO
ALTER TABLE [dbo].[CustomerBill] DROP CONSTRAINT [fk_customerbill_creditcardid]
GO
ALTER TABLE [dbo].[CreditCard] DROP CONSTRAINT [fk_creditcard_cardtypeid]
GO
ALTER TABLE [dbo].[CardRequest] DROP CONSTRAINT [fk_cardrequest_cardtypeid]
GO
ALTER TABLE [dbo].[BilledTransaction] DROP CONSTRAINT [fk_billedtxn_unbilledtxnid]
GO
ALTER TABLE [dbo].[CreditCard] DROP CONSTRAINT [DF__CreditCar__IsLoc__2B3F6F97]
GO
ALTER TABLE [dbo].[CreditCard] DROP CONSTRAINT [DF__CreditCar__IsAct__2A4B4B5E]
GO
/****** Object:  Table [dbo].[UnbilledTransaction]    Script Date: 1/2/2019 12:02:45 PM ******/
DROP TABLE [dbo].[UnbilledTransaction]
GO
/****** Object:  Table [dbo].[EMIPlan]    Script Date: 1/2/2019 12:02:45 PM ******/
DROP TABLE [dbo].[EMIPlan]
GO
/****** Object:  Table [dbo].[CustomerBill]    Script Date: 1/2/2019 12:02:45 PM ******/
DROP TABLE [dbo].[CustomerBill]
GO
/****** Object:  Table [dbo].[CreditCard]    Script Date: 1/2/2019 12:02:45 PM ******/
DROP TABLE [dbo].[CreditCard]
GO
/****** Object:  Table [dbo].[CardType]    Script Date: 1/2/2019 12:02:45 PM ******/
DROP TABLE [dbo].[CardType]
GO
/****** Object:  Table [dbo].[CardRequest]    Script Date: 1/2/2019 12:02:45 PM ******/
DROP TABLE [dbo].[CardRequest]
GO
/****** Object:  Table [dbo].[BilledTransaction]    Script Date: 1/2/2019 12:02:45 PM ******/
DROP TABLE [dbo].[BilledTransaction]
GO
/****** Object:  Table [dbo].[BilledTransaction]    Script Date: 1/2/2019 12:02:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BilledTransaction](
	[BilledTransactionId] [bigint] IDENTITY(1,1) NOT NULL,
	[UnbilledTransactionId] [bigint] NULL,
	[Amount] [decimal](18, 0) NULL,
	[Interest] [decimal](18, 0) NULL,
	[TimeStamp] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BilledTransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CardRequest]    Script Date: 1/2/2019 12:02:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardRequest](
	[RequestId] [bigint] IDENTITY(1000,1) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[CardTypeId] [tinyint] NULL,
	[RequestDate] [datetime] NOT NULL,
	[Status] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CardType]    Script Date: 1/2/2019 12:02:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CardType](
	[CardTypeId] [tinyint] IDENTITY(1,1) NOT NULL,
	[CardCategory] [varchar](20) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreditLimit] [decimal](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[CardTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CreditCard]    Script Date: 1/2/2019 12:02:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditCard](
	[CreditCardId] [bigint] IDENTITY(1,1) NOT NULL,
	[CardNo] [char](16) NOT NULL,
	[CardTypeId] [tinyint] NULL,
	[CustomerId] [bigint] NULL,
	[CVV] [varbinary](300) NOT NULL,
	[PIN] [varbinary](300) NOT NULL,
	[ValidFrom] [date] NOT NULL,
	[ValidThru] [date] NOT NULL,
	[CreatedBy] [varchar](100) NOT NULL,
	[CreatedTimeStamp] [datetime] NOT NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedTimeStamp] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IsLocked] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[CreditCardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerBill]    Script Date: 1/2/2019 12:02:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerBill](
	[BillId] [bigint] IDENTITY(1,1) NOT NULL,
	[BillDate] [date] NOT NULL,
	[TotalAmountDue] [decimal](18, 0) NULL,
	[MinAmountDue] [decimal](18, 0) NULL,
	[PaymentDueDate] [date] NOT NULL,
	[PaymentDate] [date] NULL,
	[AmountPaid] [decimal](18, 0) NULL,
	[CreditCardId] [bigint] NOT NULL,
	[LateFeeAmount] [decimal](18, 0) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BillId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EMIPlan]    Script Date: 1/2/2019 12:02:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EMIPlan](
	[PlanId] [tinyint] IDENTITY(1,1) NOT NULL,
	[PlanDuration] [tinyint] NOT NULL,
	[PlanInterest] [decimal](18, 0) NULL,
	[IsActive] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PlanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnbilledTransaction]    Script Date: 1/2/2019 12:02:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnbilledTransaction](
	[UnbilledTransactionId] [bigint] IDENTITY(1,1) NOT NULL,
	[CreditCardId] [bigint] NULL,
	[MerchantAccountNumber] [varchar](16) NOT NULL,
	[MerchantName] [varchar](50) NOT NULL,
	[Amount] [decimal](18, 0) NULL,
	[PlanId] [tinyint] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UnbilledTransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CardRequest] ON 

INSERT [dbo].[CardRequest] ([RequestId], [CustomerId], [CardTypeId], [RequestDate], [Status]) VALUES (1000, 1000000027, 1, CAST(N'2018-11-30T00:00:00.000' AS DateTime), N'Rejected')
INSERT [dbo].[CardRequest] ([RequestId], [CustomerId], [CardTypeId], [RequestDate], [Status]) VALUES (1001, 1000000027, 2, CAST(N'2018-11-30T00:00:00.000' AS DateTime), N'Rejected')
SET IDENTITY_INSERT [dbo].[CardRequest] OFF
SET IDENTITY_INSERT [dbo].[CardType] ON 

INSERT [dbo].[CardType] ([CardTypeId], [CardCategory], [IsActive], [CreditLimit]) VALUES (1, N'Platinum', 1, CAST(200000 AS Decimal(18, 0)))
INSERT [dbo].[CardType] ([CardTypeId], [CardCategory], [IsActive], [CreditLimit]) VALUES (2, N'Gold', 1, CAST(100000 AS Decimal(18, 0)))
INSERT [dbo].[CardType] ([CardTypeId], [CardCategory], [IsActive], [CreditLimit]) VALUES (3, N'Silver', 1, CAST(50000 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[CardType] OFF
ALTER TABLE [dbo].[CreditCard] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[CreditCard] ADD  DEFAULT ((0)) FOR [IsLocked]
GO
ALTER TABLE [dbo].[BilledTransaction]  WITH CHECK ADD  CONSTRAINT [fk_billedtxn_unbilledtxnid] FOREIGN KEY([UnbilledTransactionId])
REFERENCES [dbo].[UnbilledTransaction] ([UnbilledTransactionId])
GO
ALTER TABLE [dbo].[BilledTransaction] CHECK CONSTRAINT [fk_billedtxn_unbilledtxnid]
GO
ALTER TABLE [dbo].[CardRequest]  WITH CHECK ADD  CONSTRAINT [fk_cardrequest_cardtypeid] FOREIGN KEY([CardTypeId])
REFERENCES [dbo].[CardType] ([CardTypeId])
GO
ALTER TABLE [dbo].[CardRequest] CHECK CONSTRAINT [fk_cardrequest_cardtypeid]
GO
ALTER TABLE [dbo].[CreditCard]  WITH CHECK ADD  CONSTRAINT [fk_creditcard_cardtypeid] FOREIGN KEY([CardTypeId])
REFERENCES [dbo].[CardType] ([CardTypeId])
GO
ALTER TABLE [dbo].[CreditCard] CHECK CONSTRAINT [fk_creditcard_cardtypeid]
GO
ALTER TABLE [dbo].[CustomerBill]  WITH CHECK ADD  CONSTRAINT [fk_customerbill_creditcardid] FOREIGN KEY([CreditCardId])
REFERENCES [dbo].[CreditCard] ([CreditCardId])
GO
ALTER TABLE [dbo].[CustomerBill] CHECK CONSTRAINT [fk_customerbill_creditcardid]
GO
ALTER TABLE [dbo].[UnbilledTransaction]  WITH CHECK ADD  CONSTRAINT [fk_unbilledtxn_creditcardid] FOREIGN KEY([CreditCardId])
REFERENCES [dbo].[CreditCard] ([CreditCardId])
GO
ALTER TABLE [dbo].[UnbilledTransaction] CHECK CONSTRAINT [fk_unbilledtxn_creditcardid]
GO
ALTER TABLE [dbo].[UnbilledTransaction]  WITH CHECK ADD  CONSTRAINT [fk_unbilledtxn_planid] FOREIGN KEY([PlanId])
REFERENCES [dbo].[EMIPlan] ([PlanId])
GO
ALTER TABLE [dbo].[UnbilledTransaction] CHECK CONSTRAINT [fk_unbilledtxn_planid]
GO
ALTER TABLE [dbo].[BilledTransaction]  WITH CHECK ADD  CONSTRAINT [chk_billedtxn_amount] CHECK  (([Amount]>(0)))
GO
ALTER TABLE [dbo].[BilledTransaction] CHECK CONSTRAINT [chk_billedtxn_amount]
GO
ALTER TABLE [dbo].[BilledTransaction]  WITH CHECK ADD  CONSTRAINT [chk_billedtxn_interest] CHECK  (([Interest]>(0)))
GO
ALTER TABLE [dbo].[BilledTransaction] CHECK CONSTRAINT [chk_billedtxn_interest]
GO
ALTER TABLE [dbo].[CardRequest]  WITH CHECK ADD  CONSTRAINT [chk_cardrequest_Status] CHECK  (([Status]='Rejected' OR [Status]='Success' OR [Status]='Pending'))
GO
ALTER TABLE [dbo].[CardRequest] CHECK CONSTRAINT [chk_cardrequest_Status]
GO
ALTER TABLE [dbo].[CardType]  WITH CHECK ADD  CONSTRAINT [chk_cardtype_creditlimit] CHECK  (([CreditLimit]>(0)))
GO
ALTER TABLE [dbo].[CardType] CHECK CONSTRAINT [chk_cardtype_creditlimit]
GO
ALTER TABLE [dbo].[CustomerBill]  WITH CHECK ADD  CONSTRAINT [chk_customerbill_amountpaid] CHECK  (([AmountPaid]>([MinAmountDue]+[LateFeeAmount])))
GO
ALTER TABLE [dbo].[CustomerBill] CHECK CONSTRAINT [chk_customerbill_amountpaid]
GO
ALTER TABLE [dbo].[CustomerBill]  WITH CHECK ADD  CONSTRAINT [chk_customerbill_interest] CHECK  (([TotalAmountDue]>(0)))
GO
ALTER TABLE [dbo].[CustomerBill] CHECK CONSTRAINT [chk_customerbill_interest]
GO
ALTER TABLE [dbo].[CustomerBill]  WITH CHECK ADD  CONSTRAINT [chk_customerbill_minamountdue] CHECK  (([MinAmountDue]>(0)))
GO
ALTER TABLE [dbo].[CustomerBill] CHECK CONSTRAINT [chk_customerbill_minamountdue]
GO
ALTER TABLE [dbo].[EMIPlan]  WITH CHECK ADD  CONSTRAINT [chk_emiplans_planinterest] CHECK  (([PlanInterest]>=(0)))
GO
ALTER TABLE [dbo].[EMIPlan] CHECK CONSTRAINT [chk_emiplans_planinterest]
GO
ALTER TABLE [dbo].[UnbilledTransaction]  WITH CHECK ADD  CONSTRAINT [chk_unbilledtxn_amount] CHECK  (([Amount]>(0)))
GO
ALTER TABLE [dbo].[UnbilledTransaction] CHECK CONSTRAINT [chk_unbilledtxn_amount]
GO
/****** Object:  StoredProcedure [dbo].[usp_IssueCreditCard]    Script Date: 1/2/2019 12:02:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_IssueCreditCard]
( 
	@CustomerId BIGINT,
	@CardTypeId TINYINT,
	@CardNum varchar(16),
	@CvvKey VARCHAR(200),
	@PinKey VARCHAR(200),
	@TellerId TINYINT, 
	@CVV NVARCHAR(128),
	@Pin NVARCHAR(128),
	@ValidFrom SMALLDATETIME , 
	@ValidThru SMALLDATETIME ,
	@CardId BIGINT OUTPUT
)
AS 
BEGIN
	DECLARE	@EncryptedCVV VARBINARY(300)
	DECLARE @EncryptedPin VARBINARY(300)
	DECLARE @EncryptKey VARCHAR(200)
	SET @CardId=0
	BEGIN TRY
		BEGIN
		--SET @EncryptKey = (SELECT Value FROM [Configuration] WHERE [Key] = 'CVV Key')
		SET @EncryptedCVV = (SELECT encryptbypassphrase(@CvvKey, @CVV))
		--SET @EncryptKey = (SELECT Value FROM [Configuration] WHERE [Key] = 'Pin Key')
		SET @EncryptedPin = (SELECT encryptbypassphrase(@PinKey, @Pin))
		INSERT INTO CreditCard VALUES (@CardNum, @CardTypeId, @CustomerId, @EncryptedCVV, @EncryptedPin, @ValidFrom, @ValidThru,@TellerId,SYSDATETIME(),Null,null,1,0)
		SELECT @CardId=CreditCardId FROM CreditCard WHERE CardNo=@CardNum
		RETURN 1
		END
	END TRY
	BEGIN CATCH
		RETURN -99
	END CATCH
END
GO

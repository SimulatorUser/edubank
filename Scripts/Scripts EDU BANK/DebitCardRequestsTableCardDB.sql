USE [Card]
GO

/****** Object:  Table [dbo].[DebitCardRequest]    Script Date: 01/02/2019 11:59:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DebitCardRequest](
	[DebitCardRequestId] [tinyint] IDENTITY(1,1) NOT NULL,
	[AccountCustomerMappingId] [int] NOT NULL,
	[DebitCardId] [int] NOT NULL,
	[Reason] [varchar](100) NOT NULL,
	[Status] [varchar](1) NOT NULL,
 CONSTRAINT [pk_DebitCardRequest_DebitCardRequestId] PRIMARY KEY CLUSTERED 
(
	[DebitCardRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[DebitCardRequest]  WITH CHECK ADD  CONSTRAINT [chk_DebitCardRequest_Status] CHECK  (([Status]='P' OR [Status]='R' OR [Status]='A'))
GO

ALTER TABLE [dbo].[DebitCardRequest] CHECK CONSTRAINT [chk_DebitCardRequest_Status]
GO



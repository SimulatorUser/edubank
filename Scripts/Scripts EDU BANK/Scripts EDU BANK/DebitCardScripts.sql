USE [Card]
GO
/****** Object:  StoredProcedure [dbo].[usp_IssueDebitCard]    Script Date: 12/25/2018 11:23:56 AM ******/
DROP PROCEDURE [dbo].[usp_IssueDebitCard]
GO
/****** Object:  StoredProcedure [dbo].[usp_ChangeDebitCardPin]    Script Date: 12/25/2018 11:23:56 AM ******/
DROP PROCEDURE [dbo].[usp_ChangeDebitCardPin]
GO
/****** Object:  Table [dbo].[DebitCard]    Script Date: 12/25/2018 11:23:56 AM ******/
DROP TABLE [dbo].[DebitCard]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ValidateDebitCardPIN]    Script Date: 12/25/2018 11:23:56 AM ******/
DROP FUNCTION [dbo].[ufn_ValidateDebitCardPIN]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ValidateDebitCard]    Script Date: 12/25/2018 11:23:56 AM ******/
DROP FUNCTION [dbo].[ufn_ValidateDebitCard]
GO
USE [Card]
GO
/****** Object:  Sequence [dbo].[CardLast_Sequence]    Script Date: 12/25/2018 11:23:56 AM ******/
DROP SEQUENCE [dbo].[CardLast_Sequence]
GO
USE [Card]
GO
/****** Object:  Sequence [dbo].[CardFirst_Sequence]    Script Date: 12/25/2018 11:23:56 AM ******/
DROP SEQUENCE [dbo].[CardFirst_Sequence]
GO
USE [master]
GO
/****** Object:  Database [Card]    Script Date: 12/25/2018 11:23:56 AM ******/
DROP DATABASE [Card]
GO
/****** Object:  Database [Card]    Script Date: 12/25/2018 11:23:56 AM ******/
CREATE DATABASE [Card]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Card', FILENAME = N'C:\Users\vishal.verma03\Card.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Card_log', FILENAME = N'C:\Users\vishal.verma03\Card_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Card] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Card].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Card] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Card] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Card] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Card] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Card] SET ARITHABORT OFF 
GO
ALTER DATABASE [Card] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Card] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Card] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Card] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Card] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Card] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Card] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Card] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Card] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Card] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Card] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Card] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Card] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Card] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Card] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Card] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Card] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Card] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Card] SET  MULTI_USER 
GO
ALTER DATABASE [Card] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Card] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Card] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Card] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Card] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Card] SET QUERY_STORE = OFF
GO
USE [Card]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [Card]
GO
USE [Card]
GO
/****** Object:  Sequence [dbo].[CardFirst_Sequence]    Script Date: 12/25/2018 11:23:56 AM ******/
CREATE SEQUENCE [dbo].[CardFirst_Sequence] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 99
 CACHE 
GO
USE [Card]
GO
/****** Object:  Sequence [dbo].[CardLast_Sequence]    Script Date: 12/25/2018 11:23:56 AM ******/
CREATE SEQUENCE [dbo].[CardLast_Sequence] 
 AS [int]
 START WITH 0
 INCREMENT BY 1
 MINVALUE 0
 MAXVALUE 999
 CYCLE 
 CACHE 
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ValidateDebitCard]    Script Date: 12/25/2018 11:23:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_ValidateDebitCard]  
	(
		@DebitCardNumber VARCHAR(16),
		@ValidThru VARCHAR(20),
		@CVV VARCHAR(32),
		@DecryptKey VARCHAR(200)
	)
RETURNS INT
AS
BEGIN   
    DECLARE @Status INT
	DECLARE @CVVTemp VARBINARY(300)
	DECLARE @DecryptedPassword NVARCHAR(150)
	IF EXISTS (SELECT 1 FROM DebitCard WHERE DebitCardNumber=@DebitCardNumber)
	BEGIN
		IF EXISTS (SELECT 1 FROM DebitCard WHERE DebitCardNumber=@DebitCardNumber AND IsActive=1)
		BEGIN
			IF EXISTS (SELECT 1 FROM DebitCard WHERE DebitCardNumber=@DebitCardNumber AND IsLocked=0)
			BEGIN
				IF EXISTS (SELECT 1 FROM DebitCard WHERE DebitCardNumber=@DebitCardNumber AND SUBSTRING(@ValidThru, 0, 3)=DATEPART(mm, ValidThru) AND  SUBSTRING(@ValidThru, 4, 2)=SUBSTRING((CONVERT(VARCHAR,DATEPART(YY, ValidThru))), 3,2))
				BEGIN
					SELECT @CVVTemp =  CVV FROM DebitCard WHERE DebitCardNumber=@DebitCardNumber
					SET @DecryptedPassword = CONVERT(nvarchar(4000),DECRYPTBYPASSPHRASE(@DecryptKey,@CVVTemp))
					IF((SELECT CONVERT(VARCHAR(64), HASHBYTES('MD5',CONVERT(VARCHAR(128),@DecryptedPassword)),2)) = @CVV)
						SET @Status = 1
					ELSE
						SET @Status = -5
				END
				ELSE
					SET @Status = -4
			END
			ELSE
				SET @Status = -3
		END
		ELSE
			SET @Status = -2
	END
	ELSE
		SET @Status = -1
	RETURN @Status
END
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ValidateDebitCardPIN]    Script Date: 12/25/2018 11:23:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_ValidateDebitCardPIN]  
	(
		@DebitCardNumber VARCHAR(16),
		@PIN VARCHAR(32),
		@DecryptKey VARCHAR(200)
	)
RETURNS INT  
BEGIN   
    DECLARE @Status INT
	DECLARE @PINTemp VARBINARY(300)
	DECLARE @DecryptedPIN NVARCHAR(150)
	SELECT @PINTemp = Pin FROM DebitCard WHERE DebitCardNumber=@DebitCardNumber
	SET @DecryptedPIN = CONVERT(nvarchar(4000),DECRYPTBYPASSPHRASE(@DecryptKey,@PINTemp))
	IF((SELECT CONVERT(VARCHAR(64), HASHBYTES('MD5',CONVERT(VARCHAR(128),@DecryptedPIN)),2)) = @PIN)
		BEGIN
			SELECT @Status =AccountCustomerMappingId from DebitCard where DebitCardNumber=@DebitCardNumber
		END
	ELSE
		BEGIN
			SET @Status= 0
		END
	RETURN @Status
END;
GO
/****** Object:  Table [dbo].[DebitCard]    Script Date: 12/25/2018 11:23:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DebitCard](
	[DebitCardId] [int] IDENTITY(1,1) NOT NULL,
	[DebitCardNumber] [varchar](16) NOT NULL,
	[AccountCustomerMappingId] [int] NOT NULL,
	[CVV] [varbinary](300) NOT NULL,
	[ValidFrom] [smalldatetime] NOT NULL,
	[ValidThru] [smalldatetime] NOT NULL,
	[PIN] [varbinary](300) NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedTimestamp] [datetime2](7) NOT NULL,
	[CreatedBy] [tinyint] NOT NULL,
	[ModifiedTimestamp] [datetime2](7) NULL,
	[status] [char](1) NULL,
 CONSTRAINT [pk_DebitCard_DebitCardId] PRIMARY KEY CLUSTERED 
(
	[DebitCardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DebitCard] ON 

INSERT [dbo].[DebitCard] ([DebitCardId], [DebitCardNumber], [AccountCustomerMappingId], [CVV], [ValidFrom], [ValidThru], [PIN], [IsLocked], [IsActive], [CreatedTimestamp], [CreatedBy], [ModifiedTimestamp], [status]) VALUES (5, N'5166020070660326', 19, 0x0100000045AD6549DE24F48A54934AC32F6E96E398EE7C9A74B402E8FF192A583DA898898AB21D02ECC2CB4E1FE13E7C4110B26DA504F5484F6F12CB5632A7176436FC860EDE6302E61DCB6E2DC590932B14FA3BA3F988D0A6CA673F07840EF02274D851BE5F97BBC26DA5799F21763C911315F94F2374C57C2E5EE4CC60880BD3B1A36E2B299AF0FCC3762CEA0E50BA54AE6D39098FB4147A432049, CAST(N'2018-11-28T10:02:00' AS SmallDateTime), CAST(N'2028-11-30T00:00:00' AS SmallDateTime), 0x010000003F582C1C7A80075E5BB135B694C999115B5DAFB9263A1EAF83BF83D1187E37B2708132D108A028170BC8A2EC77B081DF09C5CBBE23AE507B88F850B683A86AD6B8E857BDEF692F08078144373869235C71BF17F1654517B7398E866B34D228FF1C9AF7089FA9BEAD75DFDE3E176962F85E405BCA620DBC7A7BA5A05889D59C29681BEF87B3A21D9F5B977E19EFB6351C6D7ED95D062544B0, 0, 1, CAST(N'2018-11-28T10:02:04.8883842' AS DateTime2), 1, NULL, N'S')
INSERT [dbo].[DebitCard] ([DebitCardId], [DebitCardNumber], [AccountCustomerMappingId], [CVV], [ValidFrom], [ValidThru], [PIN], [IsLocked], [IsActive], [CreatedTimestamp], [CreatedBy], [ModifiedTimestamp], [status]) VALUES (6, N'5166020070660334', 20, 0x01000000C7283F6B881C93E74771FC75347201889DADA6265C54D31448A5848D26D006B674AE5774023AAD504B0882B36364B7C4BC2114C7225622E9A63B5A0C720BDD9EDE7019953D8B6F1871B819C38A335E79531F886747C639843679C3F084828463498FB2DC5B4FA45A36D55FFC4BB3C5ED5B46B1A9253D0597ACE285A146C094DDDA8BAC12869650F35F8CFD460B83CDFE0CA3AA612E28C9CE, CAST(N'2018-11-28T16:04:00' AS SmallDateTime), CAST(N'2028-11-30T00:00:00' AS SmallDateTime), 0x010000005F9360C55F0F7D5A34791F9AFFC80B1757CC1A4DE700F56EBB5C7E22BD04D18A9ED6BE4A09974787D73A68AA467270F886A3F863F434157243080BD88AF470A081CE10BED48E9358A79E3209DBB81DA36BF7C83AA2E3F401A40C276BB2651A18C80B34E3363138D1BFE5ED55531966920E0E65BB5A299A85418836B406927B9F79415371763B30472FA171F10952D059893737A23C10FE79, 0, 1, CAST(N'2018-11-28T16:03:57.4246852' AS DateTime2), 1, CAST(N'2018-11-30T10:09:23.5766667' AS DateTime2), N'S')
INSERT [dbo].[DebitCard] ([DebitCardId], [DebitCardNumber], [AccountCustomerMappingId], [CVV], [ValidFrom], [ValidThru], [PIN], [IsLocked], [IsActive], [CreatedTimestamp], [CreatedBy], [ModifiedTimestamp], [status]) VALUES (1006, N'5166020070660839', 21, 0x01000000F0BCC5B47F2933584C07D815FCF11F2BDE5EB6F4BDC693DE7017335019BA2883ECDE2E0B1508A661CB52AE3EF2ED9C6FFB064116E140729A5A349FCD7A9AA078CB247A7A58D6BAD0788C7518344E798FF0A1DD17DD3C94A8FB5ABE83E47F7ACAA4246DF4928901CF97DCE82E441615E2185F7FA997ABAE7693BD3ACB7C6DC12E63B913E6A2756E7705AFA0DA1669F5F9D3F15270B5ED3A16, CAST(N'2018-11-30T11:36:00' AS SmallDateTime), CAST(N'2028-11-30T00:00:00' AS SmallDateTime), 0x01000000595260F0C010F8A283085D3B3A09DFBE49B0DE34F9B891F710AC687E2A08AD70E794DAE4A28F72D9F42515EF5B1F146C60F2E7BC242F9403D0D3727E463D5328B2A54D68B6CC0D984D61B2E781EE7BE790A83DB8B1ED2824FBD5B66E9ECCC8AB547EE0DD0DEBEF538F4C98AF33E8D27EBA2B6EAB2D8FAE35140759B99C05C038CEA5A88182D8EC5A41F0504947E14AC3453E3C8E5F3C4032, 0, 1, CAST(N'2018-11-30T11:35:57.4073523' AS DateTime2), 1, NULL, N'S')
SET IDENTITY_INSERT [dbo].[DebitCard] OFF
/****** Object:  StoredProcedure [dbo].[usp_ChangeDebitCardPin]    Script Date: 12/25/2018 11:23:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ChangeDebitCardPin]  
(
	@DebitCardNumber VARCHAR(16),
	@OldPin VARCHAR(32),
	@NewPin NVARCHAR(128),
	@DecryptKey VARCHAR(200)
)
AS
BEGIN
BEGIN TRY
	DECLARE @retVal INT
	DECLARE @PINTemp VARBINARY(300)
	DECLARE @DecryptedPIN NVARCHAR(150)
	DECLARE @EncryptedPin VARBINARY(300)
	SELECT @PINTemp = Pin FROM DebitCard WHERE DebitCardNumber=@DebitCardNumber
	SET @DecryptedPIN = CONVERT(nvarchar(4000),DECRYPTBYPASSPHRASE(@DecryptKey,@PINTemp))
	IF( (SELECT CONVERT(VARCHAR(64), HASHBYTES('MD5',CONVERT(VARCHAR(128),@DecryptedPIN)),2))=@OldPin)
		BEGIN
			SET @EncryptedPin = (SELECT ENCRYPTBYPASSPHRASE(@DecryptKey, @NewPin))
			UPDATE DebitCard SET PIN = @EncryptedPin, ModifiedTimeStamp = GETDATE() WHERE DebitCardNumber=@DebitCardNumber
			SET @retVal = 1
		END
	ELSE
		BEGIN
			SET @retVal = -1
		END
	
	RETURN @retVal
END TRY
BEGIN CATCH
	SET @retVal = -99
	RETURN @retVal
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_IssueDebitCard]    Script Date: 12/25/2018 11:23:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_IssueDebitCard]
( 
	@AccountCustomerMappingId INT,
	@ifsc VARCHAR(6),
	@debitConstant varchar(4),
	@CvvKey VARCHAR(200),
	@PinKey VARCHAR(200),
	@TellerId TINYINT, 
	@CVV NVARCHAR(128),
	@Pin NVARCHAR(128),
	@DebitCardNumber VARCHAR(16) OUTPUT,
	@ValidFrom SMALLDATETIME OUTPUT, 
	@ValidThru SMALLDATETIME OUTPUT 
)
AS 
BEGIN
	DECLARE @retVal VARCHAR(20)=''
	DECLARE @lastVal VARCHAR(4)
	DECLARE @startVal VARCHAR(2)
	DECLARE @temp VARCHAR(16)=''
	DECLARE @count int=1
	DECLARE @lastNum int=0
	DECLARE @intermidiateVal int=0
	DECLARE @BranchId SMALLINT
	DECLARE	@EncryptedCVV VARBINARY(300)
	DECLARE @EncryptedPin VARBINARY(300)
	DECLARE @EncryptKey VARCHAR(200)
	BEGIN TRY
		--IF EXISTS(SELECT 1 FROM AccountCustomerMapping WHERE AccountNumber = @AccountNumber AND CustomerId = @CustomerId AND IsActive=1)
		BEGIN
			--SET @AccountCustomerMappingId = (SELECT AccountCustomerMappingId FROM AccountCustomerMapping WHERE AccountNumber = @AccountNumber AND CustomerId = @CustomerId AND IsActive=1)
			--SET @BranchId = (SELECT BranchId FROM Account WHERE AccountNumber = @AccountNumber)
		   SET @lastVal= NEXT VALUE FOR CardLast_Sequence
		   IF (@lastVal=0)
		   BEGIN
		   --fetch the first 2 digits of card number using sequence
				  SET @startVal= NEXT VALUE FOR CardFirst_Sequence 

		   END
		   ELSE
		   BEGIN
				  SET @startVal= convert(VARCHAR,(SELECT current_value FROM sys.sequences WHERE name= 'CardFirst_Sequence'))

		   END
		   
		   --assign the first 4th and 6th digits of the card number
		   SET @startVal= (SELECT REPLACE(STR(@startVal, 2), ' ', '0'))
		   --assign the last 2 digits of the card number
		   SET @lastVal=(SELECT REPLACE(STR(@lastVal, 3), ' ', '0'))
		   --assign the first 4 digits of card number
		   --SET @debitConstant = (SELECT Value FROM [Configuration] WHERE [Key] = 'Card Constant')
		   --assign the first 7th to 12th digits of card number
		   --SET @ifsc = (SELECT BranchCode FROM Branch where BranchId=@BranchId)

		   SET @retVal+=@debitConstant+ @startVal+CONVERT(VARCHAR,@ifsc)+@lastVal
		   SET @temp= REVERSE(@retVal)
		   WHILE (@count<=LEN(@temp))
		   BEGIN

				  IF (@count%2!=0)
				  BEGIN

						 SET @intermidiateVal=CONVERT (INT, SUBSTRING(@temp, @count, 1)) *2 

						 IF (@intermidiateVal>9)
						 BEGIN

							   SET @intermidiateVal=@intermidiateVal-9
						 END

				  END
				  ELSE
				  BEGIN
						 SET @intermidiateVal=CONVERT (INT, SUBSTRING(@temp, @count, 1))

				  END
				  SET @lastNum+=@intermidiateVal
				  SET @count+=1
		   END

		   SET @lastNum=@lastNum%10
		   IF( @lastNum <> 0 )
		   BEGIN
				SET @lastNum = 10 - @lastNum
		   END
		   SET @retVal+=CONVERT(VARCHAR, @lastNum)
		   SET @DebitCardNumber = @retVal
		   SET @ValidFrom = GETDATE()
		   SET @ValidThru = CONVERT([smalldatetime],CONVERT([date],eomonth(dateadd(year,(10),getdate()))))
		   --logic for issue or reissue of card

		END
		--SET @EncryptKey = (SELECT Value FROM [Configuration] WHERE [Key] = 'CVV Key')
		SET @EncryptedCVV = (SELECT encryptbypassphrase(@CvvKey, @CVV))
		--SET @EncryptKey = (SELECT Value FROM [Configuration] WHERE [Key] = 'Pin Key')
		SET @EncryptedPin = (SELECT encryptbypassphrase(@PinKey, @Pin))

		INSERT INTO DebitCard(DebitCardNumber, AccountCustomerMappingId, CVV, ValidFrom, ValidThru, PIN, CreatedBy,status,isLocked,isActive,CreatedTimestamp)
					VALUES (@DebitCardNumber, @AccountCustomerMappingId, @EncryptedCVV, @ValidFrom, @ValidThru, @EncryptedPin, @TellerId,'P',0,1,SYSDATETIME())

		RETURN 1
	END TRY
	BEGIN CATCH
		RETURN -99
	END CATCH
END
GO
USE [master]
GO
ALTER DATABASE [Card] SET  READ_WRITE 
GO

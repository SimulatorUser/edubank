USE [Transaction]
GO
/****** Object:  StoredProcedure [dbo].[usp_Transact]    Script Date: 12/24/2018 2:40:42 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[usp_Transact]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transaction]') AND type in (N'U'))
ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT IF EXISTS [chk_Status]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Transaction]') AND type in (N'U'))
ALTER TABLE [dbo].[Transaction] DROP CONSTRAINT IF EXISTS [DF__Transacti__Trans__47DBAE45]
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 12/24/2018 2:40:42 PM ******/
DROP TABLE IF EXISTS [dbo].[Transaction]
GO
USE [master]
GO
/****** Object:  Database [Transaction]    Script Date: 12/24/2018 2:40:42 PM ******/
DROP DATABASE IF EXISTS [Transaction]
GO
/****** Object:  Database [Transaction]    Script Date: 12/24/2018 2:40:42 PM ******/
CREATE DATABASE [Transaction]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Transaction', FILENAME = N'C:\Users\vishal.verma03\Transaction.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Transaction_log', FILENAME = N'C:\Users\vishal.verma03\Transaction_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Transaction] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Transaction].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Transaction] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Transaction] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Transaction] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Transaction] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Transaction] SET ARITHABORT OFF 
GO
ALTER DATABASE [Transaction] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Transaction] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Transaction] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Transaction] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Transaction] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Transaction] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Transaction] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Transaction] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Transaction] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Transaction] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Transaction] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Transaction] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Transaction] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Transaction] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Transaction] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Transaction] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Transaction] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Transaction] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Transaction] SET  MULTI_USER 
GO
ALTER DATABASE [Transaction] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Transaction] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Transaction] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Transaction] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Transaction] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Transaction] SET QUERY_STORE = OFF
GO
USE [Transaction]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [Transaction]
GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 12/24/2018 2:40:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[TransactionId] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountNumber] [varchar](15) NOT NULL,
	[Amount] [money] NOT NULL,
	[TransactionDateTime] [datetime] NOT NULL,
	[Type] [bit] NOT NULL,
	[Mode] [varchar](20) NOT NULL,
	[Remarks] [varchar](30) NULL,
	[Info] [varchar](200) NOT NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[TransactionStatus] [char](1) NOT NULL,
 CONSTRAINT [pk_Transaction_TransactionId] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Transaction] ON 

INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (4, N'28541515614631', 20.0000, CAST(N'2018-11-27T19:16:09.373' AS DateTime), 1, N'DC', N'infy', N'remark', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (5, N'100075546456456', 20.0000, CAST(N'2018-11-27T19:17:17.267' AS DateTime), 1, N'DebitCard', N'Test', N'TestInfo', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (6, N'007066100000125', 20000.0000, CAST(N'2018-11-29T10:39:56.483' AS DateTime), 1, N'Teller', N'First Transaction', N'Teller', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (7, N'007066100000125', 514.6500, CAST(N'2018-11-29T11:29:10.343' AS DateTime), 0, N'DebitCardTransaction', N'Movie', N'Vishal', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (8, N'007066100000125', 250.3600, CAST(N'2018-11-29T11:53:18.220' AS DateTime), 0, N'NetBanking', N'Food', N'NetBankingPayment', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (9, N'007066100000125', 9000.0000, CAST(N'2018-11-29T12:02:19.850' AS DateTime), 0, N'NetBanking', N'Rent', N'NetBankingPayment', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (10, N'7066100000126', 40000.0000, CAST(N'2018-12-11T09:01:49.087' AS DateTime), 1, N'Teller', N'FirstSalary', N'Teller', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (11, N'7066100000126', 2000.0000, CAST(N'2018-12-11T09:02:01.367' AS DateTime), 0, N'Teller', N'FirstSalary', N'Teller', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (12, N'007066100000126', 40000.0000, CAST(N'2018-12-11T09:18:20.163' AS DateTime), 1, N'Teller', N'FirstSalary', N'Teller', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (13, N'007066100000126', 400.0000, CAST(N'2018-12-11T09:18:32.467' AS DateTime), 0, N'Teller', N'Food', N'Teller', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (15, N'007066100000126', 100.0000, CAST(N'2018-12-11T13:07:40.930' AS DateTime), 0, N'NetBanking', N'Food', N'NetBankingPayment', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (16, N'007066100000126', 100.0000, CAST(N'2018-12-11T13:18:38.893' AS DateTime), 0, N'NetBanking', N'Hiking', N'NetBankingPayment', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (17, N'007066100000126', 666.0000, CAST(N'2018-12-11T13:19:08.887' AS DateTime), 0, N'NetBanking', N'Hiking', N'NetBankingPayment', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (20, N'007066100000126', 666.0000, CAST(N'2018-12-11T13:32:45.057' AS DateTime), 0, N'NetBanking', N'food', N'NetBankingPayment', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (22, N'007066100000126', 666.0000, CAST(N'2018-12-11T13:57:26.877' AS DateTime), 0, N'DebitCardTransaction', N'Amigo Wallet Debit', N'Harsh', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (23, N'007066100000126', 666.0000, CAST(N'2018-12-11T14:37:21.580' AS DateTime), 0, N'NetBanking', N'Hiking', N'NetBankingPayment', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (24, N'007066100000126', 666.0000, CAST(N'2018-12-11T14:46:43.983' AS DateTime), 0, N'NetBanking', N'lunch', N'NetBankingPayment', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (25, N'007066100000126', 666.0000, CAST(N'2018-12-11T14:51:26.160' AS DateTime), 0, N'NetBanking', N'rtyryt', N'NetBankingPayment', N'1', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (27, N'007066100000124', 1000.0000, CAST(N'2018-12-20T18:15:20.753' AS DateTime), 1, N'FundsTransfer', N'Fees', N'Transfer By 007066100000126', N'1000000027', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (28, N'007066100000126', 1000.0000, CAST(N'2018-12-20T18:15:20.783' AS DateTime), 0, N'FundsTransfer', N'Fees', N'Transfer To 007066100000124', N'1000000027', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (29, N'007066100000124', 1000.0000, CAST(N'2018-12-20T18:18:55.170' AS DateTime), 1, N'FundsTransfer', N'Fees', N'Transfer By 007066100000126', N'1000000027', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (30, N'007066100000126', 1000.0000, CAST(N'2018-12-20T18:19:25.143' AS DateTime), 0, N'FundsTransfer', N'Fees', N'Transfer To 007066100000124', N'1000000027', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (31, N'007066100000124', 1000.0000, CAST(N'2018-12-20T18:20:27.590' AS DateTime), 1, N'FundsTransfer', N'Fees', N'Transfer By 007066100000126', N'1000000027', N'S')
INSERT [dbo].[Transaction] ([TransactionId], [AccountNumber], [Amount], [TransactionDateTime], [Type], [Mode], [Remarks], [Info], [CreatedBy], [TransactionStatus]) VALUES (32, N'007066100000126', 1000.0000, CAST(N'2018-12-20T18:20:27.590' AS DateTime), 0, N'FundsTransfer', N'Fees', N'Transfer To 007066100000124', N'1000000027', N'S')
SET IDENTITY_INSERT [dbo].[Transaction] OFF
ALTER TABLE [dbo].[Transaction] ADD  DEFAULT ('F') FOR [TransactionStatus]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [chk_Status] CHECK  (([TransactionStatus]='F' OR [TransactionStatus]='S' OR [TransactionStatus]='P'))
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [chk_Status]
GO
/****** Object:  StoredProcedure [dbo].[usp_Transact]    Script Date: 12/24/2018 2:40:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Transact]
(
	@AccountNumber VARCHAR(16),
	@Amount MONEY,
	@Type BIT,
	@Mode VARCHAR(20),
	@Remarks VARCHAR(30),
	@Info VARCHAR(100),
	@CreatedBy VARCHAR(50),
	@TransactionId BIGINT OUT
) 
AS
BEGIN
BEGIN TRY
	SET @TransactionId=0
		IF(@Amount > 0)
		BEGIN
				BEGIN TRAN
					INSERT INTO [Transaction] VALUES(@AccountNumber, @Amount, SYSDATETIME(), @Type, @Mode, @Remarks, @Info, @CreatedBy,'P')
					SELECT @TransactionId=IDENT_CURRENT('Transaction')
				COMMIT TRAN
				RETURN 1
		END
		ELSE
		BEGIN
			RETURN -1
		END
END TRY
BEGIN CATCH
	ROLLBACK
	RETURN -99
END CATCH
END
GO
USE [master]
GO
ALTER DATABASE [Transaction] SET  READ_WRITE 
GO

GO
CREATE DATABASE CreditCard;
GO
USE CreditCard;
GO
CREATE TABLE CardType(
CardTypeId TINYINT IDENTITY PRIMARY KEY,
CardCategory VARCHAR(20) NOT NULL,
IsActive BIT NOT NULL,
CreditLimit DECIMAL CONSTRAINT chk_cardtype_creditlimit CHECK (CreditLimit>0)
);
GO

CREATE TABLE EMIPlan(
PlanId TINYINT IDENTITY PRIMARY KEY,
PlanDuration TINYINT NOT NULL,
PlanInterest DECIMAL CONSTRAINT chk_emiplans_planinterest CHECK(PlanInterest>=0),
IsActive BIT NOT NULL
);
GO

CREATE TABLE CreditCard(
CreditCardId BIGINT IDENTITY PRIMARY KEY,
CardNo CHAR(16) NOT NULL,
CardTypeId TINYINT CONSTRAINT fk_creditcard_cardtypeid REFERENCES CardType(CardTypeId),
CustomerId BIGINT,
CVV VARBINARY(300) NOT NULL,
PIN VARBINARY(300) NOT NULL,
ValidFrom DATE NOT NULL,
ValidThru DATE NOT NULL,
CreatedBy VARCHAR(100) NOT NULL,
CreatedTimeStamp DATETIME NOT NULL,
ModifiedBy VARCHAR(100) NOT NULL,
ModifiedTimeStamp DATETIME NOT NULL,
IsActive BIT NOT NULL,
IsLocked BIT NOT NULL
);
GO

CREATE TABLE UnbilledTransaction(
UnbilledTransactionId BIGINT IDENTITY PRIMARY KEY,
CreditCardId BIGINT CONSTRAINT fk_unbilledtxn_creditcardid REFERENCES CreditCard(CreditCardId),
MerchantAccountNumber VARCHAR(16) NOT NULL,
MerchantName VARCHAR(50) NOT NULL,
Amount DECIMAL CONSTRAINT chk_unbilledtxn_amount CHECK (Amount >0),
PlanId TINYINT CONSTRAINT fk_unbilledtxn_planid REFERENCES EMIPlan(PlanId) NOT NULL,
TimeStamp DATETIME NOT NULL
);
GO

CREATE TABLE BilledTransaction(
BilledTransactionId BIGINT IDENTITY PRIMARY KEY,
UnbilledTransactionId BIGINT CONSTRAINT fk_billedtxn_unbilledtxnid REFERENCES UnbilledTransaction(UnbilledTransactionId),
Amount DECIMAL CONSTRAINT chk_billedtxn_amount CHECK (Amount>0),
Interest DECIMAL CONSTRAINT chk_billedtxn_interest CHECK (Interest>0),
TimeStamp DATETIME NOT NULL
);
GO

CREATE TABLE CustomerBill(
BillId BIGINT IDENTITY PRIMARY KEY,
BillDate DATE NOT NULL,
TotalAmountDue DECIMAL CONSTRAINT chk_customerbill_interest CHECK (TotalAmountDue>0),
MinAmountDue DECIMAL CONSTRAINT chk_customerbill_minamountdue CHECK (MinAmountDue>0),
PaymentDueDate DATE NOT NULL,
PaymentDate DATE,
AmountPaid DECIMAL,
CreditCardId BIGINT CONSTRAINT fk_customerbill_creditcardid REFERENCES CreditCard(CreditCardId) NOT NULL,
LateFeeAmount DECIMAL NOT NULL,
CONSTRAINT chk_customerbill_amountpaid CHECK (AmountPaid > (MinAmountDue + LateFeeAmount))
);
GO
CREATE TABLE CardRequest(
RequestId BIGINT IDENTITY(1000,1) PRIMARY KEY,
CustomerId BIGINT NOT NULL,
CardTypeId TINYINT CONSTRAINT fk_cardrequest_cardtypeid REFERENCES CardType(CardTypeId),
RequestDate DATETIME NOT NULL,
Status Varchar(20) NOT NULL CONSTRAINT chk_cardrequest_Status CHECK (Status in ('Pending','Success','Rejected'))
)
GO

CREATE PROCEDURE [dbo].[usp_IssueCreditCard]
( 
	@CustomerId INT,
	@CardTypeId VARCHAR(6),
	@CardNum varchar(4),
	@CvvKey VARCHAR(200),
	@PinKey VARCHAR(200),
	@TellerId TINYINT, 
	@CVV NVARCHAR(128),
	@Pin NVARCHAR(128),
	@ValidFrom SMALLDATETIME , 
	@ValidThru SMALLDATETIME ,
	@CardId BIGINT OUTPUT
)
AS 
BEGIN
	DECLARE	@EncryptedCVV VARBINARY(300)
	DECLARE @EncryptedPin VARBINARY(300)
	DECLARE @EncryptKey VARCHAR(200)
	SET @CardId=0
	BEGIN TRY
		BEGIN
		--SET @EncryptKey = (SELECT Value FROM [Configuration] WHERE [Key] = 'CVV Key')
		SET @EncryptedCVV = (SELECT encryptbypassphrase(@CvvKey, @CVV))
		--SET @EncryptKey = (SELECT Value FROM [Configuration] WHERE [Key] = 'Pin Key')
		SET @EncryptedPin = (SELECT encryptbypassphrase(@PinKey, @Pin))

		INSERT INTO CreditCard VALUES (@CardNum, @CardTypeId, @CustomerId, @EncryptedCVV, @EncryptedPin, @ValidFrom, @ValidThru,@TellerId,SYSDATETIME(),Null,null,1,0)
		SELECT @CardId=CreditCardId FROM CreditCard WHERE CardNo=@CardNum
		RETURN 1
		END
	END TRY
	BEGIN CATCH
		RETURN -99
	END CATCH
END
GO




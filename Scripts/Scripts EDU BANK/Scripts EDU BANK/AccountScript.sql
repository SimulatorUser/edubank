USE [Account]
GO
/****** Object:  StoredProcedure [dbo].[usp_OpenAccount]    Script Date: 12/21/2018 10:00:48 AM ******/
DROP PROCEDURE [dbo].[usp_OpenAccount]
GO
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [fk_Account_BranchId]
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 12/21/2018 10:00:48 AM ******/
DROP TABLE [dbo].[Branch]
GO
/****** Object:  Table [dbo].[AccountCustomerMapping]    Script Date: 12/21/2018 10:00:48 AM ******/
DROP TABLE [dbo].[AccountCustomerMapping]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 12/21/2018 10:00:48 AM ******/
DROP TABLE [dbo].[Account]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 12/21/2018 10:00:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[AccountId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNumber] [varchar](15) NOT NULL,
	[BranchId] [tinyint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedTimestamp] [datetime2](7) NOT NULL,
	[CreatedBy] [tinyint] NOT NULL,
	[ModifiedTimestamp] [datetime2](7) NULL,
	[ModifiedBy] [tinyint] NULL,
	[status] [char](1) NULL,
 CONSTRAINT [pk_Account_AccountId] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountCustomerMapping]    Script Date: 12/21/2018 10:00:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountCustomerMapping](
	[AccountCustomerMappingId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNumber] [varchar](15) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[status] [char](1) NULL,
 CONSTRAINT [pk_AccountCustomerMapping_AccountCustomerMappingId] PRIMARY KEY CLUSTERED 
(
	[AccountCustomerMappingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 12/21/2018 10:00:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branch](
	[BranchId] [tinyint] IDENTITY(1,1) NOT NULL,
	[IFSC] [varchar](11) NOT NULL,
	[BranchName] [varchar](50) NOT NULL,
	[BranchCode] [varchar](6) NOT NULL,
 CONSTRAINT [pk_Branch_BranchId] PRIMARY KEY CLUSTERED 
(
	[BranchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 
GO
INSERT [dbo].[Account] ([AccountId], [AccountNumber], [BranchId], [IsActive], [CreatedTimestamp], [CreatedBy], [ModifiedTimestamp], [ModifiedBy], [status]) VALUES (24, N'007066100000124', 1, 1, CAST(N'2018-11-28T10:01:51.4653843' AS DateTime2), 1, NULL, NULL, N'S')
GO
INSERT [dbo].[Account] ([AccountId], [AccountNumber], [BranchId], [IsActive], [CreatedTimestamp], [CreatedBy], [ModifiedTimestamp], [ModifiedBy], [status]) VALUES (25, N'007066100000125', 1, 1, CAST(N'2018-11-28T16:02:32.3608642' AS DateTime2), 1, NULL, NULL, N'S')
GO
INSERT [dbo].[Account] ([AccountId], [AccountNumber], [BranchId], [IsActive], [CreatedTimestamp], [CreatedBy], [ModifiedTimestamp], [ModifiedBy], [status]) VALUES (26, N'007066100000126', 1, 1, CAST(N'2018-11-30T11:34:40.9371592' AS DateTime2), 1, NULL, NULL, N'S')
GO
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[AccountCustomerMapping] ON 
GO
INSERT [dbo].[AccountCustomerMapping] ([AccountCustomerMappingId], [AccountNumber], [CustomerId], [IsActive], [status]) VALUES (19, N'007066100000124', 1000000025, 1, N'S')
GO
INSERT [dbo].[AccountCustomerMapping] ([AccountCustomerMappingId], [AccountNumber], [CustomerId], [IsActive], [status]) VALUES (20, N'007066100000125', 1000000025, 1, N'S')
GO
INSERT [dbo].[AccountCustomerMapping] ([AccountCustomerMappingId], [AccountNumber], [CustomerId], [IsActive], [status]) VALUES (21, N'007066100000126', 1000000027, 1, N'S')
GO
SET IDENTITY_INSERT [dbo].[AccountCustomerMapping] OFF
GO
SET IDENTITY_INSERT [dbo].[Branch] ON 
GO
INSERT [dbo].[Branch] ([BranchId], [IFSC], [BranchName], [BranchCode]) VALUES (1, N'EDUB0007066', N'Beberibe', N'007066')
GO
INSERT [dbo].[Branch] ([BranchId], [IFSC], [BranchName], [BranchCode]) VALUES (2, N'EDUB0007067', N'Handaqi', N'007067')
GO
INSERT [dbo].[Branch] ([BranchId], [IFSC], [BranchName], [BranchCode]) VALUES (3, N'EDUB0007068', N'Siverskiy', N'007068')
GO
INSERT [dbo].[Branch] ([BranchId], [IFSC], [BranchName], [BranchCode]) VALUES (4, N'EDUB0007069', N'Baiyanghe', N'007069')
GO
INSERT [dbo].[Branch] ([BranchId], [IFSC], [BranchName], [BranchCode]) VALUES (5, N'EDUB0007070', N'Shanling', N'007070')
GO
SET IDENTITY_INSERT [dbo].[Branch] OFF
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [fk_Account_BranchId] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branch] ([BranchId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [fk_Account_BranchId]
GO
/****** Object:  StoredProcedure [dbo].[usp_OpenAccount]    Script Date: 12/21/2018 10:00:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_OpenAccount]
	(
		@CustomerId BIGINT,
		@BranchId TINYINT,
		@TellerId TINYINT,
		@AccountNumber VARCHAR(15) OUTPUT		
	)
AS
BEGIN
	DECLARE @retVal int
	BEGIN TRY
		SET @retVal =0
			BEGIN TRAN
				DECLARE @SequenceValue VARCHAR(9), @BranchCode VARCHAR(6) 
				SET @SequenceValue = NEXT VALUE FOR AccountNumber_Sequence
				SET @BranchCode = (SELECT BranchCode FROM Branch WHERE BranchId = @BranchId)
				SET @AccountNumber = @BranchCode + @SequenceValue  
				INSERT INTO Account(AccountNumber, BranchId, CreatedBy,status,IsActive,CreatedTimestamp) 
				VALUES(@AccountNumber, @BranchId, @TellerId,'P',1,SYSDATETIME()) 
				INSERT INTO AccountCustomerMapping(AccountNumber, CustomerId,status,IsActive) 
				VALUES(@AccountNumber, @CustomerId,'P',1)
			COMMIT TRAN
			SET @retVal =1
	END TRY
	BEGIN CATCH
		ROLLBACK
		SET @retVal =99
	END CATCH
	RETURN @retVal
END
GO

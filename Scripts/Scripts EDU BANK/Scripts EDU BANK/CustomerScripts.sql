USE [Customer]
GO
/****** Object:  StoredProcedure [dbo].[usp_insertUserCredentials]    Script Date: 12/21/2018 10:03:35 AM ******/
DROP PROCEDURE [dbo].[usp_insertUserCredentials]
GO
/****** Object:  StoredProcedure [dbo].[usp_ChangePassword]    Script Date: 12/21/2018 10:03:35 AM ******/
DROP PROCEDURE [dbo].[usp_ChangePassword]
GO
ALTER TABLE [dbo].[Payee] DROP CONSTRAINT [fk_CustomerId]
GO
ALTER TABLE [dbo].[CustomerLogin] DROP CONSTRAINT [fk_CustomerLogin_CustomerId]
GO
/****** Object:  Index [UQ__Payee__BE2ACD6F7CCC5BE6]    Script Date: 12/21/2018 10:03:35 AM ******/
ALTER TABLE [dbo].[Payee] DROP CONSTRAINT [UQ__Payee__BE2ACD6F7CCC5BE6]
GO
/****** Object:  Table [dbo].[Payee]    Script Date: 12/21/2018 10:03:35 AM ******/
DROP TABLE [dbo].[Payee]
GO
/****** Object:  Table [dbo].[CustomerLogin]    Script Date: 12/21/2018 10:03:35 AM ******/
DROP TABLE [dbo].[CustomerLogin]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 12/21/2018 10:03:35 AM ******/
DROP TABLE [dbo].[Customer]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ValidateCustomerLogin]    Script Date: 12/21/2018 10:03:35 AM ******/
DROP FUNCTION [dbo].[ufn_ValidateCustomerLogin]
GO
/****** Object:  UserDefinedFunction [dbo].[ufn_ValidateCustomerLogin]    Script Date: 12/21/2018 10:03:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_ValidateCustomerLogin]
(
	@UserName VARCHAR(20),@Password VARCHAR(128),@DecryptKey VARCHAR(200)
)
RETURNS TINYINT
AS
BEGIN
	DECLARE @Hash NVARCHAR(128)
	DECLARE @PasswordData VARBINARY(300)
	DECLARE @PasswordHash NVARCHAR(128)
	DECLARE @PasswordDecrypted NVARCHAR(150)
	DECLARE @RetVal TINYINT
	IF EXISTS(SELECT CustomerLoginId FROM CustomerLogin where LoginName = @UserName)
	BEGIN
		IF EXISTS(SELECT CustomerLoginId FROM CustomerLogin where LoginName = @UserName AND IsLocked = 0)
		BEGIN
			SELECT @PasswordData = Password FROM CustomerLogin where LoginName = @UserName
			SELECT @PasswordDecrypted = CONVERT(nvarchar(4000),DECRYPTBYPASSPHRASE(@DecryptKey,@PasswordData)) 
			SELECT @Hash = CONVERT(VARCHAR(32), HashBytes('MD5', CONVERT(VARCHAR(128),@PasswordDecrypted)), 2)	
			SET @RetVal = 0
		END
		ELSE
		BEGIN
			SET @RetVal = 3
		END
	END
	ELSE
		SET @RetVal=2
	IF @Hash = @Password
		BEGIN
			SET @RetVal = 1
		END		
	RETURN @RetVal
END
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 12/21/2018 10:03:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerId] [bigint] IDENTITY(1000000001,1) NOT NULL,
	[EmailId] [varchar](255) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[DateOfBirth] [date] NOT NULL,
	[CreatedTimestamp] [datetime2](7) NOT NULL,
	[CreatedBy] [tinyint] NOT NULL,
	[ModifiedTimestamp] [datetime2](7) NULL,
	[ModifiedBy] [tinyint] NULL,
	[UUID] [varchar](12) NULL,
	[IsAddrVerified] [bit] NOT NULL,
	[status] [char](1) NULL,
 CONSTRAINT [pk_Customer_CustomerId] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerLogin]    Script Date: 12/21/2018 10:03:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerLogin](
	[CustomerLoginId] [int] IDENTITY(1,1) NOT NULL,
	[LoginName] [varchar](20) NOT NULL,
	[Password] [varbinary](300) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[IsLocked] [bit] NOT NULL,
	[ModifiedTimestamp] [datetime2](7) NULL,
	[status] [char](1) NULL,
 CONSTRAINT [pk_CustomerLogin_CustomerLoginId] PRIMARY KEY CLUSTERED 
(
	[CustomerLoginId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payee]    Script Date: 12/21/2018 10:03:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payee](
	[PayeeId] [bigint] IDENTITY(1,1) NOT NULL,
	[NickName] [varchar](50) NOT NULL,
	[AccountNumber] [varchar](15) NULL,
	[CustomerId] [bigint] NULL,
	[PreferredAmount] [money] NULL,
 CONSTRAINT [pk_PayeeId] PRIMARY KEY CLUSTERED 
(
	[PayeeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([CustomerId], [EmailId], [Name], [DateOfBirth], [CreatedTimestamp], [CreatedBy], [ModifiedTimestamp], [ModifiedBy], [UUID], [IsAddrVerified], [status]) VALUES (1000000025, N'vishal.vrm', N'Vishal', CAST(N'1997-10-10' AS Date), CAST(N'2018-11-28T10:01:46.3990954' AS DateTime2), 1, NULL, NULL, NULL, 1, N'S')
INSERT [dbo].[Customer] ([CustomerId], [EmailId], [Name], [DateOfBirth], [CreatedTimestamp], [CreatedBy], [ModifiedTimestamp], [ModifiedBy], [UUID], [IsAddrVerified], [status]) VALUES (1000000026, N'vishal.vrm', N'Vishal', CAST(N'1997-10-10' AS Date), CAST(N'2018-11-28T16:00:50.1207553' AS DateTime2), 1, NULL, NULL, NULL, 1, N'P')
INSERT [dbo].[Customer] ([CustomerId], [EmailId], [Name], [DateOfBirth], [CreatedTimestamp], [CreatedBy], [ModifiedTimestamp], [ModifiedBy], [UUID], [IsAddrVerified], [status]) VALUES (1000000027, N'Harsh.soni', N'Harsh', CAST(N'1995-10-10' AS Date), CAST(N'2018-11-30T11:32:17.9489750' AS DateTime2), 1, NULL, NULL, NULL, 1, N'S')
SET IDENTITY_INSERT [dbo].[Customer] OFF
SET IDENTITY_INSERT [dbo].[CustomerLogin] ON 

INSERT [dbo].[CustomerLogin] ([CustomerLoginId], [LoginName], [Password], [CustomerId], [IsLocked], [ModifiedTimestamp], [status]) VALUES (26, N'Vishal000', 0x0100000027249737EAE9E0AF3FEF4E5ACE4402908EF50FBC9F623682B069C58534867F335306F051E43397D78694A48BD242AC5E8D41F3CB7A8FC2755421EBDCE7DA9F20D3E27E0EEE74DBB6CF9F2DCD397B9E827DCE89E9BB6D2DA2DEE05D4B377C031A33C7F3F64965DF76BDD8927ED4FBDC7C0900AC2894E2AC3538828CF41DD8156DDF20DBCBFED36EC0EAADC68684F0B8A8211120EDCCE06C98, 1000000025, 0, NULL, N'S')
INSERT [dbo].[CustomerLogin] ([CustomerLoginId], [LoginName], [Password], [CustomerId], [IsLocked], [ModifiedTimestamp], [status]) VALUES (27, N'Vishal001', 0x01000000807668E8EC4CDB57B7488A18923754EEE1415FA983132A3F0A5C503048731020A7B8DE186402524EB1208E42F0587FBBF3BB63ADE64F5FB34E30FEBCB7A88725EBF5A5778AEEDA0DA902D2BCCE981D7693E376514E52A904, 1000000025, 0, NULL, N'P')
INSERT [dbo].[CustomerLogin] ([CustomerLoginId], [LoginName], [Password], [CustomerId], [IsLocked], [ModifiedTimestamp], [status]) VALUES (28, N'Harsh', 0x01000000FB955421B9A90ABDFFCA2B3B2B8BA9620414E7D513A917BC12E335BADB325400A5C0A861B645810CC14D160AC2C2B533277B5E23632883AA07A2946D920E4FA6B9572C82A46A823C78A9FF2553E18AD956C5DB856A37B873874A0EAE197EC25F6DD7EF2886694241A6CF8D488A8B8AAC94AA91274FDF96CCB2AE7CF8E6ECB3EA4CFB24D5AE0E0C516D55AC0FCB174E1549A2195AADFED310, 1000000027, 0, NULL, N'S')
SET IDENTITY_INSERT [dbo].[CustomerLogin] OFF
SET IDENTITY_INSERT [dbo].[Payee] ON 

INSERT [dbo].[Payee] ([PayeeId], [NickName], [AccountNumber], [CustomerId], [PreferredAmount]) VALUES (4, N'Vish', N'007066100000124', 1000000027, NULL)
SET IDENTITY_INSERT [dbo].[Payee] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Payee__BE2ACD6F7CCC5BE6]    Script Date: 12/21/2018 10:03:35 AM ******/
ALTER TABLE [dbo].[Payee] ADD UNIQUE NONCLUSTERED 
(
	[AccountNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomerLogin]  WITH CHECK ADD  CONSTRAINT [fk_CustomerLogin_CustomerId] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[CustomerLogin] CHECK CONSTRAINT [fk_CustomerLogin_CustomerId]
GO
ALTER TABLE [dbo].[Payee]  WITH CHECK ADD  CONSTRAINT [fk_CustomerId] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([CustomerId])
GO
ALTER TABLE [dbo].[Payee] CHECK CONSTRAINT [fk_CustomerId]
GO
/****** Object:  StoredProcedure [dbo].[usp_ChangePassword]    Script Date: 12/21/2018 10:03:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE PROCEDURE [dbo].[usp_ChangePassword]
(@LoginName VARCHAR(20),
@Password NVARCHAR(128),
@EncryptKey VARCHAR(200)
)
AS
BEGIN
	BEGIN TRY
	IF EXISTS(SELECT 1 FROM CustomerLogin WHERE LoginName=@LoginName)
	BEGIN
		IF @Password IS NOT NULL
		BEGIN
			DECLARE @EncryptedPassword VARBINARY(300)
			SET @EncryptedPassword = (SELECT encryptbypassphrase(@EncryptKey, @Password))
			UPDATE [dbo].[CustomerLogin] SET [Password] = @EncryptedPassword WHERE [LoginName]=@LoginName
			RETURN 1
		END
		ELSE
		BEGIN
			RETURN -2
		END
	END
	ELSE
	BEGIN
		RETURN -1
	END
	END TRY

	BEGIN CATCH
		RETURN -99
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_insertUserCredentials]    Script Date: 12/21/2018 10:03:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_insertUserCredentials](
	@CustomerId BIGINT,
	@Password NVARCHAR(128),
	@LoginName VARCHAR(100),
	@EncryptKey Varchar(50)
)
AS
BEGIN
	Declare @HashedPassowrd VARBINARY(300),@retVal int
	SET @retVal=0
	BEGIN TRY
	select @HashedPassowrd = encryptbypassphrase(@EncryptKey,@Password)
	insert into CustomerLogin(LoginName,Password,CustomerId,status,IsLocked) VALUES(@LoginName,@HashedPassowrd,@CustomerId,'P',0)
	SET @retVal=1
	END TRY
	BEGIN CATCH
	SET @retVal=-1
	END CATCH
	RETURN @retVal
END
GO

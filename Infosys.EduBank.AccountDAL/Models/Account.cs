﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.AccountDAL.Models
{
    public partial class Account
    {
        public int AccountId { get; set; }
        public string AccountNumber { get; set; }
        public byte BranchId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedTimestamp { get; set; }
        public byte CreatedBy { get; set; }
        public DateTime? ModifiedTimestamp { get; set; }
        public byte? ModifiedBy { get; set; }
        
        public char Status { get; set; }

        public Branch Branch { get; set; }
    }
}

﻿using System;
using System.Data.SqlClient;
using Infosys.EduBank.AccountDAL.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Diagnostics.CodeAnalysis;

namespace Infosys.EduBank.AccountDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class AccountContext : DbContext, IAccountContext
    {
        private string ConnectionString { get; }

        public AccountContext()
        {

        }
        public AccountContext(string connectionString){
            ConnectionString=connectionString;
        }
        public AccountContext(DbContextOptions<AccountContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<AccountCustomerMapping> AccountCustomerMapping { get; set; }
        public virtual DbSet<Branch> Branch { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(ConnectionString);//"data source=(localdb)\\ProjectsV13;initial catalog=Account;User Id=EduBankUser;Password=EduUser@123"
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.Property(e => e.AccountNumber)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.Account)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Account_BranchId");
            });

            modelBuilder.Entity<AccountCustomerMapping>(entity =>
            {
                entity.Property(e => e.AccountNumber)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Branch>(entity =>
            {
                entity.Property(e => e.BranchId).ValueGeneratedOnAdd();

                entity.Property(e => e.BranchCode)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.BranchName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ifsc)
                    .IsRequired()
                    .HasColumnName("IFSC")
                    .HasMaxLength(11)
                    .IsUnicode(false);
            });
        }

        public virtual int ExecuteSqlCommandMethod(string query,params SqlParameter[] parameters)
        {
            return this.Database.ExecuteSqlCommand(query,parameters);
        }

    }
}

﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.AccountDAL.Models
{
    public partial class AccountCustomerMapping
    {
        public int AccountCustomerMappingId { get; set; }
        public string AccountNumber { get; set; }
        public long CustomerId { get; set; }
        public bool IsActive { get; set; }
        
        public char Status { get; set; }
    }
}

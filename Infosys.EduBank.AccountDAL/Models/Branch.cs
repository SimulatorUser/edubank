﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.AccountDAL.Models
{
    public partial class Branch
    {
        public Branch()
        {
            Account = new HashSet<Account>();
        }

        public byte BranchId { get; set; }
        public string Ifsc { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }

        public ICollection<Account> Account { get; set; }
    }
}

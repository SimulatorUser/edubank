﻿using System.Collections.Generic;
using Infosys.EduBank.AccountDAL.Models;
using Infosys.EduBank.Common.Models;

namespace Infosys.EduBank.AccountDAL.Interface
{
    public interface IAccountRepository
    {
        AppSettings AppSettings{set;get;}
        IAccountContext Context { set; get; }
        bool CommitAccountInsertion(int accountCustomerMappingId);
        string CreateAccountNumber(long customerId, byte branchId, byte tellerId);
        List<Common.Models.Account> FetchAccountsByStatus(char status);
        int GetAccountMappingIdByAccountNumber(string accountNum);
        int GetAccountMappingIdByCustomerId(int customerId);
        string GetAccountNumberByAccountMappingId(int accountMappingId);
        List<string> GetAccountNumbersByCustomerId(long customerId);
        List<Models.Branch> GetAllBranches();
        string GetBranchCodeByBranchId(string branchId);
        string GetBranchCodeByBranchName(string branchName);
        byte GetBranchIdByBranchName(string branchName);
        long GetCustomerIdByAccountMappingId(int accountCustomerMappingId);
        string GetIfscByBranchName(string branchName);
        bool isAccountAndBranchValid(string accountNumber, string ifsc);
        bool IsAccountNumberActive(string accountNumber);
        bool isAccountPresent(string accountNumber);
        bool UpdateAccountStatus(int accountId, char newStatus);
        bool ToggleAccountStatus(string accountNumber,bool newStatus);
        Common.Models.Account GetAccountDetailsByAccountNumber(string accountNumber);
        List<Common.Models.Account> FetchAllAccountDetails();
        int GetAccountCustomerMappingIDByAccountNumber(string accountNumber, long customerId);
        string GetBranchCodeByAccountMappingId(int accountMappindId);
    }
}
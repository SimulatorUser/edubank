﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Infosys.EduBank.AccountDAL.Models;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Infosys.EduBank.AccountDAL.Interface
{
    public interface IAccountContext 
    {
        DbSet<Account> Account { get; set; }
        DbSet<AccountCustomerMapping> AccountCustomerMapping { get; set; }
        DbSet<Branch> Branch { get; set; }

        
        int ExecuteSqlCommandMethod(string query,params SqlParameter[] parameters);
        int SaveChanges();
    }

}
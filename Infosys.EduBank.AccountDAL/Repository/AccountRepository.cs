using Infosys.EduBank.AccountDAL.Models;
using System.Collections.Generic;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using Infosys.EduBank.Common;
using Infosys.EduBank.AccountDAL.Interface;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.AccountDAL.Repository
{
    public class AccountRepository : IAccountRepository
    {
        public Common.Models.AppSettings AppSettings{get; set;}
       private IAccountContext context;
        public IAccountContext Context{
            get{ return context;}
            set{context=value;}
        }
        public AccountRepository(Common.Models.AppSettings settings){
            context=new AccountContext(settings.AccountConnection);
            AppSettings=settings;
        }
        public List<Branch> GetAllBranches(){
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            List<Branch> BranchList = new List<Branch>();
            try{
                    BranchList=Context.Branch.ToList();
            }
            catch(Exception ex)
            {
                BranchList=null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return BranchList;
        }
        public string CreateAccountNumber(long customerId,byte branchId,byte tellerId){
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            string accountNum="";
            try{
                    SqlParameter prmCustomerId = new SqlParameter("@CustomerId", customerId);
          SqlParameter prmBranchId = new SqlParameter("@BranchId",branchId );
          SqlParameter prmTellerId = new SqlParameter("@TellerId", tellerId);
          SqlParameter prmOutAccountNumber = new SqlParameter();
          prmOutAccountNumber.Direction=System.Data.ParameterDirection.Output;
          prmOutAccountNumber.ParameterName="@AccountNumber";
            prmOutAccountNumber.Size=15;
          Context.ExecuteSqlCommandMethod("EXEC usp_OpenAccount @CustomerId,@BranchId,@TellerId,@AccountNumber OUT",prmCustomerId,prmBranchId,prmTellerId,prmOutAccountNumber);
          accountNum=prmOutAccountNumber.Value.ToString();
            }
            catch(Exception ex){
                   accountNum=string.Empty; 
                   EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return accountNum;
        }
        public byte GetBranchIdByBranchName(string branchName){
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            byte branchId=0;
            try{
                branchId=Context.Branch.Where(x=>x.BranchName==branchName).Select(x=>x.BranchId).FirstOrDefault();
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                branchId=0;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return branchId;
        }

        public int GetAccountMappingIdByAccountNumber(string accountNum){
            var methodName=Common.Utilities.GetMethod();
            int mappingId=0;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
            mappingId=Context.AccountCustomerMapping.Where(x=>x.AccountNumber==accountNum).Select(x=>x.AccountCustomerMappingId).FirstOrDefault();
            } 
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                mappingId=0;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return mappingId;
        }
        public string GetIfscByBranchName(string branchName){
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            string ifsc = string.Empty;
            try{
                  ifsc= Context.Branch.Where(x=>x.BranchName==branchName).Select(x=>x.Ifsc).FirstOrDefault(); 
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                 ifsc = string.Empty;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return ifsc;
        } 
        public bool CommitAccountInsertion(int accountCustomerMappingId){
            var methodName=Common.Utilities.GetMethod();
            bool status = false;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                AccountCustomerMapping mapping= Context.AccountCustomerMapping.Find(accountCustomerMappingId);
                if (mapping.Status != null){
                    mapping.Status = 'S';
                    Account acc = Context.Account.Where(x=> x.AccountNumber==mapping.AccountNumber).FirstOrDefault();
                    if (acc != null){
                        acc.Status = 'S';
                        Context.SaveChanges();
                        status=true;
                    }
                }
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status = false;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        public bool isAccountPresent(string accountNumber)
        {
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            bool status=false;
            try
            {
                status=Context.Account.Any(x=>x.AccountNumber==accountNumber);
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;

            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        public bool isAccountAndBranchValid(string accountNumber,string ifsc){
            var methodName=Common.Utilities.GetMethod();
            bool status = false;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                Branch branch = Context.Branch.Where(x=> x.Ifsc==ifsc).FirstOrDefault();
                if (branch!=null){
                    Account acc = Context.Account.Where(x=> (x.AccountNumber==accountNumber) && (x.Status=='S')).FirstOrDefault();
                    if (acc != null){
                        status=true;
                    }
                }
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        public string GetBranchCodeByBranchName(string branchName){
            var methodName=Common.Utilities.GetMethod();
            string branchCode=null;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
               branchCode= Context.Branch.Where(x=>x.BranchName==branchName).Select(x=>x.BranchCode).FirstOrDefault();
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                branchCode=string.Empty;   
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return branchCode;
        }
        public string GetBranchCodeByBranchId(string branchId){
            var methodName=Common.Utilities.GetMethod();
            string branchCode=null;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
               branchCode= Context.Branch.Where(x=>x.BranchId.ToString()==branchId).Select(x=>x.BranchCode).FirstOrDefault();
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                branchCode=string.Empty;   
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return branchCode;
        }
        public long GetCustomerIdByAccountMappingId(int accountCustomerMappingId){
            var methodName=Common.Utilities.GetMethod();
            long customerId=0;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                AccountCustomerMapping accMap = Context.AccountCustomerMapping.Where(x=> x.AccountCustomerMappingId==accountCustomerMappingId).FirstOrDefault();
                if (accMap!=null){
                    customerId = accMap.CustomerId;
                }
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                customerId=0;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }          
            return customerId;
        }
        public string GetAccountNumberByAccountMappingId(int accountMappingId){
            var methodName=Common.Utilities.GetMethod();
            string accountnumber=null;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                accountnumber=Context.AccountCustomerMapping.Where(x=>x.AccountCustomerMappingId==accountMappingId).Select(x=>x.AccountNumber).FirstOrDefault();
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                accountnumber=string.Empty;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return accountnumber;
        }
        public bool IsAccountNumberActive(string accountNumber){
            var methodName=Common.Utilities.GetMethod();
            bool status=false;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                status=Context.Account.Where(x=>x.AccountNumber==accountNumber && x.Status=='S').Select(x=>x.IsActive).FirstOrDefault();

            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        public int GetAccountMappingIdByCustomerId(int customerId){
            var methodName=Common.Utilities.GetMethod();
            int accountMappingId = -99;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                accountMappingId = Context.AccountCustomerMapping.Where(x=> x.CustomerId == customerId).Select(x=> x.AccountCustomerMappingId).FirstOrDefault();
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                accountMappingId = -99;

            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return accountMappingId;
        }
        public List<string> GetAccountNumbersByCustomerId(long customerId){
            var methodName=Common.Utilities.GetMethod();
            List<string> accountNumbers = new List<string>();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                accountNumbers = Context.AccountCustomerMapping.Where(x=> x.CustomerId==customerId).Select(x=> x.AccountNumber).ToList();
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                accountNumbers = null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return accountNumbers;
        }
        public string GetBranchCodeByAccountMappingId(int accountMappindId)
        {
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            string branchCode = string.Empty;
            try
            {
                string accountNumber = GetAccountNumberByAccountMappingId(accountMappindId);
                string branchId = Context.Account.Where(x=>x.AccountNumber == accountNumber).Select(x=>x.BranchId.ToString()).FirstOrDefault();
                branchCode = GetBranchCodeByBranchId(branchId);
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                branchCode= string.Empty;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return branchCode;
        }
        #region  Exercises
        //fetches list of all accounts with given status
        public List<Common.Models.Account> FetchAccountsByStatus(char status)
        {
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            List<Common.Models.Account> accounts = new List<Common.Models.Account>();
            List<Account> accountList = new List<Account>();
            Common.Models.Account account;
            try
            {
                accountList = (from a in Context.Account
                               where a.Status == status
                               select a).ToList();
                if(accountList.Any())
                {
                    foreach (var a in accountList)
                    {
                        account = new Common.Models.Account();
                        account.AccountId = a.AccountId;
                        account.AccountNumber = a.AccountNumber;
                        account.BranchId = a.BranchId;
                        account.CreatedBy = a.CreatedBy;
                        account.CreatedTimestamp = a.CreatedTimestamp;
                        account.IsActive = a.IsActive;
                        account.ModifiedBy = a.ModifiedBy;
                        account.ModifiedTimestamp = a.ModifiedTimestamp;
                        account.Status = a.Status;
                        accounts.Add(account);
                    }                    
                }
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                accounts = null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return accounts;
        }
        //Updates the Status property of the given account object
        public bool UpdateAccountStatus(int accountId,char newStatus)
        {
            var methodName=Common.Utilities.GetMethod();
            bool status = false;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            Account accountObj = new Account();
            try
            {
                accountObj = Context.Account.Find(accountId);
                if (accountObj != null)
                {
                    accountObj.Status = newStatus;
                    Context.SaveChanges();
                    status = true;
                }
                
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status = false;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        //Fetches the list of all accounts
        public List<Common.Models.Account> FetchAllAccountDetails()
        {
            var methodName=Common.Utilities.GetMethod();
            List<Common.Models.Account> accounts = new List<Common.Models.Account>();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            Common.Models.Account account;
            try
            {
                var accountList = Context.Account.ToList();
                if(accountList.Any())
                {
                    foreach (var acc in accountList)
                    {
                        account = new Common.Models.Account();
                        account.AccountId = acc.AccountId;
                        account.AccountNumber = acc.AccountNumber;
                        account.BranchId = acc.BranchId;
                        account.CreatedBy = acc.CreatedBy;
                        account.CreatedTimestamp = acc.CreatedTimestamp;
                        account.IsActive = acc.IsActive;
                        account.ModifiedBy = acc.ModifiedBy;
                        account.ModifiedTimestamp = acc.ModifiedTimestamp;
                        account.Status = acc.Status;
                        accounts.Add(account);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                accounts = null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return accounts;
        }
        //Fetches the details of the account with the given account number
        public Common.Models.Account GetAccountDetailsByAccountNumber(string accountNumber)
        {
            var methodName=Common.Utilities.GetMethod();
            Common.Models.Account account = new Common.Models.Account();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                var accountDetails = (from a in Context.Account 
                                    where a.AccountNumber == accountNumber
                                    select a).FirstOrDefault();
                if(accountDetails!=null)
                {
                    account.AccountId=accountDetails.AccountId;
                    account.AccountNumber=accountDetails.AccountNumber;
                    account.BranchId=accountDetails.BranchId;
                    account.CreatedBy=accountDetails.CreatedBy;
                    account.CreatedTimestamp=accountDetails.CreatedTimestamp;
                    account.IsActive=accountDetails.IsActive;
                    account.ModifiedBy=accountDetails.ModifiedBy;
                    account.ModifiedTimestamp=accountDetails.ModifiedTimestamp;
                    account.Status=accountDetails.Status;                    
                }
            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                account = null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return account;
        }
        //toggles the IsActive property for a given account number
        public bool ToggleAccountStatus(string accountNumber,bool newStatus)
        {
            var methodName=Common.Utilities.GetMethod();
            bool status = false;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                List<Account> accounts = (from a in Context.Account
                                where a.AccountNumber==accountNumber
                                select a).ToList();
                if(accounts.Any())
                {
                    foreach (var account in accounts)
                    {
                        account.IsActive = newStatus;
                    }
                    List<AccountCustomerMapping> accountCustomerMappings = 
                                            (from acm in Context.AccountCustomerMapping
                                            where acm.AccountNumber == accountNumber
                                            select acm).ToList();
                    if(accountCustomerMappings.Any())
                    {
                        foreach (var account in accountCustomerMappings)
                        {
                            account.IsActive = newStatus;
                        }
                    }
                    Context.SaveChanges();
                    status = true;
                }                
            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status = false;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        //Minor Enhancement
        //Gets the accountCustomerMappingID for a given account number and customer ID
        public int GetAccountCustomerMappingIDByAccountNumber(string accountNumber, long customerId)
        {
            var methodName=Common.Utilities.GetMethod();
            int accountCustomerMappingID=0;
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                accountCustomerMappingID = (from acm in context.AccountCustomerMapping
                                            where acm.AccountNumber == accountNumber
                                            && acm.CustomerId == customerId
                                            select acm.AccountCustomerMappingId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                accountCustomerMappingID = -99;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return accountCustomerMappingID;
        } 
        #endregion
    }
}
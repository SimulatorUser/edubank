﻿using System;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.Common;
using Infosys.EduBank.CustomerDAL.Repository;
using Infosys.EduBank.CardDAL.Repository;
using Infosys.EduBank.AccountDAL.Repository;
using Infosys.EduBank.TransactionDAL.Repository;
using Infosys.EduBank.CustomerDAL.Interface;
using Infosys.EduBank.CardDAL.Interface;
using Infosys.EduBank.AccountDAL.Interface;
using Infosys.EduBank.TransactionDAL.Interface;
using System.Collections.Generic;
using Infosys.EduBank.UserBL.Interface;
using Newtonsoft.Json;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.UserBL
{
    public class UserBLRepository : IUserBLRepository
    {
        public AppSettings AppSettings { set; get; }
        public ICustomerRepository CustomerRepositoryObj { set; get; }
        //AccountRepositoryObj{set;get;}
        public ICardRepository CardRepositoryObj { set; get; }

        public IAccountRepository AccountRepositoryObj { get; set; }
        public IUtilities UtilitiesObj { set; get; }
        public UserBLRepository(IUtilities util)
        {
            AppSettings = JsonConvert.DeserializeObject<AppSettings>(util.Configuration);
            CustomerRepositoryObj = new CustomerRepository(AppSettings);
            CardRepositoryObj = new CardRepository(AppSettings);
            AccountRepositoryObj = new AccountRepository(AppSettings);

            UtilitiesObj = util;
        }
        public bool ValidateUser(string loginName, string password)
        {
            bool status = false;
            EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.EntryLogMessage, 1);
            try
            {
                string hashedPass = UtilitiesObj.LoginEncryptDecrypt(password, AppSettings.SecurityKey10);
                status = CustomerRepositoryObj.CustomerLoginValidation(loginName, hashedPass, AppSettings.CustomerSqlKey);

            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(), AppSettings, ex.Message, 2, ex);
                status = false;

            }
            finally
            {
                EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.ExitLogMessage, 1);
            }
            return status;
        }
        public bool ChangePasswordForUser(string loginName, String newPassword)
        {
            bool status = false;
            EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.EntryLogMessage, 1);
            try
            {
                string hashedPass = UtilitiesObj.Encrypt(newPassword, AppSettings.SecurityKey10);
                status = CustomerRepositoryObj.CustomerUpdatePass(loginName, hashedPass, AppSettings.CustomerSqlKey);

            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(), AppSettings, ex.Message, 2, ex);
                status = false;

            }
            finally
            {
                EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.ExitLogMessage, 1);
            }
            return status;
        }
        public string ChangePinForUser(string oldPin, String newPin, string debitCardNum)
        {
            string status = "FAILED";
            EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.EntryLogMessage, 1);
            try
            {
                string hashedOldPin = UtilitiesObj.LoginEncryptDecrypt(oldPin, AppSettings.SecurityKey10);
                string encryptNewPin = UtilitiesObj.Encrypt(newPin, AppSettings.SecurityKey10);
                if (CardRepositoryObj.ChangeDebitCardPin(debitCardNum, hashedOldPin, encryptNewPin, AppSettings.PinKey) == 1)
                {
                    status = "SUCCESS";
                }

            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(), AppSettings, ex.Message, 2, ex);
                status = "FAILED";

            }
            finally
            {
                EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.ExitLogMessage, 1);
            }
            return status;
        }

        public List<string> GetAccountNumbers(string userName)
        {
            List<string> accountNumbers = new List<string>();
            EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.EntryLogMessage, 1);
            try
            {
                long customerId = CustomerRepositoryObj.GetCustomerIdByUsername(userName);
                accountNumbers = AccountRepositoryObj.GetAccountNumbersByCustomerId(customerId);
            }
            catch (Exception ex)
            {
                accountNumbers = null;
                EduBankLogger.Log(this.GetType(), AppSettings, ex.Message, 2, ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.ExitLogMessage, 1);
            }
            return accountNumbers;
        }

        public List<string> GetDebitCards(int customerId)
        {
            int accountMappingId = 0;
            List<string> debitCardNumbers = new List<string>();
            EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.EntryLogMessage, 1);
            try
            {
                accountMappingId = AccountRepositoryObj.GetAccountMappingIdByCustomerId(customerId);
                debitCardNumbers = CardRepositoryObj.GetDebitCardNumbersByMappingId(accountMappingId);
            }
            catch (Exception ex)
            {
                debitCardNumbers = null;
                EduBankLogger.Log(this.GetType(), AppSettings, ex.Message, 2, ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.ExitLogMessage, 1);
            }
            return debitCardNumbers;
        }

        public string GetAccountNumberWithMappingId(int mappingId)
        {
            string accountNum = "";
            EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.EntryLogMessage, 1);
            try
            {
                accountNum = AccountRepositoryObj.GetAccountNumberByAccountMappingId(mappingId);
            }
            catch (Exception ex)
            {
                accountNum = string.Empty;
                EduBankLogger.Log(this.GetType(), AppSettings, ex.Message, 2, ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.ExitLogMessage, 1);
            }
            return accountNum;
        }

        public bool AddPayee(Common.Models.Payee payee)
        {
            try
            {
                var customerId = CustomerRepositoryObj.GetCustomerIdByUsername(payee.UserName);
                var dalPayee = new CustomerDAL.Models.Payee()
                {
                    NickName = payee.NickName,
                    AccountNumber = payee.AccountNumber,
                    CustomerId = customerId,
                };
                return CustomerRepositoryObj.AddPayee(dalPayee);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Common.Models.Payee> GetPayees(string userName)
        {
            List<Common.Models.Payee> payeeDetail = new List<Payee>();
            List<CustomerDAL.Models.Payee> payeeList = new List<CustomerDAL.Models.Payee>();
            try
            {
                var customerId = CustomerRepositoryObj.GetCustomerIdByUsername(userName);
                payeeList = CustomerRepositoryObj.FetchAllPayees(customerId);
                foreach (var payee in payeeList)
                {
                    payeeDetail.Add(new Payee()
                    {
                        AccountNumber = payee.AccountNumber,
                        NickName = payee.NickName,
                        PayeeId = payee.PayeeId,
                        PreferredAmount = payee.PreferredAmount
                    });
                }
            }
            catch (Exception ex)
            {
                payeeList = null;
            }
            return payeeDetail;
        }
        public bool VerifyCardHolderName(int accountCustomerMappingId, string cardHolderName)
        {
            bool isValid = false;
            EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.EntryLogMessage, 1);
            try
            {
                long customerId = AccountRepositoryObj.GetCustomerIdByAccountMappingId(accountCustomerMappingId);
                string customerName = CustomerRepositoryObj.GetCustomerNameByCustomerId(customerId);
                if (customerName == cardHolderName)
                    isValid = true;
            }
            catch (Exception ex)
            {
                isValid = false;
                EduBankLogger.Log(this.GetType(), AppSettings, ex.Message, 2, ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.ExitLogMessage, 1);
            }
            return isValid;
        }
        //Minor Enhancement
        //Gets the debit card details for the given login name and account number
        public Common.Models.DebitCardDetails GetDebitCardForLoginName(string loginName, string accountNumber)
        {
            Common.Models.DebitCardDetails debitCard = new Common.Models.DebitCardDetails();
            EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.EntryLogMessage, 1);
            try
            {
                long customerId = CustomerRepositoryObj.GetCustomerIdByUsername(loginName);
                if (customerId != -1)
                {
                    int accountCustomerMappingID =
                    AccountRepositoryObj.GetAccountCustomerMappingIDByAccountNumber(accountNumber, customerId);
                    if (accountCustomerMappingID != -99)
                    {
                        debitCard = CardRepositoryObj.GetDebitCardForAccountNumber(accountCustomerMappingID);
                    }
                }
            }
            catch (Exception ex)
            {
                debitCard = null;
                EduBankLogger.Log(this.GetType(), AppSettings, ex.Message, 2, ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.ExitLogMessage, 1);
            }
            return debitCard;
        }
        //Minor Enhancement
        //Inserts a debit card replacement request
        //Checks if card is lost. If yes, block the card immediately.
        //For other reasons, a check is made on valid thru value and request is inserted accordingly
        public bool SubmitReplaceRequest(int debitCardId, string reason)
        {
            bool status = false;
            EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.EntryLogMessage, 1);
            try
            {
                DebitCardRequest reqObj = new Common.Models.DebitCardRequest();
                reqObj.Reason = reason;
                reqObj.DebitCardId = debitCardId;
                if (reason == Reason.CardIsLost.ToString())
                {
                    status = CardRepositoryObj.RequestExtendDebitCard(reqObj);
                    if (status)
                    {
                        status = SubmitBlockRequest(debitCardId);
                    }
                }
                else if (reason == Reason.CardIsDamaged.ToString())
                {
                    status = CardRepositoryObj.RequestExtendDebitCard(reqObj);
                }
                else
                {
                    DateTime validThru = CardRepositoryObj.GetValidThruForDebitCard(debitCardId);
                    if ((validThru < DateTime.Now) || (validThru.AddDays(-90) <= DateTime.Now))
                    {
                        status = CardRepositoryObj.RequestExtendDebitCard(reqObj);
                    }
                }
            }
            catch (Exception ex)
            {
                status = false;
                EduBankLogger.Log(this.GetType(), AppSettings, ex.Message, 2, ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.ExitLogMessage, 1);
            }
            return status;
        }
        //Minor Enhancement
        // Subit Card Blocking Request
        public bool SubmitBlockRequest(int debitCardId)
        {
            bool status = false;
            EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.EntryLogMessage, 1);
            try
            {
                status = CardRepositoryObj.BlockDebitCard(debitCardId);
            }
            catch (Exception ex)
            {
                status = false;
                EduBankLogger.Log(this.GetType(), AppSettings, ex.Message, 2, ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(), AppSettings, Common.Utilities.GetMethod() + " " + Constants.ExitLogMessage, 1);
            }
            return status;
        }

    }
}
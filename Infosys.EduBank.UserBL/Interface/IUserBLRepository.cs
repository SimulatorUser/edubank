﻿using System;
using System.Collections;
using System.Collections.Generic;
using Infosys.EduBank.CustomerDAL.Interface;
using Infosys.EduBank.CardDAL.Interface;
using Infosys.EduBank.AccountDAL.Interface;
using Infosys.EduBank.Common;

namespace Infosys.EduBank.UserBL.Interface
{
    public interface IUserBLRepository
    {
        Common.Models.AppSettings AppSettings{set;get;}
        IAccountRepository AccountRepositoryObj{get; set;}
        IUtilities UtilitiesObj{set;get;}
        ICustomerRepository CustomerRepositoryObj{set;get;}
        ICardRepository CardRepositoryObj{set;get;}
        bool ChangePasswordForUser(string loginName, String newPassword);
        string ChangePinForUser(string oldPin, String newPin, string debitCardNum);
        List<string> GetAccountNumbers(string userName);
        List<string> GetDebitCards(int customerId);
        bool ValidateUser(string loginName, string password);
        bool VerifyCardHolderName(int accountCustomerMappingId,string cardHolderName);
        bool AddPayee(Common.Models.Payee payee);
        List<Common.Models.Payee> GetPayees(string userName);
        string GetAccountNumberWithMappingId(int mappingId);
        Common.Models.DebitCardDetails GetDebitCardForLoginName(string loginName,string accountNumber);
        bool SubmitBlockRequest(int debitCardId);
        bool SubmitReplaceRequest(int debitCardId, string reason);
    }
}
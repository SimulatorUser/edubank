namespace Infosys.EduBank.ConfigurationSL.Models
{
    public class AppConfig
    {
        public string Secret { get; set; }
        public string PublicKey{get; set;}
        public string SecurityKey1 { get; set; }
        public string SecurityKey2 { get; set; }
        public string SecurityKey3 { get; set; }
        public string SecurityKey4 { get; set; }
        public string SecurityKey5 { get; set; }
        public string SecurityKey6 { get; set; }
        public string SecurityKey7 { get; set; }
        public string SecurityKey10 { get; set; }
        public string TellerSqlKey { get; set; }
        public string CustomerSqlKey { get; set; }
        public string CVVKey { get; set; }
        public string PinKey {get;set;}
        public string DebitCardConstant{get;set;}

        public string Connection { get; set; }
        public string Key { get; set; }

        
        //Servers
        public string TellerServer { get; set; }        
        public string UserServer { get; set; }
        public string AccountServer { get; set; }        
        public string TransactionServer { get; set; }
        public string DebitCardServer { get; set; }
        public string CreditCardServer{get;set;}
        
        //DB
        public string TellerDB { get; set; }        
        public string UserDB { get; set; }
        public string AccountDB { get; set; }        
        public string TransactionDB { get; set; }
        public string DebitCardDB { get; set; }
        public string CreditCardDB{set;get;}

        //UserName
        public string TellerUser { get; set; }        
        public string UserUser { get; set; }
        public string AccountUser { get; set; }        
        public string TransactionUser { get; set; }
        public string DebitCardUser { get; set; }
        public string CreditCardUser{set;get;}
        //Passwords
        public string TellerPassword { get; set; }        
        public string UserPassword { get; set; }
        public string AccountPassword { get; set; }        
        public string TransactionPassword { get; set; }
        public string DebitCardPassword { get; set; }
        public string CreditCardPassword{set;get;}

        public string AdoNetAppenderInfoBufferSize {get;set;}
        public string InfoLoggerName{get;set;}
        public string AdoNetAppenderInfoConnectionType {get;set;}
        public string AdoNetAppenderErrorBufferSize {get;set;}
        public string ErrorLoggerName {get;set;}
        public string AdoNetAppenderErrorConnectionType {get;set;}
        public bool InfoLogStatus{get; set;}
        public bool ErrorLogStatus{get; set;}
        public bool WarningLogStatus{get; set;}
        public string WarningLogPath{get; set;}

    }
}
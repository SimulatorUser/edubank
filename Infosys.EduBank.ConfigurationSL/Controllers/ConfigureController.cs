﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.Common;
using Infosys.EduBank.ConfigurationSL.Interface;
using System.Diagnostics;
namespace Infosys.EduBank.ConfigurationSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigureController : ControllerBase, IConfigureController
    {
        public Models.AppConfig AppConfiguration { get; set; }
        public Common.Models.AppSettings AppSettings{get; set;}
        public IUtilities Utilities { get; set; }
        public ConfigureController(Models.AppConfig config,IUtilities util)
        {
            AppConfiguration=config;
            Utilities=util;
        }
        private Common.Models.AppSettings AssignValues()
        {
            var result=new Common.Models.AppSettings(){
                    PublicKey=AppConfiguration.PublicKey,
                    SecurityKey1 =AppConfiguration.SecurityKey1,
                    SecurityKey2 =AppConfiguration.SecurityKey2,
                    SecurityKey3=AppConfiguration.SecurityKey3,
                    SecurityKey4 =AppConfiguration.SecurityKey4,
                    SecurityKey5=AppConfiguration.SecurityKey5,
                    SecurityKey6 =AppConfiguration.SecurityKey6,
                    SecurityKey7 =AppConfiguration.SecurityKey7,
                    SecurityKey10 =AppConfiguration.SecurityKey10,

                    TellerSqlKey=AppConfiguration.TellerSqlKey,
                    CustomerSqlKey =AppConfiguration.CustomerSqlKey,
                    CVVKey=AppConfiguration.CVVKey,
                    PinKey=AppConfiguration.PinKey,

                    DebitCardConstant=AppConfiguration.DebitCardConstant,
                    AdoNetAppenderInfoBufferSize=AppConfiguration.AdoNetAppenderInfoBufferSize,
                    InfoLoggerName=AppConfiguration.InfoLoggerName,
                    AdoNetAppenderInfoConnectionType=AppConfiguration.AdoNetAppenderInfoConnectionType,
                    AdoNetAppenderErrorBufferSize=AppConfiguration.AdoNetAppenderErrorBufferSize,
                    ErrorLoggerName=AppConfiguration.ErrorLoggerName,
                    AdoNetAppenderErrorConnectionType=AppConfiguration.AdoNetAppenderErrorConnectionType,
                    InfoLogStatus=AppConfiguration.InfoLogStatus,
                    ErrorLogStatus=AppConfiguration.InfoLogStatus,
                    WarningLogStatus=AppConfiguration.InfoLogStatus,
                    WarningLogPath=AppConfiguration.WarningLogPath

                        };
                    result.UserConnection=Utilities.GetConnectionString
                    (
                        AppConfiguration.Connection,
                        AppConfiguration.UserServer,
                        AppConfiguration.UserDB,
                        AppConfiguration.UserUser,
                        AppConfiguration.UserPassword,
                        AppConfiguration.Key);
                    result.TellerConnection=Utilities.GetConnectionString
                    (
                        AppConfiguration.Connection,
                        AppConfiguration.TellerServer,
                        AppConfiguration.TellerDB,
                        AppConfiguration.TellerUser,
                        AppConfiguration.TellerPassword,
                        AppConfiguration.Key);
                    result.DebitCardConnection =Utilities.GetConnectionString
                    (
                        AppConfiguration.Connection,
                        AppConfiguration.DebitCardServer,
                        AppConfiguration.DebitCardDB,
                        AppConfiguration.DebitCardUser,
                        AppConfiguration.DebitCardPassword,
                        AppConfiguration.Key);
                    result.TransactionConnection =Utilities.GetConnectionString
                    (
                        AppConfiguration.Connection,
                        AppConfiguration.TransactionServer,
                        AppConfiguration.TransactionDB,
                        AppConfiguration.TransactionUser,
                        AppConfiguration.TransactionPassword,
                        AppConfiguration.Key);
                    result.AccountConnection =Utilities.GetConnectionString
                    (
                        AppConfiguration.Connection,
                        AppConfiguration.AccountServer,
                        AppConfiguration.AccountDB,
                        AppConfiguration.AccountUser,
                        AppConfiguration.AccountPassword,
                        AppConfiguration.Key);
                    result.CreditCardConnection =Utilities.GetConnectionString
                    (
                        AppConfiguration.Connection,
                        AppConfiguration.CreditCardServer,
                        AppConfiguration.CreditCardDB,
                        AppConfiguration.CreditCardUser,
                        AppConfiguration.CreditCardPassword,
                        AppConfiguration.Key);
                    result.UserConnection="Server=tcp:simulatorserver.database.windows.net,1433;Initial Catalog=Customer;Persist Security Info=False;User ID=EduBankUser;Password=EduUser@123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                    result.TellerConnection="Server=tcp:simulatorserver.database.windows.net,1433;Initial Catalog=Teller;Persist Security Info=False;User ID=EduBankUser;Password=EduUser@123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                    result.DebitCardConnection ="Server=tcp:simulatorserver.database.windows.net,1433;Initial Catalog=DebitCard;Persist Security Info=False;User ID=EduBankUser;Password=EduUser@123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                    result.TransactionConnection ="Server=tcp:simulatorserver.database.windows.net,1433;Initial Catalog=Transacction;Persist Security Info=False;User ID=EduBankUser;Password=EduUser@123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                    result.AccountConnection ="Server=tcp:simulatorserver.database.windows.net,1433;Initial Catalog=Account;Persist Security Info=False;User ID=EduBankUser;Password=EduUser@123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                    result.CreditCardConnection="Server=tcp:simulatorserver.database.windows.net,1433;Initial Catalog=CreditCard;Persist Security Info=False;User ID=EduBankUser;Password=EduUser@123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
                    return result;
        }


        // GET api/values
        [HttpGet]
        public Common.Models.AppSettings Get()
        {
            
            return AssignValues();
        }

        [HttpGet("log")]
        public string testLogging()
        {
            try
            {
                AppSettings=AssignValues();
                Common.Logger.EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" TestMessage",1);
                Common.Logger.EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" TestMessage",3);
                throw new NullReferenceException();
            }
            catch(Exception e)
            {
                Common.Logger.EduBankLogger.Log(this.GetType(),AppSettings,"TestMessage",2,e);
            }
            return "Ok";
        }
        
    }
}

﻿using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.ConfigurationSL.Models;

namespace Infosys.EduBank.ConfigurationSL.Interface
{
    public interface IConfigureController
    {
        AppConfig AppConfiguration { get; set; }
        IUtilities Utilities { get; set; }

        AppSettings Get();
    }
}
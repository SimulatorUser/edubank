﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.TransactionDAL.Models
{
    public partial class Transaction
    {
        public long TransactionId { get; set; }
        public string AccountNumber { get; set; }
        public decimal Amount { get; set; }
        public DateTime TransactionDateTime { get; set; }
        public bool Type { get; set; }
        public string Mode { get; set; }
        public string Remarks { get; set; }
        public string Info { get; set; }
        public string CreatedBy { get; set; }
        public char TransactionStatus{get;set;}
    }
}

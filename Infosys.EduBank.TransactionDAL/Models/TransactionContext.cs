﻿using System;
using Microsoft.EntityFrameworkCore;
using Infosys.EduBank.TransactionDAL.Interface;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;


namespace Infosys.EduBank.TransactionDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class TransactionContext : DbContext, ITransactionContext
    {
        private string ConnectionString { get; set; }
        public TransactionContext(string connectionString)
        {
            this.ConnectionString=connectionString;
        }

        public TransactionContext(DbContextOptions<TransactionContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Transaction> Transaction { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.Property(e => e.AccountNumber)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Amount).HasColumnType("money");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Info)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Mode)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Remarks)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TransactionDateTime).HasColumnType("datetime");
            });
        }
        public virtual int ExecuteSqlCommandMethod(string query,params SqlParameter[] parameters)
        {
            return this.Database.ExecuteSqlCommand(query,parameters);
        }
    }
}

using Infosys.EduBank.TransactionDAL.Interface;
using Infosys.EduBank.TransactionDAL.Models;
using Infosys.EduBank.Common.Models;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Collections.Generic;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.TransactionDAL.Repository
{
    public class TransactionRepository : ITransactionRepository
    {
        public Common.Models.AppSettings AppSettings{get; set;}
         private ITransactionContext context;
        public ITransactionContext Context{
            get{ return context;}
            set{context=value;}
        }

        public TransactionRepository(Common.Models.AppSettings settings)
        {
            Context = new TransactionContext(settings.TransactionConnection);
            AppSettings=settings;
        }

        public decimal GetBalance(string accountNumber,DateTime? balanceOnDate= null){
            decimal balance=0;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                List<TransactionDAL.Models.Transaction> transactionsForAccount = Context.Transaction.Where(x=> x.AccountNumber==accountNumber && x.TransactionStatus=='S').ToList();
                if(balanceOnDate!=null)
                {
                    transactionsForAccount = Context.Transaction.Where(x=> x.AccountNumber==accountNumber && x.TransactionStatus=='S'&& x.TransactionDateTime<balanceOnDate).ToList();
                }
                foreach (var item in transactionsForAccount){
                    if (item.Type)
                        balance += item.Amount;
                    else
                        balance -= item.Amount;
                }
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                balance = 0;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return balance;
        }

        public long InsertTransaction(Common.Models.InsertTransaction transaction,byte type){
            long transactionId = 0;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                // Map to model's transaction and insert in DB.
                SqlParameter prmAccountNum = new SqlParameter("@AccountNumber",transaction.AccountNumber);
                SqlParameter prmAmount = new SqlParameter("@Amount",transaction.Amount);
                SqlParameter prmType=new SqlParameter("@Type",type);
                SqlParameter prmMode=new SqlParameter("@Mode",transaction.Mode);//Hard CODED CHANGE IT!!
                SqlParameter prmRemarks=new SqlParameter("@Remarks",transaction.Remarks);
                SqlParameter prmInfo=new SqlParameter("@Info",transaction.Info);
                SqlParameter prmCreatedBy=new SqlParameter("@CreatedBy",transaction.TellerId);

                SqlParameter prmOutTransactionId=new SqlParameter();
                prmOutTransactionId.ParameterName="@TransactionId";
                prmOutTransactionId.Direction=System.Data.ParameterDirection.Output;
                prmOutTransactionId.Size=100;

                Context.ExecuteSqlCommandMethod("EXEC usp_Transact @AccountNumber,@Amount,@Type,@Mode,@Remarks,@Info,@CreatedBy,@TransactionId out"
                ,prmAccountNum,prmAmount,prmType,prmMode,prmRemarks,prmInfo,prmCreatedBy,prmOutTransactionId);
                transactionId=Convert.ToInt64(prmOutTransactionId.Value);
            }
            catch(Exception ex){
                transactionId = 0;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return transactionId;
        }
        public bool UpdateStatus(long transactionId,char status)
        {
            var result= false;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                var obj=Context.Transaction.Where(x=>x.TransactionId==transactionId).FirstOrDefault();
                obj.TransactionStatus=status;
                Context.SaveChanges();
                result= true;
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,1,ex);
                result=false;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return result;
        }
        public List<Models.Transaction> ViewTransactions(DateTime startDate,DateTime endDate,string accountNumber){
            List<Models.Transaction> transactionsToReturn = new List<Models.Transaction>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                // fetch Transactions with Linq
                transactionsToReturn= Context.Transaction.Where(x=>x.TransactionDateTime >= startDate && x.TransactionDateTime<=endDate && x.AccountNumber==accountNumber && x.TransactionStatus=='S').ToList();
            }
            catch(Exception ex){
                transactionsToReturn = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return transactionsToReturn;
        }

    }
}
﻿using Microsoft.EntityFrameworkCore;
using Infosys.EduBank.TransactionDAL.Models;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Data.SqlClient;

namespace Infosys.EduBank.TransactionDAL.Interface
{
    public interface ITransactionContext
    {
        DbSet<Transaction> Transaction { get; set; }
        int ExecuteSqlCommandMethod(string query,params SqlParameter[] parameters);
        int SaveChanges();
        
    }
}
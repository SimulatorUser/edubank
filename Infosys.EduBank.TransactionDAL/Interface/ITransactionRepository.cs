using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TransactionDAL.Models;
using System;
using System.Collections.Generic;

namespace Infosys.EduBank.TransactionDAL.Interface
{
    public interface ITransactionRepository
    {
        AppSettings AppSettings{set;get;}
        ITransactionContext Context { get; set;}

        decimal GetBalance(string accountNumber,DateTime? balanceOnDate= null);
        long InsertTransaction(InsertTransaction transaction, byte type);
        bool UpdateStatus(long transactionId,char status);
        List<Models.Transaction> ViewTransactions(DateTime startDate, DateTime endDate, string accountNumber);
    }
}
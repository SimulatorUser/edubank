﻿using System;
using Infosys.EduBank.Common;
using Infosys.EduBank.AccountDAL.Repository;
using Infosys.EduBank.AccountDAL.Models;
using Infosys.EduBank.CardDAL.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infosys.EduBank.TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // System.Console.WriteLine("Hellos");
                // AccountRepository repo = new AccountRepository(@"data source=MYSHEC126741l\SQLExpress;initial catalog=Account;User Id=EduBankUser;Password=EduUser@123");
                // List<Branch> br=repo.GetAllBranches();
                // Console.WriteLine(br.Count);
                // Utilities util = new Utilities();
                // System.Console.WriteLine(util.Encrypt("EhAKn#7Rs1*","YzAcDCIyIX0sakQ6ZkBQHRcSdBASOw5sgGASMyEYYWdYuQlop8S654jn/KH9B3+b")); 
                // AccountRepository repos = new AccountRepository("data source=(localdb)\\ProjectsV13;initial catalog=Account;User Id=EduBankUser;Password=EduUser@123");
                // System.Console.WriteLine(repos.CreateAccountNumber(1000000001,1,1)); 
                // string cvv = string.Empty;
                // string pin = string.Empty;
               // Utilities util = new Utilities();
                //System.Console.WriteLine(util.Decrypt("YzAcDCIyIX0sakQ6ZkBQHRcSdBASOw5sgGASMyEYYWcIuZ3o/sKSlbIvckxRXKr0","YzAcDCIyIX0sakQ6ZkBQHRcSdBASOw5sgGASMyEYYWdYuQlop8S654jn/KH9B3+b"));
               //System.Console.WriteLine(util.Decrypt("YzAcDCIyIX0sakQ6ZkBQHRcSdBASOw5sgGASMyEYYWd7w3M8P88pEKzlijrksT/N","YzAcDCIyIX0sakQ6ZkBQHRcSdBASOw5sgGASMyEYYWdVdptIhMSgCynPEqWssTb9"));
              // System.Console.WriteLine(util.Encrypt("CreditCard","YzAcDCIyIX0sakQ6ZkBQHRcSdBASOw5sgGASMyEYYWdVdptIhMSgCynPEqWssTb9"));

                System.Console.WriteLine(GenerateCardTokenOptimised());



            }
            catch(Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
            
        }
        public static string GenerateCardTokenOptimised()
        {
            int[] checkArray = new int[15];
            
            var cardNum = new int[16];
 Random rnd=new Random();
            for (int d = 14; d >= 0; d--)
            {
                cardNum[d] = rnd.Next(0,9);
                checkArray[d] = ( cardNum[d] * (((d+1)%2)+1)) % 9;
            }
 
            cardNum[15] = ( checkArray.Sum() * 9 ) % 10;
 
            var sb = new StringBuilder(); 
 
            for (int d = 0; d < 16; d++)
            {
                sb.Append(cardNum[d].ToString());
            } 
            return sb.ToString();
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.Common;
using Infosys.EduBank.AuthenticationSL.Helper;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.AuthenticationSL.Models;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.AuthenticationSL.Interface;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;

namespace Infosys.EduBank.AuthenticationSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase, IAuthenticationController
    {
        public Common.Models.AppSettings AppSettings{get; set;}
        public IAuthenticationHelper AuthenticationHelper { get; set; }

        public Common.IUtilities Utilities{get;set;}
        public AuthenticationController(IUtilities util)
        {
            Utilities=util;
            var appsetting=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(Utilities.Configuration);
            AppSettings=appsetting;
            AuthenticationHelper=new AuthenticationHelper(appsetting,Utilities);

        }
        // POST api/values
        [AllowAnonymous]
        [HttpPost]
        public UserAuth Post([FromBody]User user)
        {
            ConfigureLog();
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            UserAuth userData = new UserAuth();
            try
            {
                userData=AuthenticationHelper.GenerateToken
                (user.UserName,
                user.Password,
                Utilities.GetValueFromAppSettings(Constants.AuthSecret)
                );
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                userData=null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return userData;
        }
        [HttpGet("Health")]
        public string Get()
        {
            return "Authentication Service is Up And Running";
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            AuthenticationHelper.AppSettings.UserName=AppSettings.UserName;
            AuthenticationHelper.AuthenticationBL.AppSettings.UserName=AppSettings.UserName;
            AuthenticationHelper.AuthenticationBL.CustomerRepository.AppSettings.UserName=AppSettings.UserName;
            AuthenticationHelper.AuthenticationBL.TellerRepository.AppSettings.UserName=AppSettings.UserName;
            
        }
    }
}

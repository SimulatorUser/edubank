using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System;
using System.Net;
using System.IO;
using System.Diagnostics.CodeAnalysis;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.Common;
namespace Infosys.EduBank.AuthenticationSL.Helper
{
    [ExcludeFromCodeCoverage]
    public class HttpServiceHelper
    {
        
        public Common.Models.AppSettings AppSettings{get; set;}

        public HttpServiceHelper(Common.Models.AppSettings settings)
        {
            AppSettings=settings;
        }
        private static bool AcceptAllCertifications(object sender,
        System.Security.Cryptography.X509Certificates.X509Certificate certification,
        System.Security.Cryptography.X509Certificates.X509Chain chain,
        System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
        return true;
        }
        public string GetResponse(string url)
        {
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
               httpWebRequest.ServerCertificateValidationCallback=new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                return streamReader.ReadToEnd();
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                return "Exception raised while calling other service";
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            
        }

    }
}
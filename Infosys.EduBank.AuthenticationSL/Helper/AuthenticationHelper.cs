using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Infosys.EduBank.AuthenticationBL;
using System.IdentityModel.Tokens.Jwt;
using Infosys.EduBank.AuthenticationSL.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Infosys.EduBank.AuthenticationSL.Interface;
using Infosys.EduBank.AuthenticationBL.Interface;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.AuthenticationSL.Helper
{
    public class AuthenticationHelper : IAuthenticationHelper
    {
        public IAuthenticateBL AuthenticationBL { get; set; }
        
        public Common.Models.AppSettings AppSettings { get; set; }
        private IUtilities Utilities {get;set;}
        public AuthenticationHelper(Common.Models.AppSettings appSettings,IUtilities util)
        {
            AppSettings = appSettings;
            AuthenticationBL=new AuthenticateBL(appSettings);
            Utilities=util;
        }

        public UserAuth GenerateToken(string username, string password,string secret){
            UserAuth user;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
               user=new UserAuth();
               var passwordHashed = Utilities.LoginEncryptDecrypt(password,AppSettings.SecurityKey10);
               user = AuthenticationBL.AuthenticateUser(username,passwordHashed);
               if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Role, user.Role),
                    new Claim(ClaimTypes.Name, user.Name)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            //user.Password = null;
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                user=null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return user;
        }
    }
}
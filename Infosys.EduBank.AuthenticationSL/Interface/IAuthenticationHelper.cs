﻿using Infosys.EduBank.Common.Models;
using Infosys.EduBank.AuthenticationBL.Interface;

namespace Infosys.EduBank.AuthenticationSL.Interface
{
    public interface IAuthenticationHelper
    {
        AppSettings AppSettings{set;get;}
        UserAuth GenerateToken(string username, string password, string secret);
        IAuthenticateBL AuthenticationBL { get; set; }
    }
}
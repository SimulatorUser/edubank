﻿using Infosys.EduBank.AuthenticationSL.Models;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.AuthenticationSL.Interface
{
    public interface IAuthenticationController
    {
        AppSettings AppSettings{set;get;}
        IAuthenticationHelper AuthenticationHelper { get; set; }
        string Get();
        UserAuth Post([FromBody] User user);
    }
}
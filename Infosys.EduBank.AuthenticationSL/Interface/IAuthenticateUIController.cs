﻿using Infosys.EduBank.AuthenticationSL.Models;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.AuthenticationSL.Interface
{
    public interface IAuthenticateUIController
    {
        AppSettings AppSettings{set;get;}
        string Get();
        UserAuth Post([FromBody] User user);
    }
}
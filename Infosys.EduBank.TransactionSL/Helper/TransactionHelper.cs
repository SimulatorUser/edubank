using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Infosys.EduBank.TransactionBL;
using System;
using System.Collections.Generic;
using Infosys.EduBank.TransactionSL.Interface;
using Infosys.EduBank.TransactionBL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.TransactionSL.Helper {
    public class TransactionHelper : ITransactionHelper
    {
        public ITransactionBLRepository TransactionBLRepositoryObj { get; set; }

        public Common.Models.AppSettings AppSettings { get; set; }

        public TransactionHelper(Common.Models.AppSettings appSettings)
        {
            AppSettings = appSettings;
            TransactionBLRepositoryObj = new TransactionBLRepository(appSettings);
        }

        //public long CreateTransaction(Common.Models.PayToBankTransaction transaction,string tellerId){
        //    long transactionId = 0;
        //    try{
        //        transactionId = TransactionBLRepositoryObj.CreateTransaction(transaction,tellerId,"Teller");
        //    }
        //    catch(Exception ex){
        //        transactionId = 0;
        //    }
        //    return transactionId;
        //} 

        public List<Common.Models.Transaction> ViewTransactions(DateTime startDate,DateTime endDate,string accountNumber){
            List<Common.Models.Transaction> transactionsToReturn = new List<Common.Models.Transaction>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                transactionsToReturn = TransactionBLRepositoryObj.ViewTransactions(startDate,endDate,accountNumber);
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return transactionsToReturn;
            } 

        public decimal ViewBalance(string accountNumber){
            decimal balance = -99;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                balance = TransactionBLRepositoryObj.GetBalance(accountNumber);
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                balance = -99;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }  
            return balance;
        }

         public Dictionary<DateTime,decimal> GetBalancePerDayBasis(string accountNumber)
        {
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            Dictionary<DateTime,decimal> result= new Dictionary<DateTime, decimal>();
            try
            {
                result=TransactionBLRepositoryObj.GetBalancePerDay(accountNumber);
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                result= new Dictionary<DateTime, decimal>();
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return result;
        }

    }

}

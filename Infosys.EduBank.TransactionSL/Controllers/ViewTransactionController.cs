using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.TransactionSL.Helper;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.TransactionSL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;

namespace Infosys.EduBank.TransactionSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViewTransactionController : ControllerBase, IViewTransactionController
    {
        public ITransactionHelper TransactionHelperObj { get; set; }
        public AppSettings AppSettings{ get; set; }
        private Common.IUtilities Utilities{get;set;}
        public ViewTransactionController (Common.IUtilities util)
        {
            //AppSettings=appSettings;
            Utilities=util;
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(Utilities.Configuration);
            AppSettings=appSettings;
            TransactionHelperObj=new TransactionHelper(appSettings);
        }
        
        

        // GET api/values/5
        [HttpGet("Health")]
        public ActionResult<string> Get()
        {
            return "View Transaction Service is Up And Running";
        }

        // POST api/values
        [Authorize(Roles = "User")]
        [HttpPost]
        public List<Common.Models.Transaction> Post(DateTime startDate,DateTime endDate,string accountNumber)
        {
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            List<Common.Models.Transaction> transactionsToReturn =new List<Common.Models.Transaction>();
            try{
                transactionsToReturn=TransactionHelperObj.ViewTransactions(startDate,endDate,accountNumber); 
            }
            catch(Exception ex){
                transactionsToReturn= null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return transactionsToReturn;
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            TransactionHelperObj.AppSettings.UserName=AppSettings.UserName;
            TransactionHelperObj.TransactionBLRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            TransactionHelperObj.TransactionBLRepositoryObj.TransactionRepositoryObj.AppSettings.UserName=AppSettings.UserName;
        }

    }
}

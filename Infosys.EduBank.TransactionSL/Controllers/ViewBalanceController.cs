using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.TransactionSL.Helper;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.TransactionSL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;


namespace Infosys.EduBank.TransactionSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViewBalanceController : ControllerBase, IViewBalanceController
    {
        public ITransactionHelper TransactionHelperObj { get; set; }
        public AppSettings AppSettings{ get; set; }
        private Common.IUtilities Utilities{get;set;}
        public ViewBalanceController (Common.IUtilities util)
        {
            Utilities=util;
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(Utilities.Configuration);
            AppSettings=appSettings;
            TransactionHelperObj=new TransactionHelper(AppSettings);
        }
        
        [HttpGet]
        [Authorize(Roles = "User")]
        public decimal Get(string accountNumber)
        {
            ConfigureLog();
            decimal balance = 0;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                balance = TransactionHelperObj.ViewBalance(accountNumber);
            }
            catch(Exception ex){
                balance = -99;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return balance;
        }
        [HttpGet("Health")]
        public string Get()
        {
            return "View Balance Service is Up And Running";
        }
        [HttpGet("Balance/{accountNumber}")]
        [Authorize(Roles = "Teller")]
        public Dictionary<DateTime,decimal> GetAllDateBalance(string accountNumber)
        {
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            Dictionary<DateTime,decimal> result = new Dictionary<DateTime,decimal>();
            try{
                result = TransactionHelperObj.GetBalancePerDayBasis(accountNumber);
            }
            catch(Exception ex){
                result = new Dictionary<DateTime,decimal>();
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return result;
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            TransactionHelperObj.AppSettings.UserName=AppSettings.UserName;
            TransactionHelperObj.TransactionBLRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            TransactionHelperObj.TransactionBLRepositoryObj.TransactionRepositoryObj.AppSettings.UserName=AppSettings.UserName;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TransactionBL;
using Infosys.EduBank.TransactionBL.Interface;

namespace Infosys.EduBank.TransactionSL.Interface
{
    public interface ITransactionHelper
    {
        AppSettings AppSettings { get; set; }
        ITransactionBLRepository TransactionBLRepositoryObj { get; set; }

        //long CreateTransaction(PayToBankTransaction transaction, string tellerId);
        decimal ViewBalance(string accountNumber);
        List<Transaction> ViewTransactions(DateTime startDate, DateTime endDate, string accountNumber);
        Dictionary<DateTime,decimal> GetBalancePerDayBasis(string accountNumber);
    }
}
﻿using Infosys.EduBank.TransactionSL.Helper;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.TransactionSL.Interface
{
    public interface IViewBalanceController
    {
        Common.Models.AppSettings AppSettings{set;get;}
        ITransactionHelper TransactionHelperObj { get; set; }
        decimal Get(string accountNumber);
        string Get();
    }
}
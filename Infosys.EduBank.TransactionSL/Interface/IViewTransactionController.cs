﻿using System;
using System.Collections.Generic;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TransactionSL.Helper;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.TransactionSL.Interface
{
    public interface IViewTransactionController
    {
        AppSettings AppSettings{set;get;}
        ITransactionHelper TransactionHelperObj { get; set; }
        ActionResult<string> Get();

        List<Transaction> Post(DateTime startDate, DateTime endDate, string accountNumber);

    }
}
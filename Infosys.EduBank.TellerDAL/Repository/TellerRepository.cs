using Infosys.EduBank.TellerDAL.Interface;
using Infosys.EduBank.TellerDAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.TellerDAL.Repository
{
    public class TellerRepository:ITellerRepository
    {
        public Common.Models.AppSettings AppSettings{get; set;}
        private ITellerContext context;
        public ITellerContext Context{
            get{ return context;}
            set{context = value;}
        }

        public TellerRepository(Common.Models.AppSettings settings){
            Context=new TellerContext(settings.TellerConnection);
            AppSettings=settings;
        }



        public bool TellerLogin(string username,string password,string decryptedKey){
            bool status=false;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                //var retVal = Context.Teller.FromSql("select dbo.ufn_ValidateTellerLogin({0},{1},{2})",username,password,decryptedKey).FirstOrDefault();
                var retVal = Context.ValidateTellerLoginForTest(username,password,decryptedKey);
                if (retVal == 1){
                    status=true;
                }
            }
            catch (Exception ex)
            {
                status=false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        public int GetTellerIdByUsername(string username){
           int id=0;
           EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
           try{
               id=Context.Teller.Where(z=>z.LoginName==username).Select(z=>z.TellerId).FirstOrDefault();
           }
           catch(Exception ex){
               id= 0;
               EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
           }
           finally
           {
               EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
           }
           return id;
        }
    }
}
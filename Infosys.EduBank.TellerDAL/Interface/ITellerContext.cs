﻿using Microsoft.EntityFrameworkCore;
using Infosys.EduBank.TellerDAL.Models;
namespace Infosys.EduBank.TellerDAL.Interface
{
    public interface ITellerContext
    {
        DbSet<Teller> Teller { get; set; }
        byte ValidateTellerLoginForTest(string username,string password, string decryptedKey);
    }
}
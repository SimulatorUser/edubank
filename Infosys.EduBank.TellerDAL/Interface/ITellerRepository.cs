using Infosys.EduBank.TellerDAL.Models;

namespace Infosys.EduBank.TellerDAL.Interface
{
    public interface ITellerRepository
    {
        Common.Models.AppSettings AppSettings{set;get;}
        ITellerContext Context{get;set;}
        bool TellerLogin(string username,string password,string decryptedKey);
        int GetTellerIdByUsername(string username);
    }
}
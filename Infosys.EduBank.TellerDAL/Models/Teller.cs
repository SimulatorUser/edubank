﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.TellerDAL.Models
{
    public partial class Teller
    {
        public byte TellerId { get; set; }
        public string LoginName { get; set; }
        public byte[] Password { get; set; }
    }
}

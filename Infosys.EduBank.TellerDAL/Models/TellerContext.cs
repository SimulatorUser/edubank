﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Infosys.EduBank.TellerDAL.Interface;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Diagnostics.CodeAnalysis;


namespace Infosys.EduBank.TellerDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class TellerContext : DbContext, ITellerContext
    {
        private string ConnectionString { get; }
        public TellerContext(string conn)
        {
            ConnectionString=conn;
        }

        public byte ValidateTellerLoginForTest(string username,string password, string decryptedKey){
            return (from x in this.Teller select TellerContext.ValidateTellerLogin(username,password,decryptedKey)).FirstOrDefault();
        }
        [DbFunction("ufn_ValidateTellerLogin","dbo")]
        public static byte ValidateTellerLogin(string username,string password, string decryptedKey){
            return 99;
        }

        public TellerContext(DbContextOptions<TellerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Teller> Teller { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Teller>(entity =>
            {
                entity.Property(e => e.TellerId).ValueGeneratedOnAdd();

                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(300);
            });
        }
    }
}

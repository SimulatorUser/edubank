﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Infosys.EduBank.Common;
using Infosys.EduBank.CreditDebitSL.Helper;
using System.Diagnostics.CodeAnalysis;


namespace Infosys.EduBank.CreditDebitSL
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();//.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddCors();
            IUtilities utility= new Utilities();
            HttpServiceHelper httpServiceHelperObj = new HttpServiceHelper(new Common.Models.AppSettings(){
                ErrorLogStatus=false,
                InfoLogStatus=false,
                WarningLogStatus=false,
                UserName="System"
                });
            utility.Configuration= httpServiceHelperObj.GetResponse(utility.GetValueFromAppSettings(Constants.ConfigUrl),string.Empty);
            //var appSettingsSection = Configuration.GetSection("AppSettings");
            //services.Configure<Common.Models.AppSettings>(appSettingsSection);
            // configure jwt authentication
            //var appSettings = appSettingsSection.Get<Common.Models.AppSettings>();
            var key = Encoding.ASCII.GetBytes(utility.GetValueFromAppSettings(Constants.AuthSecret));
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            services.AddHttpContextAccessor();
            services.AddSingleton(utility);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
             if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            //app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}

﻿using Infosys.EduBank.Common.Models;
using Infosys.EduBank.CreditDebitBL.Interface;
using Infosys.EduBank.CreditDebitBL;
using Microsoft.AspNetCore.Http;

namespace Infosys.EduBank.CreditDebitSL.Interface
{
    public interface ICreditHelper
    {
        AppSettings AppSettings{set;get;}
        ICreditDebitBLRepository CreditBLObj { get; set; }

        IHttpServiceHelper HttpServiceHelperObj{get;set;}
        bool UpdateTransactionStatus(long transactionId,char state);
      
        string GetToken(HttpContext context);
        long CreditMoney(InsertTransaction transaction, string token);
        long DebitMoney(InsertTransaction transaction,string token);
    }
}
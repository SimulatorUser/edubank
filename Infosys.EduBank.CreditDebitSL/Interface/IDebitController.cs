using Infosys.EduBank.Common.Models;
using Infosys.EduBank.CreditDebitSL.Helper;
using Microsoft.AspNetCore.Mvc;
namespace Infosys.EduBank.CreditDebitSL.Interface
{
    public interface IDebitController
    {
        AppSettings AppSettings{set;get;}
        ICreditHelper CreditHelperObj { get; set; }

        ActionResult<string> Get(int id);
        long Post(Models.CreditTransaction transaction);
         
    }
}
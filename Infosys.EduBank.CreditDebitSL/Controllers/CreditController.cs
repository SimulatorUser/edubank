﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.CreditDebitSL.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Infosys.EduBank.CreditDebitSL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;

namespace Infosys.EduBank.CreditDebitSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreditController : ControllerBase, ICreditController
    {
        
        public ICreditHelper CreditHelperObj { get; set; }
        public Common.Models.AppSettings AppSettings{get; set;} 
        public CreditController(Common.IUtilities util)
        {
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(util.Configuration);
            AppSettings=appSettings;
            
            CreditHelperObj = new CreditHelper(appSettings);
        }

        // GET api/values/5
        [HttpGet("Health")]
        public ActionResult<string> Get(int id)
        {
            return "Credit Service is Up And Running";
        }

        // POST api/values
        [Authorize(Roles = "Teller,User")]
        [HttpPost]
        public long Post(Models.CreditTransaction transaction)
        {
            long transactionId=0;
            ConfigureLog();
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                //Context.Current.Request
                string token= CreditHelperObj.GetToken(HttpContext);
                var modelObj=new Common.Models.InsertTransaction(){
                    AccountName=transaction.AccountName,
                    AccountNumber=transaction.AccountNumber,
                    Amount=transaction.Amount,
                    IFSC=transaction.IFSC,
                    TellerId=transaction.TellerId,
                    Remarks =transaction.Remarks,
                    Info=transaction.Info,
                    Mode=transaction.Mode
                };
                transactionId = CreditHelperObj.CreditMoney(modelObj,token);
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,1,ex);
                transactionId = 0;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return transactionId;
        }
        [Authorize(Roles = "Teller,User")]
        [HttpPut]
        public bool Put(long transactionId,char state)
        {
            var methodName=Common.Utilities.GetMethod();
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            bool status=false;
            try
            {
                status=CreditHelperObj.UpdateTransactionStatus(transactionId,state);
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,1,ex);
                status=false;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            
            return status;
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            CreditHelperObj.AppSettings.UserName=AppSettings.UserName;
            
        }
    }
}

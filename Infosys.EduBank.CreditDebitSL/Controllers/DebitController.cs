using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.CreditDebitSL.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Infosys.EduBank.CreditDebitSL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;

namespace Infosys.EduBank.CreditDebitSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DebitController:ControllerBase,IDebitController
    {
        public ICreditHelper CreditHelperObj { get; set; } 
        public Common.Models.AppSettings AppSettings{get; set;}
        public DebitController(Common.IUtilities util)
        {
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(util.Configuration);
            AppSettings=appSettings;
            CreditHelperObj = new CreditHelper(AppSettings);
        }

        // GET api/values/5
        [HttpGet("Health")]
        public ActionResult<string> Get(int id)
        {
            return "Credit Service is Up And Running";
        }
        // POST api/values
        [Authorize(Roles = "Teller,User")]
        [HttpPost]
        public long Post(Models.CreditTransaction transaction)
        {
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            long transactionId=0;
            try{
                //Context.Current.Request
                string token= CreditHelperObj.GetToken(HttpContext);
                var modelObj=new Common.Models.InsertTransaction(){
                    AccountName=transaction.AccountName,
                    AccountNumber=transaction.AccountNumber,
                    Amount=transaction.Amount,
                    IFSC=transaction.IFSC,
                    TellerId=transaction.TellerId,
                    Remarks =transaction.Remarks,
                    Info=transaction.Info,
                    Mode=transaction.Mode
                };
                transactionId = CreditHelperObj.DebitMoney(modelObj,token);
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,1,ex);
                transactionId = 0;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return transactionId;
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            CreditHelperObj.AppSettings.UserName=AppSettings.UserName;
        }
    }
}
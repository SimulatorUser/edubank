namespace Infosys.EduBank.CreditDebitSL.Models
{
    public class CreditTransaction
    {
        public string IFSC { get; set; }

        public string AccountNumber { get; set; }

        public string AccountName { get; set; }

        public decimal Amount { get; set; }
        
        public string TellerId {get ; set;}

        public string Remarks { get; set; }

        public string Info { get; set; }
        public string Mode{get;set;}

    }
}
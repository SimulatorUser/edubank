using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Infosys.EduBank.CreditDebitBL;
using Infosys.EduBank.CreditDebitBL.Interface;
using System;
using System.Net;
using System.IO;
using Infosys.EduBank.CreditDebitSL.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Authentication;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
namespace Infosys.EduBank.CreditDebitSL.Helper{
    public class CreditHelper:ICreditHelper{

        public ICreditDebitBLRepository CreditBLObj { get; set; }
        public Common.Models.AppSettings AppSettings{get; set;}

        public IHttpServiceHelper HttpServiceHelperObj{get;set;}
        public CreditHelper(Common.Models.AppSettings appSettings)
        {
            CreditBLObj = new CreditDebitBLRepository(appSettings);
            AppSettings=appSettings;
            HttpServiceHelperObj=new HttpServiceHelper(AppSettings);
        }

        public long CreditMoney(Common.Models.InsertTransaction transaction,string token){
            long transactionId = 0;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                bool isAccountAndBranchValid = false;

                string URL = "http://localhost:5151/api/validateAccount/"+transaction.AccountNumber;
                isAccountAndBranchValid = Convert.ToBoolean(HttpServiceHelperObj.GetResponse(URL,token));
                if (isAccountAndBranchValid)
                {
                    transactionId = CreditBLObj.CreditMoney(transaction);
                }
                    
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                transactionId = 0;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return transactionId;
        }
        public string GetToken(HttpContext context )
        {
            return context.GetTokenAsync("access_token").Result;
        }

        public bool UpdateTransactionStatus(long transactionId,char state){
            bool status=false;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                status=CreditBLObj.UpdateTransactionStatus(transactionId,state);
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
        return status;
        }
        public long DebitMoney(Common.Models.InsertTransaction transaction,string token){
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            long transactionId = 0;
            try{
                bool isAccountAndBranchValid = false;

                string URL = "http://localhost:5151/api/validateAccount/"+transaction.AccountNumber;
                isAccountAndBranchValid = Convert.ToBoolean(HttpServiceHelperObj.GetResponse(URL,token));
                if (isAccountAndBranchValid)
                {
                    transactionId = CreditBLObj.DebitMoney(transaction);
                }
                    
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                transactionId = 0;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return transactionId;
        }


    }
}

namespace Infosys.EduBank.UserSL.Interface
{
    public interface IHttpServiceHelper
    {
        Common.Models.AppSettings AppSettings{set;get;}
         string GetResponse(string url,string token);
         string PostResponse(string url,string body,string token);
         string PutResponse(string url,string token);
    }
}
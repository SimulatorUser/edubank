﻿using System.Collections.Generic;
using Infosys.EduBank.UserSL.Helper;

namespace Infosys.EduBank.UserSL.Interface
{
    public interface IGetAccountNumbersController
    {
        Common.Models.AppSettings AppSettings{set;get;}
        IUserHelper UserHelperObj { get; set; }

        string Get();
        List<string> Get(string userName);
    }
}
﻿using System.Collections.Generic;
using Infosys.EduBank.UserSL.Helper;

namespace Infosys.EduBank.UserSL.Interface
{
    public interface IGetDebitCardsController
    {
        Common.Models.AppSettings AppSettings{set;get;}
        IUserHelper UserHelperObj { get; set; }

        List<string> Get(int customerId);
        string Get();
    }
}
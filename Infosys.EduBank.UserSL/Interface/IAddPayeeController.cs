using Infosys.EduBank.UserSL.Helper;
using Infosys.EduBank.UserSL.Models;
namespace Infosys.EduBank.UserSL.Interface
{
    public interface IAddPayeeController
    {
        Common.Models.AppSettings AppSettings{set;get;}
        IUserHelper UserHelperObj { get; set; }

        string Get();
        bool Post(Models.PayeeDetails payeeDetails);

         
    }
}
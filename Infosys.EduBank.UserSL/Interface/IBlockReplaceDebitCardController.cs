using System.Collections.Generic;
using Infosys.EduBank.UserSL.Helper;

namespace Infosys.EduBank.UserSL.Interface
{
    public interface IBlockReplaceDebitCardController
    {
        Common.Models.AppSettings AppSettings{set;get;}
        IUserHelper UserHelperObj { get; set; }
        string Get();
        Common.Models.DebitCardDetails GetDebitCardDetails(string loginName, string accountNumber);
        bool SubmitReplaceRequest(int debitCardId, string reason);
        bool BlockDebitCard(int debitCardId);
        
    }
}
using System.Collections.Generic;

namespace Infosys.EduBank.UserSL.Interface
{
    public interface IGetPayeesController
    {
        Common.Models.AppSettings AppSettings{set;get;}
        IUserHelper UserHelperObj { get; set; }
        Common.IUtilities Utilities{get;set;}
        List<Models.PayeeDetails> Get(string userName);
        string Get();

    }
}
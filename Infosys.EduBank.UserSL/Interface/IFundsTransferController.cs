using Infosys.EduBank.UserSL.Helper;
using Infosys.EduBank.UserSL.Models;

namespace Infosys.EduBank.UserSL.Interface
{
    public interface IFundsTransferController
    {
        Common.Models.AppSettings AppSettings{set;get;}
        IUserHelper UserHelperObj { get; set; }

        string Get();
        string Post(FundsTransferRequest transferRequest);
    }
}
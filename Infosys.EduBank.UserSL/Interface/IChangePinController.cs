﻿using Infosys.EduBank.UserSL.Helper;
using Infosys.EduBank.UserSL.Models;

namespace Infosys.EduBank.UserSL.Interface
{
    public interface IChangePinController
    {
        Common.Models.AppSettings AppSettings{set;get;}
        IUserHelper UserHelperObj { get; set; }

        string Get();
        string Post(DebitCardPinDetails debitCardcredentials);
    }
}
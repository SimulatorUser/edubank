﻿using System.Collections.Generic;
using Infosys.EduBank.UserSL.Models;
using Infosys.EduBank.UserBL.Interface;
using Microsoft.AspNetCore.Http;

namespace Infosys.EduBank.UserSL.Interface
{
    public interface IUserHelper
    {
        Common.Models.AppSettings AppSettings{set;get;}
        IUserBLRepository UserBLRepositoryObj {set;get;}
        IHttpServiceHelper HttpServiceHelperobj{set;get;}
        bool ChangePassword(CustomerCredentials credentials);
        string ChangePin(DebitCardPinDetails changePinObj);
        List<string> GetAccountNumbers(string userName);
        List<string> GetDebitCards(int customerId);
        string GetAccountNumberByMappingId(int mappingId);
        bool VerifyName(int mappingId, string customerName);
        string GetToken(HttpContext context );
         bool TransferFunds(FundsTransferRequest transferRequest,string token);
         bool AddPayee(Models.PayeeDetails payeeDetails,string token);
        List<Models.PayeeDetails> GetPayees(string userName);
        Common.Models.DebitCardDetails GetDebitCardDetails(string loginName, string accountNumber);
        bool SubmitBlockRequest(int debitCardId);
        bool SubmitReplaceRequest(int debitCardId, string reason);
    }
}
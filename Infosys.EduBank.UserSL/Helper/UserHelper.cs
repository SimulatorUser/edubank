using System;
using System.Collections.Generic;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.UserBL;
using Infosys.EduBank.UserBL.Interface;
using Infosys.EduBank.UserSL.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Infosys.EduBank.UserSL.Models;
using Newtonsoft.Json;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.UserSL.Helper
{
    public class UserHelper : IUserHelper
    {
        public AppSettings AppSettings{set;get;}
        public IUserBLRepository UserBLRepositoryObj {set;get;}
        public IHttpServiceHelper HttpServiceHelperobj{set;get;}

        public UserHelper(Common.IUtilities util){
            AppSettings=JsonConvert.DeserializeObject<AppSettings>(util.Configuration);
            UserBLRepositoryObj=new UserBLRepository(util);
            HttpServiceHelperobj = new HttpServiceHelper(AppSettings);
        }
        public bool ChangePassword(Models.CustomerCredentials credentials){
            bool status=false;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
             if(UserBLRepositoryObj.ValidateUser(credentials.LoginName,credentials.OldPassword)){
                 status=UserBLRepositoryObj.ChangePasswordForUser(credentials.LoginName,credentials.NewPassword);
             }   
            }
            catch(Exception ex)
            {
                status=false;
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        public string ChangePin(Models.DebitCardPinDetails changePinObj){
            string response="Failed";
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                 //response=UserBLRepositoryObj.ChangePinForDebitCard(credentials.LoginName,credentials.NewPassword);
                int count = 0;
                        int difference;
                        
                        for (int i = 0; i < changePinObj.NewPin.Length - 1; i++)
                        {
                            difference = Convert.ToInt32(changePinObj.NewPin[i + 1].ToString()) - Convert.ToInt32(changePinObj.NewPin[i].ToString());

                            //Check whether the pin is in series
                            if (difference == 1 || difference == -1)
                            {
                                count++;
                            }
                        }
                        //Checks whether the old pin and new pin are same 
                        if (changePinObj.OldPin == changePinObj.NewPin)
                        {
                            response="Old and new pins are same";
                        }
                        else if (changePinObj.NewPin != changePinObj.ConfirmedPin)
                        {
                            response= "New and confirm pins are not same";
                        }
                        else if (count == 3)
                        {
                           response = "New pin is in series";
                        }
                        else{
                            response=UserBLRepositoryObj.ChangePinForUser(changePinObj.OldPin,changePinObj.NewPin,changePinObj.DebitCardNumber);
                        }
            }
            catch(Exception ex)
            {
                response="Failed";
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return response;
        }

        public List<string> GetDebitCards(int customerId){
            List<string> debitCardNumbers = new List<string>();
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                debitCardNumbers = UserBLRepositoryObj.GetDebitCards(customerId);
            }
            catch(Exception ex){
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                debitCardNumbers = null;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return debitCardNumbers;
        }

        public List<string> GetAccountNumbers(string userName){
            List<string> accountNumbers = new List<string>();
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                accountNumbers = UserBLRepositoryObj.GetAccountNumbers(userName);
            }
            catch(Exception ex){
                accountNumbers = null;
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return accountNumbers;
        }

        public string GetAccountNumberByMappingId(int mappingId)
        {
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                return UserBLRepositoryObj.GetAccountNumberWithMappingId(mappingId);
            }catch(Exception ex)
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                return string.Empty;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            
            
        }
         public string GetToken(HttpContext context )
        {
            return context.GetTokenAsync("access_token").Result;
        }

        public bool VerifyName(int mappingId, string customerName)
        {
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                return UserBLRepositoryObj.VerifyCardHolderName(mappingId,customerName);
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                return false;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
        }

        public bool AddPayee(Models.PayeeDetails payeeDetails,string token){
            bool status = false;
            try{
                string URL = "http://localhost:5101/Banking/ValidateAccount?accountNumber="+payeeDetails.AccountNumber+"&ifsc="+payeeDetails.IFSC;
                bool isAccountValid = Convert.ToBoolean(HttpServiceHelperobj.GetResponse(URL,token));
                
                
                if (isAccountValid){
                    var commonPayee = new Common.Models.Payee(){
                        AccountNumber = payeeDetails.AccountNumber,
                        NickName = payeeDetails.NickName,
                        UserName = payeeDetails.UserName,
                        PreferredAmount = payeeDetails.PreferredAmount
                    };

                    status = UserBLRepositoryObj.AddPayee(commonPayee);
                }
                else{
                    status = false;
                }
            }
            catch(Exception ex){
                status = false;
            }
            return status;
        }


        public List<Models.PayeeDetails> GetPayees(string userName){
            var payeeDetails = new List<Models.PayeeDetails>();
            var payeeList = new List<Common.Models.Payee>();
            try{

                payeeList=UserBLRepositoryObj.GetPayees(userName);
                foreach(var payee in payeeList){
                    payeeDetails.Add(new Models.PayeeDetails(){
                       NickName = payee.NickName,
                       AccountNumber = payee.AccountNumber,
                        PreferredAmount = Convert.ToDecimal(payee.PreferredAmount),
                    });
                }

            }
            catch(Exception ex){
                payeeDetails = null;
            }
            return payeeDetails;
        }
        public bool TransferFunds(FundsTransferRequest transferRequest,string token){
            bool status=false;
            string url;
            long userTranId,payeeTranId;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                bool payeeAccountValid,userAccountValid;
                url="http://localhost:5151/api/ValidateAccount/"+transferRequest.PayeeAccountNumber;
                payeeAccountValid=Convert.ToBoolean(HttpServiceHelperobj.GetResponse(url,token));
                url="http://localhost:5151/api/ValidateAccount/"+transferRequest.UserAccountNumber;
                userAccountValid=Convert.ToBoolean(HttpServiceHelperobj.GetResponse(url,token));
                if(payeeAccountValid && userAccountValid){
                    url = "http://localhost:5171/api/ViewBalance?accountNumber="+transferRequest.UserAccountNumber;
                    decimal balance=Convert.ToDecimal(HttpServiceHelperobj.GetResponse(url,token));
                    if(balance>=transferRequest.Amount){
                        InsertTransaction userTransaction=new InsertTransaction(){
                        AccountNumber=transferRequest.UserAccountNumber,
                        Amount=transferRequest.Amount,
                        Mode=Common.Constants.FundTransferTransactionMode,
                        Remarks=transferRequest.Remarks,
                        Info="Transfered To "+transferRequest.PayeeAccountNumber,
                        TellerId="NA",
                    };
                    url="http://localhost:5131/api/Debit/";
                    var serializedUserTran=JsonConvert.SerializeObject(userTransaction);
                    userTranId=Convert.ToInt64(HttpServiceHelperobj.PostResponse(url,serializedUserTran,token));

                    InsertTransaction payeeTransaction=new InsertTransaction(){
                        AccountNumber=transferRequest.PayeeAccountNumber,
                        Amount=transferRequest.Amount,
                        Mode=Common.Constants.FundTransferTransactionMode,
                        Remarks=transferRequest.Remarks,
                        Info="Transfered By "+transferRequest.UserAccountNumber,
                        TellerId="NA",
                    };
                    var serializedPayeeTran=JsonConvert.SerializeObject(payeeTransaction);
                    url="http://localhost:5131/api/Credit/";
                    payeeTranId=Convert.ToInt64(HttpServiceHelperobj.PostResponse(url,serializedPayeeTran,token));

                    if(payeeTranId!=0 && userTranId!=0){

                        status=true;
                    }
                    }
                } 
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;

        }
        //Gets the debit card details based on the loginName and accountNumber Minor Enhancement
        public Common.Models.DebitCardDetails GetDebitCardDetails(string loginName, string accountNumber)
        {
            Common.Models.DebitCardDetails debitCard = new Common.Models.DebitCardDetails();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                debitCard = UserBLRepositoryObj.GetDebitCardForLoginName(loginName, accountNumber);
            }
            catch (Exception ex)
            {
                debitCard = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return debitCard;
        }

        //Submits a request for replacing debit card Minor Enhancement
        public bool SubmitReplaceRequest(int debitCardId, string reason)
        {
            bool status = false;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                status = UserBLRepositoryObj.SubmitReplaceRequest(debitCardId, reason);
            }
            catch (Exception ex)
            {
                status = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        //Submits card blocking request
        public bool SubmitBlockRequest(int debitCardId)
        {
            bool status = false;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                status = UserBLRepositoryObj.SubmitBlockRequest(debitCardId);
            }
            catch (Exception ex)
            {
                status = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        
    }
}
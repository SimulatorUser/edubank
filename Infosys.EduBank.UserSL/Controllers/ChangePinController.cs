using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.UserSL.Models;
using Infosys.EduBank.UserSL.Helper;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.UserSL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;

namespace Infosys.EduBank.UserSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChangePinController : ControllerBase, IChangePinController
    {
        public IUserHelper UserHelperObj { get; set; }
        public AppSettings AppSettings{ get; set; }
        private Common.IUtilities Utilities{get;set;}

        public ChangePinController (Common.IUtilities util)
        {
            
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(util.Configuration);
            AppSettings=appSettings;
            Utilities=util;
            UserHelperObj=new UserHelper(util);
        }



        [Authorize(Roles = "User")]
        [HttpPost]
        public string Post(Models.DebitCardPinDetails debitCardcredentials)
        {
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            string response="Failed";
            try
            {
                if(ModelState.IsValid){
                       response= UserHelperObj.ChangePin(debitCardcredentials);
                }
                else{
                    response="Failed";
                }
            }
            catch (Exception ex)
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                response="Failed";
            }finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return response;
        }
         
        [HttpGet("Health")]
        public string Get()
        {
            return "Change Pin Service is Up And Running";
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            UserHelperObj.AppSettings.UserName=AppSettings.UserName;
            UserHelperObj.HttpServiceHelperobj.AppSettings.UserName=AppSettings.UserName;
            UserHelperObj.UserBLRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            // UserHelperObj.UserBLRepositoryObj.AccountRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            // UserHelperObj.UserBLRepositoryObj.CardRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            // UserHelperObj.UserBLRepositoryObj.CustomerRepositoryObj.AppSettings.UserName=AppSettings.UserName;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.UserSL.Models;
using Infosys.EduBank.UserSL.Helper;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.UserSL.Interface;
using System.Security.Claims;

namespace Infosys.EduBank.UserSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GetPayeesController: ControllerBase, IGetPayeesController
    {
        public IUserHelper UserHelperObj { get; set; }
        public AppSettings AppSettings{ get; set; }
        public Common.IUtilities Utilities{get;set;}
        public GetPayeesController (Common.IUtilities util)
        {
            Utilities=util;
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(Utilities.Configuration);
            AppSettings=appSettings;
            UserHelperObj=new UserHelper(util);
        }

        
        [Authorize(Roles = "User")]
        [HttpGet]
        public List<Models.PayeeDetails> Get(string userName)
        {
            ConfigureLog();
            List<Models.PayeeDetails> payees = new List<Models.PayeeDetails>();
            try
            {
                var token = UserHelperObj.GetToken(HttpContext);
                payees = UserHelperObj.GetPayees(userName);
                
            }
            catch (Exception)
            {
                payees = null;
            }
            return payees;
        }

        [HttpGet("Health")]
        public string Get()
        {
            return "Change Password Service is Up And Running";
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            UserHelperObj.AppSettings.UserName=AppSettings.UserName;
            UserHelperObj.HttpServiceHelperobj.AppSettings.UserName=AppSettings.UserName;
            UserHelperObj.UserBLRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            // UserHelperObj.UserBLRepositoryObj.AccountRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            // UserHelperObj.UserBLRepositoryObj.CardRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            // UserHelperObj.UserBLRepositoryObj.CustomerRepositoryObj.AppSettings.UserName=AppSettings.UserName;
        }
            
    }
        
}
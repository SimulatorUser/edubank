using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.UserSL.Models;
using Infosys.EduBank.UserSL.Helper;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.UserSL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;
namespace Infosys.EduBank.UserSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlockReplaceDebitCardController : ControllerBase, IBlockReplaceDebitCardController
    {
        public IUserHelper UserHelperObj { get; set; }
        public AppSettings AppSettings{ get; set; }
        private Common.IUtilities Utilities{get;set;}

        public BlockReplaceDebitCardController (Common.IUtilities util)
        {
            Utilities=util;
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(Utilities.Configuration);
            AppSettings=appSettings;
            UserHelperObj=new UserHelper(util);
        }
        //Gets the debit card details based on the loginName and accountNumber
        [Authorize(Roles = "User")]
        [HttpGet("{loginName}/{accountNumber}")]
        public Common.Models.DebitCardDetails GetDebitCardDetails(string loginName, string accountNumber)
        {
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            Common.Models.DebitCardDetails debitCard = new Common.Models.DebitCardDetails();
            try
            {
                debitCard = UserHelperObj.GetDebitCardDetails(loginName, accountNumber);
            }
            catch (Exception ex)
            {
                debitCard = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return debitCard;
        }

        //Submits a request for replacing debit card
        [HttpPost]
        [Authorize(Roles = "User")]
        public bool SubmitReplaceRequest(int debitCardId, string reason)
        {
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            bool status = false;
            try
            {
                status = UserHelperObj.SubmitReplaceRequest(debitCardId, reason);
            }
            catch (Exception ex)
            {
                status = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        //Submits a request for blocking debit card
        [HttpPost("block/{debitCardId}")]
        [Authorize(Roles = "User")]
        public bool BlockDebitCard(int debitCardId)
        {
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            bool status = false;
            try
            {
                status = UserHelperObj.SubmitBlockRequest(debitCardId);
            }
            catch (Exception ex)
            {
                status = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        // Return Health of the service
        [HttpGet("Health")]
        public string Get()
        {
            return "Block Replace Debit Card Service is Up And Running";
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            UserHelperObj.AppSettings.UserName=AppSettings.UserName;
            UserHelperObj.HttpServiceHelperobj.AppSettings.UserName=AppSettings.UserName;
            UserHelperObj.UserBLRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            // UserHelperObj.UserBLRepositoryObj.AccountRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            // UserHelperObj.UserBLRepositoryObj.CardRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            // UserHelperObj.UserBLRepositoryObj.CustomerRepositoryObj.AppSettings.UserName=AppSettings.UserName;
        }
    }
}
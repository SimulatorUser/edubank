namespace Infosys.EduBank.UserSL.Models{
    public class FundsTransferRequest{
        public string PayeeAccountNumber { get; set; }
        public string UserAccountNumber { get; set; }
        public decimal Amount { get; set; }
        public string Remarks {get; set;}
        public string UserName {get; set;}
    }
}
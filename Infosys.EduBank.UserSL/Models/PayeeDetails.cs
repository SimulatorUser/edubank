namespace Infosys.EduBank.UserSL.Models
{
    public class PayeeDetails
    {
        public string NickName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountHolderName { get; set; }
        public string IFSC { get; set; }
        public decimal PreferredAmount { get; set; }
        public string UserName {get;set;}
    }
}
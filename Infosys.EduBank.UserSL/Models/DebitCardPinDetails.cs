using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Infosys.EduBank.UserSL.Models
{
    public class DebitCardPinDetails
    {
        [Display(Name="Old Pin")]
        [Required(ErrorMessage = "Please enter the old pin")]
        [StringLength(4, MinimumLength = 4, ErrorMessage = "4 digits are required")]
        [RegularExpression(@"[0-9]{4}", ErrorMessage = "Password not in required format")]
        [DataType(DataType.Password)]
        public string OldPin { get; set; }

        [Display(Name = "New Pin")]
        [Required(ErrorMessage = "Please enter the new pin")]
        [StringLength(4, MinimumLength = 4, ErrorMessage = "4 digits are required")]
        [RegularExpression(@"[0-9]{4}", ErrorMessage = "Password not in required format")]
        [DataType(DataType.Password)]
        public string NewPin { get; set; }

        [Display(Name = "Confirm Pin")]
        [CompareAttribute("NewPin", ErrorMessage = "Passwords do not match")]
        [Required(ErrorMessage = "Please confirm the pin")]
        [StringLength(4, MinimumLength = 4, ErrorMessage = "4 digits are required")]
        [RegularExpression(@"[0-9]{4}", ErrorMessage = "Password not in required format")]
        public string ConfirmedPin { get; set; }

        [Required(ErrorMessage = "Please enter the Debit Card Number")]
        public string DebitCardNumber { get; set; }
    }
    }
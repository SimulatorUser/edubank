using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.TellerBL;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.TellerSL.Helper;
using Infosys.EduBank.Common;
using Infosys.EduBank.TellerSL.Interface;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;
namespace Infosys.EduBank.TellerSL.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DebitCardRequestsController : ControllerBase, IDebitCardRequestsController
    {        
        public ITellerHelper HelperObj{get; set;}
        public Common.Models.AppSettings AppSettings{get; set;}
        public DebitCardRequestsController(IUtilities util)
        {
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(util.Configuration);
            AppSettings=appSettings;
            HelperObj = new TellerHelper(util);
        }
        [HttpGet("Health")]
        public string Get()
        {
            return "Debit Cards Request Service is Up And Running";
        }
        //Minor Enhancement
        //Gets the list of debit card requests with status as pending
        [HttpGet("GetPendingDebitCardRequests")]
        [Authorize(Roles = "Teller")]
        public List<DebitCardRequest> GetPendingDebitCardRequests()
        {
            List<DebitCardRequest> debitCardRequests = new List<DebitCardRequest>();
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                debitCardRequests = HelperObj.GetPendingDebitCardRequests();
            }
            catch (Exception ex)
            {
                debitCardRequests = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return debitCardRequests;
        }
        
        //Updates the status of debit card as Approved or Rejected based on teller action
        [HttpPost]
        [Authorize(Roles = "Teller")]
        public Common.Models.Customer UpdateDebitCardStatus([FromBody]DebitCardRequest debitCardRequest,string tellerId)
        {
            Common.Models.Customer custobj = null;
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                custobj = HelperObj.UpdateDebitCardStatus(debitCardRequest, tellerId);
            }
            catch (Exception ex)
            {
                custobj = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return custobj;
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            HelperObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.HttpServiceHelperObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.AccountRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.CardRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.CustomerRepositoryObj.AppSettings.UserName=AppSettings.UserName;
        }
    }
}

using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.TellerBL;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.TellerSL.Helper;
using Infosys.EduBank.Common;
using Infosys.EduBank.TellerSL.Interface;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;

namespace Infosys.EduBank.TellerSL.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountActiveController : Controller, IAccountActiveController
    {   
        public ITellerHelper HelperObj{get; set;}
        public Common.Models.AppSettings AppSettings{get; set;}
        public AccountActiveController(IUtilities util)
        {
             var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(util.Configuration);
            AppSettings=appSettings;
            HelperObj = new TellerHelper(util);
            
        }

        //Gets the details of all the accounts
        [HttpGet("AccountDetails")]
        [Authorize(Roles = "Teller")]
        public JsonResult GetAllAccountDetails()
        {
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            List<Common.Models.Account> accounts = new List<Common.Models.Account>();
            try
            {
                accounts = HelperObj.FetchAllAccountDetails();
            }
            catch (System.Exception ex)
            {
                accounts = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return Json(accounts);
        }

        //Makes an account active/inactive
        [HttpPut]
        [Authorize(Roles = "Teller")]
        public JsonResult UpdateAccountStatus(TellerSL.Models.Account account)
        {
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            bool status = false;
            try
            {
                // var commonAccount = new Common.Models.Account(){
                //     AccountId =account.AccountId,
                //     AccountNumber =account.AccountNumber,
                //     BranchId =account.BranchId,
                //     IsActive = account.IsActive,
                //     CreatedTimeStamp = account.CreatedTimestamp,
                //     CreatedBy = account.CreatedBy,
                //     status = account.Status,
                //     ModifiedTimestamp = account.ModifiedTimestamp
                // };

                status = HelperObj.ToggleAccountStatus(account.AccountNumber, account.IsActive);
            }
            catch (System.Exception ex)
            {
                status = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return Json(status);
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            HelperObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.HttpServiceHelperObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.AccountRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.CardRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.CustomerRepositoryObj.AppSettings.UserName=AppSettings.UserName;
        }
    }
}
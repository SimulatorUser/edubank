﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.TellerSL.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Infosys.EduBank.TellerSL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;

namespace Infosys.EduBank.TellerSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TellerController : ControllerBase, ITellerController
    {

        public ITellerHelper TellerHelper { get; set; }
        public Common.Models.AppSettings AppSettings{get; set;}

        public TellerController(Common.IUtilities util){
            
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(util.Configuration);
            AppSettings=appSettings;
            TellerHelper = new TellerHelper(util);
        }


        [HttpGet("Index")]
        [Authorize(Roles = "Teller,User")]
        public async Task<string> Index()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            return accessToken;
        }


        // GET api/values
        [AllowAnonymous]
        [HttpGet]
        public List<string> Get(string accountNumber)
        {
            ConfigureLog();
            List<string> accDetails=new List<string>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
            
            accDetails=TellerHelper.FetchAccountDetails(accountNumber);
            }
            catch(Exception ex){
                accDetails=null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return accDetails;
        }

        // GET api/values/5
        [HttpGet("Health")]
        public ActionResult<string> Get()
        {
            return "Teller Service is Up And Running";
        }

        // POST api/values
        [Authorize(Roles = "Teller")]
        [HttpPost]
        public Common.Models.Customer Post(TellerSL.Models.Customer customer)
        {
            Common.Models.Customer customerdetails=null;
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                var commonCustomer = new Common.Models.Customer(){
                    Branch = customer.Branch,
                    EmailId = customer.EmailId,
                    TellerId = customer.TellerId,
                    Name = customer.Name,
                    DateOfBirth = customer.DateOfBirth,
                    UUID = customer.UUID,
                    IsAddrVerified = customer.IsAddrVerified,
                    Password = customer.Password,
                    CVV = customer.CVV,
                    Pin = customer.Pin,
                    LoginName = customer.LoginName,
                    AccountNumber = customer.AccountNumber,
                    DebitCardNumber = customer.DebitCardNumber,
                    ValidFrom = customer.ValidFrom,
                    ValidThru = customer.ValidThru
                };
                customerdetails= TellerHelper.RegisterUser(commonCustomer);
            }
            catch(Exception ex){
                customerdetails = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return customerdetails;
        }

        // PUT api/values/5
        [HttpGet("Branches")]
        [Authorize(Roles="Teller")]
        public List<Common.Models.Branch> GetAllBranches()
        {
            var output=new List<Common.Models.Branch>();
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            { 
                output=TellerHelper.AllBranches();

            }
            catch(Exception ex)
            {
                output=new List<Common.Models.Branch>();
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);

            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return output;
        }

        [HttpPost("Interest")]
        [Authorize(Roles="Teller")]
        public Dictionary<string,bool> AddInterest(string tellerId)
        {
            ConfigureLog();
            Dictionary<string,bool> result=new Dictionary<string, bool>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                var token=TellerHelper.GetToken(HttpContext);
                result=TellerHelper.AddInterests(token,tellerId);
            }
            catch(Exception ex)
            {
                result=new Dictionary<string, bool>();
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return result;
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            TellerHelper.AppSettings.UserName=AppSettings.UserName;
            TellerHelper.HttpServiceHelperObj.AppSettings.UserName=AppSettings.UserName;
            TellerHelper.TellerBLObj.AppSettings.UserName=AppSettings.UserName;
            TellerHelper.TellerBLObj.AccountRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            TellerHelper.TellerBLObj.CardRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            TellerHelper.TellerBLObj.CustomerRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            
        }
    }
}

using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.TellerBL;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.TellerSL.Helper;
using Infosys.EduBank.Common;
using Infosys.EduBank.TellerSL.Interface;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;

namespace Infosys.EduBank.TellerSL.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountStatusController : Controller, IAccountStatusController
    {        
        public ITellerHelper HelperObj{get; set;}
        public Common.Models.AppSettings AppSettings{get; set;}

        public AccountStatusController(IUtilities util)
        {
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(util.Configuration);
            AppSettings=appSettings;
            HelperObj = new TellerHelper(util);
        }

        [HttpGet]
        [Authorize(Roles = "Teller")]
        public JsonResult FetchAccountsByStatus(string status)
        {
            List<Account> accounts = new List<Account>();
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                accounts = HelperObj.FetchAccountsByStatus(status);
            }
            catch(Exception ex)
            {
                accounts = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return Json(accounts);
        }

        [HttpPut]
        [Authorize(Roles="Teller")]
        public JsonResult UpdateAccountStatus(TellerSL.Models.Account account)
        {
            bool status = false;
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                var commonAccount = new Common.Models.Account(){
                    AccountId =account.AccountId,
                    AccountNumber =account.AccountNumber,
                    BranchId =account.BranchId,
                    IsActive = account.IsActive,
                    CreatedTimestamp = account.CreatedTimestamp,
                    CreatedBy = account.CreatedBy,
                    Status = account.Status,
                    ModifiedTimestamp = account.ModifiedTimestamp
                };
                status = HelperObj.UpdateAccountStatus(commonAccount);
            }
            catch(Exception ex)
            {
                status = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return Json(status);
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            HelperObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.HttpServiceHelperObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.AccountRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.CardRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            HelperObj.TellerBLObj.CustomerRepositoryObj.AppSettings.UserName=AppSettings.UserName;
        }
    }
}


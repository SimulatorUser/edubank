﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Infosys.EduBank.AuthenticationSL.Helper;
//using Infosys.EduBank.AuthenticationSL.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Infosys.EduBank.Common;
using Microsoft.AspNetCore.Http;
using Infosys.EduBank.TellerSL.Helper;
using System.Diagnostics.CodeAnalysis;


namespace Infosys.EduBank.TellerSL
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();//.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddCors();
            //var appSettingsSection = Configuration.GetSection("AppSettings");
            //services.Configure<Common.Models.AppSettings>(appSettingsSection);
            // configure jwt authentication
            //var appSettings = appSettingsSection.Get<Common.Models.AppSettings>();
            //services.AddSingleton(appSettings);
            IUtilities utilities = new Utilities();
            var settings=new Common.Models.AppSettings()
            {
                ErrorLogStatus=false,
                InfoLogStatus=false,
                WarningLogStatus=false
            };
            utilities.Configuration= new HttpServiceHelper(settings).GetResponse(utilities.GetValueFromAppSettings(Constants.ConfigUrl),string.Empty);
             var key = Encoding.ASCII.GetBytes(utilities.GetValueFromAppSettings(Constants.AuthSecret));
             services.AddAuthentication(x =>
             {
                 x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                 x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
             })
             .AddJwtBearer(x =>
             {
                 x.RequireHttpsMetadata = false;
                 x.SaveToken = true;
                 x.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateIssuerSigningKey = true,
                     IssuerSigningKey = new SymmetricSecurityKey(key),
                     ValidateIssuer = false,
                     ValidateAudience = false
                 };
             });
             services.AddHttpContextAccessor();
             services.AddSingleton(utilities);
            // configure DI for application services
            //services.AddScoped<IUserService, UserService>();
            //services.AddScoped(Appsettings);
            //services.AddScoped<AuthenticationController>(z =>new AuthenticationController(new Helper.AuthenticationHelper(appSettings)));            
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}

using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System;
using System.Net;
using System.IO;
using System.Diagnostics.CodeAnalysis;
using Infosys.EduBank.TellerSL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.TellerSL.Helper
{
    [ExcludeFromCodeCoverage]
    public class HttpServiceHelper:IHttpServiceHelper
    {
        public Common.Models.AppSettings AppSettings{get; set;}
        private static bool AcceptAllCertifications(object sender,
        System.Security.Cryptography.X509Certificates.X509Certificate certification,
        System.Security.Cryptography.X509Certificates.X509Chain chain,
        System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
        return true;
        }
        public HttpServiceHelper(Common.Models.AppSettings settings)
        {
            AppSettings=settings;
        }
        public string GetResponse(string url,string token)
        {
            try
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Headers.Add("Content-Type","application/json");
                
                httpWebRequest.Headers.Add("Authorization", "Bearer "+token);
               httpWebRequest.ServerCertificateValidationCallback=new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                return streamReader.ReadToEnd();
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                return "Exception raised while calling other service";
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            
        }

        public string PostResponse(string url,string body,string token)
        {
            try
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Headers.Add("Content-Type","application/json");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(body);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                httpWebRequest.Headers.Add("Authorization", "Bearer "+token);
                httpWebRequest.ServerCertificateValidationCallback=new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                return streamReader.ReadToEnd();
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                return "Exception raised while calling other service";
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
        }

    }
}
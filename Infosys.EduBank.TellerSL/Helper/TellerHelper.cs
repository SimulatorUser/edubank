using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Infosys.EduBank.TellerBL;
using Infosys.EduBank.TellerBL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using Infosys.EduBank.TellerSL.Interface;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Authentication;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.TellerSL.Helper{

    public class TellerHelper : ITellerHelper
    {

        public ITellerBLRepository TellerBLObj { get; set; }
        public Common.Models.AppSettings AppSettings { get; set; }
        public IHttpServiceHelper HttpServiceHelperObj{get; set;}
        public TellerHelper(Common.IUtilities util){
            AppSettings = JsonConvert.DeserializeObject<Common.Models.AppSettings>(util.Configuration);
            TellerBLObj = new TellerBLRepository(util);
            HttpServiceHelperObj= new HttpServiceHelper(AppSettings);
        }

        public Common.Models.Customer RegisterUser(Common.Models.Customer customer){
            // Call BL methods for generating credentials, acc. no and debit card
            bool status=false;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                long customerId = TellerBLObj.CreateCustomer(customer);
                if (TellerBLObj.CreateCredentials(ref customer,customerId)){
                    var accountCustomerMappingId = TellerBLObj.CreateAccount(customerId,ref customer);
                    if (accountCustomerMappingId != 0){
                        var debitCardNo = TellerBLObj.GenerateDebitCard(accountCustomerMappingId,ref customer);
                        if (debitCardNo != null){
                            int failCount = 0;
                            while(status!=true && failCount<3){
                                status=TellerBLObj.CommitInsertions(customerId,accountCustomerMappingId,debitCardNo);
                                failCount++;
                            };
                        }
                        if(status){
                            return customer;
                        }
                        else{
                            return null ;
                        }
                            
                    }
                }
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                return null;
               
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return customer;
        }
        public List<string> FetchAccountDetails(String accountNumber){
            List<string> Data=new List<string>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                Data=TellerBLObj.GetAccountDetails(accountNumber);
            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                return null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return Data;
        }
        public List<Common.Models.Branch> AllBranches()
        {
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                return TellerBLObj.GetAllBranches();
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                return null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
        }

        #region  Exercises
        
        //Gets all the accounts having the given status
        public List<Common.Models.Account> FetchAccountsByStatus(string status)
        {
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            List<Common.Models.Account> accounts = new List<Common.Models.Account>();
            try
            {
                accounts = TellerBLObj.GetAccountsByStatus(status);
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                accounts = null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return accounts;
        }
        
        //Updates the Status property of the given account
        public bool UpdateAccountStatus(Common.Models.Account account)
        {
            bool status = false;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                status = TellerBLObj.UpdateAccountStatus(account);
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status = false;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        
        //Fetches the details of all the accounts
        public List<Common.Models.Account> FetchAllAccountDetails()
        {
            List<Common.Models.Account> accounts = new List<Common.Models.Account>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                accounts = TellerBLObj.GetAllAccountDetails();
            }
            catch (System.Exception ex)
            {
                accounts = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return accounts;
        }
        
        public bool ToggleAccountStatus(string accountNumber, bool newStatus)
        {
            bool status = false;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                status = TellerBLObj.ToggleAccountStatus(accountNumber,newStatus);   
            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status = false;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        public Dictionary<string,bool> AddInterests(string token,string tellerId)
        {
            Dictionary<string,bool> output=new Dictionary<string, bool>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                var allAccounts=TellerBLObj.GetAllAccountDetails();
                foreach(var acc in allAccounts)
                {
                    //acc.AccountNumber;
                    string URL = "http://localhost:5171/api/ViewBalance/Balance/"+acc.AccountNumber;
                    string result=HttpServiceHelperObj.GetResponse(URL,token);
                    if(!string.IsNullOrEmpty(result))
                    {
                        var allBalance = JsonConvert.DeserializeObject<Dictionary<DateTime,decimal>>(result);
                        if(allBalance!=null && allBalance.Count()!=0)
                        {
                            var interest=TellerBLObj.InterestCalculate(allBalance);
                            string creditUrl = "http://localhost:5131/api/Credit";
                            var creditObj= new Common.Models.InsertTransaction()
                            {
                                IFSC=null,
                                AccountNumber=acc.AccountNumber,
                                AccountName="NA",
                                Amount=Convert.ToDecimal(interest),
                                TellerId=tellerId,
                                
                                Remarks ="Money added by bank as interest",
                                Info=Common.Constants.EduBankInterestConstant,
                                Mode=Common.Constants.InterestTransactionMode
                            };
                            var creditStatus=HttpServiceHelperObj.PostResponse(creditUrl,JsonConvert.SerializeObject(creditObj),token);
                            output.Add(acc.AccountNumber,Convert.ToDouble(creditStatus)>0);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                output=new Dictionary<string, bool>();
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return output;
        }
        //Minor Enhancement
        //Gets the list of debit card requests with status as pending   
        public List<Common.Models.DebitCardRequest> GetPendingDebitCardRequests()
        {
            List<Common.Models.DebitCardRequest> debitCardRequests = new List<Common.Models.DebitCardRequest>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                debitCardRequests = TellerBLObj.GetPendingDebitCardRequests();
            }
            catch (Exception ex)
            {
                debitCardRequests = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return debitCardRequests;
        }
        //Minor Enhancement
        //Updates the status of debit card as Approved or Rejected based on teller action
        public Common.Models.Customer UpdateDebitCardStatus(Common.Models.DebitCardRequest debitCardRequest, string tellerId)
        {
            Common.Models.Customer custObj = null;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                custObj = TellerBLObj.UpdateDebitCardRequestStatus(debitCardRequest, tellerId);
            }
            catch (Exception ex)
            {
                custObj = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return custObj;
        }
        public string GetToken(HttpContext context)
        {
            return context.GetTokenAsync("access_token").Result;
        }

        #endregion
    }
}
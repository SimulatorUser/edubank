namespace Infosys.EduBank.TellerSL.Models
{
    public class Customer
    {        
        public string Branch { get; set; }
        
        public string EmailId { get; set; }

        public string TellerId { get; set; }

        public string Name { get; set; }        
       
        public System.DateTime DateOfBirth { get; set; }   
        
        public string UUID { get; set; }

        public bool IsAddrVerified { get; set; }

        public string Password { get; set; }

        public string CVV { get; set; }

        public string Pin { get; set; }

        public string LoginName { get; set; }

        public string AccountNumber { get; set; }

        public string DebitCardNumber { get; set; }

        public System.DateTime ValidFrom { get; set; }

        public System.DateTime ValidThru { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TellerSL.Helper;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.TellerSL.Interface
{
    public interface ITellerController
    {
        ITellerHelper TellerHelper { get; set; }
        AppSettings AppSettings{set;get;}
        List<string> Get(string accountNumber);
        List<Branch> GetAllBranches();
        ActionResult<string> Get();
        Customer Post(TellerSL.Models.Customer customer);
    }
}
﻿using System.Collections.Generic;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TellerBL;
using Infosys.EduBank.TellerBL.Interface;
using Microsoft.AspNetCore.Http;

namespace Infosys.EduBank.TellerSL.Interface
{
    public interface ITellerHelper
    {
        ITellerBLRepository TellerBLObj { get; set; }
        AppSettings AppSettings{get;set;}
        IHttpServiceHelper HttpServiceHelperObj{get; set;}
        List<Branch> AllBranches();
        List<string> FetchAccountDetails(string accountNumber);
        List<Account> FetchAccountsByStatus(string status);
        Customer RegisterUser(Common.Models.Customer customer);
        bool UpdateAccountStatus(Common.Models.Account account);
        List<Common.Models.Account> FetchAllAccountDetails();
        bool ToggleAccountStatus(string accountNumber, bool newStatus);
        Dictionary<string,bool> AddInterests(string token,string tellerId);
        List<Common.Models.DebitCardRequest> GetPendingDebitCardRequests();
        Common.Models.Customer UpdateDebitCardStatus(DebitCardRequest debitCardRequest, string tellerId);
        string GetToken(HttpContext context );

    }
}
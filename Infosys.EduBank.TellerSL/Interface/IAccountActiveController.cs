using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TellerSL.Helper;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.TellerSL.Interface
{
    public interface IAccountActiveController
    {
        AppSettings AppSettings{set;get;}
        ITellerHelper HelperObj { get; set; }

        JsonResult GetAllAccountDetails();

        JsonResult UpdateAccountStatus(TellerSL.Models.Account account);
    }
}
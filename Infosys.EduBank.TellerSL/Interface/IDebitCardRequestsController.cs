using System;
using System.Collections.Generic;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TellerSL.Helper;
using Microsoft.AspNetCore.Mvc;
namespace Infosys.EduBank.TellerSL.Interface
{
    public interface IDebitCardRequestsController
    {
        AppSettings AppSettings{set;get;}
        ITellerHelper HelperObj { get; set; }
        List<DebitCardRequest> GetPendingDebitCardRequests();
        Common.Models.Customer UpdateDebitCardStatus(DebitCardRequest debitCardRequest, string tellerId);
    }
}
﻿using Infosys.EduBank.Common.Models;
using Infosys.EduBank.DebitCardSL.Helper;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.DebitCardSL.Interface
{
    public interface IDebitCardPaymentController
    {
        AppSettings AppSettings{set;get;}
        IDebitCardHelper DebitCardHelperObj { get; set; }
         ActionResult<string> Get();

        string Post([FromBody] Models.DebitCardTransaction debitCardTransaction);
       
    }
}
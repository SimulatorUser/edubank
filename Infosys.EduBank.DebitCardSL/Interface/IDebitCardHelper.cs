﻿using Infosys.EduBank.Common.Models;
using Infosys.EduBank.DebitCardBL;
using Infosys.EduBank.DebitCardBL.Interface;
using Microsoft.AspNetCore.Http;
namespace Infosys.EduBank.DebitCardSL.Interface
{
    public interface IDebitCardHelper
    {
        AppSettings AppSettings{set;get;}
        IDebitCardBLRepository DebitCardBLObj { get; set; }
        IHttpServiceHelper HttpServiceHelperobj{set;get;}
        string DebitUsingDebitCard(DebitCardTransaction debitCardTransaction, string token);
        int VerifyDebitCard(DebitCardDetails cardDetails, string token);
        string GetToken(HttpContext context);
    }
}
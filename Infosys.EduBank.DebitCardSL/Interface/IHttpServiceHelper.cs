using System.Security.Claims;
using System;
using System.Net;
using System.IO;
namespace Infosys.EduBank.DebitCardSL.Interface
{
    public interface IHttpServiceHelper
    {
        Common.Models.AppSettings AppSettings{set;get;}
         string GetResponse(string url,string token);
    }
}
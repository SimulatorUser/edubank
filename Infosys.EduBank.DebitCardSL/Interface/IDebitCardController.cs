﻿using Infosys.EduBank.Common.Models;
using Infosys.EduBank.DebitCardSL.Helper;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.DebitCardSL.Interface
{
    public interface IDebitCardController
    {
        AppSettings AppSettings{set;get;}
        IDebitCardHelper DebitCardHelperObj { get; set; }

        string Get([FromBody] Models.DebitCardDetails cardDetails);
        ActionResult<string> Get(int id);

    }
}
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Infosys.EduBank.DebitCardBL;
using Infosys.EduBank.DebitCardBL.Interface;
using Infosys.EduBank.Common.Models;
using System;
using System.IO;
using System.Net;
using Infosys.EduBank.DebitCardSL.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Infosys.EduBank.Common;
using Newtonsoft.Json;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.DebitCardSL.Helper{

    public class DebitCardHelper : IDebitCardHelper
    {
        public Common.Models.AppSettings AppSettings{get; set;}
        public IDebitCardBLRepository DebitCardBLObj { get; set; }
        public IHttpServiceHelper HttpServiceHelperobj{set;get;}

        public DebitCardHelper(IUtilities utilities){

            DebitCardBLObj = new DebitCardBLRepository(utilities);
            AppSettings=JsonConvert.DeserializeObject<Common.Models.AppSettings>(utilities.Configuration);
            HttpServiceHelperobj=new HttpServiceHelper(AppSettings);
        }
        public int VerifyDebitCard(Common.Models.DebitCardDetails cardDetails,string token){
            int accountCustomerMappingId = 0;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                int mappingId = DebitCardBLObj.VerifyCard(cardDetails);
                if (mappingId != 0){
                    // var status=Convert.ToBoolean( 
                    //     HttpServiceHelperobj.GetResponse(
                    //         string.Format("http://localhost:5181/api/GetDebitCards/{0}/{1}"
                    //         ,mappingId
                    //         ,cardDetails.CardHolderName
                    //         )
                    //         ,token) );
                    //if (status)
                    accountCustomerMappingId = mappingId;
                }   
            }
            catch(Exception ex){
                accountCustomerMappingId=0;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return accountCustomerMappingId;
        }
        public string GetToken(HttpContext context )
        {
            return context.GetTokenAsync("access_token").Result;
        }

        public string DebitUsingDebitCard(DebitCardTransaction debitCardTransaction,string token){
            string responseCode = string.Empty;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                int accountMappingId = VerifyDebitCard(debitCardTransaction.DebitCard,token);
                if (accountMappingId !=0 ){
                    
                    string accountNumber=HttpServiceHelperobj.GetResponse(string.Format("http://localhost:5181/api/GetDebitCards/MappingId/{0}",accountMappingId),token);
                    string URL = "http://localhost:5171/api/ViewBalance?accountNumber="+accountNumber;
                    var result= HttpServiceHelperobj.GetResponse(URL,token);
                    decimal balance=Convert.ToDecimal(result);

                    if (balance > debitCardTransaction.Amount){ // Check for Balance Using Transaction SL
                        // if sufficient, call Transaction BL for inserting transaction
                        responseCode=DebitCardBLObj.DebitUsingDebitCard(debitCardTransaction,accountNumber);
                    }
                    else{
                        responseCode = "INSUFFICIENT BALANCE";
                    }
                }
                else{
                    responseCode = "FAILURE";
                }
            }
            catch(Exception ex){
                responseCode = "FAILURE";
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return responseCode;
        }

        
    }
}
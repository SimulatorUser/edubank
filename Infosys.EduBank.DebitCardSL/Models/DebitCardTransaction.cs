namespace Infosys.EduBank.DebitCardSL.Models
{
    public class DebitCardTransaction
    {
        public DebitCardDetails DebitCard { get; set; }

        public decimal Amount { get; set; }

        public string Remarks { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.DebitCardSL.Helper;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Infosys.EduBank.DebitCardSL.Interface;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.Common;
using System.Security.Claims;

namespace Infosys.EduBank.DebitCardSL.Controllers
{
    [Route("api/paymentgateway/[controller]")]
    [ApiController]
    public class DebitCardPaymentController : ControllerBase, IDebitCardPaymentController
    {
        public IDebitCardHelper DebitCardHelperObj { get; set; }
        public Common.Models.AppSettings AppSettings{get; set;}
        public DebitCardPaymentController(Common.IUtilities util)
        {
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(util.Configuration);
            AppSettings=appSettings;
            DebitCardHelperObj = new DebitCardHelper(util);
        }

        // GET api/values/5
        [Authorize(Roles = "User")]
        [HttpGet("Health")]
        public ActionResult<string> Get()
        {
            return "Debit Card Payment Service is Up And Running";
        }

        // POST api/values
        [Authorize(Roles = "User")]
        [HttpPost]
        public string Post([FromBody]Models.DebitCardTransaction debitCardTransaction)
        {
            string responseCode = string.Empty;
            ConfigureLog();
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                string token= DebitCardHelperObj.GetToken(HttpContext);
                var modelObj= new Common.Models.DebitCardTransaction(){
                    Amount=debitCardTransaction.Amount,
                    DebitCard=new DebitCardDetails(){
                        DebitCardNumber=debitCardTransaction.DebitCard.DebitCardNumber,
                        CVV=debitCardTransaction.DebitCard.CVV,
                        ValidThru=debitCardTransaction.DebitCard.ValidThru,
                        Pin=debitCardTransaction.DebitCard.Pin,
                        CardHolderName=debitCardTransaction.DebitCard.CardHolderName
                    }, 
                    Remarks =debitCardTransaction.Remarks
                };
                responseCode = DebitCardHelperObj.DebitUsingDebitCard(modelObj,token);
            }
            catch(Exception ex){
                responseCode = "FAILURE";
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,1,ex);
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return responseCode;
        }
        
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            DebitCardHelperObj.AppSettings.UserName=AppSettings.UserName;
            DebitCardHelperObj.HttpServiceHelperobj.AppSettings.UserName=AppSettings.UserName;
            DebitCardHelperObj.DebitCardBLObj.AppSettings.UserName=AppSettings.UserName;
            DebitCardHelperObj.DebitCardBLObj.CardRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            DebitCardHelperObj.DebitCardBLObj.TransactionRepositoryObj.AppSettings.UserName=AppSettings.UserName;


        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.DebitCardSL.Helper;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.DebitCardSL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;

namespace Infosys.EduBank.DebitCardSL.Controllers
{
    [Route("api/paymentgateway/[controller]")]
    [ApiController]
    public class DebitCardController : ControllerBase, IDebitCardController
    {
        public IDebitCardHelper DebitCardHelperObj { get; set; }
        public Common.Models.AppSettings AppSettings{get; set;}
        public DebitCardController(Common.IUtilities util)
        {
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(util.Configuration);
            AppSettings=appSettings;
            DebitCardHelperObj = new DebitCardHelper(util);
        }
        // GET api/values
        [Authorize(Roles = "User")]
        [HttpGet]
        public string Get([FromBody]Models.DebitCardDetails cardDetails)
        {
            ConfigureLog();
            string responseCode = string.Empty;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                var modelObj= new Common.Models.DebitCardDetails(){
                    DebitCardNumber=cardDetails.DebitCardNumber,
                    CVV=cardDetails.CVV,
                    ValidThru=cardDetails.ValidThru,
                    Pin=cardDetails.Pin,
                    CardHolderName=cardDetails.CardHolderName
                };
                string token= DebitCardHelperObj.GetToken(HttpContext);
                if(DebitCardHelperObj.VerifyDebitCard(modelObj,token) != 0){
                    responseCode = "SUCCESS";
                }
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                responseCode = "FAILURE";
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return responseCode;
        }

        // GET api/values/5
        [HttpGet("Health")]
        public ActionResult<string> Get(int id)
        {
            return "Debit Card Service is Up And Running";
        }
        
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            DebitCardHelperObj.AppSettings.UserName=AppSettings.UserName;
            DebitCardHelperObj.HttpServiceHelperobj.AppSettings.UserName=AppSettings.UserName;
            DebitCardHelperObj.DebitCardBLObj.AppSettings.UserName=AppSettings.UserName;
            DebitCardHelperObj.DebitCardBLObj.CardRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            DebitCardHelperObj.DebitCardBLObj.TransactionRepositoryObj.AppSettings.UserName=AppSettings.UserName;
        }

    }
}

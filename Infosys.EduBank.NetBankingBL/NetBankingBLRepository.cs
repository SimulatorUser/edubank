﻿using System;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.AccountDAL.Repository;
using Infosys.EduBank.TransactionDAL.Repository;
using Infosys.EduBank.NetBankingBL.Interface;
using Infosys.EduBank.TransactionDAL.Interface;
using Infosys.EduBank.AccountDAL.Interface;
namespace Infosys.EduBank.NetBankingBL
{
    public class NetBankingBLRepository : INetBankingBLRepository
    {
        public AppSettings AppSettings { get; set; }

        public IAccountRepository AccountRepositoryObj { get; set; }
        public ITransactionRepository TransactionRepositoryObj {set;get;}
        public NetBankingBLRepository(AppSettings appSettings)
        {
            AppSettings = appSettings;
            AccountRepositoryObj = new AccountRepository(appSettings);
            TransactionRepositoryObj=new TransactionRepository(appSettings);

        }

        public bool VerifyAccount(string accountNumber,string ifscCode){
            bool status=false;
            try{
                if(AccountRepositoryObj.isAccountAndBranchValid(accountNumber,ifscCode)){
                    status=true;
                }
            }
            catch(Exception ex){
                status = false;
            }
            return status;
        }
        public bool AccountExist(string accountNumber)
        {
            bool status=false;
            try{
                if(AccountRepositoryObj.isAccountPresent(accountNumber)){
                    status=true;
                }
            }
            catch(Exception ex){
                status = false;
            }
            return status;
        }
        
        public bool DebitMoneyUsingNetBanking(Common.Models.NetBankingDetails bankingDetails,string mode){
            bool status =false;
            try{
                InsertTransaction payToBankObj =new InsertTransaction();
                payToBankObj.AccountNumber=bankingDetails.AccountNumber;
                payToBankObj.Amount=bankingDetails.Amount;
                payToBankObj.IFSC=bankingDetails.IFSC;
                payToBankObj.TellerId="1";
                payToBankObj.Remarks=bankingDetails.Remarks;
                payToBankObj.Info=mode+"Payment";
                payToBankObj.Mode=Common.Constants.NetBankingTransactionMode;
                if(TransactionRepositoryObj.InsertTransaction(payToBankObj,0)!=0){
                    status=true;
                }
                
            }
            catch(Exception ex){
                    status =false;
            }
            return status;
        }
    }
}

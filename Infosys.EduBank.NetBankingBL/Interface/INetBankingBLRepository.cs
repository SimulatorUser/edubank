﻿using Infosys.EduBank.AccountDAL.Repository;
using Infosys.EduBank.AccountDAL.Interface;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TransactionDAL.Repository;
using Infosys.EduBank.TransactionDAL.Interface;
namespace Infosys.EduBank.NetBankingBL.Interface
{
    public interface INetBankingBLRepository
    {
        IAccountRepository AccountRepositoryObj { get; set; }
        AppSettings AppSettings { get; set; }
        ITransactionRepository TransactionRepositoryObj { get; set; }

        bool AccountExist(string accountNumber);
        bool DebitMoneyUsingNetBanking(NetBankingDetails bankingDetails, string mode);
        bool VerifyAccount(string accountNumber, string ifscCode);
    }
}
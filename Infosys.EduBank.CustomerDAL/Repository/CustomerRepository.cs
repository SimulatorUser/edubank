using Infosys.EduBank.CustomerDAL.Models;
using Microsoft.EntityFrameworkCore;
using Infosys.EduBank.CustomerDAL.Interface;
using System;
using System.Linq;
using Infosys.EduBank.Common.Models;
using System.Data.SqlClient;
using System.Collections.Generic;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.CustomerDAL.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        public Common.Models.AppSettings AppSettings{get; set;}
        private ICustomerContext context;
        public ICustomerContext Context{
            get{ return context;}
            set{context=value;}
        }
        public CustomerRepository(Common.Models.AppSettings settings){
            context=new CustomerContext(settings.UserConnection);
            AppSettings=settings;
        }
       // [DbFunction()]
        public bool CustomerLoginValidation(string username,string password,string decryptedKey)
        {
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            bool status=false;
            try
            {
                //var retVal = Context.Teller.FromSql("select dbo.ufn_ValidateTellerLogin({0},{1},{2})",username,password,decryptedKey).FirstOrDefault();
                var retVal = Context.ValidateCustomerLoginForTest(username,password,decryptedKey);
                if (retVal == 1){
                    status=true;
                }
            }
            catch (Exception ex)
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        public long GetCustomerIdByEmailId(string emailId)
        {
            long id=0;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
         try{
                id=Context.Customer.Where(x=>x.EmailId==emailId).Select(x=>x.CustomerId).FirstOrDefault();
         }catch(Exception ex){
              EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            id=-1;
         }
         finally
         {
              EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
         }
         return id;
        }

        public long GetCustomerIdByUsername(string username){
            long id=0;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                id=Context.CustomerLogin.Where(x=>x.LoginName==username).Select(x=>x.CustomerId).FirstOrDefault();
            }
            catch(Exception ex){
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                id=-1;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return id;
        }
        public int GetCountForUsernames(string username){
            int count=0;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
             count= Context.CustomerLogin.Where(x=> x.LoginName.Contains(username)).Count();
            }
            catch(Exception ex){
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                count=-1;
            }finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return count;
        }

        public bool CreateCustomer(Common.Models.Customer customerObj){
            Models.Customer customer= new Models.Customer();
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            bool status =false;
            try{
                    customer.EmailId=customerObj.EmailId;
                    customer.Name=customerObj.Name;
                    customer.Uuid=customerObj.UUID;
                    customer.DateOfBirth=customerObj.DateOfBirth;
                    customer.CreatedTimestamp=System.DateTime.Now;
                    customer.IsAddrVerified=true;
                    customer.CreatedBy=Convert.ToByte(customerObj.TellerId);
                    customer.Status='P';
                    Context.Customer.Add(customer);
                    context.SaveChanges();
                    status=true;
            }
            catch(Exception ex){
                status=false;
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        public bool InsertCustomerCredentials(string loginName,long customerId,string password,string encryptKey){
            // Models.CustomerLogin customerLoginObj=new Models.CustomerLogin();
            bool status=false;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                    // customerLoginObj.CustomerId=customerId;
                    // customerLoginObj.LoginName=loginName;
                    // customerLoginObj.Password=password;
                    // context.CustomerLogin.Add(customerLoginObj);
                    SqlParameter prmCustomerId = new SqlParameter("@CustomerId", customerId);
          SqlParameter prmLoginName = new SqlParameter("@Password", password);
          SqlParameter prmPassword = new SqlParameter("@LoginName", loginName);
          SqlParameter prmEncryptKey = new SqlParameter("@EncryptKey", encryptKey);
            int result = Context.ExecuteSqlCommandMethod("EXEC dbo.usp_insertUserCredentials @CustomerId,@Password,@LoginName,@EncryptKey",prmCustomerId,prmPassword,prmLoginName,prmEncryptKey);
                    //context.SaveChanges();
                    if(result==1)
                    status=true;

                    //usp_insertUserCredentials
            }
            catch(Exception ex){
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        public bool CommitCustomerInsertion(long customerId){
            bool status=false;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{

                CustomerDAL.Models.CustomerLogin custLogin = Context.CustomerLogin.Where(x=> x.CustomerId==customerId).FirstOrDefault();
                if (custLogin != null){
                    custLogin.Status = 'S';
                    CustomerDAL.Models.Customer cust = Context.Customer.Where(x=> x.CustomerId==custLogin.CustomerId).FirstOrDefault();
                    if (cust != null){
                        cust.Status = 'S';
                        Context.SaveChanges();
                        status=true;
                    }
                }
            }
            catch(Exception ex){
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status = false;
            }finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        public string GetCustomerNameByCustomerId(long customerId){
            string custName = string.Empty;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                custName = Context.Customer.Where(x=> x.CustomerId==customerId).Select(x=> x.Name).FirstOrDefault();
            }
            catch(Exception ex){
                custName = String.Empty;
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return custName;
        }

        public bool CustomerUpdatePass(string username,string password,string encryptedKey)
        {
            bool status=false;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                //var retVal = Context.Teller.FromSql("select dbo.ufn_ValidateTellerLogin({0},{1},{2})",username,password,decryptedKey).FirstOrDefault();
                SqlParameter prmLoginName = new SqlParameter("@LoginName", username);
          SqlParameter prmPassword = new SqlParameter("@Password", password);
          SqlParameter prmEncryptKey = new SqlParameter("@EncryptKey", encryptedKey);
          int result = Context.ExecuteSqlCommandMethod("EXEC dbo.usp_ChangePassword @LoginName,@Password,@EncryptKey",prmLoginName,prmPassword,prmEncryptKey);
                if (result == 1){
                    status=true;
                }
            }
            catch (Exception ex)
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        public bool AddPayee(Models.Payee payee){
            bool status = false;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                Context.Payee.Add(payee);
                Context.SaveChanges();
                status = true;
            }
            catch(Exception ex){
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status = false;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        public List<CustomerDAL.Models.Payee> FetchAllPayees(long customerId){
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
                List<CustomerDAL.Models.Payee> payeeList = new List<CustomerDAL.Models.Payee>();
                try{
                    payeeList= Context.Payee.Where(z=>z.CustomerId==customerId).ToList();
                }
                catch(Exception ex){
                     EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                    payeeList=null;
                }finally
                {
                     EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
                }
                return payeeList;
        }
    }
}
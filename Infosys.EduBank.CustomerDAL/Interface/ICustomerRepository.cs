﻿using System;
using System.Collections.Generic;
using System.Text;
using Infosys.EduBank.CustomerDAL.Models;

namespace Infosys.EduBank.CustomerDAL.Interface
{
    public interface ICustomerRepository
    {
        Common.Models.AppSettings AppSettings{set;get;}
        ICustomerContext Context { get;set; }

        bool CommitCustomerInsertion(long customerId);
        bool CreateCustomer(Common.Models.Customer customerObj);
        bool CustomerLoginValidation(string username, string password, string decryptedKey);
        bool CustomerUpdatePass(string username, string password, string encryptedKey);
        int GetCountForUsernames(string username);
        long GetCustomerIdByEmailId(string emailId);
        long GetCustomerIdByUsername(string username);
        string GetCustomerNameByCustomerId(long customerId);
        
        bool InsertCustomerCredentials(string loginName, long customerId, string password, string encryptKey);
        bool AddPayee(Models.Payee payee);
        List<CustomerDAL.Models.Payee> FetchAllPayees(long customerId);
    }
}

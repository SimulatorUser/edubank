﻿using System.Data.SqlClient;
using Infosys.EduBank.CustomerDAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Infosys.EduBank.CustomerDAL.Interface
{
    public interface ICustomerContext
    {
        DbSet<Customer> Customer { get; set; }
        DbSet<CustomerLogin> CustomerLogin { get; set; }
         DbSet<Payee> Payee { get; set; }

        int ExecuteSqlCommandMethod(string query,params SqlParameter[] parameters);
        int SaveChanges();
        byte ValidateCustomerLoginForTest(string username,string password, string decryptedKey);
    }
}
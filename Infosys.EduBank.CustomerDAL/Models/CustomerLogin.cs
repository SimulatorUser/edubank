﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.CustomerDAL.Models
{
    public partial class CustomerLogin
    {
        public int CustomerLoginId { get; set; }
        public string LoginName { get; set; }
        public byte[] Password { get; set; }
        public long CustomerId { get; set; }
        public bool IsLocked { get; set; }
        public DateTime? ModifiedTimestamp { get; set; }
        public char Status { get; set; }

        public Customer Customer { get; set; }
    }
}

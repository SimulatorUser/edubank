﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.CustomerDAL.Models
{
    public partial class Payee
    {
        public long PayeeId { get; set; }
        public string NickName { get; set; }
        public string AccountNumber { get; set; }
        public long? CustomerId { get; set; }
        public decimal? PreferredAmount { get; set; }
        public Customer Customer { get; set; }
    }
}

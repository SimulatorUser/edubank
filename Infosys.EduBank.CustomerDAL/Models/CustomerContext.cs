﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Infosys.EduBank.CustomerDAL.Interface;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;

namespace Infosys.EduBank.CustomerDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class CustomerContext : DbContext,ICustomerContext
    {
        private string ConnectionString { get; }
        public CustomerContext(string conn)
        {
            ConnectionString=conn;
        }

        public CustomerContext(DbContextOptions<CustomerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomerLogin> CustomerLogin { get; set; }
        public virtual DbSet<Payee> Payee { get; set; }
        public byte ValidateCustomerLoginForTest(string username,string password, string decryptedKey){
        return (from x in this.CustomerLogin select CustomerContext.ValidateCustomerLogin(username,password,decryptedKey)).FirstOrDefault();
    }

       [DbFunction("ufn_ValidateCustomerLogin","dbo")]
        public static byte ValidateCustomerLogin(string username,string password, string decryptedKey){
            return 0;
        }
        [DbFunction("usp_ChangePassword","dbo")]
        public static byte UpdatePassword(string username,string password, string decryptedKey){
            return 0;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.EmailId)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Uuid)
                    .HasColumnName("UUID")
                    .HasMaxLength(12)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CustomerLogin>(entity =>
            {
                entity.Property(e => e.LoginName)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerLogin)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_CustomerLogin_CustomerId");
            });

            modelBuilder.Entity<Payee>(entity =>
            {
                entity.HasIndex(e => e.AccountNumber)
                    .HasName("UQ__Payee__BE2ACD6F7CCC5BE6")
                    .IsUnique();

                entity.Property(e => e.AccountNumber)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.NickName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PreferredAmount).HasColumnType("money");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Payee)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("fk_CustomerId");
            });
        }
         public virtual int ExecuteSqlCommandMethod(string query,params SqlParameter[] parameters)
        {
            return this.Database.ExecuteSqlCommand(query,parameters);
        }
    }
}

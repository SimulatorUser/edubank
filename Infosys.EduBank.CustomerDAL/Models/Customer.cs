﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.CustomerDAL.Models
{
    public partial class Customer
    {
        public Customer()
        {
            CustomerLogin = new HashSet<CustomerLogin>();
            Payee = new HashSet<Payee>();
        }

        public long CustomerId { get; set; }
        public string EmailId { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime CreatedTimestamp { get; set; }
        public byte CreatedBy { get; set; }
        public DateTime? ModifiedTimestamp { get; set; }
        public byte? ModifiedBy { get; set; }
        public string Uuid { get; set; }
        public bool IsAddrVerified { get; set; }
        public char Status { get; set; }

        public ICollection<CustomerLogin> CustomerLogin { get; set; }
        public ICollection<Payee> Payee { get; set; }
    }
}

﻿using Infosys.EduBank.Common.Models;
using Infosys.EduBank.TellerDAL.Repository;
using Infosys.EduBank.CustomerDAL.Repository;
using Infosys.EduBank.TellerDAL.Interface;
using Infosys.EduBank.CustomerDAL.Interface;

namespace Infosys.EduBank.AuthenticationBL.Interface
{
    public interface IAuthenticateBL
    {
        AppSettings AppSettings{set;get;}
        ITellerRepository TellerRepository { get; set; }
        ICustomerRepository CustomerRepository { get; set; }
        UserAuth AuthenticateUser(string username, string password);
    }
}
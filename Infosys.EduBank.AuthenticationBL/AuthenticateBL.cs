﻿using System;
using Infosys.EduBank.TellerDAL.Repository;
using Infosys.EduBank.CustomerDAL.Repository;
using Infosys.EduBank.TellerDAL.Interface;
using Infosys.EduBank.CustomerDAL.Interface;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.AuthenticationBL.Interface;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.Common;
namespace Infosys.EduBank.AuthenticationBL
{
    public class AuthenticateBL : IAuthenticateBL
    {
        public Common.Models.AppSettings AppSettings{get; set;}
        public ITellerRepository TellerRepository { get; set; }
        public ICustomerRepository CustomerRepository { get; set; }
        private string TellerSqlKey { get; set; }
        private string CustomerSqlKey { get;set; }

        public AuthenticateBL(Common.Models.AppSettings settings)
        {
            TellerRepository=new TellerRepository(settings);
            CustomerRepository=new CustomerRepository(settings);
            TellerSqlKey=settings.TellerSqlKey;
            CustomerSqlKey=settings.CustomerSqlKey;
            AppSettings=settings;
        }
        public UserAuth AuthenticateUser(string username,string password){
            UserAuth user;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                user=new UserAuth();
                if(username.StartsWith("T")){
                    if(TellerRepository.TellerLogin(username,password,TellerSqlKey)){
                    user.Name=username;
                    user.Role="Teller";
                    user.Id=TellerRepository.GetTellerIdByUsername(username);
                    }
                    else{
                        return null;
                    }
                }
                else{
                    if(CustomerRepository.CustomerLoginValidation(username,password,CustomerSqlKey)){
                    user.Name=username;
                    user.Role="User";
                    user.Id=CustomerRepository.GetCustomerIdByUsername(username);
                    }
                    else{
                        return null;
                    }
                }

            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+ex.Message,2,ex);
                user=null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return user;
        }
    }
}

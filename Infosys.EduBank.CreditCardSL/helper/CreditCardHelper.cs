using System;
using System.Collections.Generic;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.CreditCardBL;
using Infosys.EduBank.CreditCardBL.Interface;
using Infosys.EduBank.CreditCardSL.Helper;
using Infosys.EduBank.CreditCardSL.Interface;
using Infosys.EduBank.CreditCardSL.Models;

namespace Infosys.EduBank.CreditCardSL.helper
{
    public class CreditCardHelper:ICreditCardHelper
    {
        public ICreditCardBLRepository CreditCardBLObj { get; set; }
        public Common.Models.AppSettings AppSettings { get; set; }

        public IHttpServiceHelper HttpServiceHelperObj{get;set;}
        public CreditCardHelper(Common.Models.AppSettings appSettings)
        {
            AppSettings = appSettings;
            CreditCardBLObj = new CreditCardBLRepository(appSettings);
            HttpServiceHelperObj=new HttpServiceHelper();
        }

        public bool ApplyForCreditCard(string name,byte categoryId ){
            bool status=false;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            try
            {
             status=CreditCardBLObj.ApplyForCard(name,categoryId);  
            }
            catch (Exception ex)
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        public List<CardType> GetAllCardTypes(){
            List<CardType> cardTypeList=new List<CardType>();
            List<Common.Models.CardType> cardTypes;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            try
            {
                cardTypes=CreditCardBLObj.GetAllCardType();
                foreach(var card in cardTypes){
                    cardTypeList.Add(new CardType(){
                    CardCategory=card.CardCategory,
                    CardTypeId=card.CardTypeId,
                    CreditLimit=card.CreditLimit,
                    IsActive=card.IsActive
                    });
                }
            }
            catch (Exception ex)
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                cardTypeList=null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return cardTypeList;
        }
        public decimal GetCardLimit(byte id){
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            decimal limit;
            try
            {
                limit=CreditCardBLObj.GetCardLimit(id);
            }
            catch (Exception ex)
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                limit =0;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return limit;
        }

        public List<CardRequest> GetRequests()
        {
            List<CardRequest> cardRequests = new List<CardRequest>();
            List<Common.Models.CardRequest> requestList=new List<Common.Models.CardRequest>();
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            try{
                    requestList=CreditCardBLObj.GetRequests();
                    foreach(var request in requestList){
                        cardRequests.Add(new CardRequest(){
                            CardTypeId=request.CardTypeId,
                            CustomerId=request.CustomerId,
                            RequestId=request.RequestId,
                            RequestDate=request.RequestDate,
                            Status=request.Status
                        });
                    }
            }
            catch(Exception ex){
                cardRequests=null;
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return cardRequests;
            
        }
        public Common.Models.CreditCard GenerateCreditCard(long requestId,string tellerId){
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            Common.Models.CreditCard creditCardObj =new Common.Models.CreditCard();
            try
            {
                //creditCardObj.CustomerId=creditCard.CustomerId;
                //creditCardObj.CardTypeId=creditCard.CardTypeId;
                creditCardObj.CreatedBy=tellerId;
                bool status=CreditCardBLObj.GenerateCreditCard(requestId,ref creditCardObj);
                if(!status){
                    creditCardObj=null;
                }
            }
            catch (Exception ex)
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                creditCardObj=null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
        return creditCardObj;
        }
        public bool UpdateRequestStatus(long requestId,string state){
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            bool status=false;
            try
            {
                status =CreditCardBLObj.UpdateRequestStatus(requestId,state);
            }
            catch (Exception ex)
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
    }
}
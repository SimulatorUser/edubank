using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System;
using System.Net;
using System.IO;
using Infosys.EduBank.CreditCardSL.Interface;
using System.Diagnostics.CodeAnalysis;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.Common;

namespace Infosys.EduBank.CreditCardSL.Helper
{
    [ExcludeFromCodeCoverage]
    public class HttpServiceHelper:IHttpServiceHelper
    {
        public Common.Models.AppSettings AppSettings{get; set;}
        private static bool AcceptAllCertifications(object sender,
        System.Security.Cryptography.X509Certificates.X509Certificate certification,
        System.Security.Cryptography.X509Certificates.X509Chain chain,
        System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
        return true;
        }
        public string GetResponse(string url,string token)
        {
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                if(!token.Equals(string.Empty))
                {
                    httpWebRequest.Headers.Add("Content-Type","application/json");
                    httpWebRequest.Headers.Add("Authorization", "Bearer "+token);
                }
                
               httpWebRequest.ServerCertificateValidationCallback=new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
                
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                var streamReader = new StreamReader(httpResponse.GetResponseStream());
                return streamReader.ReadToEnd();
            }
             catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                return "Exception raised while calling other service";
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            
        }

    }
}
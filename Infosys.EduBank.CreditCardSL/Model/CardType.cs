﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.CreditCardSL.Models
{
    public  class CardType
    {
        public byte CardTypeId { get; set; }
        public string CardCategory { get; set; }
        public bool IsActive { get; set; }
        public decimal? CreditLimit { get; set; }
    }
}

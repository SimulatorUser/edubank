﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.CreditCardSL.Models
{
    public class CardRequest
    {
        public long RequestId { get; set; }
        public long CustomerId { get; set; }
        public byte? CardTypeId { get; set; }
        public DateTime RequestDate { get; set; }
        public string Status { get; set; }

        //public CardType CardType { get; set; }
    }
}

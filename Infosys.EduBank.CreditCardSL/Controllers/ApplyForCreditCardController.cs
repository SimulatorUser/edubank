using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.CreditCardSL.helper;
using Infosys.EduBank.CreditCardSL.Interface;
using Infosys.EduBank.CreditCardSL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.CreditCardSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplyForCreditCardController:ControllerBase
    {
        public ICreditCardHelper CreditCardHelperObj { get; set; } 
       private Common.Models.AppSettings AppSettings;
        private Common.IUtilities Utilities{get;set;}
        public ApplyForCreditCardController(Common.IUtilities util)
        {
            Utilities=util;
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(Utilities.Configuration);
            CreditCardHelperObj = new CreditCardHelper(appSettings);
            AppSettings = appSettings;
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public bool Post(string name,byte categoryId){
            bool status = false;
            var methodName=Common.Utilities.GetMethod();
            AppSettings.UserName=GetUserName();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            try
            {
                status =CreditCardHelperObj.ApplyForCreditCard(name,categoryId);
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,1,ex);
                status = false;
            }
            finally{
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        [HttpGet]
        [Authorize(Roles = "User")]
        public List<CardType> Get(){
            // bool status = false;
            var methodName=Common.Utilities.GetMethod();
            AppSettings.UserName=GetUserName();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            List<CardType> list = new List<CardType>();
            try
            {
                list=CreditCardHelperObj.GetAllCardTypes();
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,1,ex);
            //   status = false;
            list=null;
            }
            finally{
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
             return list;
        }
        private string GetUserName()
        {
            string value="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    value=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                value="Anonymous User";
            }
            return value;
        }
    }
}
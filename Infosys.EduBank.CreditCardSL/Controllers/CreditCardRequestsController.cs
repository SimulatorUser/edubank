using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.CreditCardSL.helper;
using Infosys.EduBank.CreditCardSL.Interface;
using Infosys.EduBank.CreditCardSL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.CreditCardSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreditCardRequestsController:ControllerBase
    {
        public ICreditCardHelper CreditCardHelperObj { get; set; } 
        private Common.Models.AppSettings AppSettings;
        private Common.IUtilities Utilities{get;set;}

         
        public CreditCardRequestsController(Common.IUtilities util)
        {
            Utilities=util;
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(Utilities.Configuration);
            CreditCardHelperObj = new CreditCardHelper(appSettings);
            AppSettings = appSettings;
        }
        [HttpGet]
        [Authorize(Roles = "Teller")]
        public List<CardRequest> Get(){
            // bool status = false;
            List<CardRequest> cardRequests=new List<CardRequest>();
            AppSettings.UserName=GetUserName();
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                cardRequests=CreditCardHelperObj.GetRequests();
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,1,ex);
            //   status = false;
            cardRequests=null;
            }
            finally{
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
             return cardRequests;
        }
        [HttpPost]
        [Authorize(Roles = "Teller")]
        public Common.Models.CreditCard Post(long requestId,string tellerId){
             //bool status = false;
            var methodName=Common.Utilities.GetMethod();
            AppSettings.UserName=GetUserName();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            Common.Models.CreditCard creditCardObj=new Common.Models.CreditCard();
            try
            {
                creditCardObj=CreditCardHelperObj.GenerateCreditCard(requestId,tellerId);
            }
            catch (Exception ex)
            {
               creditCardObj = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,1,ex);
            }
            finally{
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
             return creditCardObj;
        }

        [HttpGet("{requestId}/{state}")]
        [Authorize(Roles = "Teller")]
        public bool Get(long requestId,string state){
             bool status = false;
             var methodName=Common.Utilities.GetMethod();
            AppSettings.UserName=GetUserName();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            //Common.Models.CreditCard creditCardObj=new Common.Models.CreditCard();
            try
            {
                status=CreditCardHelperObj.UpdateRequestStatus(requestId,state);
            }
            catch (Exception ex)
            {
               status = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,1,ex);
            }
            finally{
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
             return status;
        }
         private string GetUserName()
        {
            string value="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    value=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                value="Anonymous User";
            }
            return value;
        }
    }
}
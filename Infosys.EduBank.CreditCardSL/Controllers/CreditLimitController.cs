using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.CreditCardSL.helper;
using Infosys.EduBank.CreditCardSL.Interface;
using Infosys.EduBank.CreditCardSL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
namespace Infosys.EduBank.CreditCardSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class CreditLimitController:ControllerBase
    {
        public ICreditCardHelper CreditCardHelperObj { get; set; } 
        private Common.Models.AppSettings AppSettings;
        private Common.IUtilities Utilities{get;set;}
        public CreditLimitController(Common.IUtilities util)
        {
            Utilities=util;
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(Utilities.Configuration);
            CreditCardHelperObj = new CreditCardHelper(appSettings);
            AppSettings = appSettings;
        }
        [HttpGet]
        [Authorize(Roles = "User")]
        public decimal Get(byte id){
            // bool status = false;
            decimal limit=0;
             var methodName=Common.Utilities.GetMethod();
            AppSettings.UserName=GetUserName();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            try
            {
                limit=CreditCardHelperObj.GetCardLimit(id);
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,1,ex);
            //   status = false;
            limit=0;
            }
            finally{
               EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1); 
            }
             return limit;
        }
         private string GetUserName()
        {
            string value="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    value=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                value="Anonymous User";
            }
            return value;
        }
    }
}
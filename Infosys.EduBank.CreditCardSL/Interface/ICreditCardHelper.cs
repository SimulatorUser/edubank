using System.Collections.Generic;
using Infosys.EduBank.CreditCardBL.Interface;
using Infosys.EduBank.CreditCardSL.Models;

namespace Infosys.EduBank.CreditCardSL.Interface
{
    public interface ICreditCardHelper
    {
         IHttpServiceHelper HttpServiceHelperObj{get;set;}
         ICreditCardBLRepository CreditCardBLObj { get; set; }
         Common.Models.AppSettings AppSettings { get; set; }
         bool ApplyForCreditCard(string name,byte categoryId );
         List<CardType> GetAllCardTypes();
         decimal GetCardLimit(byte id);
         List<CardRequest> GetRequests();
         Common.Models.CreditCard GenerateCreditCard(long requestId,string tellerId);
         bool UpdateRequestStatus(long requestId,string state);
    }
}
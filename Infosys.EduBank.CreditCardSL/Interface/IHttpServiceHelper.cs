using System.Security.Claims;
using System;
using System.Net;
using System.IO;
namespace Infosys.EduBank.CreditCardSL.Interface
{
    public interface IHttpServiceHelper
    {
         string GetResponse(string url,string token);
         Common.Models.AppSettings AppSettings{set;get;}
    }
}
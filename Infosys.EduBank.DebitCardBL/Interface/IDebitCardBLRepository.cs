﻿using Infosys.EduBank.AccountDAL.Repository;
using Infosys.EduBank.AccountDAL.Interface;
using Infosys.EduBank.CardDAL.Repository;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.CustomerDAL.Repository;
using Infosys.EduBank.TransactionDAL.Repository;
using Infosys.EduBank.TransactionDAL.Interface;
using Infosys.EduBank.CustomerDAL.Interface;
using Infosys.EduBank.CardDAL.Interface;

namespace Infosys.EduBank.DebitCardBL.Interface
{
    public interface IDebitCardBLRepository
    {
        ICardRepository CardRepositoryObj { get; set; }
        ITransactionRepository TransactionRepositoryObj { get; set; }
        IUtilities UtilityObj { get; set; }
        AppSettings AppSettings{get;set;}
        string DebitUsingDebitCard(DebitCardTransaction debitCardTransaction, string accountNumber);
        int VerifyCard(DebitCardDetails cardDetails);
        
    }
}
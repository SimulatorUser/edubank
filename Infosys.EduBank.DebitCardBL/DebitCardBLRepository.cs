﻿using System;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.Common;
using Infosys.EduBank.CardDAL.Repository;
using Infosys.EduBank.AccountDAL.Repository;
using Infosys.EduBank.CustomerDAL.Repository;
using Infosys.EduBank.TransactionDAL.Repository;
using Infosys.EduBank.CardDAL.Interface;
using Infosys.EduBank.AccountDAL.Interface;
using Infosys.EduBank.CustomerDAL.Interface;
using Infosys.EduBank.TransactionDAL.Interface;
using Infosys.EduBank.DebitCardBL.Interface;
using Newtonsoft.Json;
using Infosys.EduBank.Common.Logger;


namespace Infosys.EduBank.DebitCardBL
{
    public class DebitCardBLRepository : IDebitCardBLRepository
    {
        public ICardRepository CardRepositoryObj{set;get;}
        public ITransactionRepository TransactionRepositoryObj{set;get;}
        public IUtilities UtilityObj {set;get;}
        public AppSettings AppSettings{get;set;}
        public DebitCardBLRepository(IUtilities util)
        {
            AppSettings = JsonConvert.DeserializeObject<Common.Models.AppSettings>(util.Configuration);
            CardRepositoryObj = new CardRepository(AppSettings);
            TransactionRepositoryObj=new TransactionRepository(AppSettings);
            UtilityObj=util;
        }

        public int VerifyCard(DebitCardDetails cardDetails){
            int accountCustomerMappingId = 0;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                //Encrypt CVV and Pin
                cardDetails.CVV=UtilityObj.LoginEncryptDecrypt(cardDetails.CVV,AppSettings.SecurityKey10);
                cardDetails.Pin= UtilityObj.LoginEncryptDecrypt(cardDetails.Pin,AppSettings.SecurityKey10);

                accountCustomerMappingId = CardRepositoryObj.VerifyCardDetails(cardDetails,AppSettings.PinKey,AppSettings.CVVKey);
            }
            catch(Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                accountCustomerMappingId = 0;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return accountCustomerMappingId;
        }


        public string DebitUsingDebitCard(DebitCardTransaction debitCardTransaction,string accountNumber){
            string responseCode = string.Empty;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                InsertTransaction bankTransaction=new InsertTransaction();
                bankTransaction.AccountNumber=accountNumber;
                bankTransaction.Amount=debitCardTransaction.Amount;
                bankTransaction.TellerId=string.Empty; 
                bankTransaction.Info=debitCardTransaction.DebitCard.CardHolderName;
                bankTransaction.Remarks=debitCardTransaction.Remarks;
                bankTransaction.Mode=Common.Constants.DebitCardTransactionMode;
                if(TransactionRepositoryObj.InsertTransaction(bankTransaction,0)!=0){
                    responseCode="SUCCESS";
                }
            }
            catch(Exception ex){
                responseCode = "FAILURE";
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return responseCode;
        }

        
    }
}

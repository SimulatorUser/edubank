<# Get current directory path #>
$src = (Get-Item -Path ".\" -Verbose).FullName;
Write-Host  $src;
<# Iterate all directories present in the current directory path #>
Get-ChildItem $src -directory | where {$_.PsIsContainer} | Select-Object -Property Name | ForEach-Object {
    $cdProjectDir = [string]::Format("cd /d {0}\{1}",$src, $_.Name);
    <#Write-Host  $cdProjectDir;#>
    if($cdProjectDir.EndsWith("SL") -Or $cdProjectDir.EndsWith("Gateway"))
    {
        $params=@("/C"; $cdProjectDir; " && dotnet run"; )
        Start-Process -Verb runas "cmd.exe" $params;
        Start-Sleep -s 2 ;
    }
}
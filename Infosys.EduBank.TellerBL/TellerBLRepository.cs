﻿using System;
using Infosys.EduBank.CustomerDAL.Repository;
using Infosys.EduBank.AccountDAL.Repository;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.CardDAL.Repository;
using System.Collections.Generic;
using Infosys.EduBank.TellerBL.Interface;
using Infosys.EduBank.CustomerDAL.Interface;
using Infosys.EduBank.CardDAL.Interface;
using Infosys.EduBank.AccountDAL.Interface;
using Newtonsoft.Json;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.TellerBL
{
    public class TellerBLRepository : ITellerBLRepository
    {
        // public string CustomerConnection{set;get;}
        // public string AccountConnection{set;get;}
        // public string DebitCardConnection{set;get;}
        // public string CustomerKey{set;get;}
        // public string CvvKey{set;get;}
        // public string PinKey{set;get;}
         public ICustomerRepository CustomerRepositoryObj{set;get;}
         public IAccountRepository AccountRepositoryObj{set;get;}
         public ICardRepository CardRepositoryObj{set;get;}
         public IUtilities Util{set;get;}
        public AppSettings AppSettings{set;get;}


        public TellerBLRepository(IUtilities util){
            AppSettings=JsonConvert.DeserializeObject<AppSettings>(util.Configuration);
            CustomerRepositoryObj= new CustomerRepository(AppSettings);
            AccountRepositoryObj=new AccountRepository(AppSettings);
            CardRepositoryObj=new CardRepository(AppSettings);
            Util=util;
            //  CvvKey=appSettings.CvvKey;
            //  PinKey=appSettings.PinKey;
        }
        public long CreateCustomer( Common.Models.Customer  customer){
            long newCustomerId = 0;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                // invoke DAL method to insert customer details, and return customerId
                 
                if(CustomerRepositoryObj.CreateCustomer(customer)){
                    newCustomerId=CustomerRepositoryObj.GetCustomerIdByEmailId(customer.EmailId);
                }
                else{
                    newCustomerId=-1;
                }

            }
            catch(Exception ex){
                    newCustomerId=-1;
                    EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return newCustomerId;
        }

        public bool CreateCredentials(ref Common.Models.Customer customer,long customerId){
            bool status = false;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                string loginName;
                int count=CustomerRepositoryObj.GetCountForUsernames(customer.Name);
                if(count !=0){
                    loginName=customer.Name.Replace(" ","")+"00"+count.ToString();
                }
                else{
                    loginName=customer.Name.Replace(" ","");
                }

                // invoke DAL method to fetch username
                string password = GeneratePassword();
                customer.LoginName=loginName;
                customer.Password=password;
            var passwordHashed =Util.Encrypt(password,AppSettings.SecurityKey10);
                // invoke third-party logic to generate password
               status= CustomerRepositoryObj.InsertCustomerCredentials(loginName,customerId,passwordHashed,AppSettings.CustomerSqlKey);
                // invoke DAL method to insert into CustomerLogin
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                    status=false;
            }finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        public int CreateAccount(long customerId,ref Common.Models.Customer customer){
            int accountCustomerMappingId = -99;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                // invoke DAL method to fetch last inserted account number
                byte branchId=0;
                branchId=Convert.ToByte(customer.Branch);
                // fecth branch id

                var accountNumber = AccountRepositoryObj.CreateAccountNumber(customerId,branchId,Convert.ToByte(customer.TellerId));
                customer.AccountNumber=accountNumber;
                // fetch AccountMapping ID
                accountCustomerMappingId=AccountRepositoryObj.GetAccountMappingIdByAccountNumber(accountNumber);
                // get branch id
                // invoke DAL method(s) to insert new account details into account, account-customer mapping
                
            }
            catch(Exception ex){
                accountCustomerMappingId=0;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return accountCustomerMappingId;
        }

        public string GenerateDebitCard(int accountCustomerMappingId,ref Common.Models.Customer customer){
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            string debitCardNo =null;
            string cvv,pin=string.Empty;
            try{
                string ifsc=AccountRepositoryObj.GetBranchCodeByBranchId(customer.Branch);
                
                GenerateDebitCardDetails(out cvv,out pin);
                customer.CVV=cvv;
                customer.Pin=pin;
                cvv=Util.Encrypt(cvv,AppSettings.SecurityKey10);
                pin=Util.Encrypt(pin,AppSettings.SecurityKey10);

                debitCardNo=CardRepositoryObj.insertDebitCard(accountCustomerMappingId,ifsc,ref customer,cvv,pin,AppSettings);
                // invoke calls to generate CVV and PIN
            }   
            catch(Exception ex){
                    debitCardNo=null;
                    EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return debitCardNo;
        }

        public string GeneratePassword()
        {
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            string password = string.Empty;
            try
            {
                string upperLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                string lowerLetters = "abcdefghijklmnopqrstuvwxyz";
                string specialCharacters = "!#$%^&*()";
                string numbers = "0123456789";

                //Object of Random Class is instantiated 
                Random rnd = new Random();

                /*A password containing 11 characters (i.e greater than 8 and less than 20) and 
                 * at least 1 uppercase letter, 1 lowercase letter, 1 number and one special character is generated*/
                password = upperLetters[rnd.Next(0, 25)].ToString() + lowerLetters[rnd.Next(0, 25)] + upperLetters[rnd.Next(0, 25)] + upperLetters[rnd.Next(0, 25)] + lowerLetters[rnd.Next(0, 25)] + specialCharacters[rnd.Next(0, 9)] + numbers[rnd.Next(0, 9)] + upperLetters[rnd.Next(0, 25)] + lowerLetters[rnd.Next(0, 25)] + numbers[rnd.Next(0, 9)] + specialCharacters[rnd.Next(0, 9)];
                // Info logging with the Type of TellerBL.
            }
            catch (Exception ex)
            {
                // Logging in case of any errors with the Type of TellerBL.
                // Specifying the error message with the error message "Error occurred while generating password".
                //Setting password to empty string in case of exception
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                password = string.Empty;
            }finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return password;
        }

        public bool CommitInsertions(long customerId,int accountCustomerMappingId,string debitCardNo){
            bool status = false;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                status = CardRepositoryObj.CommitCardInsertion(debitCardNo);
                status = status && AccountRepositoryObj.CommitAccountInsertion(accountCustomerMappingId);
                status = status && CustomerRepositoryObj.CommitCustomerInsertion(customerId);
            }
            catch(Exception ex){
                status = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        public void GenerateDebitCardDetails(out string cvv, out string pin)
        {
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                //Object of Random Class is instantiated 
                Random rnd = new Random();

                //CVV of 3 random digits is generated
                cvv = Convert.ToString(rnd.Next(0, 9)) + Convert.ToString(rnd.Next(0, 9)) + Convert.ToString(rnd.Next(0, 9));

                //Pin of 4 random digits is generated
                pin = Convert.ToString(rnd.Next(0, 9)) + Convert.ToString(rnd.Next(0, 9)) + Convert.ToString(rnd.Next(0, 9)) + Convert.ToString(rnd.Next(0, 9));

                // Info logging with the Type of TellerBL.
               // EduBankLogger.Log(typeof(TellerBL), "TellerBL", "GenerateDebitCardDetails executed successfully in TellerBL", 1);
            }
            catch (Exception ex)
            {
                // Logging in case of any errors with the Type of TellerBL.
                // Specifying the error message with the error message "Error occurred while generating PIN and CVV of debit card".
               // EduBankLogger.Log(typeof(TellerBL), "TellerBL", "Error occurred while generating PIN and CVV of debit card", 2, ex);

                // Setting CVV and Pin to empty string in case of exception
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                cvv = string.Empty;
                pin = string.Empty;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
        }
        public List<string> GetAccountDetails(string accountNumber){
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            List<string> data=new List<string>();
            try
            {
                int mappingId=AccountRepositoryObj.GetAccountMappingIdByAccountNumber(accountNumber);
                long custId=AccountRepositoryObj.GetCustomerIdByAccountMappingId(mappingId);
                string Name=CustomerRepositoryObj.GetCustomerNameByCustomerId(custId);
                data.Add(Name);
                if(AccountRepositoryObj.IsAccountNumberActive(accountNumber)){
                    data.Add("Account is Active");
                }
                else{
                    data.Add("Account is Inactive");
                }
            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            data=null;                
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return data;
        }

        public List<Common.Models.Branch> GetAllBranches(){
            List<Common.Models.Branch> data=new List<Common.Models.Branch>();
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                var allBranches=AccountRepositoryObj.GetAllBranches();
                foreach(AccountDAL.Models.Branch b in allBranches)
                {
                    data.Add(new Common.Models.Branch(){
                        BranchId=b.BranchId,
                        BranchCode=b.BranchCode,
                        BranchName=b.BranchName
                    });
                }

            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                data=null;                
            }finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return data;
        }

        #region Exercises
        
        //Gets the list of accounts whose Status property is 
        public List<Common.Models.Account> GetAccountsByStatus(string status)
        {
            List<Common.Models.Account> accounts = new List<Common.Models.Account>();
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            char statusChar;
            try
            {
                statusChar = Convert.ToChar(status.Substring(0,1));                
                accounts = AccountRepositoryObj.FetchAccountsByStatus(statusChar);
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                accounts = null;
            }finally{
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return accounts;
        }

        //Updates the property Status of the account object
        public bool UpdateAccountStatus(Common.Models.Account account)
        {
            bool status = false;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            char newStatus;
            try
            {
                newStatus = account.Status;
                status = AccountRepositoryObj.UpdateAccountStatus(account.AccountId,newStatus);
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status = false;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        
        //Fetches the details of the account for a given account number
        public Common.Models.Account GetAccountDetailsByAccountNumber(string accountNumber)
        {
            Common.Models.Account account = new Common.Models.Account();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                account = AccountRepositoryObj.GetAccountDetailsByAccountNumber(accountNumber);
            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                account = null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return account;
        }

        //Fetches the details of all the accounts
        public List<Common.Models.Account> GetAllAccountDetails()
        {
            List<Common.Models.Account> accounts = new List<Common.Models.Account>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                accounts = AccountRepositoryObj.FetchAllAccountDetails();                
            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                accounts = null;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return accounts;
        }

        //Toggles the property IsActive of the Account object
        public bool ToggleAccountStatus(string accountNumber, bool newStatus)
        {
            bool status = false;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                status = AccountRepositoryObj.ToggleAccountStatus(accountNumber,newStatus);
            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status = false;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        public double InterestCalculate(Dictionary<DateTime,decimal> dateWiseBalance)
        {
            double result = 0;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                double interestRate=7.5;
                double currentInterest=0;
                double currentPrinciple=0;
                foreach(var obj in dateWiseBalance)
                {
                    currentPrinciple=Convert.ToDouble(obj.Value)+currentInterest;
                    currentInterest=(currentPrinciple*interestRate)/36500;
                    result+=currentInterest;
                }
            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                result = -99;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return result;
        }
        //Minor Enhancement
        //Gets the list of all pending debit card replacement requests for the teller to approve/reject
        public List<DebitCardRequest> GetPendingDebitCardRequests()
        {
            List<DebitCardRequest> debitCardRequests = new List<DebitCardRequest>();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                debitCardRequests = CardRepositoryObj.GetPendingDebitCardRequests();
            }
            catch (Exception ex)
            {
                debitCardRequests = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return debitCardRequests;
        }
        //Minor Enhancement
        //Updates the status of debit card request as either approved/rejected Neha
        public Common.Models.Customer UpdateDebitCardRequestStatus(DebitCardRequest debitCardRequest, string tellerId)
        {
            bool result = false;
            Common.Models.Customer custobj = new Customer();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                custobj.TellerId = tellerId;
                result = CardRepositoryObj.UpdateDebitCardRequest(debitCardRequest.DebitCardRequestId,debitCardRequest.Status);
                if(result && debitCardRequest.Status=="A")
                {
                        bool blockStatus = CardRepositoryObj.BlockDebitCard(debitCardRequest.DebitCardId);
                        if(blockStatus)
                        {
                            string debitCardNo = GenerateNewDebitCard(debitCardRequest,ref custobj);
                            if(debitCardNo!=string.Empty || debitCardNo!="")
                            {
                                bool debitCardInsertion = CardRepositoryObj.CommitCardInsertion(debitCardNo);
                                if(debitCardInsertion)
                                {
                                    return custobj;
                                }
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                custobj = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return custobj;
        }
        //Minor Enhancement
        //Generates a new debit card for the replacement request
        public string GenerateNewDebitCard (DebitCardRequest debitCardRequest,ref Common.Models.Customer custobj)
        {
            string debitCardNo =string.Empty;
            string cvv,pin=string.Empty;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                // invoke calls to generate CVV and PIN
                GenerateDebitCardDetails(out cvv,out pin);
                custobj.CVV=cvv;
                custobj.Pin=pin;
                cvv=Util.Encrypt(cvv,AppSettings.SecurityKey10);
                pin=Util.Encrypt(pin,AppSettings.SecurityKey10);
                string branchCode = AccountRepositoryObj.GetBranchCodeByAccountMappingId(debitCardRequest.AccountCustomerMappingId);
                //Calls insert debit card request
                debitCardNo = CardRepositoryObj.insertDebitCard(debitCardRequest.AccountCustomerMappingId,branchCode,ref custobj,cvv,pin,AppSettings);
            }
            catch(Exception ex)
            {
                debitCardNo = string.Empty;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return debitCardNo;
        }
        
        #endregion
    }
    
}

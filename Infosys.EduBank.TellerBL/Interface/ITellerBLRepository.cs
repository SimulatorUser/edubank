﻿using System.Collections.Generic;
using Infosys.EduBank.AccountDAL.Repository;
using Infosys.EduBank.CardDAL.Repository;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.CustomerDAL.Repository;
using Infosys.EduBank.CustomerDAL.Interface;
using Infosys.EduBank.CardDAL.Interface;
using Infosys.EduBank.AccountDAL.Interface;
using System;

namespace Infosys.EduBank.TellerBL.Interface
{
    public interface ITellerBLRepository
    {
        IAccountRepository AccountRepositoryObj { get; set; }
        AppSettings AppSettings { get; set; }
        ICardRepository CardRepositoryObj { get; set; }
        ICustomerRepository CustomerRepositoryObj { get; set; }
        IUtilities Util { get; set; }

        bool CommitInsertions(long customerId, int accountCustomerMappingId, string debitCardNo);
        int CreateAccount(long customerId, ref Customer customer);
        bool CreateCredentials(ref Customer customer, long customerId);
        long CreateCustomer(Customer customer);
        string GenerateDebitCard(int accountCustomerMappingId, ref Customer customer);
        void GenerateDebitCardDetails(out string cvv, out string pin);
        string GeneratePassword();
        List<string> GetAccountDetails(string accountNumber);
        List<Account> GetAccountsByStatus(string status);
        List<Branch> GetAllBranches();
        bool UpdateAccountStatus(Account account);
        Common.Models.Account GetAccountDetailsByAccountNumber(string accountNumber);
        List<Common.Models.Account> GetAllAccountDetails();
        bool ToggleAccountStatus(string accountNumber, bool newStatus);
        double InterestCalculate(Dictionary<DateTime,decimal> dateWiseBalance);
        string GenerateNewDebitCard (DebitCardRequest debitCardRequest,ref Common.Models.Customer custobj);
        List<DebitCardRequest> GetPendingDebitCardRequests();
        Common.Models.Customer UpdateDebitCardRequestStatus(DebitCardRequest debitCardRequest, string tellerId);
    }
}
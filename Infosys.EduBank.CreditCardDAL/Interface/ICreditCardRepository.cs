using System.Collections.Generic;
using Infosys.EduBank.CreditCardDAL.Models;

namespace Infosys.EduBank.CreditCardDAL.Interface
{
    public interface ICreditCardRepository
    {
           ICreditCardContext Context{set;get;}
          List<CardType> GetAllCardType();
         Common.Models.AppSettings AppSettings { get; set; }
           bool ApplyForCard(CardRequest request);
           byte GetCardTypeIdByCardCategory(string cardCategory);
           
        decimal GetCreditLimitById(byte id);
        long AddCreditCard(Common.Models.CreditCard creditCard,string cvv,string pin,string cvvkey,string pinKey);
        List<CardRequest> GetCardRequest();
        bool UpdateRequestStatus(long requestId,string state);

        bool GetRequestInfo(long requestId,out byte cardTypeId,out long customerId);
    }
}
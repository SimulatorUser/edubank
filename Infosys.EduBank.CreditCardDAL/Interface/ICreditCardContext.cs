using System.Data.SqlClient;
using Infosys.EduBank.CreditCardDAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Infosys.EduBank.CreditCardDAL.Interface
{
    public interface ICreditCardContext
    {
         DbSet<BilledTransaction> BilledTransaction { get; set; }
        DbSet<CardRequest> CardRequest { get; set; }
        DbSet<CardType> CardType { get; set; }
        DbSet<CreditCard> CreditCard { get; set; }
        DbSet<CustomerBill> CustomerBill { get; set; }
        DbSet<Emiplan> Emiplan { get; set; }
        int ExecuteSqlCommandMethod(string query,params SqlParameter[] parameters);
        DbSet<UnbilledTransaction> UnbilledTransaction { get; set; }
        int SaveChanges();
    }
}
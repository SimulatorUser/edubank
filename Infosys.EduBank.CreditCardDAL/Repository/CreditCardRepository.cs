using System;
using System.Collections.Generic;
using System.Linq;
using Infosys.EduBank.CreditCardDAL.Models;
using Infosys.EduBank.CreditCardDAL.Interface;
using Infosys.EduBank.Common.Models;
using System.Data.SqlClient;
using System.Data;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.Common;
//using System.Data;

namespace Infosys.EduBank.CreditCardDAL.Repository
{
    public class CreditCardRepository:ICreditCardRepository
    {
       private ICreditCardContext context;
        public ICreditCardContext Context{
            get{ return context;}
            set{context=value;}
        }
        public Common.Models.AppSettings AppSettings{set;get;}
        public CreditCardRepository(Common.Models.AppSettings appSettings){
            Context=new CreditCardContext(appSettings.CreditCardConnection);//connectionString
            AppSettings=appSettings;
        }

        public List<Models.CardType> GetAllCardType(){
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            List<Models.CardType> cardTypes=new List<Models.CardType>();
            try{
                cardTypes=Context.CardType.Select(x=>x).ToList();
            }
            catch(Exception ex){
                cardTypes=null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return cardTypes;
        }
        public bool ApplyForCard(Models.CardRequest request){
            bool status=false;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                Context.CardRequest.Add(request);
                Context.SaveChanges();
                status=true;
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        public byte GetCardTypeIdByCardCategory(string cardCategory){
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            byte id= 0;
            try
            {
                id=Context.CardType.Where(x=>x.CardCategory==cardCategory).Select(x=>x.CardTypeId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                id=0;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return id;
        }

        public decimal GetCreditLimitById(byte id){
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            decimal limit=0;
            try
            {
          
                limit=Convert.ToDecimal(Context.CardType.Where(z=>z.CardTypeId==id).Select(z=>z.CreditLimit).FirstOrDefault());
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                limit =0;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return limit;
        }
        public List<Models.CardRequest> GetCardRequest(){
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            List<Models.CardRequest> requests=new List<Models.CardRequest>();
            try
            {
                requests=Context.CardRequest.Where(x=>x.Status=="Pending").Select(x=>x).ToList();
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                requests=null;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return requests;
        }

        public long AddCreditCard(Common.Models.CreditCard creditCard, string cvv, string pin,string cvvKey,string pinKey)
        {
            //CreditCardDAL.Models.CreditCard creditCardInfo = new CreditCardDAL.Models.CreditCard();
            long id=0;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                SqlParameter prmCustomerId = new SqlParameter("@CustomerId", creditCard.CustomerId);
                SqlParameter prmCardTypeId = new SqlParameter("@CardTypeId", creditCard.CardTypeId);
                SqlParameter prmCardNum = new SqlParameter("@CardNum", creditCard.CardNo);
                SqlParameter prmCvvKey = new SqlParameter("@CvvKey", cvvKey);
                SqlParameter prmPinKey = new SqlParameter("@PinKey", pinKey);
                SqlParameter prmTellerId = new SqlParameter("@TellerId", creditCard.CreatedBy);
                SqlParameter prmCVV = new SqlParameter("@CVV", cvv);
                SqlParameter prmPin = new SqlParameter("@Pin", pin);
                SqlParameter prmValidFrom = new SqlParameter("@ValidFrom", creditCard.ValidFrom);
                SqlParameter prmValidThru = new SqlParameter("@ValidThru", creditCard.ValidThru);
                SqlParameter prmOutID = new SqlParameter();
                prmOutID.Direction=System.Data.ParameterDirection.Output;
                prmOutID.ParameterName="@CardId";
                prmOutID.SqlDbType=SqlDbType.BigInt;
                prmOutID.Size=20;
                int result = Context.ExecuteSqlCommandMethod("EXEC [usp_IssueCreditCard] @CustomerId,@CardTypeId,@CardNum,@CvvKey,@PinKey,@TellerId,@CVV,@Pin,@ValidFrom,@ValidThru,@CardId OUT"
                ,prmCustomerId,prmCardTypeId,prmCardNum,prmCvvKey,prmPinKey,prmTellerId,prmCVV,prmPin,prmValidFrom,prmValidThru,prmOutID);
                id=Convert.ToInt64(prmOutID.Value);
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
               id=0;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return id;
        }

        public bool UpdateRequestStatus(long requestId,string state){
            bool status=false;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                Models.CardRequest cardRequestObj = Context.CardRequest.Find(requestId);
                cardRequestObj.Status=state;
                Context.SaveChanges();
                status=true;
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        public bool GetRequestInfo(long requestId,out byte cardTypeId,out long customerId){
            bool state=false;
            cardTypeId=0;
            customerId=0;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try
            {
                Models.CardRequest cardRequest =Context.CardRequest.Find(requestId);
                cardTypeId=Convert.ToByte(cardRequest.CardTypeId);
                customerId=cardRequest.CustomerId;
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
               state=false;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return state;
        }
    }
}
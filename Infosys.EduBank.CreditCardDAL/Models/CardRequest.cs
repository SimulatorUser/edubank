﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
namespace Infosys.EduBank.CreditCardDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class CardRequest
    {
        public long RequestId { get; set; }
        public long CustomerId { get; set; }
        public byte? CardTypeId { get; set; }
        public DateTime RequestDate { get; set; }
        public string Status { get; set; }

        public CardType CardType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
namespace Infosys.EduBank.CreditCardDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class UnbilledTransaction
    {
        public UnbilledTransaction()
        {
            BilledTransaction = new HashSet<BilledTransaction>();
        }

        public long UnbilledTransactionId { get; set; }
        public long? CreditCardId { get; set; }
        public string MerchantAccountNumber { get; set; }
        public string MerchantName { get; set; }
        public decimal? Amount { get; set; }
        public byte PlanId { get; set; }
        public DateTime TimeStamp { get; set; }

        public CreditCard CreditCard { get; set; }
        public Emiplan Plan { get; set; }
        public ICollection<BilledTransaction> BilledTransaction { get; set; }
    }
}

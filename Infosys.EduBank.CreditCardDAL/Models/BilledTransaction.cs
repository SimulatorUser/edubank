﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.CreditCardDAL.Models
{
    public partial class BilledTransaction
    {
        public long BilledTransactionId { get; set; }
        public long? UnbilledTransactionId { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Interest { get; set; }
        public DateTime TimeStamp { get; set; }

        public UnbilledTransaction UnbilledTransaction { get; set; }
    }
}

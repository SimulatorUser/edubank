﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
namespace Infosys.EduBank.CreditCardDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class CardType
    {
        public CardType()
        {
            CardRequest = new HashSet<CardRequest>();
            CreditCard = new HashSet<CreditCard>();
        }

        public byte CardTypeId { get; set; }
        public string CardCategory { get; set; }
        public bool IsActive { get; set; }
        public decimal? CreditLimit { get; set; }

        public ICollection<CardRequest> CardRequest { get; set; }
        public ICollection<CreditCard> CreditCard { get; set; }
    }
}

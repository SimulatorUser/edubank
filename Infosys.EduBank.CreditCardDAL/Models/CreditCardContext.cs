﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Infosys.EduBank.CreditCardDAL.Interface;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
namespace Infosys.EduBank.CreditCardDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class CreditCardContext : DbContext,ICreditCardContext
    {
        private string Connection{set;get;}
        public CreditCardContext(string connection)
        {
            Connection=connection;
        }

        public CreditCardContext(DbContextOptions<CreditCardContext> options)
            : base(options)
        {
        }

        public virtual DbSet<BilledTransaction> BilledTransaction { get; set; }
        public virtual DbSet<CardRequest> CardRequest { get; set; }
        public virtual DbSet<CardType> CardType { get; set; }
        public virtual DbSet<CreditCard> CreditCard { get; set; }
        public virtual DbSet<CustomerBill> CustomerBill { get; set; }
        public virtual DbSet<Emiplan> Emiplan { get; set; }
        public virtual DbSet<UnbilledTransaction> UnbilledTransaction { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(Connection);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BilledTransaction>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.Interest).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.TimeStamp).HasColumnType("datetime");

                entity.HasOne(d => d.UnbilledTransaction)
                    .WithMany(p => p.BilledTransaction)
                    .HasForeignKey(d => d.UnbilledTransactionId)
                    .HasConstraintName("fk_billedtxn_unbilledtxnid");
            });

            modelBuilder.Entity<CardRequest>(entity =>
            {
                entity.HasKey(e => e.RequestId);

                entity.Property(e => e.RequestDate).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.CardType)
                    .WithMany(p => p.CardRequest)
                    .HasForeignKey(d => d.CardTypeId)
                    .HasConstraintName("fk_cardrequest_cardtypeid");
            });

            modelBuilder.Entity<CardType>(entity =>
            {
                entity.Property(e => e.CardTypeId).ValueGeneratedOnAdd();

                entity.Property(e => e.CardCategory)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreditLimit).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<CreditCard>(entity =>
            {
                entity.Property(e => e.CardNo)
                    .IsRequired()
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.Cvv)
                    .IsRequired()
                    .HasColumnName("CVV")
                    .HasMaxLength(300);

                entity.Property(e => e.IsActive).HasDefaultValueSql("((1))");

                entity.Property(e => e.IsLocked).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedTimeStamp).HasColumnType("datetime");

                entity.Property(e => e.Pin)
                    .IsRequired()
                    .HasColumnName("PIN")
                    .HasMaxLength(300);

                entity.Property(e => e.ValidFrom).HasColumnType("date");

                entity.Property(e => e.ValidThru).HasColumnType("date");

                entity.HasOne(d => d.CardType)
                    .WithMany(p => p.CreditCard)
                    .HasForeignKey(d => d.CardTypeId)
                    .HasConstraintName("fk_creditcard_cardtypeid");
            });

            modelBuilder.Entity<CustomerBill>(entity =>
            {
                entity.HasKey(e => e.BillId);

                entity.Property(e => e.AmountPaid).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BillDate).HasColumnType("date");

                entity.Property(e => e.LateFeeAmount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MinAmountDue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PaymentDate).HasColumnType("date");

                entity.Property(e => e.PaymentDueDate).HasColumnType("date");

                entity.Property(e => e.TotalAmountDue).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.CreditCard)
                    .WithMany(p => p.CustomerBill)
                    .HasForeignKey(d => d.CreditCardId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_customerbill_creditcardid");
            });

            modelBuilder.Entity<Emiplan>(entity =>
            {
                entity.HasKey(e => e.PlanId);

                entity.ToTable("EMIPlan");

                entity.Property(e => e.PlanId).ValueGeneratedOnAdd();

                entity.Property(e => e.PlanInterest).HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<UnbilledTransaction>(entity =>
            {
                entity.Property(e => e.Amount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MerchantAccountNumber)
                    .IsRequired()
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.MerchantName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeStamp).HasColumnType("datetime");

                entity.HasOne(d => d.CreditCard)
                    .WithMany(p => p.UnbilledTransaction)
                    .HasForeignKey(d => d.CreditCardId)
                    .HasConstraintName("fk_unbilledtxn_creditcardid");

                entity.HasOne(d => d.Plan)
                    .WithMany(p => p.UnbilledTransaction)
                    .HasForeignKey(d => d.PlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_unbilledtxn_planid");
            });
        }
        public virtual int ExecuteSqlCommandMethod(string query,params SqlParameter[] parameters)
        {
            return this.Database.ExecuteSqlCommand(query,parameters);
        }
    }
}

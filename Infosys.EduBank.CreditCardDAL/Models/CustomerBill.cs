﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
namespace Infosys.EduBank.CreditCardDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class CustomerBill
    {
        public long BillId { get; set; }
        public DateTime BillDate { get; set; }
        public decimal? TotalAmountDue { get; set; }
        public decimal? MinAmountDue { get; set; }
        public DateTime PaymentDueDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? AmountPaid { get; set; }
        public long CreditCardId { get; set; }
        public decimal LateFeeAmount { get; set; }

        public CreditCard CreditCard { get; set; }
    }
}

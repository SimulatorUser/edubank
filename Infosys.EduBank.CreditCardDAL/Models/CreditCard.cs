﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
namespace Infosys.EduBank.CreditCardDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class CreditCard
    {
        public CreditCard()
        {
            CustomerBill = new HashSet<CustomerBill>();
            UnbilledTransaction = new HashSet<UnbilledTransaction>();
        }

        public long CreditCardId { get; set; }
        public string CardNo { get; set; }
        public byte? CardTypeId { get; set; }
        public long? CustomerId { get; set; }
        public byte[] Cvv { get; set; }
        public byte[] Pin { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidThru { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedTimeStamp { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedTimeStamp { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsLocked { get; set; }

        public CardType CardType { get; set; }
        public ICollection<CustomerBill> CustomerBill { get; set; }
        public ICollection<UnbilledTransaction> UnbilledTransaction { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
namespace Infosys.EduBank.CreditCardDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class Emiplan
    {
        public Emiplan()
        {
            UnbilledTransaction = new HashSet<UnbilledTransaction>();
        }

        public byte PlanId { get; set; }
        public byte PlanDuration { get; set; }
        public decimal? PlanInterest { get; set; }
        public bool IsActive { get; set; }

        public ICollection<UnbilledTransaction> UnbilledTransaction { get; set; }
    }
}

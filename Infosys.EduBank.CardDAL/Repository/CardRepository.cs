using Infosys.EduBank.CardDAL.Models;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.CardDAL.Interface;
using Microsoft.EntityFrameworkCore;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.Common;

namespace Infosys.EduBank.CardDAL.Repository
{
    public class CardRepository : ICardRepository
    {
        public Common.Models.AppSettings AppSettings{get; set;}
        private ICardContext context;
            public ICardContext Context{
                get{ return context;}
                set{context=value;}
            }
        

        public CardRepository(Common.Models.AppSettings settings){
            context=new CardContext(settings.DebitCardConnection);
            AppSettings=settings;
        }

        public string insertDebitCard(int mappingId,string ifsc,ref Customer customer,string cvv,string pin,AppSettings appSettings){
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            string cardNum=string.Empty;
            DateTime validFrom= DateTime.Now;
            DateTime validThru= DateTime.Now;
            try{

                SqlParameter prmCustomerMappingId = new SqlParameter("@AccountCustomerMappingId", mappingId);
          SqlParameter prmIfsc = new SqlParameter("@ifsc",ifsc);
          SqlParameter prmTellerId = new SqlParameter("@TellerId", customer.TellerId);
          SqlParameter prmCvv = new SqlParameter("@CVV", cvv);
          SqlParameter prmPin = new SqlParameter("@Pin", pin);
          SqlParameter prmCvvKey = new SqlParameter("@CvvKey", appSettings.CVVKey);
          SqlParameter prmPinKey = new SqlParameter("@PinKey", appSettings.PinKey);
          SqlParameter prmDebitCardConstant = new SqlParameter("@debitConstant", appSettings.DebitCardConstant);
          
          SqlParameter prmOutDebitCardNumber = new SqlParameter();
          prmOutDebitCardNumber.Direction=System.Data.ParameterDirection.Output;
          prmOutDebitCardNumber.ParameterName="@DebitCardNumber";
          prmOutDebitCardNumber.Size=16;

          SqlParameter prmOutValidFrom = new SqlParameter();
          prmOutValidFrom.Direction=System.Data.ParameterDirection.Output;
          prmOutValidFrom.ParameterName="@ValidFrom";
          prmOutValidFrom.SqlDbType=SqlDbType.SmallDateTime;
          prmOutValidFrom.Size=10;

          SqlParameter prmOutValidThru = new SqlParameter();
          prmOutValidThru.Direction=System.Data.ParameterDirection.Output;
          prmOutValidThru.ParameterName="@ValidThru";
          prmOutValidThru.SqlDbType=SqlDbType.SmallDateTime;
          prmOutValidThru.Size=10;
           // SqlParameterCollection  collection= new SqlParameterCollection(prmCustomerMappingId);
            Context.ExecuteSqlCommandMethod("EXEC usp_IssueDebitCard @AccountCustomerMappingId,@ifsc,@debitConstant,@CvvKey,@PinKey,@TellerId,@CVV,@Pin,@DebitCardNumber out,@ValidFrom out,@ValidThru out",
            prmCustomerMappingId,prmIfsc,prmDebitCardConstant,prmCvvKey,prmPinKey,prmTellerId,prmCvv,prmPin,prmOutDebitCardNumber,prmOutValidFrom,prmOutValidThru);
            cardNum=prmOutDebitCardNumber.Value.ToString();
            customer.DebitCardNumber=cardNum;
            customer.ValidFrom=Convert.ToDateTime(prmOutValidFrom.Value);
            customer.ValidThru=Convert.ToDateTime(prmOutValidThru.Value);
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                cardNum="";
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return cardNum;
        }

        public bool CommitCardInsertion(string debitCardNo){
            bool status = false;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                DebitCard card= Context.DebitCard.Where(x=> x.DebitCardNumber==debitCardNo).FirstOrDefault();
                if (card != null){
                    card.Status = 'S';
                    Context.SaveChanges();
                    status=true;
                }
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        public int VerifyCardDetails(DebitCardDetails cardDetails,string pinKey,string cvvKey){
            int accountCustomerMappingId=0;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                int state = Context.ValidateDebitCardForTest(cardDetails.DebitCardNumber,cardDetails.ValidThru,cardDetails.CVV,cvvKey);
               
               if(state==1){
                 
                 accountCustomerMappingId=Context.ValidateDebitCardPinForTest(cardDetails.DebitCardNumber,cardDetails.Pin,pinKey);
               }
               
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                accountCustomerMappingId = 0;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return accountCustomerMappingId;
        }
        public int ChangeDebitCardPin(string debitCardNumber, string hashOldPin, string encryptNewPin,string decryptKey){
            int state=0;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                SqlParameter prmDebitCardNum = new SqlParameter("@DebitCardNumber",debitCardNumber);
                SqlParameter prmOldPin = new SqlParameter("@OldPin", hashOldPin);
                SqlParameter prmNewPin = new SqlParameter("@NewPin", encryptNewPin);
                SqlParameter prmDecryptKey = new SqlParameter("@DecryptKey", decryptKey);
                state=Context.ExecuteSqlCommandMethod("EXEC usp_ChangeDebitCardPin @DebitCardNumber,@OldPin,@NewPin,@DecryptKey",
                prmDebitCardNum,prmOldPin,prmNewPin,prmDecryptKey);
                
            }
            catch (System.Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                state=0;
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return state;
        }

        public List<string> GetDebitCardNumbersByMappingId(int accountCustomerMappingId){
            List<string> debitCardNumbers = new List<string>();
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
                debitCardNumbers = Context.DebitCard.Where(x=> x.AccountCustomerMappingId==accountCustomerMappingId).Select(x=> x.DebitCardNumber).ToList();
            }
            catch(Exception ex){
                debitCardNumbers = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return debitCardNumbers;
        }
        //Minor Enhancement
        //Gets the debit card details associated with a given account customer mapping ID
        public Infosys.EduBank.Common.Models.DebitCardDetails GetDebitCardForAccountNumber(int accountCustomerMappingId)
        {
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            DebitCard debitCard = new DebitCard();
            Infosys.EduBank.Common.Models.DebitCardDetails card = new Infosys.EduBank.Common.Models.DebitCardDetails();
            try
            {
                debitCard = (from d in context.DebitCard
                              where d.AccountCustomerMappingId == accountCustomerMappingId
                              select d).LastOrDefault();
                if(debitCard!=null)
                {
                    card.DebitCardId = debitCard.DebitCardId;
                    card.DebitCardNumber = debitCard.DebitCardNumber;
                    card.ValidThru = debitCard.ValidThru.ToString();
                }
            }
            catch (Exception ex)
            {
                card = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return card;
        }
        //Minor Enhancement
        //Inserts a debit card replacement request with values sent by user
        public bool RequestExtendDebitCard(Common.Models.DebitCardRequest reqObj)
        {
            bool result = false;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            Models.DebitCardRequest cardRequest = new Models.DebitCardRequest();
            try
            {
                var accountCustomerMappingId = (from d in context.DebitCard
                              where d.DebitCardId == reqObj.DebitCardId 
                              select d.AccountCustomerMappingId).LastOrDefault();
                var replacementRequest = (from d in Context.DebitCardRequest
                                            where (d.DebitCardId==reqObj.DebitCardId && (d.Status=="P" ||
                                            d.Status=="A")) select d);
                if(!replacementRequest.Any())
                {                    
                    cardRequest.AccountCustomerMappingId = accountCustomerMappingId;
                    cardRequest.DebitCardId = reqObj.DebitCardId;
                    cardRequest.Reason = reqObj.Reason;
                    cardRequest.Status = "P";
                    context.DebitCardRequest.Add(cardRequest);
                    context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return result;
        }
        //Minor Enhancement
        //Inserts a debit card block request with debit card ID
        public bool BlockDebitCard(int debitCardId)
        {
            bool result = false;
            DebitCard debitCard = new DebitCard();
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                debitCard = (from d in context.DebitCard
                              where d.DebitCardId == debitCardId
                              select d).FirstOrDefault();
                debitCard.IsActive = false;
                context.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return result;
        }
        //Minor Enhancement
        //Fetches the list of all debit card requests with status as pending for teller
        public List<Infosys.EduBank.Common.Models.DebitCardRequest> GetPendingDebitCardRequests()
        {
            List<Infosys.EduBank.Common.Models.DebitCardRequest> debitCardRequests = new List<Infosys.EduBank.Common.Models.DebitCardRequest>();
            Common.Models.DebitCardRequest debitCardRequest;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                List<Models.DebitCardRequest> cardRequests = (from dcr in context.DebitCardRequest
                                                       where dcr.Status == "P"
                                                       select dcr).ToList();
                if(cardRequests.Any())
                {
                    foreach (var cr in cardRequests)
                    {
                        var debitCard = (from dc in context.DebitCard where dc.DebitCardId == cr.DebitCardId
                                            select dc).FirstOrDefault();
                        debitCardRequest = new Common.Models.DebitCardRequest();
                        debitCardRequest.AccountCustomerMappingId = cr.AccountCustomerMappingId;
                        debitCardRequest.DebitCardId = cr.DebitCardId;
                        debitCardRequest.DebitCardRequestId = cr.DebitCardRequestId;
                        debitCardRequest.Reason = cr.Reason;
                        debitCardRequest.Status = cr.Status;
                        debitCardRequest.ValidThru = debitCard.ValidThru.ToString();;
                        debitCardRequest.DebitCardNumber= debitCard.DebitCardNumber;
                        debitCardRequests.Add(debitCardRequest);
                    }
                }
            }
            catch (Exception ex)
            {
                debitCardRequests = null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return debitCardRequests;
        }
        //Minor Enhancement
        //Updates the status of debit card request as either approved or rejected based on teller action
        public bool UpdateDebitCardRequest(int debitCardRequestId, string newStatus)
        {
            bool status = false;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                Models.DebitCardRequest debitCardRequest = (from dcr in context.DebitCardRequest
                                                     where dcr.DebitCardRequestId == debitCardRequestId
                                                     select dcr).FirstOrDefault();
                debitCardRequest.Status = newStatus;
                context.SaveChanges();
                status = true;
            }
            catch (Exception ex)
            {
                status = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
        //Minor Enhancement
        //Gets the valid through date for a given debit card
        public DateTime GetValidThruForDebitCard(int debitCardId)
        {
            DateTime date = new DateTime();
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                date = (from dc in context.DebitCard
                        where dc.DebitCardId == debitCardId
                        select dc.ValidThru).FirstOrDefault();
            }
            catch (Exception ex)
            {
                date = new DateTime(0001,01,01);
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return date;
        }
    }
}
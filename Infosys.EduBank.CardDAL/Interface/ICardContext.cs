﻿using Microsoft.EntityFrameworkCore;
using Infosys.EduBank.CardDAL.Models;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Data.SqlClient;

namespace Infosys.EduBank.CardDAL.Interface
{
    public interface ICardContext
    {
        DbSet<DebitCard> DebitCard { get; set; }
        DbSet<DebitCardRequest> DebitCardRequest { get; set; }
        int SaveChanges();
        int ExecuteSqlCommandMethod(string query,params SqlParameter[] parameters);
        int ValidateDebitCardPinForTest(string debitCardNumber, string pin,string decryptKey);
         int ValidateDebitCardForTest(string debitCardNumber,string validThru, string cvv,string decryptKey);
    }
}
﻿using System.Collections.Generic;
using Infosys.EduBank.CardDAL.Models;
using Infosys.EduBank.Common.Models;

namespace Infosys.EduBank.CardDAL.Interface
{
    public interface ICardRepository
    {
        AppSettings AppSettings{set;get;}
        ICardContext Context { get; set;}

        int ChangeDebitCardPin(string debitCardNumber, string hashOldPin, string encryptNewPin, string decryptKey);
        bool CommitCardInsertion(string debitCardNo);
        List<string> GetDebitCardNumbersByMappingId(int accountCustomerMappingId);
        string insertDebitCard(int mappingId, string ifsc, ref Customer customer, string cvv, string pin, AppSettings appSettings);
        int VerifyCardDetails(DebitCardDetails cardDetails, string pinKey, string cvvKey);
        Infosys.EduBank.Common.Models.DebitCardDetails GetDebitCardForAccountNumber(int accountCustomerMappingId);
        bool BlockDebitCard(int debitCardId);
        bool RequestExtendDebitCard(Common.Models.DebitCardRequest reqObj);
        bool UpdateDebitCardRequest(int debitCardRequestId, string newStatus);
        System.DateTime GetValidThruForDebitCard(int debitCardId);
        List<Infosys.EduBank.Common.Models.DebitCardRequest> GetPendingDebitCardRequests();
    }
}
﻿using System;
using System.Collections.Generic;

namespace Infosys.EduBank.CardDAL.Models
{
    public partial class DebitCard
    {
        public int DebitCardId { get; set; }
        public string DebitCardNumber { get; set; }
        public int AccountCustomerMappingId { get; set; }
        public byte[] Cvv { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidThru { get; set; }
        public byte[] Pin { get; set; }
        public bool IsLocked { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedTimestamp { get; set; }
        public byte CreatedBy { get; set; }
        public DateTime? ModifiedTimestamp { get; set; }
        public char Status { get; set; }
    }
}

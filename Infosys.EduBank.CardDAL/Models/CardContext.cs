﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Infosys.EduBank.CardDAL.Interface;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;

namespace Infosys.EduBank.CardDAL.Models
{
    [ExcludeFromCodeCoverage]
    public partial class CardContext : DbContext, ICardContext
    {
        private string ConnectionString{set;get;}
     
        
        public CardContext()
        {
        }
        public CardContext(string connectionString)
        {
            ConnectionString=connectionString;
        }

        public CardContext(DbContextOptions<CardContext> options)
            : base(options)
        {
            
        }
        public int ValidateDebitCardForTest(string debitCardNumber,string validThru, string cvv,string decryptKey){
            return (from x in this.DebitCard select CardContext.ValidateDebitCard(debitCardNumber,validThru,cvv,decryptKey)).FirstOrDefault();
        }
        public int ValidateDebitCardPinForTest(string debitCardNumber, string pin,string decryptKey){
            return (from x in this.DebitCard select CardContext.ValidateDebitCardPIN(debitCardNumber,pin,decryptKey)).FirstOrDefault();
        }
        [DbFunction("ufn_ValidateDebitCard","dbo")]
        public static int ValidateDebitCard(string debitCardNumber,string validThru, string cvv,string decryptKey){
            return 0;
        }
        [DbFunction("ufn_ValidateDebitCardPIN","dbo")]
        public static int ValidateDebitCardPIN(string debitCardNumber,string pin, string decryptedKey){
            return 0;
        }

        public virtual DbSet<DebitCard> DebitCard { get; set; }

        public virtual DbSet<DebitCardRequest> DebitCardRequest { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DebitCard>(entity =>
            {
                entity.Property(e => e.Cvv)
                    .IsRequired()
                    .HasColumnName("CVV")
                    .HasMaxLength(300);

                entity.Property(e => e.DebitCardNumber)
                    .IsRequired()
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Pin)
                    .IsRequired()
                    .HasColumnName("PIN")
                    .HasMaxLength(300);

                entity.Property(e => e.ValidFrom).HasColumnType("smalldatetime");

                entity.Property(e => e.ValidThru).HasColumnType("smalldatetime");
            });
            modelBuilder.Entity<DebitCardRequest>(entity =>
            {
                entity.Property(e => e.DebitCardRequestId).ValueGeneratedOnAdd();

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });
        }
         public virtual int ExecuteSqlCommandMethod(string query,params SqlParameter[] parameters)
        {
            return this.Database.ExecuteSqlCommand(query,parameters);
        }
    }
}

using System;
using System.Collections.Generic;
using Infosys.EduBank.Common;
using Infosys.EduBank.CreditCardDAL.Interface;
using Infosys.EduBank.CustomerDAL.Interface;

namespace Infosys.EduBank.CreditCardBL.Interface
{
    public interface ICreditCardBLRepository
    {
         ICreditCardRepository CreditCardRepositoryObj{set;get;}
         Common.Models.AppSettings AppSettings { get; set; }
         IUtilities Util{set;get;}
         ICustomerRepository CustomerRepositoryObj{set;get;}
         bool ApplyForCard(string name,byte categoryId);
         List<Common.Models.CardType> GetAllCardType();
         decimal GetCardLimit(byte id);

         List<Common.Models.CardRequest> GetRequests();
         string GenerateCardTokenOptimised();
          bool GenerateCreditCard(long requestId,ref Common.Models.CreditCard creditCard);

         bool UpdateRequestStatus(long requestId,string state);
    }
}
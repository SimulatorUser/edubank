using System;
using Infosys.EduBank.CreditCardDAL.Repository;
using Infosys.EduBank.CreditCardDAL.Interface;
using System.Diagnostics.CodeAnalysis;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.Common;
using Infosys.EduBank.CreditCardBL.Interface;
using Infosys.EduBank.CustomerDAL.Interface;
using Infosys.EduBank.CustomerDAL.Repository;
using System.Collections.Generic;
using Infosys.EduBank.CreditCardDAL.Models;
using System.Linq;
using System.Text;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.CreditCardBL
{
    public class CreditCardBLRepository:ICreditCardBLRepository
    {
    public ICreditCardRepository CreditCardRepositoryObj{set;get;}
    public ICustomerRepository CustomerRepositoryObj{set;get;}
    public Common.Models.AppSettings AppSettings { get; set; }
    public IUtilities Util{set;get;}
    public CreditCardBLRepository(Common.Models.AppSettings appsetting){
        AppSettings=appsetting;

        CreditCardRepositoryObj=new CreditCardRepository(AppSettings);
        CustomerRepositoryObj=new CustomerRepository(AppSettings);
        Util= new Utilities();
    }
    public bool ApplyForCard(string name,byte categoryId){
        bool status=false;
            CreditCardDAL.Models.CardRequest cardRequest =new CreditCardDAL.Models.CardRequest();
        var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try{
        cardRequest.CustomerId=CustomerRepositoryObj.GetCustomerIdByUsername(name);
        cardRequest.CardTypeId=categoryId;
        cardRequest.RequestDate=DateTime.Now;
       // cardRequest.Status="Pending";
       status=CreditCardRepositoryObj.ApplyForCard(cardRequest);
        }
        catch(Exception ex){
            status=false;
        }
        return status;
    }

    public List<Common.Models.CardType> GetAllCardType(){
        List<Common.Models.CardType> cardTypeList=new List<Common.Models.CardType>();
        List<CreditCardDAL.Models.CardType> cardList;
        var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
        try
        {
            cardList=CreditCardRepositoryObj.GetAllCardType();
            foreach(var item in cardList){
                cardTypeList.Add(new Common.Models.CardType(){
                    CardCategory=item.CardCategory,
                    CardTypeId=item.CardTypeId,
                    CreditLimit=item.CreditLimit,
                    IsActive=item.IsActive
                });
            }
        }
        catch (Exception ex)
        {
            cardTypeList=null;
        }
        return cardTypeList;
    }
    public decimal GetCardLimit(byte id){
        decimal limit;
        var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
        try{
            limit = CreditCardRepositoryObj.GetCreditLimitById(id);
        }
        catch(Exception ex){
            limit=0;
        }
        return limit;
    }
        public List<Common.Models.CardRequest> GetRequests(){
            List<Common.Models.CardRequest> cardRequestList=new List<Common.Models.CardRequest>();
            List< CreditCardDAL.Models.CardRequest > requestsList = new List<CreditCardDAL.Models.CardRequest>();
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            try
            {
                requestsList=CreditCardRepositoryObj.GetCardRequest();
                foreach(var request in requestsList){
                    cardRequestList.Add(new Common.Models.CardRequest(){
                        CardTypeId=request.CardTypeId,
                        CustomerId=request.CustomerId,
                        RequestDate=request.RequestDate,
                        RequestId=request.RequestId,
                        Status=request.Status
                    });
                }
                
            }
            catch (Exception ex)
            {
                cardRequestList=null;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return cardRequestList;
        }
        public bool GenerateCreditCard(long requestId,ref Common.Models.CreditCard creditCard){
            bool status=false;
            string cvv,pin;
           // CreditCardBLRepository repositoryObj=new CreditCardBLRepository();
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            try
            {
                byte cardTypeId;
                long customerId;
             CreditCardRepositoryObj.GetRequestInfo(requestId,out cardTypeId,out customerId );   
             creditCard.CardTypeId=cardTypeId;
             creditCard.CustomerId=customerId;
                creditCard.CardNo=GenerateCardTokenOptimised();
                GenerateCreditCardDetails(out cvv,out pin);
                creditCard.Cvv=cvv;
                creditCard.Pin=pin;
                cvv=Util.Encrypt(cvv,AppSettings.SecurityKey10);
                pin=Util.Encrypt(pin,AppSettings.SecurityKey10);
                creditCard.CreatedTimeStamp=DateTime.Now;
                creditCard.ValidFrom=DateTime.Today;
                int year=creditCard.ValidFrom.Year+20;
                int month=creditCard.ValidFrom.Month;
                int day=creditCard.ValidFrom.Day;
                creditCard.ValidThru=new DateTime(year,month,day);
                creditCard.CreditCardId=CreditCardRepositoryObj.AddCreditCard(creditCard,cvv,pin,AppSettings.CVVKey,AppSettings.PinKey);
               if(creditCard.CreditCardId!=0)
               {
                   status=true;
               }

            }
            catch (System.Exception ex)
            {
                status=false;
                creditCard=null;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
         
         
         [ExcludeFromCodeCoverage]
         public void GenerateCreditCardDetails(out string cvv, out string pin)
        {
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            
            try
            {
                //Object of Random Class is instantiated 
                Random rnd = new Random();

                //CVV of 3 random digits is generated
                cvv = Convert.ToString(rnd.Next(0, 9)) + Convert.ToString(rnd.Next(0, 9)) + Convert.ToString(rnd.Next(0, 9));

                //Pin of 4 random digits is generated
                pin = Convert.ToString(rnd.Next(0, 9)) + Convert.ToString(rnd.Next(0, 9)) + Convert.ToString(rnd.Next(0, 9)) + Convert.ToString(rnd.Next(0, 9));

                // Info logging with the Type of TellerBL.
               // EduBankLogger.Log(typeof(TellerBL), "TellerBL", "GenerateDebitCardDetails executed successfully in TellerBL", 1);
            }
            catch (Exception ex)
            {
                // Logging in case of any errors with the Type of TellerBL.
                // Specifying the error message with the error message "Error occurred while generating PIN and CVV of debit card".
               // EduBankLogger.Log(typeof(TellerBL), "TellerBL", "Error occurred while generating PIN and CVV of debit card", 2, ex);

                // Setting CVV and Pin to empty string in case of exception
                cvv = string.Empty;
                pin = string.Empty;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
        }
        [ExcludeFromCodeCoverage]
        public string GenerateCardTokenOptimised()
        {
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            var sb = new StringBuilder(); 
            try
            {
                int[] checkArray = new int[15];
            
            var cardNum = new int[16];
            Random rnd=new Random();
            for (int d = 14; d >= 0; d--)
            {
                cardNum[d] = rnd.Next(0,9);
                checkArray[d] = ( cardNum[d] * (((d+1)%2)+1)) % 9;
            }
 
            cardNum[15] = ( checkArray.Sum() * 9 ) % 10;
 
            
 
            for (int d = 0; d < 16; d++)
            {
                sb.Append(cardNum[d].ToString());
            } 
            }
            catch(Exception ex){
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return sb.ToString();
        }

         public bool UpdateRequestStatus(long requestId,string state){
            bool status=false;
            var methodName=Common.Utilities.GetMethod();
            EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.EntryLogMessage,1);
            try
            {
                status =CreditCardRepositoryObj.UpdateRequestStatus(requestId,state);
            }
            catch (Exception ex)
            {
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status=false;;
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,methodName+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }
    }
}
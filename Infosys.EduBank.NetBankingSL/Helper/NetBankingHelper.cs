using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System;
using System.Net;
using System.IO;
using Infosys.EduBank.NetBankingBL.Interface;
using Infosys.EduBank.Common.Models;
using Infosys.EduBank.NetBankingBL;
using Infosys.EduBank.NetBankingSL.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Infosys.EduBank.NetBankingSL.Models;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;

namespace Infosys.EduBank.NetBankingSL.Helper {
    public class NetBankingHelper : INetBankingHelper
    {

        public INetBankingBLRepository NetBankingBLRepositoryObj { get; set; }
        public AppSettings AppSettings { get; set; }
        public IHttpServiceHelper HttpServiceHelperObj { get;set; }

        public NetBankingHelper(AppSettings appSettings)
        {
            AppSettings=appSettings;
            NetBankingBLRepositoryObj = new NetBankingBLRepository(appSettings);
            HttpServiceHelperObj=new HttpServiceHelper(appSettings);
        }

        public bool VerifyAccount(string accountNumber,string ifscCode){
            bool status=false;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                if(ifscCode==null || ifscCode.Equals(string.Empty))
                {
                    status=NetBankingBLRepositoryObj.AccountExist(accountNumber);
                }
                else
                {
                    status = NetBankingBLRepositoryObj.VerifyAccount(accountNumber,ifscCode);
                }
                
            }
            catch(Exception ex){
                status = false;
                EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            }
            return status;
        }
        public string GetToken(HttpContext context)
        {
            return context.GetTokenAsync("access_token").Result;
        }
        public string DebitUsingNetBanking(Common.Models.NetBankingDetails netBankingDetails,string token){
            string responseCode = string.Empty;
             EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                if (VerifyAccount(netBankingDetails.AccountNumber,netBankingDetails.IFSC)){
                    string URL = "http://localhost:5171/api/ViewBalance?accountNumber="+netBankingDetails.AccountNumber;
                    var result=HttpServiceHelperObj.GetResponse(URL,token);
                    decimal balance=Convert.ToDecimal(result);
                    if(balance>=netBankingDetails.Amount){
                       if(NetBankingBLRepositoryObj.DebitMoneyUsingNetBanking(netBankingDetails,"NetBanking")){
                           responseCode="SUCCESS";
                       }}
                       else{
                           responseCode="INSUFFICIENT BALANCE";
                       }
                }
            }
            catch(Exception ex){
                responseCode = "FAILURE";
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return responseCode;
        }
    }
}
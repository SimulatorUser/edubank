namespace Infosys.EduBank.NetBankingSL.Interface
{
    public interface IHttpServiceHelper
    {
        Common.Models.AppSettings AppSettings{set;get;}
        string GetResponse(string url,string token);
    }
}
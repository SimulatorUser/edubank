﻿using Infosys.EduBank.Common.Models;
using Infosys.EduBank.NetBankingBL.Interface;
using Microsoft.AspNetCore.Http;

namespace Infosys.EduBank.NetBankingSL.Interface
{
    public interface INetBankingHelper
    {
        AppSettings AppSettings { get; set; }
        INetBankingBLRepository NetBankingBLRepositoryObj { get; set; }
        IHttpServiceHelper HttpServiceHelperObj{set;get;}

        string DebitUsingNetBanking(NetBankingDetails netBankingDetails, string token);
        bool VerifyAccount(string accountNumber, string ifscCode);
        string GetToken(HttpContext context);
    }
}
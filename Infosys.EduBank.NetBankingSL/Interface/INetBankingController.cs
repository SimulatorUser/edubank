﻿using Infosys.EduBank.Common.Models;
using Infosys.EduBank.NetBankingSL.Helper;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.NetBankingSL.Interface
{
    public interface INetBankingController
    {
        AppSettings AppSettings{set;get;}
        INetBankingHelper NetBankingHelper { get; set; }

        string Post([FromBody] NetBankingSL.Models.NetBankingDetails netBankingDetails);
        ActionResult<string> Get(int id);
    }
}
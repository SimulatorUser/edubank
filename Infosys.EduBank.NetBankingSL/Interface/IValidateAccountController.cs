﻿using Infosys.EduBank.NetBankingSL.Helper;
using Microsoft.AspNetCore.Mvc;

namespace Infosys.EduBank.NetBankingSL.Interface
{
    public interface IValidateAccountController
    {
        Common.Models.AppSettings AppSettings{set;get;}
        INetBankingHelper NetBankingHelper { get; set; }
        bool Get(string accountNumber);
        bool Get(string accountNumber, string ifsc);
        ActionResult<string> Get();

    }
}
namespace Infosys.EduBank.NetBankingSL.Models
{
    public class NetBankingDetails
    {
        public string AccountNumber { get; set; }

        public string IFSC { get; set; }

        public string Remarks { get; set; }

        public string AccountHolderName { get; set; }

        public decimal Amount { get; set; }

    }
}
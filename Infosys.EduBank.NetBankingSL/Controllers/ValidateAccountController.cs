﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.NetBankingSL.Helper;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Infosys.EduBank.NetBankingSL.Interface;
using Infosys.EduBank.Common.Logger;
using Infosys.EduBank.Common;
using System.Security.Claims;

namespace Infosys.EduBank.NetBankingSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValidateAccountController : ControllerBase, IValidateAccountController
    {
        public Common.Models.AppSettings AppSettings{get; set;}
        public INetBankingHelper NetBankingHelper { get; set; }
        //public AppSettings AppSettings { get; set; }
        private Common.IUtilities Utilities{get;set;}

        public ValidateAccountController(Common.IUtilities util)
        {
            Utilities=util;
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(Utilities.Configuration);
            AppSettings=appSettings;
            NetBankingHelper = new NetBankingHelper(appSettings);
        }


        // GET api/values
        [Authorize(Roles = "User")]
        [HttpGet]
        public bool Get(string accountNumber, string ifsc)
        {
            ConfigureLog();
            bool status = false;
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            try{
                status = NetBankingHelper.VerifyAccount(accountNumber,ifsc);
            }
            catch(Exception ex){
                status = false;
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            }
            return status;
        }

        [Authorize(Roles = "User,Teller")]
        [HttpGet("{accountNumber}")]
        public bool Get(string accountNumber)
        {
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            bool status = false;
            try{
                status = NetBankingHelper.VerifyAccount(accountNumber,string.Empty);
            }
            catch(Exception ex){
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                status = false;
            }
            finally
            {
                 EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return status;
        }

        // GET api/values/5
        [HttpGet("Health")]
        public ActionResult<string> Get()
        {
            return "Validate Account Service is Up And Running";
        }

        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            NetBankingHelper.HttpServiceHelperObj.AppSettings.UserName=AppSettings.UserName;
            NetBankingHelper.AppSettings.UserName=AppSettings.UserName;
            NetBankingHelper.NetBankingBLRepositoryObj.AppSettings.UserName= AppSettings.UserName;
            NetBankingHelper.NetBankingBLRepositoryObj.AccountRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            //NetBankingHelper.NetBankingBLRepositoryObj.TransactionRepositoryObj.AppSettings.UserName=AppSettings.UserName;
        }
    }
}

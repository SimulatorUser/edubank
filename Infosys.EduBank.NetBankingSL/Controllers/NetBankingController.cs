using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Infosys.EduBank.NetBankingSL.Helper;
using Infosys.EduBank.Common.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Infosys.EduBank.NetBankingSL.Interface;
using Infosys.EduBank.Common;
using Infosys.EduBank.Common.Logger;
using System.Security.Claims;

namespace Infosys.EduBank.NetBankingSL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NetBankingController : ControllerBase, INetBankingController
    {
        public Common.Models.AppSettings AppSettings{get; set;}
        public INetBankingHelper NetBankingHelper { get; set; }
        //public AppSettings AppSettings { get; set; }
        private Common.IUtilities Utilities{get;set;}

        public NetBankingController(Common.IUtilities util)
        {
            Utilities=util;
            var appSettings=Newtonsoft.Json.JsonConvert.DeserializeObject<Common.Models.AppSettings>(Utilities.Configuration);
            AppSettings=appSettings;
            NetBankingHelper = new NetBankingHelper(AppSettings);
        }

        // GET api/values/5
        [HttpGet("Health")]
        public ActionResult<string> Get(int id)
        {
            return "Net Banking Service is Up And Running";
        }

        // POST api/values
        [Authorize(Roles = "User")]
        [HttpPost]
        public string Post([FromBody]Models.NetBankingDetails netBankingDetails)
        {
            ConfigureLog();
            EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.EntryLogMessage,1);
            Common.Models.NetBankingDetails commonNetBankingDetails = new Common.Models.NetBankingDetails(){
                    AccountNumber = netBankingDetails.AccountNumber,
                    IFSC = netBankingDetails.IFSC,
                    Remarks = netBankingDetails.Remarks,
                    AccountHolderName = netBankingDetails.AccountHolderName,
                    Amount = netBankingDetails.Amount
            };

            string responseCode = string.Empty;
            try{
                string token= NetBankingHelper.GetToken(HttpContext);
                responseCode= NetBankingHelper.DebitUsingNetBanking(commonNetBankingDetails,token);
            }
            catch(Exception ex){
                 EduBankLogger.Log(this.GetType(),AppSettings,ex.Message,2,ex);
                responseCode = "FAILURE";
            }
            finally
            {
                EduBankLogger.Log(this.GetType(),AppSettings,Common.Utilities.GetMethod()+" "+Constants.ExitLogMessage,1);
            }
            return responseCode;
        }
        private void ConfigureLog()
        {
            AppSettings.UserName="Anonymous User";
            try
            {
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    IEnumerable<Claim> claims = identity.Claims; 
                    AppSettings.UserName=identity.FindFirst(ClaimTypes.Name).Value;
                }
            }catch(Exception ex)
            {
                AppSettings.UserName="Anonymous User";
            }
            NetBankingHelper.HttpServiceHelperObj.AppSettings.UserName=AppSettings.UserName;
            NetBankingHelper.AppSettings.UserName=AppSettings.UserName;
            NetBankingHelper.NetBankingBLRepositoryObj.AppSettings.UserName= AppSettings.UserName;
            NetBankingHelper.NetBankingBLRepositoryObj.AccountRepositoryObj.AppSettings.UserName=AppSettings.UserName;
            //NetBankingHelper.NetBankingBLRepositoryObj.TransactionRepositoryObj.AppSettings.UserName=AppSettings.UserName;
        }
    }
}

using System.ComponentModel.DataAnnotations;

namespace Infosys.EduBank.PaymentApp.Models
{
    public class InternetBankingTransaction
    {       
        [Display(Name="Username")]
        [Required]
         public string LoginName { get; set; }

        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        [Required]
        public decimal? Amount { get; set; }

        public string TransactionInfo { get; set; }

        public string Remarks { get; set; }
    }
}

using System.Data;
using System;
namespace Infosys.EduBank.PaymentApp.Models
{
    public class DebitCardDetails{
        public string DebitCardNumber { get; set; }

        public string CVV { get; set; }


        public string ValidThru { get; set; }

        public string Pin { get; set; }

        public string CardHolderName { get; set; }
    }
}
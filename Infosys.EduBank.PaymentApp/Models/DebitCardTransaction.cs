namespace Infosys.EduBank.PaymentApp.Models
{
    public class DebitCardTransaction
    {
        public DebitCardDetails DebitCard { get; set; }

        public decimal Amount { get; set; }

        public string Remarks { get; set; }

        public string MerchantName { get; set; }

        public string MerchantUserId { get; set; }

        public string MerchantReturnURL { get; set; }
    }
}